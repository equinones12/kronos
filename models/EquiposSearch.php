<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Equipos;

/**
 * EquiposSearch represents the model behind the search form about `app\models\Equipos`.
 */
class EquiposSearch extends Equipos
{   


    public $tipoEquipo;
    public $sucursal;
    public $funcionario;
    public $estado;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipo_id', 'equipo_tipo_equipo_id', 'equipo_systema_operativo_id', 'equipo_office_id', 'equipo_sucursal_id', 'equipo_usuario_id', 'equipo_estado_id'], 'integer'],
            [['equipo_identificacion_tecnologia', 'equipo_nombre_actual', 'equipo_marca', 'equipo_ram', 'equipo_tipo_memoria_ram', 'equipo_marca_disco_duro', 'equipo_capacidad_disco_duro', 'equipo_procesador', 'equipo_velocidad_procesador', 'equipo_cantidad_nucleos_procesador', 'equipo_pantalla', 'equipo_numero_activo_contabilidad', 'equipo_fecha_ingreso', 'equipo_valor', 'tipoEquipo','sucursal','funcionario','estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Equipos::find();

        // add conditions that should always apply here

        $query->joinWith(['funcionarios', 'tipoMaquina','sucursal', 'estado']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['tipoEquipo'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sucursal'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['sucursales.sucursal_nombre' => SORT_ASC],
            'desc' => ['sucursales.sucursal_nombre' => SORT_DESC],
        ];


        $dataProvider->sort->attributes['funcionario'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['funcionarios.funcionario_nombres' => SORT_ASC],
            'desc' => ['funcionarios.funcionario_nombres' => SORT_DESC],
        ];


        $dataProvider->sort->attributes['estado'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'equipo_id' => $this->equipo_id,
            'equipo_tipo_equipo_id' => $this->equipo_tipo_equipo_id,
            'equipo_systema_operativo_id' => $this->equipo_systema_operativo_id,
            'equipo_office_id' => $this->equipo_office_id,
            'equipo_sucursal_id' => $this->equipo_sucursal_id,
            'equipo_usuario_id' => $this->equipo_usuario_id,
            'equipo_estado_id' => $this->equipo_estado_id,
            'equipo_fecha_ingreso' => $this->equipo_fecha_ingreso,
        ]);

        $query->andFilterWhere(['like', 'equipo_identificacion_tecnologia', $this->equipo_identificacion_tecnologia])
            ->andFilterWhere(['like', 'equipo_nombre_actual', $this->equipo_nombre_actual])
            ->andFilterWhere(['like', 'equipo_marca', $this->equipo_marca])
            ->andFilterWhere(['like', 'equipo_ram', $this->equipo_ram])
            ->andFilterWhere(['like', 'equipo_tipo_memoria_ram', $this->equipo_tipo_memoria_ram])
            ->andFilterWhere(['like', 'equipo_marca_disco_duro', $this->equipo_marca_disco_duro])
            ->andFilterWhere(['like', 'equipo_capacidad_disco_duro', $this->equipo_capacidad_disco_duro])
            ->andFilterWhere(['like', 'equipo_procesador', $this->equipo_procesador])
            ->andFilterWhere(['like', 'equipo_velocidad_procesador', $this->equipo_velocidad_procesador])
            ->andFilterWhere(['like', 'equipo_cantidad_nucleos_procesador', $this->equipo_cantidad_nucleos_procesador])
            ->andFilterWhere(['like', 'equipo_pantalla', $this->equipo_pantalla])
            ->andFilterWhere(['like', 'equipo_numero_activo_contabilidad', $this->equipo_numero_activo_contabilidad])
            ->andFilterWhere(['like', 'tipos_detalles.tipo_detalle_nombre', $this->tipoEquipo])
            ->andFilterWhere(['like', 'sucursales.sucursal_nombre', $this->sucursal])
            ->andFilterWhere(['like', 'funcionarios.funcionario_nombres', $this->funcionario])
            ->andFilterWhere(['like', 't2.tipo_detalle_nombre', $this->estado])
            ->andFilterWhere(['like', 'equipo_valor', $this->equipo_valor]);

        return $dataProvider;
    }
}
