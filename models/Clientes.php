<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $clientes_id
 * @property string|null $clientes_nombre
 * @property string|null $clientes_nit
 * @property string|null $clientes_direccion
 * @property string|null $clientes_fechacreacion
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clientes_nombre'], 'required'],
            [['clientes_fechacreacion', 'clientes_licencia_fecha_desde', 'clientes_tipolicencia'], 'safe'],
            [['clientes_nombre', 'clientes_direccion'], 'string', 'max' => 150],
            [['clientes_nit', 'clientes_licencia'], 'string', 'max' => 50],
            [['clientes_tipolicencia'], 'string'],
            [['clientes_cantidadusuarios'], 'integer'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'clientes_id' => 'Cliente ID',
            'clientes_nombre' => 'Nombre o Razon social',
            'clientes_nit' => 'Nit',
            'clientes_direccion' => 'Direccion',
            'clientes_fechacreacion' => 'Fecha creacion',
            'clientes_licencia' => 'Licencia',
            'clientes_licencia_fecha_desde' => 'Fecha desde',
            'clientes_licencia_fecha_hasta' => 'Fecha hasta',
            'clientes_tipolicencia' => 'Tipo de licencia',
            'clientes_cantidadusuarios' => 'Cantidad usuarios',
            
        ];
    }
}
