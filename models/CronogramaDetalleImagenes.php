<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cronograma_detalle_imagenes".
 *
 * @property integer $cronograma_detalle_imagen_id
 * @property integer $cronograma_detalle_imagen_detalle_id
 * @property integer $cronograma_detalle_imagen_equipo_id
 * @property string $cronograma_detalle_imagen_file
 */
class CronogramaDetalleImagenes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cronograma_detalle_imagenes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cronograma_detalle_imagen_detalle_id', 'cronograma_detalle_imagen_equipo_id'], 'integer'],
            [['cronograma_detalle_imagen_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cronograma_detalle_imagen_id' => 'Cronograma Detalle Imagen ID',
            'cronograma_detalle_imagen_detalle_id' => 'Cronograma Detalle Imagen Detalle ID',
            'cronograma_detalle_imagen_equipo_id' => 'Cronograma Detalle Imagen Equipo ID',
            'cronograma_detalle_imagen_file' => 'Cronograma Detalle Imagen File',
        ];
    }
}
