<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_detalles".
 *
 * @property integer $tipo_detalle_id
 * @property integer $tipo_detalle_tipo_id
 * @property string $tipo_detalle_nombre
 * @property integer $tipo_detalle_activo
 */
class TiposDetalles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipos_detalles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_detalle_tipo_id'], 'required'],
            [['tipo_detalle_tipo_id', 'tipo_detalle_activo'], 'integer'],
            [['tipo_detalle_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tipo_detalle_id' => 'Tipo Detalle ID',
            'tipo_detalle_tipo_id' => 'Tipo Detalle Tipo ID',
            'tipo_detalle_nombre' => 'Tipo Detalle Nombre',
            'tipo_detalle_activo' => 'Tipo Detalle Activo',
        ];
    }

    public function getTiposd(){

        return $this->hasOne(Tipos::className(), ['tipo_id' => 'tipo_detalle_tipo_id']);
    }
}
