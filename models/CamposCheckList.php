<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campos_check_list".
 *
 * @property integer $campo_check_list_id
 * @property integer $campo_check_list_tipo_detalle_id
 * @property string $campo_check_list_nombre
 * @property integer $campo_check_list_popover_trae
 * @property string $campo_check_list_popover_valor
 */
class CamposCheckList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campos_check_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campo_check_list_tipo_detalle_id', 'campo_check_list_popover_trae'], 'integer'],
            [['campo_check_list_popover_valor'], 'string'],
            [['campo_check_list_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'campo_check_list_id' => 'Campo Check List ID',
            'campo_check_list_tipo_detalle_id' => 'Campo Check List Tipo Detalle ID',
            'campo_check_list_nombre' => 'Campo Check List Nombre',
            'campo_check_list_popover_trae' => 'Campo Check List Popover Trae',
            'campo_check_list_popover_valor' => 'Campo Check List Popover Valor',
        ];
    }
}
