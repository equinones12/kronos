<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CheckList;

/**
 * CheckListSearch represents the model behind the search form about `app\models\CheckList`.
 */
class CheckListSearch extends CheckList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['check_list_id', 'check_list_cronograma_id', 'check_list_equipo_id', 'check_list_usuario_registra', 'check_list_usuario_confirma', 'check_list_campo_id'], 'integer'],
            [['check_list_fecha_registro', 'check_list_valor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CheckList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'check_list_id' => $this->check_list_id,
            'check_list_cronograma_id' => $this->check_list_cronograma_id,
            'check_list_equipo_id' => $this->check_list_equipo_id,
            'check_list_usuario_registra' => $this->check_list_usuario_registra,
            'check_list_fecha_registro' => $this->check_list_fecha_registro,
            'check_list_usuario_confirma' => $this->check_list_usuario_confirma,
            'check_list_campo_id' => $this->check_list_campo_id,
        ]);

        $query->andFilterWhere(['like', 'check_list_valor', $this->check_list_valor]);

        return $dataProvider;
    }
}
