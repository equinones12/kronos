<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cronogramas;

/**
 * CronogramasSearch represents the model behind the search form about `app\models\Cronogramas`.
 */
class CronogramasSearch extends Cronogramas
{
    
    public $tipoCronograma;
    public $sucursal;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cronograma_id', 'cronograma_sucursal_id', 'cronograma_aprobado', 'cronograma_quien_aprueba', 'cronograma_tipo_mantenimiento'], 'integer'],
            [['cronograma_fecha_inicio', 'cronograma_fecha_fin', 'cronograma_quien_realiza', 'cronograma_fecha_aprobacion','tipoCronograma','sucursal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cronogramas::find();

        // add conditions that should always apply here

        $query->joinWith(['tipom','sucursal']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['sucursal'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['sucursales.sucursal_nombre' => SORT_ASC],
            'desc' => ['sucursales.sucursal_nombre' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tipoCronograma'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cronograma_id' => $this->cronograma_id,
            'cronograma_sucursal_id' => $this->cronograma_sucursal_id,
            'cronograma_fecha_inicio' => $this->cronograma_fecha_inicio,
            'cronograma_fecha_fin' => $this->cronograma_fecha_fin,
            'cronograma_aprobado' => $this->cronograma_aprobado,
            'cronograma_quien_aprueba' => $this->cronograma_quien_aprueba,
            'cronograma_fecha_aprobacion' => $this->cronograma_fecha_aprobacion,
            'cronograma_tipo_mantenimiento' => $this->cronograma_tipo_mantenimiento,
        ]);

        $query->andFilterWhere(['like', 'tipos_detalles.tipo_detalle_nombre', $this->tipoCronograma])
                ->andFilterWhere(['like', 'sucursales.sucursal_nombre', $this->sucursal])
                ->andFilterWhere(['like', 'cronograma_quien_realiza', $this->cronograma_quien_realiza]);


        return $dataProvider;
    }
}
