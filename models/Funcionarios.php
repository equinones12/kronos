<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funcionarios".
 *
 * @property integer $funcionario_id
 * @property string $funcionario_nombres
 * @property integer $funcionario_sucursal_id
 * @property string $funcionario_cargo
 * @property integer $funcionario_estado_id
 */
class Funcionarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'funcionarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['funcionario_nombres','funcionario_sucursal_id','funcionario_cargo','funcionario_estado_id'],'required'],
            [['funcionario_sucursal_id','funcionario_estado_id' ], 'integer'],
            [['funcionario_nombres', 'funcionario_cargo'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'funcionario_id' => 'Funcionario ID',
            'funcionario_nombres' => 'Funcionario Nombres',
            'funcionario_sucursal_id' => 'Funcionario Sucursal ID',
            'funcionario_cargo' => 'Funcionario Cargo',
            'funcionario_estado_id' => 'Funcionario Estado ID',
        ];
    }

    public function getEquipos(){

        return $this->hasOne(Equipos::className(), ['equipo_usuario_id' => 'funcionario_id']);
    }

    public function getSucursales(){

        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'funcionario_sucursal_id']);
    }

    public function getTiposdetalles(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'funcionario_estado_id']);
    }
}
