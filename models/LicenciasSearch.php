<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Licencias;

/**
 * LicenciasSearch represents the model behind the search form about `app\models\Licencias`.
 */
class LicenciasSearch extends Licencias
{
    
    public $tipo;
    public $tipoDetalle;
    public $version;
    public $estado;
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['licencia_id', 'licencia_equipo_id', 'licencia_tipo_licencia_id', 'licencia_estado_id', 'licencia_sucursal_id'], 'integer'],
            [['licencia_numero', 'licencia_version', 'licencia_especificaciones','tipo','tipoDetalle','licencia_identificacion_tecnologia','version','estado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Licencias::find();
        // echo '<pre>';
        // print_r($E);
        // die();  

        // add conditions that should always apply here

        $query->joinWith(['tipolicencia', 'tipolicencia.tiposd','version','estado']);                   
              

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['tipoDetalle'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['tipos_detalles.tipo_detalle_nombre' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tipo'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['tipos.tipo_nombre' => SORT_ASC],
            'desc' => ['tipos.tipo_nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['version'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['version.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['version.tipo_detalle_nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['estado'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['estado.tipo_detalle_nombre' => SORT_ASC],
            'desc' => ['estado.tipo_detalle_nombre' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'licencia_id' => $this->licencia_id,
            'licencia_equipo_id' => $this->licencia_equipo_id,            
            'licencia_tipo_licencia_id' => $this->licencia_tipo_licencia_id,
            'licencia_estado_id' => $this->licencia_estado_id,
            'licencia_sucursal_id' => $this->licencia_sucursal_id,
        ]);

        $query->andFilterWhere(['like', 'licencia_numero', $this->licencia_numero])            
            ->andFilterWhere(['like', 't2.tipo_detalle_nombre', $this->version])
            ->andFilterWhere(['like', 't3.tipo_detalle_nombre', $this->estado])
            ->andFilterWhere(['like', 'tipos_detalles.tipo_detalle_nombre', $this->tipoDetalle])
            ->andFilterWhere(['like', 'tipos.tipo_nombre', $this->tipo])
            ->andFilterWhere(['like', 'licencia_identificacion_tecnologia', $this->licencia_identificacion_tecnologia])
            ->andFilterWhere(['like', 'licencia_especificaciones', $this->licencia_especificaciones]);

        // echo '<pre>';
        // print_r($query);
        // die();

        return $dataProvider;
    }
}
