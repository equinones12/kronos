<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Funcionarios;

/**
 * FuncionariosSearch represents the model behind the search form about `app\models\Funcionarios`.
 */
class FuncionariosSearch extends Funcionarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['funcionario_id', 'funcionario_sucursal_id'], 'integer'],
            [['funcionario_nombres', 'funcionario_cargo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Funcionarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'funcionario_id' => $this->funcionario_id,
            'funcionario_sucursal_id' => $this->funcionario_sucursal_id,
            // 'funcionario_estado_id' => $this->funcionario_estado_id,
        ]);

        $query->andFilterWhere(['like', 'funcionario_nombres', $this->funcionario_nombres])
            ->andFilterWhere(['like', 'funcionario_cargo', $this->funcionario_cargo]);

        return $dataProvider;
    }
}
