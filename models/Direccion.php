<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "direccion".
 *
 * @property int $iddireccion
 * @property string|null $direccion_descripcion
 * @property string|null $direccion_convencion
 * @property int|null $direccion_estado
 */
class Direccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direccion_descripcion', 'direccion_convencion'], 'string'],
            [['direccion_estado'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddireccion' => 'Iddireccion',
            'direccion_descripcion' => 'Direccion Descripcion',
            'direccion_convencion' => 'Direccion Convencion',
            'direccion_estado' => 'Direccion Estado',
        ];
    }
}
