<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

class Users extends ActiveRecord{
    
	
	public function rules()
	{
	    return [
	        [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['role' => 'id']],

	    ];
	}

	public function getRol()
	{
	    return $this->hasOne(Rol::className(), ['id' => 'role']);
	}
	
    public static function getDb()
    {
        return Yii::$app->db;
    }
    
    public static function tableName()
    {
        return 'users';
    }
    
}