<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "licencias".
 *
 * @property integer $licencia_id
 * @property integer $licencia_equipo_id
 * @property integer $licencia_tipo_licencia_id
 * @property string $licencia_numero
 * @property string $licencia_version
 * @property string $licencia_especificaciones
 * @property integer $licencia_estado_id
 * @property integer $licencia_sucursal_id
 * @property string $licencia_identificacion_tecnologia
 */
class Licencias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'licencias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['licencia_sucursal_id','licencia_tipo_licencia_id','licencia_numero','licencia_identificacion_tecnologia','licencia_especificaciones','licencia_version'],'required'],
            [['licencia_equipo_id', 'licencia_tipo_licencia_id', 'licencia_estado_id', 'licencia_sucursal_id'], 'integer'],
            [['licencia_especificaciones'], 'string'],
            [['licencia_numero','licencia_identificacion_tecnologia'], 'string', 'max' => 255],
            [['licencia_version'], 'string', 'max' => 100],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'licencia_id' => 'Licencia ID',
            'licencia_equipo_id' => 'Licencia Equipo ID',
            'licencia_tipo_licencia_id' => 'Licencia Tipo Licencia ID',
            'licencia_numero' => 'Clave del Producto',
            'licencia_version' => 'Version',
            'licencia_especificaciones' => 'Especificaciones Adicionales',
            'licencia_estado_id' => 'Licencia Estado ID',
            'licencia_sucursal_id' => 'Licencia Sucursal ID',
            'licencia_identificacion_tecnologia' => 'Identificación Tecnología',
            'observacion' => 'Observacion'
        ];
    }

    public function getSucursales(){
 
        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'licencia_sucursal_id']);
    }

    public function getTipolicencia(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'licencia_tipo_licencia_id']);
    }
    public function getVersion(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'licencia_version'])->from(['t2' => TiposDetalles::tableName()]);
    }
    public function getEstado(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'licencia_estado_id'])->from(['t3' => TiposDetalles::tableName()]);
    }
    

    
}
