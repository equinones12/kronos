<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;

class FormRegister extends model{
 
    public $username;
    public $email;
    public $email2;
    public $password;
    public $password_repeat;
    public $tipoIdentificacion;
    public $role;
    public $nombres;
    public $apellidos;
    public $numeroIdentificacion;
    public $direccion;
    public $telefono;
    public $telefono2;
    public $tipoArea;
    public $tipoEstado;
    public $visitador;
    public $puntosrecaudo_idpuntosrecaudo;
    public $cargos_idcargos;
    public $tipoContrato;
    public $fechaInicioContrato;
    public $fechaFinContrato;
    public $cargo;
    public $users_idcliente;
    
    public $naturaleza;
    public $actividad_economica;
    public $lugar_expedicion_documento;
    public $municipio_id;
    public $barrio_segun_usuario;
    public $estrato;
    public $dir1;
    public $dir2;
    public $dir3;
    public $dir4;
    public $dir5;
    public $dir6;
    public $dir7;
    public $dir8;
    public $dir9;
    public $dir10;
    public $dir11;
    public $cantidadestablecimientos;
    
    
    public function rules()
    {
        return [
            [['username', 'tipoIdentificacion', 'numeroIdentificacion' , 'nombres', 'apellidos', 'email', 'telefono', 'password', 'password_repeat','dir1'], 'required', 'message' => 'Campo requerido'],
            ['username', 'match', 'pattern' => "/^.{3,50}$/", 'message' => 'Mínimo 3 y máximo 50 caracteres'],
            ['username', 'match', 'pattern' => "/^[0-9a-z]+$/i", 'message' => 'Sólo se aceptan letras y números'],
            [['tipoIdentificacion', 'numeroIdentificacion', 'tipoContrato', 'telefono', 'telefono2', 'tipoArea', 'tipoEstado'], 'number'],
            [['naturaleza','municipio_id', 'cantidadestablecimientos'], 'integer'],
            ['username', 'username_existe'],
            ['email', 'email_existe'],
            ['email2', 'email_existe2'],
            ['numeroIdentificacion', 'identificacion_existe'],
            [['direccion','apellidos','dir1','dir2','dir3','dir4','dir5','dir6','dir7','dir8','dir9','dir10','dir11'], 'string'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            ['password', 'match', 'pattern' => "/^.{6,16}$/", 'message' => 'Mínimo 6 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Los passwords no coinciden'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Nombre usuario',
            'email' => 'Email',
            'password' => 'Contraseña',
            'password_repeat' => 'Repetir contraseña',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'activate' => 'Activate',
            'role' => 'Role',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'foto' => 'Foto',
            'tipoIdentificacion' => 'Tipo Identificacion',
            'numeroIdentificacion' => 'Numero Identificacion',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'tipoContrato' => 'Tipo Contrato',
            'fechaInicioContrato' => 'Fecha Inicio Contrato',
            'fechaFinContrato' => 'Fecha Fin Contrato',
            'cargo' => 'Cargo',
            'naturaleza' => 'Naturaleza',
            'actividad_economica' => 'Actividad Economica',
            'lugar_expedicion_documento' => 'Lugar Expedicion Documento',
            'municipio_id' => 'Municipio',
            'barrio_segun_usuario' => 'Barrio',
            'estrato' => 'Nivel Socioeconomico',
            'dir1' => 'DIAGONAL *',
            'dir2' => '56 *',
            'dir3' => 'A',
            'dir4' => 'BIS',
            'dir5' => 'C',
            'dir6' => 'SUR',
            'dir7' => '84 *',
            'dir8' => 'F',
            'dir9' => '24',
            'dir10' => 'ESTE',
            'dir11' => 'Complemento',
            'cantidadestablecimientos' => 'Cantidad de establecimientos a registrar',
        ];
    }
    
    public function email_existe($attribute, $params)
    {
  
      //Buscar el email en la tabla
      $table = Users::find()->where("email=:email", [":email" => $this->email]);
      
      //Si el email existe mostrar el error
      if ($table->count() == 1)
      {
        // $this->addError($attribute, "El email seleccionado ya se encuentra registrado");
      }
    }

    public function email_existe2($attribute, $params)
    {
  
      //Buscar el email en la tabla
      $table = Users::find()->where("email2=:email2", [":email2" => $this->email2]);
      
      //Si el email existe mostrar el error
      if ($table->count() == 1)
      {
        // $this->addError($attribute, "El email seleccionado ya se encuentra registrado");
      }
    }

    public function identificacion_existe($attribute, $params)
    {
  
      //Buscar el numeroIdentificacion en la tabla
      $table = Users::find()->where("numeroIdentificacion=:numeroIdentificacion", [":numeroIdentificacion" => $this->numeroIdentificacion]);
      
      //Si el numeroIdentificacion existe mostrar el error
      if ($table->count() == 1)
      {
        $this->addError($attribute, "El numero de identificacion seleccionado ya se encuentra registrado");
      }
    }
 
    public function username_existe($attribute, $params)
    {
      //Buscar el username en la tabla
      $table = Users::find()->where("username=:username", [":username" => $this->username]);
      
      //Si el username existe mostrar el error
      if ($table->count() == 1)
      {
          $this->addError($attribute, "El usuario seleccionado ya existe");
      }
    }
 
}