<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departamentos".
 *
 * @property int $idDepartamento
 * @property string $nombre
 * @property int $codigo
 * @property string $fechaCreacion
 * @property int $concepto
 * @property int $contabilidad
 */
class Departamentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idDepartamento', 'codigo', 'concepto', 'contabilidad'], 'integer'],
            [['nombre', 'fechaCreacion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idDepartamento' => 'Id Departamento',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'fechaCreacion' => 'Fecha Creacion',
            'concepto' => 'Concepto',
            'contabilidad' => 'Contabilidad',
        ];
    }
}
