<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_perfil".
 *
 * @property integer $idmenu_perfil
 * @property integer $usuario_idusuario
 * @property integer $opcion
 * @property integer $item
 * @property integer $subitem
 */
class Menuperfil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_perfil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_idusuario', 'opcion', 'item', 'subitem'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idmenu_perfil' => 'Idmenu Perfil',
            'usuario_idusuario' => 'Usuario Idusuario',
            'opcion' => 'Opcion',
            'item' => 'Item',
            'subitem' => 'Subitem',
        ];
    }
}
