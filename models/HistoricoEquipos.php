<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "historico_equipos".
 *
 * @property integer $idhistorico_equipos
 * @property integer $historico_equipo_id
 * @property string $equipo_identificacion_tecnologia
 * @property string $equipo_nombre_actual
 * @property integer $equipo_tipo_equipo_id
 * @property string $equipo_marca
 * @property string $equipo_modelo
 * @property string $equipo_ram
 * @property string $equipo_tipo_memoria_ram
 * @property string $equipo_marca_disco_duro
 * @property string $equipo_capacidad_disco_duro
 * @property string $equipo_procesador
 * @property string $equipo_velocidad_procesador
 * @property string $equipo_cantidad_nucleos_procesador
 * @property integer $equipo_systema_operativo_id
 * @property integer $equipo_office_id
 * @property string $equipo_pantalla
 * @property integer $equipo_sucursal_id
 * @property integer $equipo_area_id
 * @property integer $equipo_usuario_id
 * @property integer $equipo_estado_id
 * @property string $equipo_numero_activo_contabilidad
 * @property string $equipo_fecha_ingreso
 * @property string $equipo_valor
 * @property string $equipo_observaciones
 * @property integer $equipo_ip
 * @property integer $usuario_modifica
 * @property string $fecha_creacion
 * @property integer $proceso_id
 * @property string $observacion
 */
class HistoricoEquipos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historico_equipos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['historico_equipo_id', 'equipo_tipo_equipo_id', 'equipo_systema_operativo_id', 'equipo_office_id', 'equipo_sucursal_id', 'equipo_area_id', 'equipo_usuario_id', 'equipo_estado_id', 'usuario_modifica', 'proceso_id'], 'integer'],
            [['equipo_fecha_ingreso', 'fecha_creacion'], 'safe'],
            [['equipo_identificacion_tecnologia', 'equipo_marca', 'equipo_ram'], 'string', 'max' => 100],
            [['equipo_nombre_actual', 'equipo_modelo', 'equipo_procesador', 'equipo_pantalla', 'equipo_numero_activo_contabilidad', 'equipo_valor', 'equipo_observaciones', 'observacion', 'equipo_ip'], 'string', 'max' => 255],
            [['equipo_tipo_memoria_ram', 'equipo_marca_disco_duro', 'equipo_capacidad_disco_duro', 'equipo_velocidad_procesador', 'equipo_cantidad_nucleos_procesador'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorico_equipos' => 'Idhistorico Equipos',
            'historico_equipo_id' => 'Historico Equipo ID',
            'equipo_identificacion_tecnologia' => 'Equipo Identificacion Tecnologia',
            'equipo_nombre_actual' => 'Equipo Nombre Actual',
            'equipo_tipo_equipo_id' => 'Equipo Tipo Equipo ID',
            'equipo_marca' => 'Equipo Marca',
            'equipo_modelo' => 'Equipo Modelo',
            'equipo_ram' => 'Equipo Ram',
            'equipo_tipo_memoria_ram' => 'Equipo Tipo Memoria Ram',
            'equipo_marca_disco_duro' => 'Equipo Marca Disco Duro',
            'equipo_capacidad_disco_duro' => 'Equipo Capacidad Disco Duro',
            'equipo_procesador' => 'Equipo Procesador',
            'equipo_velocidad_procesador' => 'Equipo Velocidad Procesador',
            'equipo_cantidad_nucleos_procesador' => 'Equipo Cantidad Nucleos Procesador',
            'equipo_systema_operativo_id' => 'Equipo Systema Operativo ID',
            'equipo_office_id' => 'Equipo Office ID',
            'equipo_pantalla' => 'Equipo Pantalla',
            'equipo_sucursal_id' => 'Equipo Sucursal ID',
            'equipo_area_id' => 'Equipo Area ID',
            'equipo_usuario_id' => 'Equipo Usuario ID',
            'equipo_estado_id' => 'Equipo Estado ID',
            'equipo_numero_activo_contabilidad' => 'Equipo Numero Activo Contabilidad',
            'equipo_fecha_ingreso' => 'Equipo Fecha Ingreso',
            'equipo_valor' => 'Equipo Valor',
            'equipo_observaciones' => 'Equipo Observaciones',
            'equipo_ip' => 'Equipo Ip',
            'usuario_modifica' => 'Usuario Modifica',
            'fecha_creacion' => 'Fecha Creacion',
            'proceso_id' => 'Proceso ID',
            'observacion' => 'Observacion',
        ];
    }
    public function getEquipos(){

        return $this->hasOne(Equipos::className(), ['historico_equipo_id' => 'equipo_id']);
    }
    public function getUsers(){

        return $this->hasOne(Users::className(),['id' => 'usuario_modifica']);
    }
    public function getSucursales(){

        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'equipo_sucursal_id']);
    }

    public function getAreas(){

        return $this->hasOne(Areas::className(), ['area_id' => 'equipo_area_id']);
    }
    public function getFuncionarios(){

        return $this->hasOne(Funcionarios::className(), ['funcionario_id' => 'equipo_usuario_id']);
    } 
    public function getEstado(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'equipo_estado_id'])
                            ->from(['t2' => TiposDetalles::tableName()]);
    }
    public function getLicencia(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'equipo_systema_operativo_id'])
                            ->from(['t2' => TiposDetalles::tableName()]);
    }  
    public function getLicenciaoff(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'equipo_office_id'])
                            ->from(['t2' => TiposDetalles::tableName()]);
    }   
}
