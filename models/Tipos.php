<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos".
 *
 * @property int $tipo_id
 * @property string $tipo_nombre
 * @property int|null $tipo_activo
 * @property int|null $tipo_creador
 * @property string|null $tipo_fcreador
 * @property int|null $tipo_modificado
 * @property string|null $tipo_fmodificado
 */
class Tipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_nombre'], 'required'],
            [['tipo_activo', 'tipo_creador', 'tipo_modificado'], 'integer'],
            [['tipo_fcreador', 'tipo_fmodificado'], 'safe'],
            [['tipo_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tipo_id' => 'Tipo ID',
            'tipo_nombre' => 'Tipo Nombre',
            'tipo_activo' => 'Tipo Activo',
            'tipo_creador' => 'Tipo Creador',
            'tipo_fcreador' => 'Tipo Fcreador',
            'tipo_modificado' => 'Tipo Modificado',
            'tipo_fmodificado' => 'Tipo Fmodificado',
        ];
    }
}
