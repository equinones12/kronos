<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "municipios".
 *
 * @property int $idMunicipio
 * @property string $nombre
 * @property int $esCapital
 * @property int $codigo
 * @property string $fechaCreacion
 * @property int $departamento_iddepartamento
 * @property int $sucursales_idsucursal
 */
class Municipios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'municipios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fechaCreacion'], 'string'],
            [['esCapital', 'codigo', 'departamento_iddepartamento', 'sucursales_idsucursal'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idMunicipio' => 'Id Municipio',
            'nombre' => 'Nombre',
            'esCapital' => 'Es Capital',
            'codigo' => 'Codigo',
            'fechaCreacion' => 'Fecha Creacion',
            'departamento_iddepartamento' => 'Departamento Iddepartamento',
            'sucursales_idsucursal' => 'Sucursales Idsucursal',
        ];
    }

    public function getDepartamento()
    {
        return $this->hasOne(Departamentos::className(), [ 'idDepartamento' => 'departamento_iddepartamento']);
    }


}
