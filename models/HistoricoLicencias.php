<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "historico_licencias".
 *
 * @property integer $idhistorico_licencias
 * @property integer $historico_licencia_id
 * @property integer $licencia_equipo_id
 * @property integer $licencia_tipo_licencia_id
 * @property string $licencia_numero
 * @property string $licencia_version
 * @property string $licencia_especificaciones
 * @property integer $licencia_estado_id
 * @property integer $licencia_sucursal_id
 * @property string $licencia_identificacion_tecnologia
 * @property integer $usuario_modifica
 * @property string $fecha_creacion
 * @property integer $proceso_id
 * @property string $observacion
 */
class HistoricoLicencias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historico_licencias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['historico_licencia_id', 'licencia_equipo_id', 'licencia_tipo_licencia_id', 'licencia_estado_id', 'licencia_sucursal_id', 'usuario_modifica', 'proceso_id'], 'integer'],
            [['fecha_creacion'], 'safe'],
            [['licencia_numero', 'licencia_version', 'licencia_especificaciones', 'licencia_identificacion_tecnologia'], 'string', 'max' => 100],
            [['observacion'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorico_licencias' => 'Idhistorico Licencias',
            'historico_licencia_id' => 'Historico Licencia ID',
            'licencia_equipo_id' => 'Licencia Equipo ID',
            'licencia_tipo_licencia_id' => 'Licencia Tipo Licencia ID',
            'licencia_numero' => 'Licencia Numero',
            'licencia_version' => 'Licencia Version',
            'licencia_especificaciones' => 'Licencia Especificaciones',
            'licencia_estado_id' => 'Licencia Estado ID',
            'licencia_sucursal_id' => 'Licencia Sucursal ID',
            'licencia_identificacion_tecnologia' => 'Licencia Identificacion Tecnologia',
            'usuario_modifica' => 'Usuario Modifica',
            'fecha_creacion' => 'Fecha Creacion',
            'proceso_id' => 'Proceso ID',
            'observacion' => 'Observacion',
        ];
    }

    public function getUser(){

        return $this->hasOne(Users::className(), ['id'=>'usuario_modifica']);
    
    }

    public function getEstado(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'proceso_id']);
    }
}
