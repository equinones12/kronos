<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_list".
 *
 * @property integer $check_list_id
 * @property integer $check_list_cronograma_id
 * @property integer $check_list_equipo_id
 * @property integer $check_list_usuario_registra
 * @property string $check_list_fecha_registro
 * @property integer $check_list_usuario_confirma
 * @property integer $check_list_campo_id
 * @property string $check_list_valor
 */
class CheckList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['check_list_cronograma_id', 'check_list_equipo_id', 'check_list_usuario_registra', 'check_list_usuario_confirma', 'check_list_campo_id'], 'integer'],
            [['check_list_fecha_registro', 'file'], 'safe'],
            [['check_list_valor'], 'string', 'max' => 255],
                 [ 'file', 'file', 
                   'skipOnEmpty' => true,
                   'uploadRequired' => 'No has seleccionado ningún archivo', //Error
                   // 'maxSize' => 1024*1024*1, //1 MB
                   // 'tooBig' => 'El tamaño máximo permitido es 1MB', //Error
                   'extensions' => 'jpg',
                   'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}', //Error
                   'maxFiles' => 60,
                   // 'tooMany' => 'El máximo de archivos permitidos son {limit}', //Error
               ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'check_list_id' => 'Check List ID',
            'check_list_cronograma_id' => 'Check List Cronograma ID',
            'check_list_equipo_id' => 'Check List Equipo ID',
            'check_list_usuario_registra' => 'Check List Usuario Registra',
            'check_list_fecha_registro' => 'Check List Fecha Registro',
            'check_list_usuario_confirma' => 'Check List Usuario Confirma',
            'check_list_campo_id' => 'Check List Campo ID',
            'check_list_valor' => 'Check List Valor',
        ];
    }
}
