<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "soporte".
 *
 * @property integer $idsoporte
 * @property integer $tiposoporte_id
 * @property integer $equipos_idequipo
 * @property string $fecha_creacion
 * @property string $fecha_solicitud
 * @property string $fecha_cierre
 * @property string $observacion_soporte
 * @property string $observacion_respuesta
 */
class Soporteequipos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soporte_equipos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idsoporte'], 'required'],
            [['idsoporte', 'tiposoporte_id', 'equipos_idequipo','estado_soporte','telefono'], 'integer'],
            [['fecha_creacion', 'fecha_solicitud', 'fecha_cierre'], 'safe'],
            [['email'],'email'],
            [['observacion_soporte', 'observacion_respuesta','evidencia','nombre','apellido'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idsoporte' => 'Idsoporte',
            'tiposoporte_id' => 'Tiposoporte ID',
            'equipos_idequipo' => 'Equipos Idequipo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_solicitud' => 'Fecha Solicitud',
            'fecha_cierre' => 'Fecha Cierre',
            'observacion_soporte' => 'Observacion Soporte',
            'observacion_respuesta' => 'Observacion Respuesta',
        ];
    }

    public function getTiposoporte(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'tiposoporte_id']);
    }
    public function getEquipo(){

        return $this->hasOne(equipos::className(), ['equipo_id' => 'equipos_idequipo']);
    }
}
