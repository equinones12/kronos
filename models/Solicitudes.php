<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitudes".
 *
 * @property int $solicitudes_id
 * @property string|null $solicitudes_tiposolicitud
 * @property string|null $solicitudes_nombres
 * @property string|null $solicitudes_telefono
 * @property string|null $solicitudes_email
 * @property string|null $solicitudes_ubicacion
 * @property string|null $solicitudes_detalle
 * @property int|null $solicitudes_estado
 */
class Solicitudes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitudes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['solicitudes_detalle'], 'string'],
            [['solicitudes_estado'], 'integer'],
            [['solicitudes_tiposolicitud', 'solicitudes_nombres', 'solicitudes_email', 'solicitudes_ubicacion'], 'string', 'max' => 150],
            [['solicitudes_telefono'], 'string', 'max' => 80],
            [['solicitudes_descripcionrespuesta'], 'string'],
            [['solicitudes_fechacreacion','solicitudes_fechacreaciont','solicitudes_fecharespuesta', 'solicitudes_fecharespuestat'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'solicitudes_id' => 'Solicitud No.',
            'solicitudes_tiposolicitud' => 'Tiposolicitud',
            'solicitudes_nombres' => 'Nombres',
            'solicitudes_telefono' => 'Telefono',
            'solicitudes_email' => 'Email',
            'solicitudes_ubicacion' => 'Ubicacion',
            'solicitudes_detalle' => 'Solicitud',
            'solicitudes_estado' => 'Estado',
            'solicitudes_descripcionrespuesta' => 'Respuesta',
        ];
    }
}
