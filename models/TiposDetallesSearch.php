<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TiposDetalles;

/**
 * TiposDetallesSearch represents the model behind the search form of `app\models\TiposDetalles`.
 */
class TiposDetallesSearch extends TiposDetalles
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_detalle_id', 'tipo_detalle_tipo_id', 'tipo_detalle_activo', 'tipo_detalle_creador', 'tipo_detalle_modificado'], 'integer'],
            [['tipo_detalle_nombre', 'tipo_detalle_fcreador', 'tipo_detalle_fmodificado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiposDetalles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tipo_detalle_id' => $this->tipo_detalle_id,
            'tipo_detalle_tipo_id' => $this->tipo_detalle_tipo_id,
            'tipo_detalle_activo' => $this->tipo_detalle_activo,
            'tipo_detalle_creador' => $this->tipo_detalle_creador,
            'tipo_detalle_fcreador' => $this->tipo_detalle_fcreador,
            'tipo_detalle_modificado' => $this->tipo_detalle_modificado,
            'tipo_detalle_fmodificado' => $this->tipo_detalle_fmodificado,
        ]);

        $query->andFilterWhere(['like', 'tipo_detalle_nombre', $this->tipo_detalle_nombre]);

        return $dataProvider;
    }
}
