<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    
    public $id;
    public $username;
    public $email;
    public $email2;
    public $password;
    public $authKey;
    public $accessToken;
    public $activate;
    public $role;
    public $foto;
    public $tipoIdentificacion;
    public $nombres;
    public $apellidos;
    public $numeroIdentificacion;
    public $direccion;
    public $telefono;
    public $telefono2;
    public $tipoContrato;
    public $fechaInicioContrato;
    public $fechaFinContrato;
    public $fechaCreacion;
    public $cargo;
    public $users_idcliente;
    public $naturaleza;
    public $actividad_economica;
    public $lugar_expedicion_documento;
    public $municipio_id;
    public $barrio_segun_usuario;
    public $estrato;
    public $dir1;
    public $dir2;
    public $dir3;
    public $dir4;
    public $dir5;
    public $dir6;
    public $dir7;
    public $dir8;
    public $dir9;
    public $dir10;
    public $dir11;
    public $verification_code;
    public $cantidadestablecimientos;
    public $cliente_id;
    
    // ///////////// ============== ROLES ================== /////////
        // 1. Administrador     ->  isUserAdmin
        // 2. Cliente           ->  isCliente
    // ///////////// ======================================= /////////

    // ========== Validacion rol Administrador      ========
        public static function isUserAdmin($id)
        {
           if (Users::findOne(['id' => $id, 'activate' => '1', 'role' => 1])){
            return true;
           } else {

            return false;
           }
        }
    // ========== Validacion rol cliente               ========
        public static function isCliente($id)
        {
           if (Users::findOne(['id' => $id, 'activate' => '1', 'role' => 2])){
           return true;
           } else {

           return false;
           }
        }


    /* busca la identidad del usuario a través de su $id */
        public static function findIdentity($id)
        {
            $user = Users::find()
                    ->where("activate=:activate", [":activate" => 1])
                    ->andWhere("id=:id", ["id" => $id])
                    ->one();
            
            return isset($user) ? new static($user) : null;
        }
    /* Busca la identidad del usuario a través de su token de acceso */
        public static function findIdentityByAccessToken($token, $type = null)
        {
            
            $users = Users::find()
                    ->where("activate=:activate", [":activate" => 1])
                    ->andWhere("accessToken=:accessToken", [":accessToken" => $token])
                    ->all();
            
            foreach ($users as $user) {
                if ($user->accessToken === $token) {
                    return new static($user);
                }
            }

            return null;
        }
    /* Busca la identidad del usuario a través del username */
        public static function findByUsername($username)
        {
            $users = Users::find()
                    ->where("activate=:activate", ["activate" => 1])
                    ->andWhere("username=:username", [":username" => $username])
                    ->all();
            
            foreach ($users as $user) {
                if (strcasecmp($user->username, $username) === 0) {
                    return new static($user);
                }
            }

            return null;
        }
    /* Regresa el id del usuario */
        public function getId()
        {
            return $this->id;
        }
    /* Regresa la clave de autenticación */
        public function getAuthKey()
        {
            return $this->authKey;
        }
    /* Valida la clave de autenticación */
        public function validateAuthKey($authKey)
        {
            return $this->authKey === $authKey;
        }
    // VALIDA EL PASSWORD
        public function validatePassword($password)
        {
            /* Valida el password */
            if (crypt($password, $this->password) == $this->password)
            {
            return $password === $password;
            }
        }
}
