<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clientes;

/**
 * ClientesSearch represents the model behind the search form of `app\models\Clientes`.
 */
class ClientesSearch extends Clientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clientes_id'], 'integer'],
            [['clientes_nombre', 'clientes_nit', 'clientes_direccion', 'clientes_fechacreacion', 'clientes_licencia', 'clientes_licencia_fecha_desde', 'clientes_licencia_fecha_hasta', 'clientes_tipolicencia', 'clientes_cantidadusuarios'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'clientes_id' => $this->clientes_id,
            'clientes_fechacreacion' => $this->clientes_fechacreacion,
        ]);

        $query->andFilterWhere(['like', 'clientes_nombre', $this->clientes_nombre])
            ->andFilterWhere(['like', 'clientes_nit', $this->clientes_nit])
            ->andFilterWhere(['like', 'clientes_licencia', $this->clientes_licencia])
            ->andFilterWhere(['like', 'clientes_licencia_fecha_desde', $this->clientes_licencia_fecha_desde])
            ->andFilterWhere(['like', 'clientes_licencia_fecha_hasta', $this->clientes_licencia_fecha_hasta])

            ->andFilterWhere(['like', 'clientes_tipolicencia', $this->clientes_tipolicencia])
            ->andFilterWhere(['like', 'clientes_cantidadusuarios', $this->clientes_cantidadusuarios])
            
            ->andFilterWhere(['like', 'clientes_direccion', $this->clientes_direccion]);

        return $dataProvider;
    }
}
