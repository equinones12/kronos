<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Solicitudes;

/**
 * SolicitudesSearch represents the model behind the search form of `app\models\Solicitudes`.
 */
class SolicitudesSearch extends Solicitudes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['solicitudes_id', 'solicitudes_estado'], 'integer'],
            [['solicitudes_tiposolicitud', 'solicitudes_nombres', 'solicitudes_telefono', 'solicitudes_email', 'solicitudes_ubicacion', 'solicitudes_detalle'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Solicitudes::find();
        $query->where(['solicitudes_estado' => 0 ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'solicitudes_id' => $this->solicitudes_id,
            'solicitudes_estado' => $this->solicitudes_estado,
        ]);

        $query->andFilterWhere(['like', 'solicitudes_tiposolicitud', $this->solicitudes_tiposolicitud])
            ->andFilterWhere(['like', 'solicitudes_nombres', $this->solicitudes_nombres])
            ->andFilterWhere(['like', 'solicitudes_telefono', $this->solicitudes_telefono])
            ->andFilterWhere(['like', 'solicitudes_email', $this->solicitudes_email])
            ->andFilterWhere(['like', 'solicitudes_ubicacion', $this->solicitudes_ubicacion])
            ->andFilterWhere(['like', 'solicitudes_detalle', $this->solicitudes_detalle]);

        return $dataProvider;
    }
}
