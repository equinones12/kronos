<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "controladores".
 *
 * @property integer $idcontrolador
 * @property string $nombreControlador
 *
 * @property Rback[] $rbacks
 */
class Controladores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'controladores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreControlador'], 'required'],
            [['nombreControlador'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcontrolador' => 'Idcontrolador',
            'nombreControlador' => 'Nombre Controlador',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacks()
    {
        return $this->hasMany(Rback::className(), ['idControlador' => 'idcontrolador']);
    }
}
