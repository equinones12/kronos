<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property integer $equipo_id
 * @property string $equipo_identificacion_tecnologia
 * @property string $equipo_nombre_actual
 * @property integer $equipo_tipo_equipo_id
 * @property string $equipo_marca
 * @property string $equipo_ram
 * @property string $equipo_tipo_memoria_ram
 * @property string $equipo_cantidad_memoria_ram
 * @property string $equipo_marca_disco_duro
 * @property string $equipo_capacidad_disco_duro
 * @property string $equipo_procesador
 * @property string $equipo_velocidad_procesador
 * @property string $equipo_cantidad_nucleos_procesador
 * @property integer $equipo_systema_operativo_id
 * @property integer $equipo_office_id
 * @property string $equipo_pantalla
 * @property integer $equipo_sucursal_id
 * @property integer $equipo_usuario_id
 * @property integer $equipo_estado_id
 * @property string $equipo_numero_activo_contabilidad
 * @property string $equipo_fecha_ingreso
 * @property string $equipo_valor
 * @property string $equipo_observaciones
 * @property integer $equipo_area_id
 * @property integer $equipo_ip
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipo_identificacion_tecnologia','equipo_nombre_actual','equipo_tipo_equipo_id','equipo_marca','equipo_modelo','equipo_capacidad_disco_duro','equipo_tipo_memoria_ram','equipo_marca_disco_duro','equipo_ram','equipo_procesador'],'required'],
            [['equipo_velocidad_procesador','equipo_velocidad_procesador','equipo_systema_operativo_id','equipo_office_id','equipo_sucursal_id','equipo_area_id','equipo_usuario_id','equipo_estado_id','equipo_observaciones'],'required'],
            [['equipo_tipo_equipo_id', 'equipo_systema_operativo_id', 'equipo_office_id', 'equipo_sucursal_id', 'equipo_usuario_id', 'equipo_estado_id', 'equipo_area_id'], 'integer'],
            [['equipo_fecha_ingreso', 'equipo_observaciones','equipo_id','equipo_ip'], 'safe'],
            [['equipo_identificacion_tecnologia', 'equipo_marca', 'equipo_ram'], 'string', 'max' => 100],
            [['equipo_nombre_actual', 'equipo_procesador', 'equipo_pantalla', 'equipo_numero_activo_contabilidad', 'equipo_valor', 'equipo_modelo'], 'string', 'max' => 255],
            [['equipo_tipo_memoria_ram',  'equipo_marca_disco_duro', 'equipo_capacidad_disco_duro', 'equipo_velocidad_procesador', 'equipo_cantidad_nucleos_procesador'], 'string', 'max' => 45],
            [['equipo_baja_activo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }
    public function upload($name)
    {
        if($this->validate()){
            $this->equipo_baja_activo->saveAs('images/actas/'.$this->equipo_baja_activo->baseName.'.'.$this->equipo_baja_activo->extension);
            return true;            
        }else{
            return false;
        }
    }
    public function validacion()
    {
        if($this->equipo_baja_activo->extensions != ('pdf'))
        {
            echo "Archivos no validos";
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'equipo_id' => 'Equipo ID',
            'equipo_identificacion_tecnologia' => 'Identificacion T & T',
            'equipo_nombre_actual' => 'Nombre Actual',
            'equipo_tipo_equipo_id' => 'Tipo de Equipo',
            'equipo_marca' => 'Marca',
            'equipo_modelo' => 'Modelo',
            'equipo_ram' => 'Ram',
            'equipo_tipo_memoria_ram' => 'Tipo de Memoria Ram',
            'equipo_marca_disco_duro' => 'Marca de Disco Duro',
            'equipo_capacidad_disco_duro' => 'Capacidad de Disco Duro',
            'equipo_procesador' => 'Procesador',
            'equipo_velocidad_procesador' => 'Velocidad del Procesador',
            'equipo_cantidad_nucleos_procesador' => 'Nucleos del Procesador',
            'equipo_systema_operativo_id' => 'Sistema Operativo',
            'equipo_office_id' => 'Office',
            'equipo_pantalla' => 'Pantalla',
            'equipo_sucursal_id' => 'Sucursal',
            'equipo_area_id' => 'Area',
            'equipo_usuario_id' => 'Usuario',
            'equipo_estado_id' => 'Estado',
            'equipo_numero_activo_contabilidad' => 'Numero Activo Contabilidad',
            'equipo_fecha_ingreso' => 'Fecha Ingreso',
            'equipo_valor' => 'Valor',
            'equipo_observaciones' => 'Especificaciones adicionales',
            'equipo_ip'=>'Ip asignada'
        ];
    }

    public function getFuncionarios(){

        return $this->hasOne(Funcionarios::className(), ['funcionario_id' => 'equipo_usuario_id']);
    }

    public function getTipoMaquina(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'equipo_tipo_equipo_id' ]);
    }

    public function getSucursal(){

        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'equipo_sucursal_id']);
    }

    public function getArea(){

        return $this->hasOne(Areas::className(), ['area_id' => 'equipo_area_id']);
    }

    public function getEstado(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'equipo_estado_id'])
                            ->from(['t2' => TiposDetalles::tableName()]);
    }
    public function getLicencias(){

        return $this->hasOne(Licencias::classname(),['licencia_equipo_id' => 'equipo_id']);
    }
    public function getHistoricoLicencias(){

        return $this->hasOne(HistoricoLicencias::className(),['licencia_equipo_id' => 'equipo_id']);
    }
    public function getHistoricoEquipos(){

        return $this->hasOne(HistoricoEquipos::className(),['historico_equipo_id' => 'equipo_id']);
    }
    public function getMarca(){

        return $this->hasOne(TiposDetalles::className(),['tipo_detalle_id' =>'equipo_marca']);
    }
}
