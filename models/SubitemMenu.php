<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subitem_menu".
 *
 * @property integer $id
 * @property string $action
 * @property string $etiqueta
 * @property integer $id_item
 * @property integer $estado
 *
 * @property ItemMenu $idItem
 */
class SubitemMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subitem_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_item'], 'integer'],
            [['action', 'etiqueta'], 'string', 'max' => 45],
            [['id_item'], 'exist', 'skipOnError' => true, 'targetClass' => ItemMenu::className(), 'targetAttribute' => ['id_item' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'etiqueta' => 'Etiqueta',
            'id_item' => 'Id Item',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdItem()
    {
        return $this->hasOne(ItemMenu::className(), ['id' => 'id_item']);
    }
}
