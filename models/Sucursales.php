<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sucursales".
 *
 * @property integer $sucursal_id
 * @property string $sucursal_nombre
 * @property string $sucursal_direccion
 * @property string $sucursal_telefono
 * @property string $sucursal_contacto
 */
class Sucursales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sucursales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sucursal_nombre', 'sucursal_direccion', 'sucursal_telefono'], 'string', 'max' => 100],
            [['sucursal_contacto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sucursal_id' => 'Sucursal ID',
            'sucursal_nombre' => 'Sucursal Nombre',
            'sucursal_direccion' => 'Sucursal Direccion',
            'sucursal_telefono' => 'Sucursal Telefono',
            'sucursal_contacto' => 'Sucursal Contacto',
        ];
    }
}
