<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cronogramas_detalles".
 *
 * @property integer $cronograma_detalle_id
 * @property integer $cronograma_detalle_cronograma_id
 * @property integer $cronograma_detalle_equipo_id
 * @property integer $cronograma_detalle_estado
 * @property integer $cronograma_detalle_check_list_id
 * @property string $cronograma_detalle_fecha_realizado
 * @property string $cronograma_detalle_hora_inicio
 * @property string $cronograma_detalle_hora_fin
 * @property string $cronograma_detalle_quien_realiza
 */
class CronogramasDetalles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cronogramas_detalles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cronograma_detalle_cronograma_id', 'cronograma_detalle_equipo_id', 'cronograma_detalle_estado', 'cronograma_detalle_check_list_id'], 'integer'],
            [['cronograma_detalle_fecha_realizado', 'cronograma_detalle_hora_inicio', 'cronograma_detalle_hora_fin'], 'safe'],
            [['cronograma_detalle_quien_realiza'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cronograma_detalle_id' => 'Cronograma Detalle ID',
            'cronograma_detalle_cronograma_id' => 'Cronograma Detalle Cronograma ID',
            'cronograma_detalle_equipo_id' => 'Cronograma Detalle Equipo ID',
            'cronograma_detalle_estado' => 'Cronograma Detalle Estado',
            'cronograma_detalle_check_list_id' => 'Cronograma Detalle Check List ID',
            'cronograma_detalle_fecha_realizado' => 'Cronograma Detalle Fecha Realizado',
            'cronograma_detalle_hora_inicio' => 'Cronograma Detalle Hora Inicio',
            'cronograma_detalle_hora_fin' => 'Cronograma Detalle Hora Fin',
            'cronograma_detalle_quien_realiza' => 'Cronograma Detalle Quien Realiza',
        ];
    }
}
