<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cronogramas".
 *
 * @property integer $cronograma_id
 * @property integer $cronograma_sucursal_id
 * @property string $cronograma_fecha_inicio
 * @property string $cronograma_fecha_fin
 * @property string $cronograma_quien_realiza
 * @property integer $cronograma_aprobado
 * @property integer $cronograma_quien_aprueba
 * @property string $cronograma_fecha_aprobacion
 * @property integer $cronograma_tipo_mantenimiento
 */
class Cronogramas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cronogramas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cronograma_sucursal_id', 'cronograma_aprobado', 'cronograma_quien_aprueba', 'cronograma_tipo_mantenimiento'], 'integer'],
            [['cronograma_fecha_inicio', 'cronograma_fecha_fin', 'cronograma_fecha_aprobacion'], 'safe'],
            [['cronograma_quien_realiza'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cronograma_id' => 'Cronograma ID',
            'cronograma_sucursal_id' => 'Cronograma Sucursal ID',
            'cronograma_fecha_inicio' => 'Fecha de Inicio',
            'cronograma_fecha_fin' => 'Fecha de Finalización',
            'cronograma_quien_realiza' => 'Quien Realiza',
            'cronograma_aprobado' => 'Cronograma Aprobado',
            'cronograma_quien_aprueba' => 'Cronograma Quien Aprueba',
            'cronograma_fecha_aprobacion' => 'Cronograma Fecha Aprobacion',
            'cronograma_tipo_mantenimiento' => 'Tipo de Mantenimiento',
        ];
    }

    public function getFuncionarios(){

        return $this->hasOne(Funcionarios::className(), ['funcionario_id' => 'cronograma_quien_realiza']);
    }

    public function getSucursal(){

        return $this->hasOne(Sucursales::className(), ['sucursal_id' => 'cronograma_sucursal_id']);
    }

    public function getTipom(){

        return $this->hasOne(TiposDetalles::className(), ['tipo_detalle_id' => 'cronograma_tipo_mantenimiento']);
    }

}
