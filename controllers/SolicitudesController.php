<?php

namespace app\controllers;

use Yii;
use app\models\Solicitudes;
use app\models\SolicitudesSearch;
use app\models\SolicitudescerradasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SolicitudesController implements the CRUD actions for Solicitudes model.
 */
class SolicitudesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Solicitudes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SolicitudesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCerradas()
    {
        $searchModel = new SolicitudescerradasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cerradas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjaxfindone($id){
        $model = Solicitudes::findOne($id);

        echo '<table class="table table-hover table-sm">';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Solicitud No</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_id.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Tipo de solicitud</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_tiposolicitud.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Nombre solicitante</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_nombres.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Telefono solicitante</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_telefono.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Email solicitante</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_email.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Ubicacion</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_ubicacion.'</td>';
                echo '</tr>';
            echo '</tbody>';
            echo '<thead>';
                echo '<tr>';
                    echo '<th>Descripcion solicitud</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                echo '<tr>';
                    echo '<td>'.$model->solicitudes_detalle.'</td>';
                echo '</tr>';
            echo '</tbody>';
        echo '</table>';
        
    }

    public function actionCerrarcorreo($id, $descripcion){
        
        $model = Solicitudes::findOne($id);
        $model->solicitudes_fecharespuesta = date('Y-m-d');
        $model->solicitudes_fecharespuestat = date('Y-m-d H:i:s');
        $model->solicitudes_descripcionrespuesta = $descripcion;
        $model->solicitudes_estado = 1;
        $model->save();

        $body = '';
        $body    .= '<center>';
            $body    .= '<div style="width:750px; height:550px; background:#F1F1F1;">';
                // BLOQUE INFORMACION CLIENTE
                    $body    .= '<div style="float:left; padding:10px; text-align: left; width:750px; padding:5px; backgrond:green;">';
                        $body    .= '<p>Buen dia Sr(a) <br><b>'. $model->solicitudes_nombres . '</b></p>';
                        $body    .= '<p><b>Fecha solicitud: </b> '. $model->solicitudes_fechacreaciont .'</p>';
                        $body    .= '<p><b>Tipo de solicitud: </b> '. $model->solicitudes_tiposolicitud .'</p>';
                        $body    .= '<p><b>Telefono: </b> '. $model->solicitudes_telefono .'</p>';
                        $body    .= '<p><b>Ubicacion: </b> '. $model->solicitudes_ubicacion .'</p>';
                        $body    .= '<p><b>Solicitud: </b> '. $model->solicitudes_detalle .'</p>';
                        $body    .= '<br><br>';
                        $body    .= '<p><b>Respuesta: </b> '. $model->solicitudes_descripcionrespuesta .'</p>';
                        $body    .= '<p>Adjunto a este correo electronico se encuentra la informacion solicitada en relacion a PEM.</p>';
                        
                        // $body    .= '<img itemprop="image" data-type="image" style="margin:0 auto; background:white;" src="http://35.175.13.101/OsaPlus/web/images/f52610_b3fa64f9ff4d4ac0a9d8f745fa6b0a82.png" alt="OSA">';
                    $body    .= '</div>';
                // BLOQUE FOOTER
                $body    .= '<div style="float:left; color:white; width:750px; padding:10px 0px; border:1px solid #F1F1F1; background:#195176;">';
                    $body    .= '<h3>Stellar WAY S.A.S &copy; '.date('Y').'</h3>';
                    $body    .= '<h4>Bogota - Colombia</h4>';
                    $body    .= '<h4>Todos los derechos Reservados</h4>';
                    $body    .= '<a style="color:white;" href="http://stellarway.com/"><h4>Stellar WAY S.A.S</h4></a>';
                $body    .= '</div>';
            $body    .= '</div>';
        $body    .= '</center>';
        
        $switfmailer = Yii::$app->mailer->compose();
        $switfmailer->setTo($model->solicitudes_email);
        $switfmailer->setFrom([ 'noreply@stellarway.com' => 'Stellar WAY S.A.S' ]);
        $switfmailer->setSubject('Informacion PEM');
        // if ($campana->campana_tipocampana == 891) {
        //     $switfmailer->attachContent($pdf->Output('S'), ['fileName' => 'liquidacion.pdf', 'contentType' => 'application/pdf']);
        // }
        $switfmailer->setHtmlBody($body);
        $switfmailer->send();
        
    }

    public function actionCerrarsolicitud($id, $descripcion){
        $model = Solicitudes::findOne($id);
        $model->solicitudes_fecharespuesta = date('Y-m-d');
        $model->solicitudes_fecharespuestat = date('Y-m-d H:i:s');
        $model->solicitudes_descripcionrespuesta = $descripcion;
        $model->solicitudes_estado = 1;
        $model->save();
    }

    /**
     * Displays a single Solicitudes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Solicitudes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Solicitudes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->solicitudes_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Solicitudes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->solicitudes_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Solicitudes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Solicitudes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Solicitudes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Solicitudes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
