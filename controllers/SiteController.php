<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Soporteequipos;
use app\models\ContactForm;
use yii\web\Response;
use app\models\FormRegister; 
use app\models\Users; 
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Rol;
use yii\db\Query;
use yii\data\Pagination;
use app\models\FormSearch;
use app\models\OpcionMenu;
use app\models\ItemMenu;
use app\models\SubitemMenu;
use app\models\Menuperfil;
use app\models\Controlusuariomenu;
use app\models\AccesosDirectos; 


use yii\helpers\Html;


class SiteController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'user', 'admin','index','register'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','register'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserVentas(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserTesoreria(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserComercial(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserAlmacen(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserProgramador(Yii::$app->user->identity->id);
                      },
                   ],

                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserDigitador(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                        //Los usuarios simples tienen permisos sobre las siguientes acciones
                        'actions' => ['logout', 'user','index'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario simple
                            return User::isConductor(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        //Los usuarios simples tienen permisos sobre las siguientes acciones
                        'actions' => ['logout', 'user','index'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario simple
                            return User::isUserAuxiliar(Yii::$app->user->identity->id);
                        },
                    ],

                    [
                        //Los usuarios simples tienen permisos sobre las siguientes acciones
                        'actions' => ['logout', 'user','index'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario simple
                            return User::isCompras(Yii::$app->user->identity->id);
                        },
                    ],

                ],
            ],
     //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
     //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $userIdentity = Users::findOne(yii::$app->user->identity->id);
        $accesosDirectos = array();
            $arrayTapas = AccesosDirectos::find()
                                ->where(['idRol' => yii::$app->user->identity->role ])
                                ->all();
            foreach ($arrayTapas as $a) {
                array_push($accesosDirectos, $a->numerotapa);
            }
            $roles = Rol::find()->all();


        if(!Yii::$app->user->isGuest){

            $idUsuario =  Yii::$app->user->identity->id;

            // se instancia la Clase Query.
            $query = new Query();
            $connection = Yii::$app->db;

            $query = $connection->createCommand('SELECT *
                                                from    users
                                                where   1=1
                                                        and id='.$idUsuario);
            $result = $query->queryAll();

            foreach ($result as $key ) {}   

            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// INICIO DE LAS VALIDACION SEGUN EL ROL DEL USUARIO LOGUEADO Y ASI MISMO PINTURA DEL MENU PRINCIPAL
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            ##################################    
            # render para rol de administrador
            ##################################    
            if($key['role']==1){

                // se instancia la Clase Query.
                $query = new Query();
                $connection = Yii::$app->db;

                // consulta que muestra la informacion en el index del modulo de Visita
                $query = $connection->createCommand('SELECT *,
                                                            (select count(*) from cronogramas_detalles where cronograma_detalle_cronograma_id = cronogramas.cronograma_id and cronograma_detalle_realizado=0) as cuenta
                                                     from   cronogramas
                                                            join sucursales on (cronograma_sucursal_id = sucursal_id)
                                                     where  1=1
                                                            and cronograma_realizado = 0
                                                            and cronograma_quien_realiza = '.$idUsuario);
                $result = $query->queryAll();


                // consulta que muestra la informacion en el index del modulo de Visita
                $query = $connection->createCommand('SELECT count(*) as cuenta
                                                     from   equipos
                                                     where  1=1');
                $equipos = $query->queryAll();


                $query = $connection->createCommand('SELECT count(*) as cuenta
                                                     from   licencias
                                                     where  1=1');
                $licencias = $query->queryAll();

                $soporrealizados = Soporteequipos::find()->where(['estado_soporte'=>1])->count('idsoporte');
                $soporpendientes = Soporteequipos::find()->where(['estado_soporte'=>0])->count('idsoporte');


                // echo "<pre>";
                // print_r($result);die();

                if(!$result){
                    $result = null;
                }    

                return $this->render('index', [
                    'result'=> $result,
                    'userIdentity' => $userIdentity,                    
                    'accesosDirectos' => $accesosDirectos,
                    'roles' => $roles,
                    'equipos' => $equipos,
                    'licencias' => $licencias,
                    'soporrealizados' => $soporrealizados,
                    'soporpendientes' => $soporpendientes
                ]);
            }

            #####################################################   
            # render para rol de Control Calidad
            #####################################################
            if($key['role']==2){

                // sentencia con la que se declara el id de usuario logueado
                $idUsuario =  Yii::$app->user->identity->id;
                // se instancia la Clase Query.
                $query = new Query();
                $connection = Yii::$app->db;


                // consulta que muestra la informacion en el index del modulo de Visita
                $query = $connection->createCommand('SELECT *,
                                                            (select count(*) from cronogramas_detalles where cronograma_detalle_cronograma_id = cronogramas.cronograma_id and cronograma_detalle_realizado=0) as cuenta
                                                     from   cronogramas
                                                            join sucursales on (cronograma_sucursal_id = sucursal_id)
                                                     where  cronograma_quien_realiza = '.$idUsuario);
                $result = $query->queryAll();

                // echo "<pre>";
                // print_r($result);die();
                
                $result = $result!=null ? $result : null;

                return $this->render('index', [
                    'result'=> $result,
                    'userIdentity' => $userIdentity,
                    'accesosDirectos' => $accesosDirectos,
                    'roles' => $roles

                ]);

            }

        }

        return $this->render('index',[
            'userIdentity' => $userIdentity,
            'accesosDirectos' => $accesosDirectos,
            'roles' => $roles
        ]);
    }

    public function actionDesaprobadasindex(){

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand("SELECT *, 
                                                    (select concat (nombres, ' ' , apellidos) from users where id = subactividad_tecnico_tecnico_id ) as tecnico
                                              from  subactividades
                                                    join actividades on (subactividad_actividad_id = actividad_id)
                                                    join tipos_detalles on (subactividad_tipo_actividad_id = tipo_detalle_id)
                                                    join subactividades_tecnicos on (subactividad_id = subactividad_tecnico_subactividad_id)
                                            where   subactividad_control_calidad = 2 
                                            order by subactividad_id desc
                                            limit 15");

        $desaprobadasrecientesA = $query->queryAll();


        $desaprobadasrecientes = Subactividades::find()
                                ->joinWith('subactividadtecnico')
                               ->where(['subactividad_control_calidad' => 2 ])
                               ->orderBy(['subactividad_id' => SORT_DESC])
                               ->limit(15)->all();
        

        // echo "<pre>";
        // print_r($desaprobadasrecientes);
        // die;
        
        return $this->render('desaprobadasindex',[
            'desaprobadasrecientes' => $desaprobadasrecientes,
            'desaprobadasrecientesA' => $desaprobadasrecientesA
        ]);
    
    }

    public function actionDesaprobadasresult(){
        $subactividades = Subactividades::find();
        $subactividades->joinWith('actividad');
        $subactividades->where(['subactividad_control_calidad' => 2]);
        if ($_REQUEST['ordenservicio']) {
            $subactividades->andWhere(['actividad_orden_servicio_id' => $_REQUEST['ordenservicio']]);
        }
        if ($_REQUEST['actividad']) {
            $subactividades->andWhere(['actividad_tipo_actividad_id' => $_REQUEST['actividad']]);
        }
        if ($_REQUEST['subactividad']) {
            $subactividades->andWhere(['subactividad_tipo_actividad_id' => $_REQUEST['subactividad']]);
        }
        if ($_REQUEST['fechadesde']) {
            $subactividades->andWhere(['between', 'subactividad_fcreacion', $_REQUEST['fechadesde'] . ' 01:00:00', $_REQUEST['fechahasta'] . ' 23:00:00' ]);
        }
        

        $modelsubactividades = $subactividades->all();
        return $this->render('desaprobadasresult',[
            'modelsubactividades' => $modelsubactividades
         ]);
    }

    public function actionControlmenu($ruta){
        $controlmenu = new Controlusuariomenu();
        $controlmenu->control_usuario_menu_ruta = $ruta;
        $controlmenu->control_usuario_menu_usuario_id = yii::$app->user->identity->id;
        $controlmenu->control_usuario_menu_fechat = date('Y-m-d H:i:s');
        $controlmenu->control_usuario_menu_fecha = date('Y-m-d');
        $controlmenu->save();
        return $ruta;
    }
    
    public function actionComercialadminindex(){
        $query = new Query();
        $connection = Yii::$app->db;
         // consulta que muestra la informacion en el index del modulo de Visita
         $query = $connection->createCommand("SELECT cotizacion_orden_servicio_id as ordenes
                                                from cotizaciones
                                            where  cotizacion_aprobada in (368,369,370)
                                                    and cotizacion_orden_servicio_id is not null
                                        ");
        $validapendiente = $query->queryAll();

        $arrayValida = array();
        foreach ($validapendiente as $a) {
            array_push($arrayValida,$a['ordenes']);
            $validate = "and ordenes_servicio_id not in (".implode(',',$arrayValida).")";
        }
        if(empty($arrayValida)) {
            $validate=null;
        }

        $filter = '';

        if (Yii::$app->request->post()) {
            // echo '<pre>'; print_r( $_POST ); die;
            
            if ($_POST['fechadesde']) {
                $filter .= " and actividad_fecha_entrega BETWEEN  '".$_POST['fechadesde']."' AND '".$_POST['fechahasta']."' ";
            }
            if ($_POST['ordenservicio']) {
                $filter .= " and ordenes_servicio_id = ".$_POST['ordenservicio']." ";
            }
            if ($_POST['estado']) {
                $filter .= " and ordenes_servicio_estado_id = ".$_POST['estado']." ";
            }
            if ($_POST['cliente']) {
                $filter .= " and ordenes_servicio_cliente_id = ".$_POST['cliente']." ";
            }
        }

        $query = $connection->createCommand("SELECT *,
            (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=ordenes_servicio_tipo_orden_id) as tipoOrden
            from    actividades 
            join tipos_detalles on (actividad_tipo_actividad_id = tipo_detalle_id)
            join ordenes_servicio on (actividad_orden_servicio_id = ordenes_servicio_id)
            join maquinas on (ordenes_servicio_maquina_id = maquina_id)
            join clientes on (ordenes_servicio_cliente_id = cliente_id)
            where   1=1
            and actividad_estado_id = 31
            and actividad_tipo_actividad_id = 79
            $validate
            $filter
        ");
        $result = $query->queryAll();

        

        return $this->render('comercialadminindex', [
            'result'=> $result,
        ]);

    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(["site/index"]);
        }

        $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {

                return $this->redirect(["site/index"]);

            } else {
                return $this->render('login', [
                    'model' => $model,
                ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUser(){
        return $this->render("user");
    }

    public function actionAdmin(){
        return $this->render("admin");
    }

    private function randKey($str='', $long=0){
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;

        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }

        return $key;
    }

    public function actionConfirm(){

        $table = new Users;

        if (Yii::$app->request->get())
        {
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                //Realizamos la consulta para obtener el registro
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
      
                //Si el registro existe
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = 1;

                    if ($activar->update())
                    {
                        echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                    }
                    else
                    {
                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";

                    }
                }
                else //Si no existe redireccionamos a login
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else //Si id no es un número entero redireccionamos a login
            {
                return $this->redirect(["site/login"]);
            }
        }
    }

    public function actionRegister()
    { 

        // 


        $opciones = OpcionMenu::find()->all();
        $items = ItemMenu::find()->all();
        $subitems = SubitemMenu::find()->all();
        // 
        
        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);

        }

        if ($model->load(Yii::$app->request->post()))
        {
            
            if($model->validate())
            {

                // echo "<pre>";
                // print_r($_REQUEST);die();

                //Preparamos la consulta para guardar el usuario
                $user = new Users;
                $user->username = $model->username;
                $user->email = $model->email;
                $user->role = $model->role; 
                $user->nombres = $model->nombres; 
                $user->apellidos = $model->apellidos; 
                $user->numeroIdentificacion = $model->numeroIdentificacion; 
                $user->direccion = $model->direccion; 
                $user->telefono = $model->telefono; 
                $user->especialidad = $_REQUEST['especialidad'];
                $user->rh = $_REQUEST['FormRegister']['rh'] ;
                $user->salario =  $_REQUEST['FormRegister']['salario'];
                $user->fechaInicioContrato = $_REQUEST['fechaInicio'];
                $user->fechaFinalContrato = $_REQUEST['fechaFinal'];
                $user->tipoContrato = $_REQUEST['tipoContrato'];
                

                //Encriptamos el password
                $user->password = crypt($model->password, Yii::$app->params["salt"]);
                // $user->password = md5($model->password);

                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $user->authKey = $this->randKey("abcdef0123456789", 200);
                //Creamos un token de acceso único para el usuario
                $user->accessToken = $this->randKey("abcdef0123456789", 200);
                //Si el registro es guardado correctamente
                if ($user->insert())
                {

                    // CREACION DEL MENU PARA EL PERFIL CREADO
                    if (isset($_POST['opciones'])) {
                        foreach ($_POST['opciones'] as $opcion) {
                            
                            $opcionExplode = explode(',', $opcion);
                            $opcion = $opcionExplode[0];
                            $item = $opcionExplode[1];
                            $subitem = $opcionExplode[2];

                            $menu = new Menuperfil();
                            $menu->usuario_idusuario = $user->id;
                            $menu->opcion = $opcion;
                            $menu->item = $item;
                            $menu->subitem = $subitem;
                            $menu->save();
                        }
                    }
                    
                    $newUser = Users::findOne($user->id);
                    $msg = "El funcionario ".$newUser->username.' ha sido registrado';
                    $alert = 'success';
                    $graphicon = 'ok';
                    return $this->render("newUser", 
                        [
                            "model" => $newUser, 
                            "msg" => $msg,
                            "alert" => $alert,
                            "graphicon" => $graphicon
                        ]
                    );
                }
                else
                {
                    $newUser = false;
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                    $alert = 'danger';
                    $graphicon = 'remove';

                    return $this->render("newUser", 
                        [
                            "model" => $newUser, 
                            "msg" => $msg,
                            "alert" => $alert,
                            "graphicon" => $graphicon
                        ]
                    );
                }
            }
            else
            {
                $model->getErrors();

            }
        }
        $roles = Rol::find()->all();
        return $this->render("register", 
            [
                "model" => $model, 
                "msg" => $msg,
                "roles"=>$roles,
                'opciones' => $opciones,
                'items' => $items,
                'subitems' => $subitems,
            ]);
    }



}