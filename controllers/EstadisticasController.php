<?php

namespace app\controllers;

use Yii;
use app\models\Equipos;
use app\models\EquiposSearch;
use app\models\Licencias;
use app\models\HistoricoEquipos;
use app\models\HistoricoLicencias;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use app\models\Areas;
use app\models\Sucursales;
use app\models\Soporteequipos;
use app\models\User;
use yii\filters\AccessControl;
use kartik\date\DatePicker;



class EstadisticasController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin','index','view','estadisticasequipos','estadisticaslicencias','estadisticascronogramas'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','view','estadisticasequipos','estadisticaslicencias','estadisticascronogramas'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index','view','estadisticasequipos','estadisticaslicencias','estadisticascronogramas'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario de recaudo
                          return User::isUserLinalca(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
             //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
             //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Equipos models.
     * @return mixed
     */
    public function actionIndex()
    {
       
    }

    /**
     * Displays a single Equipos model.
     * @param integer $id
     * @return mixed
     */
    public function actionEstadisticasequipos()
    {      
        // Declaración de las variables
        $equiposZona = null;
        $equiposSuc = null;
        $zona = null;
        $filtro = null;
        $Equipos_activos=null;
        $Equipos_inactivos=null;
        $Equipos_dado_bajas=null;       
        $total_equipos=null;
        $totalequipos=null;
        $totalequipsuc=null;
        if (Yii::$app->request->post()){  

           // Cargue de la variable filtro
            $filtro .= ((isset($_REQUEST['select-zona']) && $_REQUEST['select-zona'] != "" ))? 'and Zona_id = '.$_REQUEST['select-zona'] : null;
            $filtro .= ((isset($_REQUEST['select-sucursales']) && $_REQUEST['select-sucursales'] != "" ))? 'and sucursal_id = '.$_REQUEST['select-sucursales'] : null;
            $filtro .= ((isset($_REQUEST['marca-equipo']) && $_REQUEST['marca-equipo'] != ""))? ' and equipo_marca =' .$_REQUEST['marca-equipo'] : null;
            $filtro .= ((isset($_REQUEST['estado-equipo']) && $_REQUEST['estado-equipo'] != ""))? ' and equipo_estado_id =' .$_REQUEST['estado-equipo'] : null;
            $filtro .= ((isset($_REQUEST['area']) && $_REQUEST['area'] != ""))? ' and equipo_area_id =' .$_REQUEST['area'] : null;
            $filtro .= ((isset($_REQUEST['sistema_operativo']) && $_REQUEST['sistema_operativo'] != ""))? ' and equipo_systema_operativo_id =' .$_REQUEST['sistema_operativo'] : null;

          $query = new Query();
          $connection = yii::$app->db;
          // Consulta de los equipos por zona
          $query = $connection->createCommand("select Zona,
                                                        count(Equipos_activos) as Equipos_activos,
                                                        count(Equipos_inactivos) as Equipos_inactivos,
                                                        count(Equipos_dado_bajas) as Equipos_dado_bajas,                                                       
                                                        count(total_equipos) as total_equipos
                                                from (       
                                                        select   Zona_id as Zona,
                                                                    case when equipo_estado_id = 5 then equipo_id end as Equipos_activos,
                                                                    case when equipo_estado_id = 6 then equipo_id end as Equipos_inactivos,
                                                                    case when equipo_estado_id = 24 then equipo_id end as Equipos_dado_bajas,                                                                    
                                                                    count(equipo_id) as total_equipos
                                                                    
                                                        from    equipos
                                                                join sucursales on (equipos.equipo_sucursal_id=sucursal_id)
                                                        where   1 = 1 
                                                                $filtro
                                                        group by equipo_id                                                       
                                                        ) as d 
                                                group by Zona");
          $equiposZona = $query->queryAll(); 
          //Consulta de los equipos por sucursal
          $query = $connection->createCommand("select sucursal,
                                                        count(Equipos_activos) as Equipos_activos,
                                                        count(Equipos_inactivos) as Equipos_inactivos,
                                                        count(Equipos_dado_bajas) as Equipos_dado_bajas,                                                        
                                                        count(total_equipos) as total_equipos
                                                from (       
                                                        select   sucursal_nombre as sucursal,
                                                                    case when equipo_estado_id = 5 then equipo_id end as Equipos_activos,
                                                                    case when equipo_estado_id = 6 then equipo_id end as Equipos_inactivos,
                                                                    case when equipo_estado_id = 24 then equipo_id end as Equipos_dado_bajas,                                                                    
                                                                    count(equipo_id) as total_equipos
                                                                    
                                                        from    equipos
                                                                join sucursales on (equipos.equipo_sucursal_id=sucursal_id)
                                                        where   1 = 1 
                                                                $filtro
                                                        group by equipo_id                                                       
                                                        ) as d 
                                                group by sucursal");
                                                
          $equiposSuc = $query->queryAll();  
         
        // selección de columanas       
          $zonaN = array_column($equiposZona,'Zona');
          $equiAct = array_column($equiposZona,'Equipos_activos');
          $equiInact = array_column($equiposZona,'Equipos_inactivos');
          $equiDbaj = array_column($equiposZona,'Equipos_dado_bajas');
          $equiTequip = array_column($equiposZona,'total_equipos');
          $totEq = array_column($equiposZona,'total_equipos','Zona');
          $totEqSuc = array_column($equiposSuc,'total_equipos','sucursal');
          $totS = array_column($equiposSuc,'total_equipos');

         // Convertir los arreglos en formato json
          $totalequipos = json_encode($totEq);
          $totalequipos = str_replace('":',"',",$totalequipos);
          $totalequipos = str_replace('","',"],['",$totalequipos);
          $totalequipos = str_replace('{"',"['",$totalequipos);
          $totalequipos = str_replace('"}',"]",$totalequipos);
          $totalequipos = str_replace('"',"",$totalequipos);
          
          $totalequipsuc = json_encode($totEqSuc);
          $totalequipsuc = str_replace('":',"',",$totalequipsuc);
          $totalequipsuc = str_replace('","',"],['",$totalequipsuc);
          $totalequipsuc = str_replace('{"',"['",$totalequipsuc);
          $totalequipsuc = str_replace('"}',"]",$totalequipsuc);
          $totalequipsuc = str_replace('"',"",$totalequipsuc);

          $zona = json_encode($zonaN);
          $zona = str_replace("[","",$zona);
          $zona = str_replace("]","",$zona); 
          $zona = str_replace('"',"'",$zona); 

          $Equipos_activos = json_encode($equiAct);
          $Equipos_activos = str_replace("[","",$Equipos_activos);
          $Equipos_activos = str_replace("]","",$Equipos_activos); 
          $Equipos_activos = str_replace('"',"",$Equipos_activos);
          
          $Equipos_inactivos = json_encode($equiInact);
          $Equipos_inactivos = str_replace("[","",$Equipos_inactivos);
          $Equipos_inactivos = str_replace("]","",$Equipos_inactivos); 
          $Equipos_inactivos = str_replace('"',"",$Equipos_inactivos);

          $Equipos_dado_bajas = json_encode($equiDbaj);
          $Equipos_dado_bajas = str_replace("[","",$Equipos_dado_bajas);
          $Equipos_dado_bajas = str_replace("]","",$Equipos_dado_bajas); 
          $Equipos_dado_bajas = str_replace('"',"",$Equipos_dado_bajas);          

          $total_equipos = json_encode($equiTequip);
          $total_equipos = str_replace("[","",$total_equipos);
          $total_equipos = str_replace("]","",$total_equipos); 
          $total_equipos = str_replace('"',"",$total_equipos); 
          //Render a la vista estadisticasequipos
         return $this->render('estadisticasequipos',[
             'equiposZona' => $equiposZona,
             'equiposSuc' => $equiposSuc,
             'zona' => $zona,
             'Equipos_activos' => $Equipos_activos,
             'Equipos_inactivos' => $Equipos_inactivos,
             'Equipos_dado_bajas' => $Equipos_dado_bajas,            
             'total_equipos' => $total_equipos,
             'totalequipos' => $totalequipos,
             'totalequipsuc' => $totalequipsuc, 
             'filtro' => $filtro,            
         ]);

        }else {
            return $this->render('estadisticasequipos',[
                'equiposZona' => $equiposZona,
                'equiposSuc' => $equiposSuc,
                'zona' => $zona,
                'Equipos_activos' => $Equipos_activos,
                'Equipos_inactivos' => $Equipos_inactivos,
                'Equipos_dado_bajas' => $Equipos_dado_bajas,               
                'total_equipos' => $total_equipos,
                'totalequipos' => $totalequipos,
                'totalequipsuc' => $totalequipsuc, 
                'filtro' => $filtro,               
            ]);
        }  
        
           
    }

    public function actionEstadisticaslicencias(){       
        
        $filtro = null;
        $licenciasSuc = null;
        $licenciasZonas = null;
        $licenciasZonas = null;
        $licenciasSuc = null;
        $totalzonas = null;
        $totalsucur = null;
        $zonaL = null;
        $sucL = null;
        $asignadas = null;
        $Sin_asignar = null;
        $totalLic = null;
        
        if (Yii::$app->request->post()){
            // Cargue de la variable filtro
            $filtro .= ((isset($_REQUEST['select-zona']) && $_REQUEST['select-zona'] != "" ))? 'and Zona_id = '.$_REQUEST['select-zona'] : null;
            $filtro .= ((isset($_REQUEST['select-sucursales']) && $_REQUEST['select-sucursales'] != "" ))? 'and sucursal_id = '.$_REQUEST['select-sucursales'] : null;
            $filtro .= ((isset($_REQUEST['version']) && $_REQUEST['version'] != ""))? ' and licencia_version =' .$_REQUEST['version'] : null;
            $filtro .= ((isset($_REQUEST['tipo_licencia']) && $_REQUEST['tipo_licencia'] != ""))? ' and licencia_tipo_licencia_id in (select tipo_detalle_id from tipos_detalles where tipo_detalle_tipo_id in(' .$_REQUEST['tipo_licencia'].'))' : null;
            $filtro .= ((isset($_REQUEST['select-estado']) && $_REQUEST['select-estado'] != ""))? ' and licencia_estado_id =' .$_REQUEST['select-estado'] : null;  
            //  echo '<pre>';
            // print_r($filtro);
            // die();          
        // instacia clase query
        $query = new Query();
        $connection = yii::$app->db;
        //Consulta licencias por zona
       
        $query = $connection->createCommand("select Zona,
                                                    count(Sin_asignar) as Sin_asignar,
                                                    count(Asignado) as Asignado,
                                                    count(licencia_id) as Total
                                            from (       
                                                select Zona_id as Zona,licencia_id,
                                                                case when licencia_estado_id = 20 then licencia_id end as Sin_asignar,
                                                                case when licencia_estado_id = 19 then licencia_id end as Asignado
                                                        from   licencias
                                                                join sucursales on (licencia_sucursal_id=sucursal_id)
                                                        where  1 = 1 
                                                               $filtro  
                                                ) as f   
                                            group by Zona");
        $licenciasZonas = $query->queryAll();
        //Consulta licencias por sucursal
        $query = $connection->createCommand("select Sucursales,
                                                    count(Sin_asignar) as Sin_asignar,
                                                    count(Asignado) as Asignado,
                                                    count(licencia_id) as Total
                                            from (       
                                                select sucursal_nombre as Sucursales,licencia_id,
                                                                case when licencia_estado_id = 20 then licencia_id end as Sin_asignar,
                                                                case when licencia_estado_id = 19 then licencia_id end as Asignado
                                                        from   licencias
                                                                join sucursales on (licencia_sucursal_id=sucursal_id)
                                                        where  1 = 1
                                                               $filtro   
                                                ) as f   
                                            group by Sucursales");
        $licenciasSuc = $query->queryAll();

        // selección de columnas
        $zon = array_column($licenciasZonas,'Zona');
        $suc = array_column($licenciasSuc,'Sucursales');
        $asig = array_column($licenciasZonas,'Asignado');
        $sinAsing = array_column($licenciasZonas,'Sin_asignar');
        $tot = array_column($licenciasZonas,'Total');
        $totZona = array_column($licenciasZonas,'Total','Zona');
        $totSuc = array_column($licenciasSuc,'Total','Sucursales');

        // convertir los arreglos en formato json

        $totalzonas = json_encode($totZona);
        $totalzonas = str_replace('":',"',",$totalzonas);
        $totalzonas = str_replace('","',"],['",$totalzonas);
        $totalzonas = str_replace('{"',"['",$totalzonas);
        $totalzonas = str_replace('"}',"]",$totalzonas);
        $totalzonas = str_replace('"',"",$totalzonas);        

        $totalsucur = json_encode($totSuc);
        $totalsucur = str_replace('":',"',",$totalsucur);
        $totalsucur = str_replace('","',"],['",$totalsucur);
        $totalsucur = str_replace('{"',"['",$totalsucur);
        $totalsucur = str_replace('"}',"]",$totalsucur);
        $totalsucur = str_replace('"',"",$totalsucur);

        $zonaL = json_encode($zon);
        $zonaL = str_replace("[","",$zonaL);
        $zonaL = str_replace("]","",$zonaL); 
        $zonaL = str_replace('"',"'",$zonaL);

        $sucL = json_encode($suc);
        $sucL = str_replace("[","",$sucL);
        $sucL = str_replace("]","",$sucL); 
        $sucL = str_replace('"',"'",$sucL);

        $asignadas = json_encode($asig);
        $asignadas = str_replace("[","",$asignadas);
        $asignadas = str_replace("]","",$asignadas); 
        $asignadas = str_replace('"',"",$asignadas);       

        $Sin_asignar = json_encode($sinAsing);
        $Sin_asignar = str_replace("[","",$Sin_asignar);
        $Sin_asignar = str_replace("]","",$Sin_asignar); 
        $Sin_asignar = str_replace('"',"",$Sin_asignar);

        $totalLic = json_encode($tot);
        $totalLic = str_replace("[","",$totalLic);
        $totalLic = str_replace("]","",$totalLic); 
        $totalLic = str_replace('"',"",$totalLic);
        
          return $this->render('estadisticaslicencias',[
                  'licenciasZonas' => $licenciasZonas,
                  'licenciasSuc' => $licenciasSuc,
                  'totalzonas' => $totalzonas,
                  'totalsucur' => $totalsucur,
                  'zonaL' => $zonaL,
                  'sucL' => $sucL,
                  'asignadas' => $asignadas,
                  'Sin_asignar' => $Sin_asignar,
                  'totalLic' => $totalLic,
                  'filtro' => $filtro,
          ]);
        }else{
            return $this->render('estadisticaslicencias',[
                    'licenciasZonas' => $licenciasZonas,
                    'licenciasSuc' => $licenciasSuc,
                    'totalzonas' => $totalzonas,
                    'totalsucur' => $totalsucur,
                    'zonaL' => $zonaL,
                    'sucL' => $sucL,
                    'asignadas' => $asignadas,
                    'Sin_asignar' => $Sin_asignar,
                    'totalLic' => $totalLic,
                    'filtro' => $filtro,
            ]);
        }

    }

    public function actionEstadisticascronogramas(){

        // echo '<pre>';
        // print_r($_REQUEST);
        // die();
        
        $filtro = null;
        $cronogramaZonas = null;
        $cronogramaSucursales = null;
        $cronogramaz = null;
        $cronogramasuc = null;
        $zonaC = null;
        $preventivo = null;
        $correctivo = null;
        $realizado = null;
        $pendiente = null;

        if(yii::$app->request->post()){
            //cargar la variable filtro
            $filtro .= ((isset($_REQUEST['select-zona']) && $_REQUEST['select-zona'] != "" ))? 'and Zona_id = '.$_REQUEST['select-zona'] : null;
            $filtro .= ((isset($_REQUEST['select-sucursales']) && $_REQUEST['select-sucursales'] != "" ))? ' and sucursal_id = '.$_REQUEST['select-sucursales'] : null;
            $filtro .= ((isset($_REQUEST['select-mantenimiento']) && $_REQUEST['select-mantenimiento'] != ""))? ' and cronograma_tipo_mantenimiento =' .$_REQUEST['select-mantenimiento'] : null;
            $filtro .= ((isset($_REQUEST['estado']) && $_REQUEST['estado'] != ""))? ' and cronograma_detalle_realizado =' .$_REQUEST['estado'] : null;
            $filtro .= ((isset($_REQUEST['aprobado']) && $_REQUEST['aprobado'] != ""))? ' and cronograma_detalle_aprobado =' .$_REQUEST['aprobado'] : null; 
            $filtro .= ((isset($_REQUEST['tecnico']) && $_REQUEST['tecnico'] != ""))? ' and cronograma_quien_realiza =' .$_REQUEST['tecnico'] : null;   
           // se instancia la clase query
            $query = new Query();
            $connection = yii::$app->db;
           // Consultar            
            $query = $connection->createCommand("select zona,
                                                            count(distinct(idCronograma)) as Cronogramas,
                                                            count(distinct(cronograma_detalle_id)) as Mantenimientos,
                                                            sum(Preventivo) as Preventivos,
                                                            sum(Correctivo) as Correctivos,
                                                            sum(aprobado) as aprobado,
                                                            sum(sin_aprobar) as sin_aprobar,
                                                            sum(realizado) as realizado,
                                                            sum(Pendiente) as Pendiente
                                                    from (
                                                                    select Zona_id as zona,sucursal_nombre as Sucursal,equipo_nombre_actual,cronograma_fecha_inicio,cronograma_fecha_fin,cronograma_detalle_cronograma_id as idCronograma,
                                                                            (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_realiza,
                                                                            case when cronograma_tipo_mantenimiento = 21 then 1 else '' end as Preventivo,
                                                                            case when cronograma_tipo_mantenimiento = 22 then 1 else '' end as Correctivo,
                                                                            case when cronograma_realizado = 1 then 'REALIZADO' else 'PENDIENTE'  end as Estado,
                                                                            cronograma_detalle_fecha as Fecha_de_mantenimiento,cronograma_detalle_id,
                                                                            case when cronograma_detalle_aprobado = 1 then 1 else '' end as aprobado,
                                                                            case when cronograma_detalle_aprobado = 0 then 1 else '' end as sin_aprobar,
                                                                            case when cronograma_detalle_realizado = 1 then 1 else '' end as realizado,
                                                                            case when cronograma_detalle_realizado = 0 then 1 else '' end as Pendiente,
                                                                            (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_aprueba						 						 
                                                                    from   equipos
                                                                            join cronogramas_detalles on (equipo_id=cronograma_detalle_equipo_id)
                                                                            join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                            join cronogramas on (cronograma_detalle_cronograma_id=cronograma_id)
                                                                    where 1=1
                                                                           $filtro
                                                                    order by Zona_id,sucursal_nombre
                                                                ) as d     
                                                    group by zona");
            $cronogramaZonas = $query->queryAll(); 
            $query = $connection->createCommand("select Sucursal,
                                                        count(distinct(idCronograma)) as Cronogramas,
                                                        count(distinct(cronograma_detalle_id)) as Mantenimientos,
                                                        sum(Preventivo) as Preventivos,
                                                        sum(Correctivo) as Correctivos,
                                                        sum(aprobado) as aprobado,
                                                        sum(sin_aprobar) as sin_aprobar,
                                                        sum(realizado) as realizado,
                                                        sum(Pendiente) as Pendiente
                                                from (
                                                                select sucursal_nombre as Sucursal,cronograma_detalle_cronograma_id as idCronograma,
                                                                        (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_realiza,
                                                                        case when cronograma_tipo_mantenimiento = 21 then 1 else '' end as Preventivo,
                                                                        case when cronograma_tipo_mantenimiento = 22 then 1 else '' end as Correctivo,
                                                                        case when cronograma_realizado = 1 then 'REALIZADO' else 'PENDIENTE'  end as Estado,
                                                                        cronograma_detalle_fecha as Fecha_de_mantenimiento,cronograma_detalle_id,
                                                                        case when cronograma_detalle_aprobado = 1 then 1 else '' end as aprobado,
                                                                        case when cronograma_detalle_aprobado = 0 then 1 else '' end as sin_aprobar,
                                                                        case when cronograma_detalle_realizado = 1 then 1 else '' end as realizado,
                                                                        case when cronograma_detalle_realizado = 0 then 1 else '' end as Pendiente,
                                                                        (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_aprueba						 						 
                                                                from   equipos
                                                                        join cronogramas_detalles on (equipo_id=cronograma_detalle_equipo_id)
                                                                        join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                        join cronogramas on (cronograma_detalle_cronograma_id=cronograma_id)
                                                                where 1=1
                                                                    $filtro
                                                                order by Zona_id,sucursal_nombre
                                                            ) as d     
                                                group by Sucursal"); 
            $cronogramaSucursales = $query->queryAll();                          
            
            // selección de columnas
            $zona1 = array_column($cronogramaZonas,'zona');
            $suc1 = array_column($cronogramaSucursales,'Sucursal');
            $prevent = array_column($cronogramaZonas,'Preventivos');
            $correct = array_column($cronogramaZonas,'Correctivos');
            $realiz = array_column($cronogramaZonas,'realizado');
            $pendient = array_column($cronogramaZonas,'Pendiente');
            $cronoZona = array_column($cronogramaZonas,'Mantenimientos','zona');
            $cronoSuc = array_column($cronogramaSucursales,'Mantenimientos','Sucursal');

            // convertir los arreglos en formato json

            $cronogramaz = json_encode($cronoZona);
            $cronogramaz = str_replace('":',"',",$cronogramaz);
            $cronogramaz = str_replace('","',"],['",$cronogramaz);
            $cronogramaz = str_replace('{"',"['",$cronogramaz);
            $cronogramaz = str_replace('"}',"]",$cronogramaz);
            $cronogramaz = str_replace('"',"",$cronogramaz);  
            
            $cronogramasuc = json_encode($cronoSuc);
            $cronogramasuc = str_replace('":',"',",$cronogramasuc);
            $cronogramasuc = str_replace('","',"],['",$cronogramasuc);
            $cronogramasuc = str_replace('{"',"['",$cronogramasuc);
            $cronogramasuc = str_replace('"}',"]",$cronogramasuc);
            $cronogramasuc = str_replace('"',"",$cronogramasuc);  

            $zonaC = json_encode($zona1);
            $zonaC = str_replace("[","",$zonaC);
            $zonaC = str_replace("]","",$zonaC); 
            $zonaC = str_replace('"',"'",$zonaC);            
              
            $sucuC = json_encode($suc1);
            $sucuC = str_replace("[","",$sucuC);
            $sucuC = str_replace("]","",$sucuC); 
            $sucuC = str_replace('"',"'",$sucuC);

            $preventivo = json_encode($prevent);
            $preventivo = str_replace("[","",$preventivo);
            $preventivo = str_replace("]","",$preventivo); 
            $preventivo = str_replace('"',"",$preventivo);

            $correctivo = json_encode($correct);
            $correctivo = str_replace("[","",$correctivo);
            $correctivo = str_replace("]","",$correctivo); 
            $correctivo = str_replace('"',"",$correctivo);

            $realizado = json_encode($realiz);
            $realizado = str_replace("[","",$realizado);
            $realizado = str_replace("]","",$realizado); 
            $realizado = str_replace('"',"",$realizado);

            $pendiente = json_encode($pendient);
            $pendiente = str_replace("[","",$pendiente);
            $pendiente = str_replace("]","",$pendiente); 
            $pendiente = str_replace('"',"",$pendiente);

            return $this->render('estadisticascronogramas',[
                'filtro' => $filtro,
                'cronogramaZonas' => $cronogramaZonas,
                'cronogramaSucursales' => $cronogramaSucursales,
                'cronogramaz' => $cronogramaz,
                'cronogramasuc' => $cronogramasuc,
                'zonaC' => $zonaC,
                'sucuC' => $sucuC,
                'preventivo' => $preventivo,
                'correctivo' => $correctivo,
                'realizado' => $realizado,
                'pendiente' => $pendiente,
            ]);
        }else{
            return $this->render('estadisticascronogramas',[
                'filtro' => $filtro,
                'cronogramaZonas' => $cronogramaZonas,
                'cronogramaSucursales' => $cronogramaSucursales,
                'cronogramaz' => $cronogramaz,
                'cronogramasuc' => $cronogramasuc,
                'zonaC' => $zonaC,
                'preventivo' => $preventivo,
                'realizado' => $realizado,
                'pendiente' => $pendiente,
                'correctivo' => $correctivo,
            ]);
        }

    }
   // action para la muestra de las sucursales dependiendo de la zona 
    public function actionLists($id){
     // instancia de la clase query
        $query = new Query();
        $connection = yii::$app->db;

        $query = $connection->createCommand("SELECT sucursal_id,sucursal_nombre
                                             FROM   sucursales
                                             where  Zona_id = $id");

        $sucursales = $query->queryAll();                                             

        
        if($sucursales){
            $html = '';
            $html .= "<option value=''>[--Seleccione Sucursal --]</option>";
            foreach ($sucursales as $s) {
                $html .= "<option value='".$s['sucursal_id']."'>".$s['sucursal_nombre']."</option>";
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            Yii::$app->response->data  =  $html;
        }else{
            $html = '';
            $html .= "<option value=''>[-- Seleccione Sucursales --]</option>";
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            Yii::$app->response->data  =  $html;
        }                                  
    }
    public function actionEstadisticassoportes(){

            $filtro = null;        
             $soportezonas = null;
             $soportesucursales = null;
             $zonaS = null;
             $soportes = null;
             $totalsoportes = null;
             $zonaE = null;
             $tezonas = null;
             $equipos = null;
             $soportequipos = null;
             $sucursales = null;
             $totalsucursales = null;
        // $soporte = null;

        if (Yii::$app->request->post()){ 
            $filtro .= ((isset($_REQUEST['select-zona']) && $_REQUEST['select-zona'] != "" ))? 'and Zona_id = '.$_REQUEST['select-zona'] : null;
            $filtro .= ((isset($_REQUEST['select-sucursales']) && $_REQUEST['select-sucursales'] != "" ))? ' and sucursal_id = '.$_REQUEST['select-sucursales'] : null;
            $filtro .= ((isset($_REQUEST['select-soporte']) && $_REQUEST['select-soporte'] != ""))? ' and tiposoporte_id =' .$_REQUEST['select-soporte'] : null;       

            $query = new Query;
            $connection = Yii::$app->db;

            $query = $connection->createCommand("select ZONA,
                                                            sum(EQUIPO_NO_ENCIENDE) as EQUIPO_NO_ENCIENDE,
                                                            sum(NO_ARRANCA_SISTEMA_OPERATIVO) as NO_ARRANCA_SISTEMA_OPERATIVO,
                                                            sum(PROBLEMAS_DE_CONEXIÓN) as PROBLEMAS_DE_CONEXIÓN,
                                                            sum(EQUIPO_SIN_CARPETA_DE_RED) as EQUIPO_SIN_CARPETA_DE_RED,
                                                            sum(NO_HAY_CONEXIÓN_CON_IMPRESORA) as NO_HAY_CONEXIÓN_CON_IMPRESORA,
                                                            sum(ERROR_CON_EL_OFFICCE) as ERROR_CON_EL_OFFICCE,
                                                            sum(CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN) as CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN,
                                                            sum(EQUIPO_LENTO) as EQUIPO_LENTO
                                                    from (       
                                                            select ZONA,
                                                                    case when tiposoporte_id = 106 then 1 else 0 end as EQUIPO_NO_ENCIENDE, 
                                                                    case when tiposoporte_id = 107 then 1 else 0 end as NO_ARRANCA_SISTEMA_OPERATIVO,
                                                                    case when tiposoporte_id = 108 then 1 else 0 end as PROBLEMAS_DE_CONEXIÓN,
                                                                    case when tiposoporte_id = 109 then 1 else 0 end as EQUIPO_SIN_CARPETA_DE_RED,
                                                                    case when tiposoporte_id = 110 then 1 else 0 end as NO_HAY_CONEXIÓN_CON_IMPRESORA,
                                                                    case when tiposoporte_id = 111 then 1 else 0 end as ERROR_CON_EL_OFFICCE,
                                                                    case when tiposoporte_id = 112 then 1 else 0 end as CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN,
                                                                    case when tiposoporte_id = 113 then 1 else 0 end as EQUIPO_LENTO
                                                            from (       
                                                                    select Zona_id as ZONA,tipo_detalle_nombre as Soporte,tiposoporte_id
                                                                    from   soporte_equipos
                                                                            join equipos on (equipos_idequipo = equipo_id)
                                                                            join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                            join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                                    where 1 = 1
                                                                          $filtro                                                                          
                                                                ) as d
                                                        ) as f 
                                                    group by ZONA ");

            $soportezonas = $query->queryAll();

            $query = $connection->createCommand("select SUCURSALES,
                                                            sum(EQUIPO_NO_ENCIENDE) as EQUIPO_NO_ENCIENDE,
                                                            sum(NO_ARRANCA_SISTEMA_OPERATIVO) as NO_ARRANCA_SISTEMA_OPERATIVO,
                                                            sum(PROBLEMAS_DE_CONEXIÓN) as PROBLEMAS_DE_CONEXIÓN,
                                                            sum(EQUIPO_SIN_CARPETA_DE_RED) as EQUIPO_SIN_CARPETA_DE_RED,
                                                            sum(NO_HAY_CONEXIÓN_CON_IMPRESORA) as NO_HAY_CONEXIÓN_CON_IMPRESORA,
                                                            sum(ERROR_CON_EL_OFFICCE) as ERROR_CON_EL_OFFICCE,
                                                            sum(CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN) as CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN,
                                                            sum(EQUIPO_LENTO) as EQUIPO_LENTO
                                                    from (       
                                                            select SUCURSALES,
                                                                    case when tiposoporte_id = 106 then 1 else 0 end as EQUIPO_NO_ENCIENDE, 
                                                                    case when tiposoporte_id = 107 then 1 else 0 end as NO_ARRANCA_SISTEMA_OPERATIVO,
                                                                    case when tiposoporte_id = 108 then 1 else 0 end as PROBLEMAS_DE_CONEXIÓN,
                                                                    case when tiposoporte_id = 109 then 1 else 0 end as EQUIPO_SIN_CARPETA_DE_RED,
                                                                    case when tiposoporte_id = 110 then 1 else 0 end as NO_HAY_CONEXIÓN_CON_IMPRESORA,
                                                                    case when tiposoporte_id = 111 then 1 else 0 end as ERROR_CON_EL_OFFICCE,
                                                                    case when tiposoporte_id = 112 then 1 else 0 end as CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN,
                                                                    case when tiposoporte_id = 113 then 1 else 0 end as EQUIPO_LENTO
                                                            from (       
                                                                    select sucursal_nombre as SUCURSALES,tipo_detalle_nombre as Soporte,tiposoporte_id
                                                                    from   soporte_equipos
                                                                            join equipos on (equipos_idequipo = equipo_id)
                                                                            join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                            join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                                    where 1 = 1
                                                                          $filtro        
                                                                ) as d
                                                        ) as f 
                                                    group by SUCURSALES ");

            $soportesucursales = $query->queryAll();
            
            $query = $connection->createCommand("select Soporte,count(id) as Total
                                                    from (
                                                            select tipo_detalle_nombre as Soporte,tiposoporte_id as id
                                                            from   soporte_equipos
                                                                join equipos on (equipos_idequipo = equipo_id)
                                                                join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                            where tipo_detalle_tipo_id = 13
                                                                 $filtro
                                                        ) as d  
                                                    group by Soporte ");

            $estadisticasoporte = $query->queryAll(); 
            
            $query = $connection->createCommand("select Zona,count(id) as Total
                                                    from (
                                                            select Zona_id as Zona,tiposoporte_id as id
                                                            from   soporte_equipos
                                                                join equipos on (equipos_idequipo = equipo_id)
                                                                join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                            where tipo_detalle_tipo_id = 13
                                                                  $filtro
                                                        ) as d  
                                                    group by Zona");
            $estadzona = $query->queryAll();

            $query = $connection->createCommand("select Sucursales,count(id) as Total
                                                from (
                                                        select sucursal_nombre as Sucursales,tiposoporte_id as id
                                                        from   soporte_equipos
                                                            join equipos on (equipos_idequipo = equipo_id)
                                                            join sucursales on (equipo_sucursal_id=sucursal_id)
                                                            join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                        where tipo_detalle_tipo_id = 13
                                                             $filtro                                                                  
                                                    ) as d  
                                                group by Sucursales");
            $estasuc = $query->queryAll();
            
            $query = $connection->createCommand("select Equipo,count(id) as Total
                                                    from (
                                                            select equipo_identificacion_tecnologia as Equipo,tiposoporte_id as id
                                                            from   soporte_equipos
                                                                join equipos on (equipos_idequipo = equipo_id)
                                                                join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                join tipos_detalles on (tiposoporte_id = tipo_detalle_id)
                                                            where tipo_detalle_tipo_id = 13
                                                                  $filtro
                                                        ) as d  
                                                    group by Equipo 
                                                    order by Total desc limit 10 ");
            $top = $query->queryAll();                                                    
                                                                
            // echo '<pre>';
            // print_r($soporte);
            // die();
            $zona = array_column($soportezonas,'ZONA');
            $EQUIPO_NO_ENCIENDE = array_column($soportezonas,'EQUIPO_NO_ENCIENDE');
            $NO_ARRANCA_SISTEMA_OPERATIVO = array_column($soportezonas,'NO_ARRANCA_SISTEMA_OPERATIVO');
            $PROBLEMAS_DE_CONEXIÓN = array_column($soportezonas,'PROBLEMAS_DE_CONEXIÓN');
            $EQUIPO_SIN_CARPETA_DE_RED = array_column($soportezonas,'EQUIPO_SIN_CARPETA_DE_RED');
            $NO_HAY_CONEXIÓN_CON_IMPRESORA = array_column($soportezonas,'NO_HAY_CONEXIÓN_CON_IMPRESORA');
            $ERROR_CON_EL_OFFICCE = array_column($soportezonas,'ERROR_CON_EL_OFFICCE');
            $CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN = array_column($soportezonas,'CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN');
            $EQUIPO_LENTO = array_column($soportezonas,'EQUIPO_LENTO');
            $estsoporte = array_column($estadisticasoporte,'Soporte');            
            $totsoporte = array_column($estadisticasoporte,'Total');
            $zon = array_column($estadzona,'Zona');
            $tezona = array_column($estadzona,'Total');
            $equip = array_column($top,'Equipo');
            $totalseq = array_column($top,'Total');
            $sucurs = array_column($estasuc,'Sucursales');
            $totalsuc = array_column($estasuc,'Total');

            $zonaS = json_encode($zona);
            $zonaS = str_replace("[","",$zonaS);
            $zonaS = str_replace("]","",$zonaS); 
            $zonaS = str_replace('"',"'",$zonaS);

            $zonaE = json_encode($zon);
            $zonaE = str_replace("[","",$zonaE);
            $zonaE = str_replace("]","",$zonaE); 
            $zonaE = str_replace('"',"'",$zonaE);

            $sucursales = json_encode($sucurs);
            $sucursales = str_replace("[","",$sucursales);
            $sucursales = str_replace("]","",$sucursales); 
            $sucursales = str_replace('"',"'",$sucursales);

            $equipos = json_encode($equip);
            $equipos = str_replace("[","",$equipos);
            $equipos = str_replace("]","",$equipos); 
            $equipos = str_replace('"',"'",$equipos);
            
            $soportes = json_encode($estsoporte);
            $soportes = str_replace("[","",$soportes);
            $soportes = str_replace("]","",$soportes); 
            $soportes = str_replace('"',"'",$soportes);

            $tezonas = json_encode($tezona);
            $tezonas = str_replace("[","",$tezonas);
            $tezonas = str_replace("]","",$tezonas); 
            $tezonas = str_replace('"',"",$tezonas);
            
            $totalsoportes = json_encode($totsoporte);
            $totalsoportes = str_replace("[","",$totalsoportes);
            $totalsoportes = str_replace("]","",$totalsoportes); 
            $totalsoportes = str_replace('"',"",$totalsoportes);
            
            $soportequipos = json_encode($totalseq);
            $soportequipos = str_replace("[","",$soportequipos);
            $soportequipos = str_replace("]","",$soportequipos); 
            $soportequipos = str_replace('"',"",$soportequipos);

            $totalsucursales = json_encode($totalsuc);
            $totalsucursales = str_replace("[","",$totalsucursales);
            $totalsucursales = str_replace("]","",$totalsucursales); 
            $totalsucursales = str_replace('"',"",$totalsucursales);
                                         
            return $this->render('estadisticassoportes',[
                'filtro' => $filtro,        
                'soportezonas' => $soportezonas,
                'soportesucursales' => $soportesucursales,
                'zonaS' => $zonaS,
                'soportes' => $soportes,
                'totalsoportes' => $totalsoportes,
                'zonaE' => $zonaE,
                'tezonas' => $tezonas,
                'equipos' => $equipos,
                'soportequipos' => $soportequipos,
                'sucursales' => $sucursales,
                'totalsucursales' => $totalsucursales,                
            ]);

        }else {

            return $this->render('estadisticassoportes',[
                'filtro' => $filtro,        
                'soportezonas' => $soportezonas,
                'soportesucursales' => $soportesucursales,
                'zonaS' => $zonaS,
                'soportes' => $soportes,
                'totalsoportes' => $totalsoportes,
                'zonaE' => $zonaE,
                'tezonas' => $tezonas,
                'equipos' => $equipos,
                'soportequipos' => $soportequipos,
                'sucursales' => $sucursales,
                'totalsucursales' => $totalsucursales,
            ]);
        }
                                                           

    }
    

}
