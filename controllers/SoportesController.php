<?php

namespace app\controllers;
use app\models\Soporteequipos;

class SoportesController extends \yii\web\Controller
{
    public function actionIndex()
    
    {
        $model = Soporteequipos::find()->all();
        return $this->render('index',[
            'model' => $model,
        ]);
    }

}
