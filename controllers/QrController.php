<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use kartik\mpdf\Pdf;
use yii\helpers\Url;
use yii\data\Pagination;
use app\models\Equipos;
use app\models\User;
use yii\filters\AccessControl;
// use app\models\Maquinas;
// use app\models\MaquinasSearch;


class QrController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin','index','view'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','view'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index','view'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario de recaudo
                          return User::isUserLinalca(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
             //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
             //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientes models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new MaquinasSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = new Query();
        $connection = Yii::$app->db;

        if (Yii::$app->request->get() or  isset($_GET['page'])){

            // echo "<pre>";
            // print_r($_REQUEST);die();

            $totalCount = 0;
            $limit      = 10000000000;
            $from       = (isset($_GET['page'])) ? ($_GET['page']-1)*$limit : 0; // Match according to your query 

            // variable necesaria para los filtros del Query
            $filtro =null;
            // se realiza la validacion en caso de que venga el filtro de campo y valueCampo para ser concatenada a la variable 
            // filtro la cual sera utilizada en el query.
            if(isset($_REQUEST['campo']) && $_REQUEST['campo']!=""){   
                
                $campo = $_REQUEST['campo'];
                $valueCampo = $_REQUEST['valueCampo'];
                // validaciones para los filtros 
                if($campo=="razon"){ $filtro .= " and cliente_razon_social like '%$valueCampo%' "; }
                if($campo=="identificacion"){ $filtro .= " and cliente_identificacion like '%$valueCampo%'";}
                if($campo=="direccion"){ $filtro .= " and cliente_direccion like '%$valueCampo%'";}
                if($campo=="numeroMaquina"){ $filtro .= " and equipo_id like '%$valueCampo%'";}
                if($campo=="tipoMaquina"){ $filtro .= " and upper(tipo_detalle_nombre) like upper('%$valueCampo%')";}
            }
            // consulta que muestra la informacion en el index del modulo de Visita
            $sql = 'SELECT    	*	 
                        from    equipos 
                                join tipos_detalles on (equipos.equipo_tipo_equipo_id=tipo_detalle_id)
                                join funcionarios on (equipo_usuario_id = funcionario_id)
                        where   1=1
                                '.$filtro.'' ; 

            $command = $connection->createCommand($sql.' LIMIT '.$from.','.$limit);
            $count   = $connection->createCommand('SELECT COUNT(*) as total FROM ('.$sql.') a')->queryAll();
            $result         = $command->queryAll();
            $totalCount     =   $count[0]['total'];
            $pages = new Pagination(['totalCount' => $totalCount, 'pageSize' => $limit]);

            // echo "<pre>";
            // print_r($sql);die();

            if(!$result){ 
                // se renderiza a la Vista Index pasando parametros(Clientes, result)
                return $this->render('index', [
                    'result'=>0, 
                    'pages' =>0,
                ]);


            }else{
                // se renderiza a la Vista Index pasando parametros(Clientes, result)
                return $this->render('index', [
                    'result'=>$result, 
                    'pages'=>$pages, 
                ]);
            }
            
        }else{  
            // se renderiza a la Vista Index pasando parametros(Clientes, result)
            return $this->render('index', [ 
                'searchModel' => null,
                'dataProvider' => null,
                'pages'=>0,
                'result' => 0 ,
            ]);
        }
    }

    public function actionGeneracion(){

    	// echo "<pre>";
    	// print_r($_REQUEST);die();

    	$idMaquinas = implode(",", $_REQUEST['seleccion']);

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;    


        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *    
                                                from    equipos 
                                                        join tipos_detalles on (equipos.equipo_tipo_equipo_id=tipo_detalle_id)
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                        join sucursales on (equipos.equipo_sucursal_id=sucursales.sucursal_id)
                                                        join areas on (equipos.equipo_area_id = areas.area_id)
                                                        
                                                where   1=1
                                                        and equipo_id in ('.$idMaquinas.')');
        $result = $query->queryAll();

        // echo '<pre>';
        // print_r($result);die();
        // # ================= INICIO CONSTRUCCION DE PDF
        // # ================= INICIO PRIMER BLOQUE ==============
        $content = '';
        // ==== Codigo de negocio
        $content .= '<table id="tabla-result"  border=1 cellspacing=4 cellpadding=3 bordercolor="red">';
           foreach ($result as $detalleA) {
           		$content .= '<tr>';
	                $content .= '<td><img src="'.Url::base().'/QR/'.$detalleA['equipo_id'].'.png" width="160px"  height ="160px" /></td>';
                    $content .= '<td colspan="3" align="center">
                                    <p>Id: <class=""></No>'.$detalleA['equipo_id'].'</p>
                                    <p>Identificacion: <class=""></No>'.$detalleA['equipo_identificacion_tecnologia'].'</p>
                                    <p>Nombre: <class=""></No>'.$detalleA['equipo_nombre_actual'].'</p>
                                     <p>'.$detalleA['tipo_detalle_nombre'].'</p>
                                     <p>'.$detalleA['funcionario_nombres'].'</p>
                                     <p>'.$detalleA['sucursal_nombre'].'</p>
                                     <p>'.$detalleA['area_nombre'].'</p>
                                     <p>Activo: '.$detalleA['area_nombre'].'</p>
                                </td>';
	            $content .= '</tr><br>';
            }    
            $content .= '</table>';
          /*<?php foreach ($result as $key ) { ?>
        <?php } ?>
        </table>';*/
        // # ================= FIN SEGUNDO BLOQUE ==============
        // # ================= FIN CONSTRUCCION DE PDF
        $pdf = new Pdf([
            'marginTop' => 20,
            'marginLeft' => 20,
            'marginRight' => 20,
            'marginBottom' => 20,
            'format' => Pdf::FORMAT_A4,
            'defaultFont' => 'courier'
        ]);
        $mpdf = $pdf->api; // fetches mpdf api
        $mpdf->WriteHtml($content); // call mpdf write html
        echo $mpdf->Output('Generacion_codigos_QR_'.date('Y-m-d').'.pdf', 'D'); // call the mpdf api output as needed
    }
}
