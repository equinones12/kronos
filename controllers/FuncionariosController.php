<?php

namespace app\controllers;

use Yii;
use app\models\Funcionarios;
use app\models\FuncionariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\Equipos;
use yii\filters\AccessControl;
use app\models\User;
/**
 * FuncionariosController implements the CRUD actions for Funcionarios model.
 */
class FuncionariosController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin','index','view','create','update'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','view','create','update'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index','view','create','update'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario de recaudo
                          return User::isUserLinalca(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
             //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
             //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Funcionarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FuncionariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Funcionarios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Funcionarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Funcionarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->funcionario_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Funcionarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->funcionario_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Funcionarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionLists($id){


        $countFuncionarios = Equipos::find()->where(['equipo_sucursal_id'=>$id])->count();
         // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *
                                                from    equipos 
                                                        left join funcionarios on (equipo_usuario_id = funcionario_id)
                                                where   1=1
                                                        and equipo_sucursal_id ='.$id
                                                );
        $result = $query->queryAll();
        
        // echo "<pre>";
        // print_r($result);

        if($countFuncionarios > 0){
            
            echo "<option>[--Seleccione Funcionario--]</option>";
            foreach ($result as $funcionario => $value) {

                echo "<option value='".$value['equipo_id']."'>".$value['funcionario_nombres']." - ".$value['equipo_identificacion_tecnologia']."</option>";
            }

        }else{

            echo "<option>No hay Funcionarios registrados para esta sucursal</option>";
        }        
    }
    /**
     * Finds the Funcionarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Funcionarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Funcionarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
