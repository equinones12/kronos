<?php
 
namespace app\controllers;

use Yii;
use app\models\Equipos;
use app\models\EquiposSearch;
use app\models\Licencias;
use app\models\HistoricoEquipos;
use app\models\HistoricoLicencias;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use app\models\Areas;
use app\models\Sucursales;
use app\models\User;
use app\models\Users;
use app\models\Soporteequipos;
use app\models\Cronogramas;
use yii\filters\AccessControl;
use kartik\date\DatePicker;


/**
 * EquiposController implements the CRUD actions for Equipos model.
 */


require(__DIR__ . '/../vendor/phpqrcode/qrlib.php');


class EquiposController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

      public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['logout', 'user', 'admin','index','view','create','update'],
              'rules' => [
                  [
                      //El administrador tiene permisos sobre las siguientes acciones
                      'actions' => ['logout', 'admin','index','view','create','update'],
                      //Esta propiedad establece que tiene permisos
                      'allow' => true,
                      //Usuarios autenticados, el signo ? es para invitados
                      'roles' => ['@'],
                      //Este método nos permite crear un filtro sobre la identidad del usuario
                      //y así establecer si tiene permisos o no
                      'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un administrador
                          return User::isUserAdmin(Yii::$app->user->identity->id);
                      },
                  ],
                  [
                     //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                     'actions' => ['logout', 'user','index','view','create','update'],
                     //Esta propiedad establece que tiene permisos
                     'allow' => true,
                     //Usuarios autenticados, el signo ? es para invitados
                     'roles' => ['@'],
                     //Este método nos permite crear un filtro sobre la identidad del usuario
                     //y así establecer si tiene permisos o no
                     'matchCallback' => function ($rule, $action) {
                        //Llamada al método que comprueba si es un usuario de recaudo
                        return User::isUserLinalca(Yii::$app->user->identity->id);
                    },
                  ],
              ],
          ],
           //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
           //sólo se puede acceder a través del método post
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['post'],
              ],
          ],
      ];
  }

    /**
     * Lists all Equipos models.
     * @return mixed
         */
    public function actionIndex()
    { 
        $model = Equipos::find()->All();
        $searchModel = new EquiposSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Equipos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_tipo_equipo_id) as TipoE,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_systema_operativo_id) as sistemaO,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_office_id) as office,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_estado_id) as estado,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_procesador) as procesador,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_ram) as ram,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_capacidad_disco_duro) as capacidad_disk_duro,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_marca) as equipo_marca,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = equipo_tipo_memoria_ram) as tipo_memoria_ram
                                                from    equipos 
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                        join sucursales on (equipo_sucursal_id = sucursal_id)
                                                        join areas on (equipo_area_id = area_id)
                                                where   1=1
                                                        and equipo_id ='.$id
                                                );
        $result = $query->queryAll();


        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = licencia_tipo_licencia_id) as producto, 
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=licencia_version) as licencia_versiones,
                                                        (select     tipo_nombre 
                                                            from    tipos_detalles 
                                                                    join tipos on (tipo_detalle_tipo_id = tipo_id)
                                                            where   tipo_detalle_id = licencia_tipo_licencia_id) as tipo
                                                from    licencias                                                         
                                                where   1=1
                                                        and licencia_equipo_id ='.$id
                                                );
        $licencias = $query->queryAll();        

        $query = $connection->createCommand("SELECT     *,
                                                        (select CONCAT(nombres,' ',apellidos) from users where id = cronograma_quien_realiza ) as quienRealiza,
                                                        case when cronograma_detalle_realizado = 0 then 'Pendiente'
                                                             when cronograma_detalle_realizado = 1 then 'Realizado' end as estado, 
                                                        case when cronograma_detalle_aprobado = 2 then 'NO  por: '
                                                             when cronograma_detalle_aprobado = 1 then 'SI  por: ' end as aprobado, 
                                                        (select  funcionario_nombres from funcionarios where funcionario_id = cronograma_detalle_quien_aprueba) as quienAprueba

                                                from    cronogramas_detalles 
                                                        join cronogramas on (cronograma_detalle_cronograma_id = cronograma_id)                                                        
                                                where   1=1
                                                         
                                                        -- and cronograma_detalle_realizado = 0
                                                        and cronograma_detalle_equipo_id =".$id
                                                );
        $cronogramas = $query->queryAll();

        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = licencia_tipo_licencia_id) as producto, 
                                                        (select     tipo_nombre 
                                                            from    tipos_detalles 
                                                                    join tipos on (tipo_detalle_tipo_id = tipo_id)
                                                            where   tipo_detalle_id = licencia_tipo_licencia_id) as tipo
                                                from    historico_licencias                                                         
                                                where   1=1
                                                        and licencia_equipo_id ='.$id
                                                );
        $historico = $query->queryAll(); 
        $historicoequipos = HistoricoEquipos::find()->where(['historico_equipo_id'=>$id])->all();
        $historicoli = Historicolicencias::find()->where(['licencia_equipo_id'=>$id])->all();
        $soporte = Soporteequipos::find()->where(['equipos_idequipo'=>$id])->All();


        //  echo "<pre>";
        //  print_r($historicoequipos);die();

        $model_equipos= new Equipos();
        return $this->render('view', [
            'model' => $result,
            'licencias' => $licencias,
            'cronogramas' => $cronogramas,
            'historico' => $historico,
            'historicoequipos' => $historicoequipos,
            'historicoli' => $historicoli,
            'model_equipos' => $model_equipos,
            'soporte' => $soporte,
        ]);     
    }

    /**
     * Creates a new Equipos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()    
    {
        //  echo "<pre>";
        //  print_r($_REQUEST['fecha']);die();        
        $model = new Equipos();
        if(yii::$app->request->post()){
            $fecha = $_REQUEST['fecha'];
            $model->equipo_fecha_ingreso = $_REQUEST['fecha'];
        }        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->equipo_id]);
        } else {
            return $this->render('create', [
                'model' => $model,                
            ]);
        }
    }
    public function actionActualizar($id){
                    
        $obser = $_REQUEST['Equipos']['equipo_observaciones'];               
        $usua = Yii::$app->user->identity->id;
        $dia1 = date('Y-m-d H:i:s');
        $equipos = Equipos::find()->where(['equipo_id'=>$id])->all();                                    
        $model = $this->findModel($id); 

        foreach($equipos as $equi) { 
                 
        }         
            // Query para insertar historico de equipos  
            $query = new Query();
            $connection = Yii::$app->db;                                                      
            $query = $connection->createCommand()->insert('historico_equipos',
                                                    ['historico_equipo_id' => $equi['equipo_id'],
                                                    'equipo_identificacion_tecnologia' =>$equi['equipo_identificacion_tecnologia'],
                                                    // 'equipo_nombre_actual' =>$equi['equipo_nombre_actual'],
                                                    'equipo_tipo_equipo_id' =>$equi['equipo_tipo_equipo_id'],
                                                    'equipo_marca' =>$equi['equipo_marca'],
                                                    'equipo_modelo' =>$equi['equipo_modelo'],
                                                    'equipo_ram' =>$equi['equipo_ram'],
                                                    'equipo_tipo_memoria_ram'=>$equi['equipo_tipo_memoria_ram'],
                                                    'equipo_marca_disco_duro' =>$equi['equipo_marca_disco_duro'],
                                                    'equipo_capacidad_disco_duro' =>$equi['equipo_capacidad_disco_duro'],
                                                    'equipo_procesador' =>$equi['equipo_procesador'],
                                                    'equipo_velocidad_procesador' =>$equi['equipo_velocidad_procesador'],
                                                    'equipo_cantidad_nucleos_procesador' =>$equi['equipo_cantidad_nucleos_procesador'],
                                                    'equipo_systema_operativo_id' =>$equi['equipo_systema_operativo_id'],
                                                    'equipo_office_id' =>$equi['equipo_office_id'],
                                                    'equipo_pantalla' =>$equi['equipo_pantalla'],
                                                    'equipo_sucursal_id' =>$equi['equipo_sucursal_id'],
                                                    'equipo_area_id' =>$equi['equipo_area_id'],
                                                    'equipo_usuario_id' =>$equi['equipo_usuario_id'],
                                                    'equipo_estado_id' =>$equi['equipo_estado_id'],
                                                    'equipo_numero_activo_contabilidad' =>$equi['equipo_numero_activo_contabilidad'],
                                                    'equipo_fecha_ingreso' =>$equi['equipo_fecha_ingreso'],
                                                    'equipo_valor' =>$equi['equipo_valor'],
                                                    'equipo_observaciones' =>$equi['equipo_observaciones'],
                                                    'equipo_ip' =>$equi['equipo_ip'],
                                                    'usuario_modifica' =>$usua,
                                                    'fecha_creacion' =>$dia1,
                                                    'proceso_id' =>26, 
                                                    'observacion' => $obser                                                    
                                                    ])->execute();                                                     
            $historico = $query;                                                       
        if ($model->load($_REQUEST) && $model->save()) {                                    
  
                return $this->redirect(['view', 'id' => $model->equipo_id]);
        } else {            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Equipos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {                                                      
        $model = $this->findModel($id);                                         
       
            return $this->render('update', [
                'model' => $model,
            ]);
       
    }

    /**
     * Deletes an existing Equipos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionLists($id){        
        
        $validate = 0;        
     
        /*$countEquipos = Equipos::find()
                            ->where(['equipo_sucursal_id'=>$id])->count();

        $equipos = Equipos::find()
                            ->where(['equipo_sucursal_id'=>$id])
                            ->Andwhere('')
                            ->all();
                            */
        $query = new Query();
        $connection = Yii::$app->db;

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *
                                                from    equipos 
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                        join sucursales on (equipo_sucursal_id = sucursal_id)
                                                        join areas on (equipo_area_id = area_id)
                                                where   1=1
                                                        and equipo_id not in (select cronograma_detalle_equipo_id
                                                                                from cronogramas 
                                                                                     join cronogramas_detalles on (cronograma_id = cronograma_detalle_cronograma_id)
                                                                                where cronograma_realizado = 0)
                                                        and equipo_sucursal_id ='.$id.'
                                                order by area_id asc'
                                                );
        $equipos = $query->queryAll();
        $countEquipos = count($equipos);

        if($countEquipos > 0){                  
            $datos = array();                  
            foreach ($equipos as $equipo) {
                                                
                $data = '';
                $data .= "<div class='col-md-3 mb-3'>";
                $data .= "<div class='card alert alert-primary mb-3' style='max-width: 18rem; border-color: #007BFF';>";
                $data .=  "<div class='card-header alert alert-primary'>".$equipo['equipo_identificacion_tecnologia']." - ".$equipo['area_nombre']."</div>";
                $data .=   "<div class='card-body text-primary'>";
                $data .=   "<input type='date' class='form-control' name=fecha[".$equipo['equipo_id']."]>";
                $data .=    "Hora Inicio<input type='time' name='horaI[".$equipo['equipo_id']."]' value='08:00' max='17:00' min='08:00:00' step='1' class='form-control'>";
                $data .=     "Hora Fin<input type='time' name='horaF[".$equipo['equipo_id']."]' value='08:00' max='17:00' min='08:00' step='1' class='form-control'>";
                $data .=     "</div>";
                $data .=    "</div>";             
                $data .=  "</div>";
                $datos[] = $data;              
            } 
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            Yii::$app->response->data  =  $datos;
                                        
                        
        }else{
            $data = '';
            $data = "<div class='col-md-12 mb-3'>";
            $data .=  "<br><br><div class='alert alert-danger' role='alert'>
                        NO SE ENCONTRARON MAQUINAS PARA LA SUCURSAL SELECCIONADA
                    </div>";
            $data .= "</div>"; 
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
            Yii::$app->response->data  =  $data;       
            // print_r($data);         
        }                       
    }


    /**
     * Finds the Equipos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Equipos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Equipos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionActualizaqr()
    {

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;


        // echo "LLEGA AL ACTUALIZA LOS CODIGOS QR";
        // consulta de la Visita
        $query = $connection->createCommand('SELECT *
                                            from    equipos 
                                            where   1=1');
        $detalle = $query->queryAll();

        // echo "<pre>";
        // print_r($detalle);die();
        $dir = 'QR/';
        if(!file_exists($dir))
            mkdir($dir);
        
        $tamanio = 5;
        $level = 'H';
        $frameSize = 1;
        // $contenido = 'http://www.codigosdeprogramacion.com';
            // echo '<img src="'.$filename.'" />';  
        foreach ($detalle as $key => $value) {
            
            $filename = $dir.$value['equipo_id'].'.png';    
            $contenido = 'localhost/kronos/web/index.php?r=equipos%2Fview&id='.$value['equipo_id'];
            \QRcode::png($contenido, $filename, $level, $tamanio, $frameSize);
                        

        }
    }

    public function actionRedes(){
     
     $model=equipos::findOne(1);
     $ip=$model->equipo_ip;    

     // echo '<pre>';
     // print_r($ip); die();
        return $this->render('redes', [
            'model' => $this->findModel(1),
            'ip'=>$ip,
        ]);
    }
    public function actionIp($id){
        // echo '<pre>';
        // print_r($id); die();
        $model=equipos::findOne($id);
        $ip=$model->equipo_ip;
        $html = '';
        $html .= '<text>'.$ip.'</text>';
        echo $html;
    
        
    }

    public function actionEstado($id){
        $model=equipos::findOne($id);               

            $ll=shell_exec('ping '.$model->equipo_ip);
            // echo '<pre>'; print_r($ll);            

        $html='';
        $html .= '<div class="card border-primary mb-3">';
        $html .= '<div class="card-header text-white" style="background-color: #337AB7;"><b>Respuesta de ping</b></div>';
        $html .= '<div class="card-body">';
        echo $html;
        // $html.='<text>'.($ll).'<text>';
        echo '<pre>';
        print_r($ll);            
    }

    public function actionDardebaja(){  
        // echo '<pre>';
        // print_r($_REQUEST);
        // die();
        $id = $_REQUEST['id']; 
        $model=equipos::findOne($id);                         
        $obs = $_REQUEST['observacion'];               
        $idlic = Licencias::find()->where(['licencia_equipo_id' =>$id])->all(); 
        $usuario = Yii::$app->user->identity->id;
        $dia = date('Y-m-d H:i:s');
        $model->equipo_baja_activo=UploadedFile::getInstance($model,'equipo_baja_activo');                                             

           $query = new Query();
           $connection = Yii::$app->db;
           $transaction = $connection->beginTransaction(); 
           try{
          //Query para historico de la licencia  
                foreach($idlic as $licencia){ 
                    $query = $connection->createCommand()->insert('historico_licencias',
                                                                    ['historico_licencia_id' => $licencia['licencia_id'],
                                                                    'licencia_equipo_id' => $licencia['licencia_equipo_id'],
                                                                    'licencia_tipo_licencia_id' => $licencia['licencia_tipo_licencia_id'],
                                                                    'licencia_numero' => $licencia['licencia_numero'],
                                                                    'licencia_version' => $licencia['licencia_version'],
                                                                    'licencia_especificaciones' => $licencia['licencia_especificaciones'],
                                                                    'licencia_estado_id' => $licencia['licencia_estado_id'],
                                                                    'licencia_sucursal_id' => $licencia['licencia_sucursal_id'],
                                                                    'licencia_identificacion_tecnologia' => $licencia['licencia_identificacion_tecnologia'],
                                                                    'usuario_modifica' => $usuario,
                                                                    'fecha_creacion' => $dia,
                                                                    'proceso_id' =>24,
                                                                    'observacion' =>$_REQUEST['observacion'],
                                                                    ])->execute();        
                }                                      
                // Query para actualizar el estado del equipo
                $query = $connection->createCommand('UPDATE equipos
                                                        SET equipo_estado_id = 24
                                                        WHERE equipo_id ='.$id.'
                                                        ')->execute();

                //Liberación de licencias
                    foreach ($idlic as $idl){            
                        $query = $connection->createCommand("UPDATE licencias
                                                            SET licencia_estado_id = 20
                                                            WHERE licencia_id ='" .$idl->licencia_id."'
                                                            ")->execute(); 

                    }                                                                                         
                $transaction->commit();                   
                if($model->save()){   
                    if ($model->equipo_baja_activo) {
                        if ($model->upload($model->equipo_id)) {
                            // file is uploaded successfully
                        }else{
                            echo 'no guardo';
                            die();
                        }
                    }               
                    // $model->incapacidad->saveAs('intranetosa/images/' .$model->incapacidad->name.'.'.$model->incapacidad->extension);
                    return $this->redirect(['view', 'id' => $model->equipo_id]);

                }else{
                    echo '<pre>';
                    print_r( $model->getErrors() );
                    die();
                }
                return $this->redirect(['view','id'=>$id]); 

            }catch(Exception $ac) {
                $transaction->rollback();
                echo 'transaction cancelada';
            }                                         
    }

    public function actionVerhistorico($id){             
        $historico = HistoricoEquipos::find()->where(['historico_equipo_id' =>$id])->all();        
        // echo '<pre>';
        // print_r($historico);
        // die();
        foreach ($historico as $histequip){            
        }
       // Modal historico equipo  
        $html = '';
        $html .= '<div class="form-row">';
         $html .= '<div class="col-lg-12">';
                $html .= '<div class="card card-primary">';        
                    $html .= '<div class="card-header">HISTORICO DEL ÉQUIPO</div>';
                    $html .= '<div class="">'; 
                        $html .= '<div class="card card-primary">';        
                            $html .= '<div class="card-header">INFORMACIÓN GENERAL</div>';
                            $html .= '<div class="card-body">';        
                                $html .= '<div class="table-responsive">';
                                    $html .= '<table class="table table-striped table-hover table-bordered" >';
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Indentificacón T & T</th>';
                                                $html .=  '<th>Nombre Actual</th>';
                                                $html .=  '<th>Ubicación T & T</th>';
                                                $html .=  '<th>Ip Asignada</th>';         
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip['equipo_identificacion_tecnologia'].'</td>';
                                                $html .= '<td>'.$histequip['equipo_nombre_actual'].'</td>';
                                                $html .= '<td>'.$histequip->sucursales->sucursal_nombre. " - ". $histequip->areas->area_nombre.'</td>';
                                                $html .= '<td>'.$histequip['equipo_ip'].'</td>';
                                            $html .= '</tr>';
                                        $html .= '</tbody>';
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Funcionario</th>';
                                                $html .=  '<th>Cargo</th>';
                                                $html .=  '<th>Correo</th>';
                                                $html .=  '<th>Estado Equipo</th>';         
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip->funcionarios->funcionario_nombres.'</td>';
                                                $html .= '<td>'.$histequip->funcionarios->funcionario_cargo.'</td>';
                                                $html .= '<td>'.$histequip->funcionarios->funcionario_email.'</td>';
                                                $html .= '<td>'.$histequip->estado->tipo_detalle_nombre.'</td>';
                                            $html .= '</tr>';
                                        $html .= '</tbody>';        
                                        $html .= '<div>'; 
                                    $html .= '</table>'; 
                                $html .= '</div>'; 
                            $html .= '</div>';
                        $html .= '</div>'; 
                        
                        $html .= '<div class="card card-primary">';        
                            $html .= '<div class="card-header">INFORMACIÓN DE LA MAQUINA</div>';
                            $html .= '<div class="card-body">';        
                                $html .= '<div class="table-responsive">';
                                    $html .= '<table class="table table-striped table-hover table-bordered" >';
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Marca del Equipo</th>';
                                                $html .=  '<th>Modelo del Equipo</th>';
                                                $html .=  '<th>Sistema Operativo</th>';
                                                $html .=  '<th>Paquete de Office</th>';         
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip->equipo_marca.'</td>';
                                                $html .= '<td>'.$histequip->equipo_modelo.'</td>';
                                                $html .= '<td>'.$histequip->licencia->tipo_detalle_nombre.'</td>';
                                                $html .= '<td>'.$histequip->licenciaoff->tipo_detalle_nombre.'</td>';
                                            $html .= '</tr>';
                                        $html .= '</tbody>';
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Pantalla</th>';
                                                $html .=  '<th>RAM</th>';
                                                $html .=  '<th>Tipo de Memoria RAM</th>';
                                                $html .=  '<th>Marca Disco Duro</th>';         
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip->equipo_pantalla.'</td>';
                                                $html .= '<td>'.$histequip->equipo_ram.'</td>';
                                                $html .= '<td>'.$histequip->equipo_tipo_memoria_ram.'</td>';
                                                $html .= '<td>'.$histequip->equipo_marca_disco_duro.'</td>';
                                            $html .= '</tr>';
                                        $html .= '</tbody>';
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Capacidad Disco Duro</th>';
                                                $html .=  '<th>Procesador</th>';
                                                $html .=  '<th>Velocidad del Procesador</th>';
                                                $html .=  '<th>Nucleos</th>';         
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip->equipo_capacidad_disco_duro.'</td>';
                                                $html .= '<td>'.$histequip->equipo_procesador.'</td>';
                                                $html .= '<td>'.$histequip->equipo_velocidad_procesador.'</td>';
                                                $html .= '<td>'.$histequip->equipo_cantidad_nucleos_procesador.'</td>';
                                            $html .= '</tr>';
                                        $html .= '</tbody>';
                                    $html .= '</table>';
                                $html .= '</div>'; 
                            $html .= '</div>'; 
                        $html .= '</div>'; 
                        
                        $html .= '<div class="card card-primary">';        
                            $html .= '<div class="card-header">INFORMACIÓN CONTABLE</div>';
                            $html .= '<div class="card-body">';        
                                $html .= '<div class="table-responsive">';
                                    $html .= '<table class="table table-striped table-hover table-bordered" >';                      
                                        $html .= '<thead>';
                                            $html .= '<tr>';
                                                $html .=  '<th>Número de Activo</th>';
                                                $html .=  '<th>Fecha de Ingreso</th>';
                                                $html .=  '<th>Valor</th>';                                
                                            $html .= '<tr>';
                                        $html .= '</thead>';
                                        $html .= '<tbody>';
                                            $html .= '<tr>';
                                                $html .= '<td>'.$histequip->equipo_numero_activo_contabilidad.'</td>';
                                                $html .= '<td>'.$histequip->equipo_fecha_ingreso.'</td>';
                                                $html .= '<td>'.$histequip->equipo_valor.'</td>';                                
                                            $html .= '</tr>';
                                        $html .= '</tbody>';
                                    $html .= '</table>';
                                $html .= '</div>'; 
                            $html .= '</div>'; 
                        $html .= '</div>';
                    $html .= '</div>'; 
                $html .= '</div>';
         $html .= '</div>';
        $html .= '</div>';    
        echo $html;
    }
    public function actionVermantenimiento($id){
      
        $mantenimiento = Soporteequipos::find()->where(['equipos_idequipo'=>$id])->all();
        foreach ($mantenimiento as $key){
            $estado = $key->estado_soporte;
            $estadonom = $key->estado_soporte == 1 ? "Realizado" : "Pendiente";
        } 
        $color = 'card-primary'; 
        if ($estado){
            $color = 'card-success';
        }else{
            $color = 'card-primary';
        }       


        $data = '';
        $data .= '<div class="form-row">';
            $data .= '<div class="col-lg-12">';
                    $data .= '<div class="card '.$color.'">';        
                        $data .= '<div class="card-header">Detalle del soporte</div>';                     
                            $data .= '<div class="card '.$color.'">';        
                                $data .= '<div class="card-header">INFORMACIÓN GENERAL</div>';
                                $data .= '<div class="card-body">';        
                                    $data .= '<div class="table-responsive">';
                                        $data .= '<table class="table table-striped table-hover table-bordered" >';
                                            $data .= '<thead>';
                                                $data .= '<tr>';
                                                    $data .=  '<th>Quién solicita</th>';
                                                    $data .=  '<th>Correo</th>';
                                                    $data .=  '<th>Teléfono</th>';
                                                    $data .=  '<th>Fecha solicitud</th>';         
                                                $data .= '<tr>';
                                            $data .= '</thead>';
                                            $data .= '<tbody>';
                                                foreach($mantenimiento as $value){
                                                    $data .= '<tr>';
                                                        $data .= '<td>'.$value->nombre.' '.$value->apellido.'</td>';
                                                        $data .= '<td>'.$value->email.'</td>';
                                                        $data .= '<td>'.$value->telefono.'</td>';
                                                        $data .= '<td>'.$value->fecha_solicitud.'</td>';
                                                    $data .= '</tr>';
                                                }    
                                            $data .= '</tbody>';
                                            $data .= '<thead>';
                                                $data .= '<tr>';
                                                    $data .=  '<th>Tipo de soporte</th>';
                                                    $data .=  '<th>Estado</th>';                                                    
                                                    $data .=  '<th>Fecha de respuesta</th>';                                                    
                                                    $data .=  '<th>Responsable</th>';                                                                                                        
                                                $data .= '<tr>';
                                            $data .= '</thead>';
                                            $data .= '<tbody>';
                                                $data .= '<tr>';
                                                    $data .= '<td>'.$value->tiposoporte->tipo_detalle_nombre.'</td>';
                                                    $data .= '<td>'.$estadonom.'</td>';
                                                    if ($value->estado_soporte){
                                                        $data .= '<td>'.$value->fecha_cierre.'</td>';
                                                    }else{
                                                        $data .= '<td>Sin fecha</td>';
                                                    }  
                                                    $data .= '<td>'.$value->equipo->funcionarios->funcionario_nombres.'</td>';                                                                                              
                                                $data .= '</tr>';
                                            $data .= '</tbody>';                                                     
                                        $data .= '</table>'; 
                                    $data .= '</div>'; 
                                $data .= '</div>';
                            $data .= '</div>'; 
                            $data .= '<div class="card '.$color.'">';        
                                $data .= '<div class="card-header">INFORMACIÓN TÉCNICA</div>';
                                $data .= '<div class="card-body">';        
                                    $data .= '<div class="table-responsive">';
                                        $data .= '<table class="table table-striped table-hover table-bordered" >';
                                            $data .= '<thead>';
                                                $data .= '<tr>';
                                                    $data .=  '<th colspan="5">Evidencia</th>';                                                           
                                                $data .= '<tr>';
                                            $data .= '</thead>';
                                            $data .= '<tbody>';
                                                foreach($mantenimiento as $value){
                                                    $data .= '<tr>';
                                                    $data .= '<td colspan="5"><a download="'.$value->evidencia.'" href="http://35.175.13.101/OsaIntranet/web/soporte/'.$value->evidencia.'"> <img src="http://35.175.13.101/OsaIntranet/web/soporte/'.$value->evidencia.'"style="width: 555px; height: 450px;"> </a></td>'; 
                                                    $data .= '</tr>';
                                                }    
                                            $data .= '</tbody>';
                                            $data .= '<thead>';
                                                $data .= '<tr>';
                                                    $data .=  '<th>Observación</th>';                                                                                                                                                         
                                                $data .= '<tr>';
                                            $data .= '</thead>';
                                            $data .= '<tbody>';
                                                $data .= '<tr>';
                                                    $data .= '<td>'.$value->observacion_soporte.'</td>';                                                                                                                                                   
                                                $data .= '</tr>';
                                            $data .= '</tbody>'; 
                                            if ($estado){                                            
                                                $data .= '<thead>';
                                                    $data .= '<tr>';
                                                        $data .=  '<th>Respuesta</th>';                                                                                                                                                         
                                                    $data .= '<tr>';
                                                $data .= '</thead>';
                                                $data .= '<tbody>';
                                                    $data .= '<tr>';
                                                        $data .= '<td>'.$value->observacion_respuesta.'</td>';                                                                                                                                                   
                                                    $data .= '</tr>';
                                                $data .= '</tbody>'; 
                                            }                                                        
                                        $data .= '</table>'; 
                                    $data .= '</div>'; 
                                $data .= '</div>';
                            $data .= '</div>';                                                                                                                  
                    $data .= '</div>';
            $data .= '</div>';
        $data .= '</div>';    
        echo $data;  


        // Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        // Yii::$app->response->data  =  $html;
        
    }
    public function actionGestionarmantenimiento($id,$observacion){ 
        //  echo '<pre>';
        // print_r($observacion);
        // die();       

        if (yii::$app->request->get()){             

            Soporteequipos::updateAll(['observacion_respuesta' => $observacion,'estado_soporte'=> 1,'fecha_cierre'=>date('Y-m-d')],['and', ['equipos_idequipo'=> $id]]);
            $tecnico = Users::find()->where(['id'=> Yii::$app->user->identity->id])->all();
            foreach($tecnico as $val){}

            $mantenimiento2 = Soporteequipos::find()->where(['equipos_idequipo'=>$id])->all();
            foreach ($mantenimiento2 as $key){
                $correo = $key->email;                
            }  

            $body="";
                $subject = "Soporte equipos - " . date('Y-m-d H:i:s'); 
                $body    .= '<div style="float:left; padding:10px; text-align: left; width:100%; padding:5px; backgrond:green;">';
                    $body    .= '<p>Buen dia</p>';
                    $body    .= '<p>Se informa que hemos gestionado su caso.</p>';                          
                    $body    .= '<br>';
                    $body .= '<table id="tabla-datos" class="display" width="80%" border="1" style="text-align:center;">';
                    $body .=  '<thead>';
                    $body .=  '<tr style="background-color:#0A66B2">';
                    $body .=         '<th style="color:white;">FECHA RECEPCIÓN</th>';
                    $body .=         '<th style="color:white;">FECHA DE RESPUESTA</th>';  
                    $body .=         '<th style="color:white;">FUNCIONARIO</th>';
                    $body .=         '<th style="color:white;">TÉCNICO</th>';
                    $body .=         '<th style="color:white;">TIPO DE MANTENIMIENTO</th>';                            
                    // $body .=         '<th style="color:white;">DESCARGAR</th>';
                    $body .=    ' </tr>';
                    $body .=    ' </thead>';                
                    $body .=     '<tbody>';
                    $body .=    ' <tr style="background-color:#F1F1F1">';                      
                    $body .=         '<td>'.$key->fecha_solicitud.'</td>'; 
                    $body .=         '<td>'.date('Y-m-d').'</td>';
                    $body .=         '<td>'.$key->nombre.' '.$key->apellido.'</td>';
                    $body .=         '<td>'.$val->nombres.' '.$val->apellidos.'</td>';
                    $body .=         '<td>'.$key->tiposoporte->tipo_detalle_nombre.'</td>';                            
                    $body .=    ' </tr>';               
                    $body .=     '</tbody>';
                    $body .= '</table>';
                    $body    .= '<br>';
                    $body    .= '<p><b>Respuesta: </b></p>';                    
                    $body    .= '<p>'.$key->observacion_respuesta.'</p>';
                    $body    .= '<br>';
                    $body    .= '<p>Quedamos atentos a cualquier inquietud..</p>';
            $email = Yii::$app->mailer->compose();
            $email->setTo($correo);
            $email->setFrom(['oficinavirtual@saycoacinpro.org.co' => Yii::$app->params["title"]]);
            $email->setSubject('oficinavirtual@saycoacinpro.org.co');
            // $email->attach(Yii::getAlias('@webroot').'/Psl/bancos/'.$file.'');       
            $email->setHtmlBody($body);            
            $email->send();

            return false;
            
        }
        
    }
    
   
}
