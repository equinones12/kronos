<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\User;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    // ///////////// ============== ROLES ================== /////////
        // 1. Administrador     ->  isUserAdmin
        // 2. Caja              ->  isUserCaja
        // 3. Ventas Mostrador  ->  isUserVentasMostrador
        // 4. Bodega            ->  isUserBodega
    // ///////////// ======================================= /////////
    // =========================== RBACK Control de permisos de usuarios por roles  ===
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['index','criptpass'],
                    'rules' => [
                        // Acciones rol ADMINISTRADOR
                        [
                            'actions' => ['index','view','update','criptpass'],
                            'allow' => true,
                            'roles' => ['@'],
                            'matchCallback' => function ($rule, $action) {
                                return User::isUserAdmin(Yii::$app->user->identity->id);
                            },
                        ]
                    ],
                ],
            ];
        }
        public function actionCriptpass(){
            $pass = '';
            if (Yii::$app->request->post()) {
                $pass = Yii::$app->request->post('pass');
                return $this->render('criptpass', [
                    'pass'=>crypt($pass,Yii::$app->params["salt"])
                ]);

            }else{
                return $this->render('criptpass', [
                    'pass'=>$pass
                ]);
            }
        }
    // ========================================= users/index                        =======================
        public function actionIndex()
        {
            $searchModel = new UsersSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    // ========================================= users/view                         =======================
        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    // ========================================= users/Update                       =======================
        public function actionUpdate($id)
        {
            if ($id == yii::$app->user->identity->id || yii::$app->user->identity->role == 1 ) {
                $model = Users::findOne($id);
                $actualPassword = $model->password;
                if ($model->load(Yii::$app->request->post())) {
                    // echo '<pre>';
                    // print_r($model);
                    // die();
                    if ($actualPassword != $model->password) {
                        $model->password = crypt($model->password, Yii::$app->params["salt"]);
                    }
                    if ($model->validate()) {
                        if ($model->save()) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }else{
                        return $this->render('update', [
                            'model' => $model,
                        ]); 
                    }
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }else{
                return $this->goHome();
            }
        }
    // ========================================= findModel                          =======================
        protected function findModel($id)
        {
            if (($model = Users::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
}
