<?php

namespace app\controllers;
use Yii;
use yii\helpers\Url;
use kartik\mpdf\Pdf;

use app\models\Clientes;


class PdfController extends \yii\web\Controller
{
    public function actionLicencia($id)
   	{
        $model = Clientes::findOne($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    	$pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            // 'destination' => Pdf::DEST_DOWNLOAD,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Licencia_'.$model->clientes_nombre.'-'.$model->clientes_nit.'..pdf',
            'content' => $this->renderPartial('licencia',array('model'=>$model),true),
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'options' => [
            ],
            'methods' => [
                'SetTitle' => 'Licencia No. '.$model->clientes_licencia,
                'SetSubject' => 'Generado por HELPDESK de STELLAR',
                // 'SetHeader' => ['STELLAR || '],
                // 'SetFooter' => ['|LICENCIA|'],
                'SetAuthor' => 'Ing Camilo Bautista',
                'SetCreator' => 'Ing Camilo Bautista',
                'SetKeywords' => 'HELPDESK,STELLAR,PDF',
            ]
        ]);
    
        return $pdf->render();
		
    }
   

}
