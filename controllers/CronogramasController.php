<?php

namespace app\controllers;

use Yii;
use app\models\Cronogramas;
use app\models\CronogramasSearch;
use app\models\Soporteequipos;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Users;
/**
 * CronogramasController implements the CRUD actions for Cronogramas model.
 */
class CronogramasController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin','index','view','create'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','view','create'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index','view','create'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario de recaudo
                          return User::isUserLinalca(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
             //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
             //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cronogramas models.
     * @return mixed
     */
    public function actionIndex()
    {   
        $searchModel = new CronogramasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $filtro = null;
        if (Yii::$app->request->post()){

            // echo "<pre>";
            // print_r($_REQUEST);
            // die();
            // se instancia la Clase Query.
            $query = new Query();
            $connection = Yii::$app->db;
            $filtro .= isset($_REQUEST['sucursal'])!=" " ? ' and Zona_id = '.$_REQUEST['sucursal'] : "";
            $filtro .= isset($_REQUEST['tipo_mantenimiento'])!=" " ? ' and cronograma_tipo_mantenimiento = '.$_REQUEST['tipo_mantenimiento'] : "";
            $filtro .= isset($_REQUEST['estadoMantenimiento'])!=" " ?' and cronograma_realizado = '.$_REQUEST['estadoMantenimiento'] : "";
            $filtro .= isset($_REQUEST['fechaInicial'])!="" ? " and cronograma_fecha_inicio >= '".$_REQUEST['fechaInicial']."'" : "";
            $filtro .= isset($_REQUEST['fechaFinal'])!="" ? " and cronograma_fecha_fin <= '".$_REQUEST['fechaFinal']."' ": "";

            // echo $filtro;
            // die();
            // consulta que muestra la informacion en el index del modulo de Visita
            $query = $connection->createCommand("SELECT     *,
                                                            case when cronograma_realizado = 0 then 'PENDIENTE' else 'REALIZADO' end as estado,
                                                            (select count(*) from cronogramas_detalles where cronograma_detalle_cronograma_id = cronograma_id) as numeroEquipos       
                                                    from    cronogramas                                                           
                                                            join sucursales on (cronograma_sucursal_id=sucursal_id)
                                                            join users on (cronograma_quien_realiza = id)
                                                    where   1=1
                                                    ".$filtro."");
            $result = $query->queryAll();

            // echo "<pre>";
            // print_r($result);die();
            return $this->render('index', [
                'result' => $result,
            ]); 

        }else{

          return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                "result" => null,
            ]);  
        }

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single Cronogramas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;


        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = cronograma_tipo_mantenimiento) as tipom
                                                from    cronogramas
                                                        join sucursales on (cronograma_sucursal_id = sucursal_id)
                                                        join users on (cronograma_quien_realiza = id)
                                                where   1=1
                                                        and cronograma_id ='.$id.'
                                                '
                                                );
        $Cronograma = $query->queryAll();

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *

                                                from    cronogramas_detalles
                                                        join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                        join areas on (equipo_area_id = area_id)
                                                where   1=1
                                                        and cronograma_detalle_cronograma_id ='.$id.'
                                                order by cronograma_detalle_fecha asc'
                                                );
        $CronogramaDetalles = $query->queryAll();

        // echo "<pre>";
        // print_r($Cronograma);
        // print_r($CronogramaDetalles);
        // die();
        return $this->render('view', [
            // 'model' => $this->findModel($id),
            'Cronograma' => $Cronograma,
            'CronogramaDetalles' => $CronogramaDetalles,
        ]);


    }

    /**
     * Creates a new Cronogramas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cronogramas();

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/) {


            // echo "<pre>";
            // print_r($_REQUEST);die;
            // se instancia la Clase Query.
            $query = new Query();
            $connection = Yii::$app->db;
            // echo "<pre>";
            // print_r($_REQUEST);die();

            // INSERCION EN LA TABLA CRONOGRA
            $sql = $connection -> createCommand ()-> insert ( 'cronogramas' , 
                                                    [ 
                                                     'cronograma_sucursal_id' => $_REQUEST['select-sucursal'],
                                                     'cronograma_fecha_inicio' => $_REQUEST['cronograma_fecha_inicio'],
                                                     'cronograma_fecha_fin' => $_REQUEST['cronograma_fecha_fin'],
                                                     'cronograma_quien_realiza' => $_REQUEST['Cronogramas']['cronograma_quien_realiza'],
                                                     'cronograma_tipo_mantenimiento' => $_REQUEST['Cronogramas']['cronograma_tipo_mantenimiento'],
                                                    ]
                                                    )-> execute ();
            $idCronograma = Yii::$app->db->getLastInsertID();

            #############################################################
            # RECORRIDO E INSERCION DE LOS DETALLES DEL CRONOGRAMA
            #############################################################

            foreach ($_REQUEST['horaI'] as $keyD => $valueD) {

                // INSERCION EN LA TABLA CRONOGRA
                $sql = $connection -> createCommand ()-> insert ( 'cronogramas_detalles' , 
                                                        [ 
                                                         'cronograma_detalle_cronograma_id' => $idCronograma,
                                                         'cronograma_detalle_equipo_id' => $keyD,
                                                         'cronograma_detalle_hora_inicio' => $valueD,
                                                         'cronograma_detalle_hora_fin' => $_REQUEST['horaF'][$keyD],
                                                         'cronograma_detalle_fecha' => $_REQUEST['fecha'][$keyD],
                                                        ]
                                                        )-> execute ();
                

            }

            return $this->redirect(['view', 'id' => $idCronograma]);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Cronogramas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cronograma_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cronogramas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cronogramas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cronogramas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cronogramas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLista(){

        // echo "<pre>";
        // print_r($_REQUEST);die();

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT *,
                                                    (select count(*) from cronogramas_detalles where cronograma_detalle_cronograma_id) as cuenta
                                             from   cronogramas
                                                    join cronogramas_detalles on (cronograma_id = cronograma_detalle_cronograma_id)
                                                    join sucursales on (cronograma_sucursal_id = sucursal_id)
                                                    join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                    join funcionarios on (equipo_usuario_id = funcionario_id)
                                             where  1=1
                                                    and cronograma_id = '.$_REQUEST['id'].'
                                             order by cronograma_detalle_fecha, cronograma_detalle_hora_inicio asc       ');
        $result = $query->queryAll();

        // echo "<pre>";
        // print_r($result);die();

        if(!$result){
            $result = null;
        }    

        return $this->render('lista', [
            'result'=> $result,
        ]);

    }

}
