<?php

namespace app\controllers;

use Yii;
use app\models\CheckList;
use app\models\CheckListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use app\models\Areas;
use kartik\date\DatePicker;

/**
 * CheckListController implements the CRUD actions for CheckList model.
 */
class CheckListController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CheckList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CheckListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CheckList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die();

        $model = CheckList::findOne($id);

        $query = new Query();
        $connection = Yii::$app->db;
        $idUsuario = yii::$app->user->identity->id;

        $query = $connection->createCommand("SELECT     *,
                                                        (select CONCAT(nombres,' ',apellidos) from users where id = cronograma_quien_realiza ) as quienRealiza,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = cronograma_tipo_mantenimiento) as tipoMantenimiento, 
                                                        case when cronograma_detalle_aprobado = 0 then 'Pendiente por Aprobar'
                                                             when cronograma_detalle_aprobado = 1 then 'Aprobado' end as estado, 
                                                        ( select check_list_fecha_registro from check_list where check_list_cronograma_id = ".$id." and check_list_equipo_id = ".$_REQUEST['Equipo']." order by 1 desc limit 1 )  as fechaRegistro
                                                from    cronogramas_detalles 
                                                        join cronogramas on (cronograma_detalle_cronograma_id = cronograma_id)
                                                        join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                where   1=1
                                                        and equipo_id = ".$_REQUEST['Equipo']."
                                                        and cronograma_detalle_id =".$id."
                                                         ");
        $cronogramas = $query->queryAll();
        
        $query = $connection->createCommand("SELECT     *

                                                from    check_list
                                                        join campos_check_list on (check_list_campo_id = campo_check_list_id) 
                                                where   1=1
                                                        and check_list_equipo_id = ".$_REQUEST['Equipo']."
                                                        ");
        $campos = $query->queryAll();    

        $query = $connection->createCommand("SELECT *
                                            from    cronograma_detalle_imagenes
                                            where   1=1
                                                    and cronograma_detalle_imagen_equipo_id =".$_REQUEST['Equipo']."
                                                    and cronograma_detalle_imagen_detalle_id=".$_REQUEST['id']."");
        $imagenes = $query->queryAll();

        // echo "<pre>";
        // print_r($cronogramas);
        // // print_r($campos);
        // die();

        return $this->render('view', [
            'cronogramas' => $cronogramas,
            'campos' => $campos,
            'imagenes' => $imagenes,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CheckList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CheckList();

        // echo "<pre>";
        // print_r($_REQUEST);die();
        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;
        $idUsuario = yii::$app->user->identity->id;

        $query = $connection->createCommand("SELECT     *,
                                                        (select CONCAT(nombres,' ',apellidos) from users where id = cronograma_quien_realiza ) as quienRealiza
                                                from    cronogramas_detalles 
                                                        join cronogramas on (cronograma_detalle_cronograma_id = cronograma_id)
                                                        join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                where   1=1
                                                        and cronograma_realizado = 0 
                                                        and equipo_id =".$_REQUEST['id']."
                                                        and cronograma_id=".$_REQUEST['cronograma']."");
        $cronogramas = $query->queryAll();
        
        $query = $connection->createCommand("SELECT     *
                                                from    campos_check_list 
                                                where   1=1
                                                        ");
        $campos = $query->queryAll();    
        // echo "<pre>";
        // print_r($campos);die();



        if (Yii::$app->request->post() ) {

            // echo "<pre>";
            // print_r($_REQUEST);die();

            $model->file = UploadedFile::getInstances($model, 'file');

            // print_r($model->file);

            // // die();
            // die();

            // echo "<pre>";
            // print_r($_REQUEST);die();
            foreach ($_REQUEST['Valores'][8] as $key => $value) {
                
                // INSERCION EN LA TABLA CRONOGRA
                $sql = $connection -> createCommand ()-> insert ( 'check_list' , 
                                                        [ 
                                                         'check_list_cronograma_id' => $_REQUEST['cronograma'],
                                                         'check_list_equipo_id' => $_REQUEST['id'],
                                                         'check_list_usuario_registra' => $idUsuario,
                                                         'check_list_fecha_registro' => date('Y-m-d H:i:s'),
                                                         'check_list_campo_id' => $key,
                                                         'check_list_valor' => strtoupper($value),
                                                        ]
                                                        )-> execute ();
                // $idCronograma = Yii::$app->db->getLastInsertID();


            }

            if ($model->file) {
                $contador=0;
                foreach ($model->file as $file) {
                    $file->saveAs('mantenimientos/' . $_REQUEST['cronograma'].$_REQUEST['id'].$contador.'.' . $file->extension);

                    $sql = $connection -> createCommand ()-> insert ( 'cronograma_detalle_imagenes' , 
                                                [ 
                                                    'cronograma_detalle_imagen_detalle_id' => $_REQUEST['cronograma'],
                                                    'cronograma_detalle_imagen_ruta' => 'mantenimientos/' . $_REQUEST['cronograma'].$_REQUEST['id'].$contador.'.' . $file->extension,
                                                    'cronograma_detalle_imagen_equipo_id' => $_REQUEST['id'].'.' . $file->extension,
                                                ]
                                                )-> execute ();

                $contador++;    
                }
            }

            $query = $connection->createCommand("UPDATE cronogramas_detalles 
                                            SET     cronograma_detalle_aprobado=0,
                                                    cronograma_detalle_realizado = 1, 
                                                    cronograma_tiempo_final_utilizado='".$_REQUEST['TiempoFinal']."'
                                            where   1=1
                                                    and cronograma_detalle_id =".$_REQUEST['cronograma']."");
            $validado = $query->execute();

            
            return $this->redirect(['confirma', 'Equipo' => $_REQUEST['id'], 'cronograma' => $_REQUEST['cronograma'] ]);

        } else {
            
            return $this->render('create', [
                'model' => $model,
                'cronogramas'=>$cronogramas,
                'campos'=>$campos,
            ]);
        }
    }

    /**
     * Updates an existing CheckList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        
        // echo "<pre>";
        // print_r($_REQUEST);
        // die();

        $query = new Query();
        $connection = Yii::$app->db;
        $idUsuario = yii::$app->user->identity->id;

        $query = $connection->createCommand("SELECT     *,
                                                        (select CONCAT(nombres,' ',apellidos) from users where id = cronograma_quien_realiza ) as quienRealiza,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = cronograma_tipo_mantenimiento) as tipoMantenimiento, 
                                                        case when cronograma_detalle_aprobado = 0 then 'Pendiente por Aprobar'
                                                             when cronograma_detalle_aprobado = 1 then 'Aprobado' end as estado
                                                from    cronogramas_detalles 
                                                        join cronogramas on (cronograma_detalle_cronograma_id = cronograma_id)
                                                        join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                where   1=1
                                                        and cronograma_detalle_equipo_id=".$_REQUEST['Equipo']."
                                                         ");
        $cronogramas = $query->queryAll();  

        
        $query = $connection->createCommand("SELECT     *
                                                from    check_list
                                                        join cronogramas_detalles on (check_list_cronograma_id = cronograma_detalle_id)
                                                        join campos_check_list on (check_list_campo_id = campo_check_list_id)
                                                where   1=1
                                                        and cronograma_detalle_id=".$_REQUEST['cronograma']."
                                                        and check_list_equipo_id=".$_REQUEST['Equipo']." ");
        $checklist = $query->queryAll();

        // echo "<pre>";
        // print_r($cronogramas);
        // print_r($checklist);
        // die();
        if (Yii::$app->request->post() ) {

            // echo "<pre>";
            // print_r($_REQUEST);die();

            foreach ($_REQUEST['Valores'][8] as $key => $value) {
                
               
                 $query = $connection->createCommand("UPDATE check_list
                                            SET     check_list_valor='$value'
                                            where   1=1
                                                    and check_list.check_list_id =".$key."");

                $validado = $query->execute();

            }   

            return $this->redirect(['view', 'id'=>  $_REQUEST['cronograma'],'Equipo' => $_REQUEST['Equipo'] ]); 

        }else{

            return $this->render('update', [
                'cronogramas'=>$cronogramas,
                'checklist'=>$checklist,
            ]);

        }

        

    }

    /**
     * Deletes an existing CheckList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CheckList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CheckList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CheckList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionConfirma(){

        // echo "<pre>";
        // print_r($_REQUEST);die();

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand("SELECT     *,
                                                    (select CONCAT(nombres,' ',apellidos) from users where id = cronograma_quien_realiza ) as quienRealiza,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = cronograma_tipo_mantenimiento) as tipoMantenimiento
                                            from    cronogramas_detalles 
                                                    join cronogramas on (cronograma_detalle_cronograma_id = cronograma_id)
                                                    join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                            where   1=1
                                                    and cronograma_realizado = 0 
                                                    and equipo_id =".$_REQUEST['Equipo']."
                                                    and cronograma_detalle_id=".$_REQUEST['cronograma']."");
        $cronogramas = $query->queryAll();

        $query = $connection->createCommand("SELECT *
                                            from    check_list
                                                    join campos_check_list on (check_list_campo_id = campo_check_list_id)
                                            where   1=1
                                                    and check_list_equipo_id =".$_REQUEST['Equipo']."
                                                    and check_list_cronograma_id=".$_REQUEST['cronograma']."");
        $checklist = $query->queryAll();
        $query = $connection->createCommand("SELECT *
                                            from    cronograma_detalle_imagenes
                                            where   1=1
                                                    and cronograma_detalle_imagen_equipo_id =".$_REQUEST['Equipo']."
                                                    and cronograma_detalle_imagen_detalle_id=".$_REQUEST['cronograma']."");
        $imagenes = $query->queryAll();
        // echo "<pre>";
        // print_r($imagenes);
        // // print_r($checklist);
        // die();

        return $this->render('confirmacion', [
                'cronogramas' => $cronogramas,
                'checklist' => $checklist,
                'imagenes' => $imagenes, 
            ]);

    }

    public function actionValidarcodigo(){

        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand("SELECT *
                                            from    funcionarios 
                                            where   1=1
                                                    and funcionario_key =".$_REQUEST['id']."");
        $validado = $query->queryOne();
        // echo "<pre>";
        // print_r($validado);
        if ($validado!=null) {
            $arrayName = array('state' => true, 'id' =>$validado['funcionario_id'] );
        }else{
            
            $arrayName = array('state' => false );

        }

        echo json_encode($arrayName);

    }

    public function actionAprobacion(){

        
        // echo "<pre>";
        // print_r($_REQUEST);
        // die;


        $query = new Query();
        $connection = Yii::$app->db;
        $query = $connection->createCommand("UPDATE cronogramas_detalles
                                            SET     cronograma_detalle_aprobado=1,
                                                    cronograma_detalle_quien_aprueba = '".$_REQUEST['aprueba']."',
                                                    cronograma_detalle_realizado = 1,
                                                    cronograma_detalle_observacion_usuario = '".$_REQUEST['observacion']."'
                                            where   1=1
                                                    and cronograma_detalle_id =".$_REQUEST['cronograma']."
                                                    and cronograma_detalle_equipo_id = ".$_REQUEST['equipo']." ");
        $validado = $query->execute();
        // echo "<pre>";
        // print_r($query);die();
        if ($validado!=null) {
            
            return $this->redirect(['view', 'id' => $_REQUEST['cronograma'], 'Equipo'=> $_REQUEST['equipo'] ]);

        }
    }




    public function actionGuardarfirmadigitalindependiente()
    {
        $result = array();
        $imagedata = base64_decode($_POST['data']);
        $filename = 'mantenimiento_'.$_POST['idproducto'].'_'.$_POST['equipo'];
        //Location to where you want to created sign image
        $file_name = 'images/firmasdigitales/'.$filename.'.png';
        file_put_contents($file_name,$imagedata);
        $result['file_name'] = $file_name;
        echo $file_name;
    }


    
}
