<?php

namespace app\controllers;
use Yii;
use yii\db\Query;

require(__DIR__ . '/../vendor/phpexcel/Classes/PHPExcel.php');
// require(__DIR__ . '/../vendor/barcodemaster/src/BarcodeGeneratorHTML.php');

class ExcelController extends \yii\web\Controller
{
    public function actionMasiva()
    {
    	if (yii::$app->request->post()) {
    		return $this->render('index',[
    			'masiva' => yii::$app->request->post('masiva'),
    		]);	
    	}
        
    }

    # LIQUIDACIONES
    public function actionCronogramas()
    {

        // echo "<pre>";
        // print_r($_REQUEST);//die();        
        $datos = unserialize($_REQUEST['datos']);

        // echo "datos: ".$datos;
        // die();

         // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;


        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = cronograma_tipo_mantenimiento) as tipom
                                                from    cronogramas
                                                        join sucursales on (cronograma_sucursal_id = sucursal_id)
                                                        -- join areas on (equipo_area_id = area_id)

                                                where   1=1
                                                        and cronograma_id ='.$datos
                                                );
        $Cronograma = $query->queryAll();

        // consulta que muestra la informacion en el index del modulo de Visita
        $query = $connection->createCommand('SELECT     *

                                                from    cronogramas_detalles
                                                        join equipos on (cronograma_detalle_equipo_id = equipo_id)
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                        -- join areas on (equipo_area_id = area_id)
                                                where   1=1
                                                        and cronograma_detalle_cronograma_id ='.$datos.'
                                                order by cronograma_detalle_fecha asc'
                                                );
        $CronogramaDetalles = $query->queryAll();



        $datos = 1;
        
        if (yii::$app->request->post()) {
            return $this->render('cronogramasexcel',[
                //'masiva' => yii::$app->request->post('masiva'),
                'Cronograma' => $Cronograma,
                'CronogramaDetalles' => $CronogramaDetalles,
            ]); 
        }
    }

    # LIQUIDACIONES
    public function actionCronogramas2()
    {

        // echo "<pre>";
        // print_r($_REQUEST);

        $datos = unserialize($_REQUEST['datos']);

        // echo "<pre>";print_r($datos);
        // die();

        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;
        
        if (yii::$app->request->post()) {
            return $this->render('cronogramasexcel2',[
                //'masiva' => yii::$app->request->post('masiva'),
                'datos' => $datos,
            ]); 
        }
    }

    public function actionReporteequipos(){
        if(yii::$app->request->post()){
            $filtro = $_REQUEST['filtro'];
        }else{
            $filtro = null;
        }

        $query = new Query();
        $connection = yii::$app->db;
        // consulta reporte detallado de equipos
        $query = $connection->createCommand("select sucursales.Zona_id,sucursal_nombre as Sucursales,area_nombre as area,equipo_nombre_actual,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_marca) as Marca_de_equipo,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_estado_id) as Estado,
                                                    (select funcionario_nombres from funcionarios where funcionario_id=equipo_usuario_id) as Responsable,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_ram) as Capacidad_Ram,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_tipo_memoria_ram) as Tipo_de_ram,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_capacidad_disco_duro) as Capacidad_disco_duro,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_procesador) as Procesasador,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_systema_operativo_id) as Sistema_operativo,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=equipo_office_id) as Licencia_officce
                                                        
                                            from  equipos
                                                  join sucursales on (equipo_sucursal_id=sucursal_id)
                                                  join areas on (equipo_area_id=area_id)
                                            where 1 = 1
                                                 $filtro
                                            order by Zona_id,Sucursales");  
        $equiposGeneral = $query->queryAll();                                               
        // Consulta de los equipos por zona
        $query = $connection->createCommand("select Zona,
                                                    count(Equipos_activos) as Equipos_activos,
                                                    count(Equipos_inactivos) as Equipos_inactivos,
                                                    count(Equipos_dado_bajas) as Equipos_dado_bajas,                                                       
                                                    count(total_equipos) as total_equipos
                                            from (       
                                                    select   Zona_id as Zona,
                                                                case when equipo_estado_id = 5 then equipo_id end as Equipos_activos,
                                                                case when equipo_estado_id = 6 then equipo_id end as Equipos_inactivos,
                                                                case when equipo_estado_id = 24 then equipo_id end as Equipos_dado_bajas,                                                                    
                                                                count(equipo_id) as total_equipos
                                                                
                                                    from    equipos
                                                            join sucursales on (equipos.equipo_sucursal_id=sucursal_id)
                                                    where   1 = 1 
                                                            $filtro
                                                    group by equipo_id                                                       
                                                    ) as d 
                                            group by Zona");
        $equiposZona = $query->queryAll(); 
        //Consulta de los equipos por sucursal
        $query = $connection->createCommand("select sucursal,
                                                    count(Equipos_activos) as Equipos_activos,
                                                        count(Equipos_inactivos) as Equipos_inactivos,
                                                    count(Equipos_dado_bajas) as Equipos_dado_bajas,                                                        
                                                    count(total_equipos) as total_equipos
                                            from (       
                                                    select   sucursal_nombre as sucursal,
                                                                case when equipo_estado_id = 5 then equipo_id end as Equipos_activos,
                                                                case when equipo_estado_id = 6 then equipo_id end as Equipos_inactivos,
                                                                case when equipo_estado_id = 24 then equipo_id end as Equipos_dado_bajas,                                                                    
                                                                count(equipo_id) as total_equipos
                                                                
                                                    from    equipos
                                                            join sucursales on (equipos.equipo_sucursal_id=sucursal_id)
                                                    where   1 = 1 
                                                            $filtro
                                                    group by equipo_id                                                       
                                                    ) as d 
                                            group by sucursal");

        $equiposSuc = $query->queryAll();  
        
        if ($equiposGeneral && $equiposZona && $equiposSuc){
            return $this->render('reporteequipos',[
                        'equiposGeneral' => $equiposGeneral,
                        'equiposZona' =>$equiposZona,
                        'equiposSuc' =>$equiposSuc,
            ]);
        }else{
            echo "No se encontraron registros para el reporte";
        } 
    }

    public function actionReportelicencias(){

        if(yii::$app->request->post()){
            $filtro = $_REQUEST['filtro'];
        }else{
            $filtro = null;
        }
       
        // se instancia la clase query

        $query = new Query();
        $connection = yii::$app->db;
        
        // consulta general de licencias
        $query = $connection->createCommand("select Zona_id as Zona,sucursal_nombre as Sucursal,
                                                    (select tipo_nombre from tipos where tipo_id in (select tipo_detalle_tipo_id from tipos_detalles where tipo_detalle_id=licencia_tipo_licencia_id)) as Tipo_licencia,
                                                    licencia_numero as Llave,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=licencia_version) as Version,
                                                    (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id=licencia_estado_id) as Estado,
                                                    licencia_especificaciones,licencia_identificacion_tecnologia as Ubicacion_tecnologia       
                                            from   licencias
                                                    join sucursales on (licencia_sucursal_id=sucursal_id)
                                            where  1 = 1
                                                   $filtro
                                            order by Zona_id,Sucursal");                
        $licenciasGeneral = $query->queryAll();
        $query = $connection->createCommand("select Zona,
                                                    count(Sin_asignar) as Sin_asignar,
                                                    count(Asignado) as Asignado,
                                                    count(licencia_id) as Total
                                            from (       
                                                select Zona_id as Zona,licencia_id,
                                                                case when licencia_estado_id = 20 then licencia_id end as Sin_asignar,
                                                                case when licencia_estado_id = 19 then licencia_id end as Asignado
                                                        from   licencias
                                                                join sucursales on (licencia_sucursal_id=sucursal_id)
                                                        where  1 = 1 
                                                               $filtro  
                                                ) as f   
                                            group by Zona");
        $ReportelicenciasZonas = $query->queryAll();
        //Consulta licencias por sucursal
        $query = $connection->createCommand("select Sucursales,
                                                    count(Sin_asignar) as Sin_asignar,
                                                    count(Asignado) as Asignado,
                                                    count(licencia_id) as Total
                                            from (       
                                                select sucursal_nombre as Sucursales,licencia_id,
                                                                case when licencia_estado_id = 20 then licencia_id end as Sin_asignar,
                                                                case when licencia_estado_id = 19 then licencia_id end as Asignado
                                                        from   licencias
                                                                join sucursales on (licencia_sucursal_id=sucursal_id)
                                                        where  1 = 1
                                                               $filtro   
                                                ) as f   
                                            group by Sucursales");
        $ReportelicenciasSucursales = $query->queryAll();

        if($licenciasGeneral && $ReportelicenciasZonas && $ReportelicenciasSucursales){

            return $this->render('reportelicencias',[
                     'licenciasGeneral' => $licenciasGeneral,
                     'ReportelicenciasZonas' => $ReportelicenciasZonas,
                     'ReportelicenciasSucursales' => $ReportelicenciasSucursales,
            ]);
        }else{
            echo "No se encontraron registros";
        }

    }

    public function actionReportecronogramas(){

        // cargar variable filtro

        if(yii::$app->request->post()){
            $filtro = $_REQUEST['filtro'];
        }else{
            $filtro = null;
        }

        // instanciar la clase query
        $query = new Query();
        $connection = yii::$app->db;
         // Creacion de consultas
        $query = $connection->createCommand("select 
                                                    Zona_id as zona,sucursal_nombre as Sucursal,equipo_nombre_actual,cronograma_fecha_inicio,cronograma_fecha_fin,
                                                    (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_realiza,
                                                    case when cronograma_tipo_mantenimiento = 21 then 'PREVENTIVO'
                                                            when cronograma_tipo_mantenimiento = 22 then 'CORRECTIVO'end as Tipo_mantenimiento,
                                                    case when cronograma_realizado = 1 then 'REALIZADO' else 'PENDIENTE'  end as Estado,
                                                    cronograma_detalle_fecha as Fecha_de_mantenimiento,
                                                    case when cronograma_detalle_aprobado = 1 then 'APROBADO' else 'SIN APROBAR' end as Estado_manteniento,
                                                    case when cronograma_detalle_realizado = 1 then 'SI' else 'NO' end as Mantenimiento_realizado,
                                                    (select funcionario_nombres from funcionarios where funcionario_id=cronograma_detalle_quien_aprueba) as Quien_aprueba,
                                                    cronograma_detalle_fecha,cronograma_detalle_hora_inicio as Hora_inicio,cronograma_detalle_hora_fin as Hora_fin,
                                                    cast((CAST(timediff(cronograma_detalle_hora_inicio,cronograma_detalle_hora_fin) AS signed)/10000) as decimal(6,2)) * -1  as Tiempo_utilizado
                                                
                                                from   equipos
                                                    join cronogramas_detalles on (equipo_id=cronograma_detalle_equipo_id)
                                                    join sucursales on (equipo_sucursal_id=sucursal_id)
                                                    join cronogramas on (cronograma_detalle_cronograma_id=cronograma_id)
                                                where 1=1
                                                order by Zona_id,sucursal_nombre
                                        "); 
        $cronogramageneral = $query->queryAll();        
        $query = $connection->createCommand("select zona,
                                                            count(distinct(idCronograma)) as Cronogramas,
                                                            count(distinct(cronograma_detalle_id)) as Mantenimientos,
                                                            sum(Preventivo) as Preventivos,
                                                            sum(Correctivo) as Correctivos,
                                                            sum(aprobado) as aprobado,
                                                            sum(sin_aprobar) as sin_aprobar,
                                                            sum(realizado) as realizado,
                                                            sum(Pendiente) as Pendiente
                                                    from (
                                                                    select Zona_id as zona,sucursal_nombre as Sucursal,equipo_nombre_actual,cronograma_fecha_inicio,cronograma_fecha_fin,cronograma_detalle_cronograma_id as idCronograma,
                                                                            (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_realiza,
                                                                            case when cronograma_tipo_mantenimiento = 21 then 1 else '' end as Preventivo,
                                                                            case when cronograma_tipo_mantenimiento = 22 then 1 else '' end as Correctivo,
                                                                            case when cronograma_realizado = 1 then 'REALIZADO' else 'PENDIENTE'  end as Estado,
                                                                            cronograma_detalle_fecha as Fecha_de_mantenimiento,cronograma_detalle_id,
                                                                            case when cronograma_detalle_aprobado = 1 then 1 else '' end as aprobado,
                                                                            case when cronograma_detalle_aprobado = 0 then 1 else '' end as sin_aprobar,
                                                                            case when cronograma_detalle_realizado = 1 then 1 else '' end as realizado,
                                                                            case when cronograma_detalle_realizado = 0 then 1 else '' end as Pendiente,
                                                                            (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_aprueba						 						 
                                                                    from   equipos
                                                                            join cronogramas_detalles on (equipo_id=cronograma_detalle_equipo_id)
                                                                            join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                            join cronogramas on (cronograma_detalle_cronograma_id=cronograma_id)
                                                                    where 1=1                                                                           
                                                                    order by Zona_id,sucursal_nombre
                                                                ) as d     
                                                    group by zona");
            $cronogramaZonas = $query->queryAll();             
            $query = $connection->createCommand("select Sucursal,
                                                        count(distinct(idCronograma)) as Cronogramas,
                                                        count(distinct(cronograma_detalle_id)) as Mantenimientos,
                                                        sum(Preventivo) as Preventivos,
                                                        sum(Correctivo) as Correctivos,
                                                        sum(aprobado) as aprobado,
                                                        sum(sin_aprobar) as sin_aprobar,
                                                        sum(realizado) as realizado,
                                                        sum(Pendiente) as Pendiente
                                                from (
                                                                select sucursal_nombre as Sucursal,cronograma_detalle_cronograma_id as idCronograma,
                                                                        (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_realiza,
                                                                        case when cronograma_tipo_mantenimiento = 21 then 1 else '' end as Preventivo,
                                                                        case when cronograma_tipo_mantenimiento = 22 then 1 else '' end as Correctivo,
                                                                        case when cronograma_realizado = 1 then 'REALIZADO' else 'PENDIENTE'  end as Estado,
                                                                        cronograma_detalle_fecha as Fecha_de_mantenimiento,cronograma_detalle_id,
                                                                        case when cronograma_detalle_aprobado = 1 then 1 else '' end as aprobado,
                                                                        case when cronograma_detalle_aprobado = 0 then 1 else '' end as sin_aprobar,
                                                                        case when cronograma_detalle_realizado = 1 then 1 else '' end as realizado,
                                                                        case when cronograma_detalle_realizado = 0 then 1 else '' end as Pendiente,
                                                                        (select concat_ws(' ',nombres,apellidos) from users where id=cronograma_quien_realiza) as Quien_aprueba						 						 
                                                                from   equipos
                                                                        join cronogramas_detalles on (equipo_id=cronograma_detalle_equipo_id)
                                                                        join sucursales on (equipo_sucursal_id=sucursal_id)
                                                                        join cronogramas on (cronograma_detalle_cronograma_id=cronograma_id)
                                                                where 1=1                                                                    
                                                                order by Zona_id,sucursal_nombre
                                                            ) as d     
                                                group by Sucursal"); 
            $cronogramaSucursales = $query->queryAll();              

            if ($cronogramageneral){

                return $this->render('reportecronogramas',[
                    'cronogramageneral' => $cronogramageneral,
                    'cronogramaZonas' => $cronogramaZonas,
                    'cronogramaSucursales' => $cronogramaSucursales,
                ]);
            }else{
                echo 'no se encontraron resultados';
            }
    }
    
}
