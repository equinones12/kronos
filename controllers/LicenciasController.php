<?php

namespace app\controllers;

use Yii;
use app\models\Licencias;
use app\models\LicenciasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TiposDetalles;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\User;

/**
 * LicenciasController implements the CRUD actions for Licencias model.
 */
class LicenciasController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin','index','view','create','update'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin','index','view','create','update'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios de recaudo tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user','index','view','create','update'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario de recaudo
                          return User::isUserLinalca(Yii::$app->user->identity->id);
                      },
                    ],
                ],
            ],
             //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
             //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Licencias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LicenciasSearch();
        $model = Licencias::find()->All();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);    
    }    

    /**
     * Displays a single Licencias model.
     * @param integer $id
     * @return mixed
     *
     */
    public function actionView($id)
    {
        
        // se instancia la Clase Query.
        $query = new Query();
        $connection = Yii::$app->db;

        $query = $connection->createCommand('SELECT     *,
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = licencia_tipo_licencia_id) as producto, 
                                                        (select tipo_detalle_nombre from tipos_detalles where tipo_detalle_id = licencia_version) as version_nombre,
                                                        (select     tipo_nombre 
                                                            from    tipos_detalles 
                                                                    join tipos on (tipo_detalle_tipo_id = tipo_id)
                                                            where   tipo_detalle_id = licencia_tipo_licencia_id) as tipo, 
                                                        (select sucursal_nombre from sucursales where sucursal_id = equipo_sucursal_id) as sucursal,
                                                        (select area_nombre from areas where area_id = equipo_area_id) as area

                                                from    licencias 
                                                        join equipos on (licencia_equipo_id = equipo_id)
                                                        join funcionarios on (equipo_usuario_id = funcionario_id)
                                                where   1=1
                                                        and licencia_id ='.$id
                                                );
        $licencias = $query->queryAll();
        // echo "<pre>";
        // print_r($licencias);die();


        return $this->render('view', [
            'model' => $this->findModel($id),
            'licencias' => $licencias,
        ]);
    }

    /**
     * Creates a new Licencias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Licencias();

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/) {

            

            $model->licencia_equipo_id = $_REQUEST['select-funcionarios'];
            $model->licencia_sucursal_id = $_REQUEST['select-sucusales'];
            $model->licencia_tipo_licencia_id = $_REQUEST['select-tiposD'];

            if ($model->save()) {

               return $this->redirect(['view', 'id' => $model->licencia_id]);
            }

            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Licencias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {       
        //    $obs = $_REQUEST['licencia_especificaciones'];
        //    $id = $_REQUEST['id'];
           
           $idlic = Licencias::find()->where(['licencia_id' =>$id])->all(); 
           $usuario = Yii::$app->user->identity->id;           
           $dia = date('Y-m-d H:i:s');
           
        $model = $this->findModel($id); 
        // echo '<pre>';
        // print_r($observacion);
        // die;                 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {           
            $query = new Query();
            $connection = Yii::$app->db;                      
            foreach($idlic as $licencia){ 
             //Query para historico de la licencia    
                $query = $connection->createCommand()->insert('historico_licencias',
                                                                ['historico_licencia_id' => $licencia['licencia_id'],
                                                                'licencia_equipo_id' => $licencia['licencia_equipo_id'],
                                                                'licencia_tipo_licencia_id' => $licencia['licencia_tipo_licencia_id'],
                                                                'licencia_numero' => $licencia['licencia_numero'],
                                                                'licencia_version' => $licencia['licencia_version'],
                                                                'licencia_especificaciones' => $licencia['licencia_especificaciones'],
                                                                'licencia_estado_id' => $licencia['licencia_estado_id'],
                                                                'licencia_sucursal_id' => $licencia['licencia_sucursal_id'],
                                                                'licencia_identificacion_tecnologia' => $licencia['licencia_identificacion_tecnologia'],
                                                                'usuario_modifica' => $usuario,
                                                                'fecha_creacion' => $dia,
                                                                'proceso_id' =>26,
                                                                'observacion' =>$_REQUEST['Licencias']['licencia_especificaciones'],
                                                                ])->execute(); 
               
            }           
                  
            return $this->redirect(['view', 'id' => $model->licencia_id]);
        } else {                      
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Licencias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionLists($id){
     
        $countMunicipios = TiposDetalles::find()->where(['tipo_detalle_tipo_id'=>$id])->count();

        $tiposDetalles = TiposDetalles::find()->where(['tipo_detalle_tipo_id'=>$id])->all();

        if($countMunicipios > 0){
            echo "<option>[--Seleccione Licencia--]</option>";
            foreach ($tiposDetalles as $tiposDetalle) {
                echo "<option value='".$tiposDetalle->tipo_detalle_id."'>".$tiposDetalle->tipo_detalle_nombre."</option>";
            }
        }else{
            echo "<option>Opción Inválida</option>";
        }        
    }



    /**
     * Finds the Licencias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Licencias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Licencias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

