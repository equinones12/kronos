<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = 'Ticket ID : ' . $model->tickets_id;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="tickets-view">

    <h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>

    <!-- Button trigger modal -->
    <?php if ($model->tickets_estado == 0) { ?>
        <button type="button" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
            Cerrar ticket
        </button>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'tickets_id',
            // 'tickets_fechacreacion',
            'tickets_fechacreaciont',
            [
                'attribute' => 'tickets_cliente_id',
                'value' => $model->clientes->clientes_id . ' - ' . $model->clientes->clientes_nombre
            ],
            [
                'attribute' => 'tickets_tipo_soporte',
                'value' => $model->tipoticket->tipo_detalle_nombre
            ],
            
            'tickets_modulo',
            'tickets_proceso',
            // 'tickets_pantallazo',
            'tickets_descripcion:ntext',
            'tickets_contacto_nombre',
            'tickets_contacto_telefono',
            
            [
                'attribute' => 'tickets_estado',
                'value' => $model->tickets_estado == 0 ? 'ABIERTO' : 'CERRADO'
            ],
            [
                'attribute' => 'tickets_quienresponde',
                'value' => $model->tickets_quienresponde,
                'visible' => $model->tickets_estado == 0 ? false : true,
            ],
            [
                'attribute' => 'tickets_fecharespuestat',
                'value' => $model->tickets_fecharespuestat,
                'visible' => $model->tickets_estado == 0 ? false : true,
            ],
            [
                'attribute' => 'tickets_descripcionrespuesta',
                'value' => $model->tickets_descripcionrespuesta,
                'visible' => $model->tickets_estado == 0 ? false : true,
            ],
            [
                'attribute' => 'tickets_tiemporespuesta',
                'value' => $model->tickets_tiemporespuesta,
                'visible' => $model->tickets_estado == 0 ? false : true,
            ]
        ],
    ]) ?>

    <?php if ($model->tickets_pantallazo) { ?>
        <div class="col-lg-12">
            <p><strong>Pantallazo error</strong></p>
            <img class="img-fluid" src="data:image/jpeg;base64,<?php echo base64_encode( $model->tickets_pantallazo ) ?>"/>
        </div>
    <?php } ?>

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary text-white">
        <h5 class="modal-title" id="exampleModalLabel">Cerrar ticket ID: <?php echo $model->tickets_id ?></h5>
      </div>
        <?php $form = ActiveForm::begin([
            'action' => ['tickets/cerraticket']
          ]); ?>
            <div class="modal-body">
                <?= $form->field($model, 'tickets_descripcionrespuesta')->textarea(['rows' => 2, 'required' => true]) ?>
                <input type="hidden" name="id" value="<?php echo $model->tickets_id ?>">
            </div>
            <div class="modal-footer">
                <div class="col-12 noPadding">
                    <div class="col-12 form-group">
                        <?= Html::submitButton('Cerra ticket', ['class' => 'btn btn-primary btn-block']) ?>
                    </div>
                    <div class="col-12 form-group">
                        <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>