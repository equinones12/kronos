<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $titulo;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-index">

    <h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tickets_id',
            'tickets_fechacreacion',
            'tickets_fechacreaciont',
            [
                'attribute' => 'clientes',
                'label' => 'Cliente',
                'value' => 'clientes.clientes_nombre'
            ],
            [
                'attribute' => 'tipoticket',
                'label' => 'Tipo ticket',
                'value' => 'tipoticket.tipo_detalle_nombre'
            ],
            // 'tickets_tipo_soporte',
            //'tickets_modulo',
            //'tickets_proceso',
            //'tickets_pantallazo',
            //'tickets_descripcion:ntext',
            //'tickets_contacto_nombre',
            //'tickets_contacto_telefono',
            //'tickets_estado',
            //'tickets_quienresponde',
            //'tickets_fecharespuesta',
            //'tickets_fecharespuestat',
            //'tickets_descripcionrespuesta:ntext',
            //'tickets_tiemporespuesta',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{responder}',
                'buttons' => [
                    'responder' => function ($url, $model, $key) {
                        if ($model->tickets_estado == 0) {
                            return Html::a('<span class="fas fa-check"></span> Responder ticket', ['view', 'id'=>$model->tickets_id], ['class' => 'btn-sm btn btn-block btn-primary'] );
                        }else{
                            return Html::a('<span class="fas fa-check"></span> Ver ticket', ['view', 'id'=>$model->tickets_id], ['class' => 'btn-sm btn btn-block btn-primary'] );
                        }
                        
                    },
                ]
            ],
        ],
    ]); ?>


</div>
