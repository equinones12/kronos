<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tickets-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tickets_fechacreacion')->textInput() ?>

    <?= $form->field($model, 'tickets_fechacreaciont')->textInput() ?>

    <?= $form->field($model, 'tickets_cliente_id')->textInput() ?>

    <?= $form->field($model, 'tickets_tipo_soporte')->textInput() ?>

    <?= $form->field($model, 'tickets_modulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tickets_proceso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tickets_pantallazo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tickets_descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tickets_contacto_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tickets_contacto_telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tickets_estado')->textInput() ?>

    <?= $form->field($model, 'tickets_quienresponde')->textInput() ?>

    <?= $form->field($model, 'tickets_fecharespuesta')->textInput() ?>

    <?= $form->field($model, 'tickets_fecharespuestat')->textInput() ?>

    <?= $form->field($model, 'tickets_descripcionrespuesta')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tickets_tiemporespuesta')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
