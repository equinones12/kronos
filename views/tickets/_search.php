<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TicketsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tickets-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tickets_id') ?>

    <?= $form->field($model, 'tickets_fechacreacion') ?>

    <?= $form->field($model, 'tickets_fechacreaciont') ?>

    <?= $form->field($model, 'tickets_cliente_id') ?>

    <?= $form->field($model, 'tickets_tipo_soporte') ?>

    <?php // echo $form->field($model, 'tickets_modulo') ?>

    <?php // echo $form->field($model, 'tickets_proceso') ?>

    <?php // echo $form->field($model, 'tickets_pantallazo') ?>

    <?php // echo $form->field($model, 'tickets_descripcion') ?>

    <?php // echo $form->field($model, 'tickets_contacto_nombre') ?>

    <?php // echo $form->field($model, 'tickets_contacto_telefono') ?>

    <?php // echo $form->field($model, 'tickets_estado') ?>

    <?php // echo $form->field($model, 'tickets_quienresponde') ?>

    <?php // echo $form->field($model, 'tickets_fecharespuesta') ?>

    <?php // echo $form->field($model, 'tickets_fecharespuestat') ?>

    <?php // echo $form->field($model, 'tickets_descripcionrespuesta') ?>

    <?php // echo $form->field($model, 'tickets_tiemporespuesta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
