<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <div class="jumbotron">
        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/user.png" alt="..." class="img-circle">
        <p class="lead"><?php echo $model->nombres.' '.$model->apellidos; ?></p>
    </div>

    <!-- informacion personal -->
    
      <div class="card border-secondary">
        <div class="card-header text-success">Informacion personal</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
              <tbody>
                <tr>
                  <td><strong>Nombres: </strong><?php echo $model->nombres; ?></td>
                  <td><strong>Apellidos: </strong><?php echo $model->apellidos; ?></td>
                </tr>
                <tr>
                  <td><strong>Tipo de identificacion: </strong><?php echo $model->tipoidentificacion->nombre; ?></td>
                  <td><strong>Identificacion: </strong><?php echo $model->numeroIdentificacion; ?></td>
                </tr>
                <tr>
                  <td><strong>Direccion: </strong><?php echo $model->direccion; ?></td>
                  <td><strong>Telefono: </strong><?php echo $model->telefono; ?></td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Email: </strong><?php echo $model->email; ?></td>
                </tr>
                <tr>
                  <td><strong>Username: </strong><?php echo $model->username; ?></td>
                  <!-- <td><strong>Rol usuario: </strong><?php //echo $model->rol->nombre; ?></td> -->
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    
    <br>
      <div class="card border-secondary">
        <div class="card-header text-success">Informacion del sistema</div>
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-hover table-bordered">
                <tbody>
                  <tr>
                    <td><strong>Username: </strong><?php echo $model->username; ?></td>
                    <td><strong>Rol usuario: </strong><?php echo $model->rol->nombre; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Estado usuario: </strong><?php echo $model->activate ? 'ACTIVO' : 'INACTIVO'; ?></td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    <br>

    <div class="col-lg-3 col-12 noPadding form-group">
        <?= Html::a('Actualizar informacion', ['update', 'id' => $model->id], ['class' => 'btn-block btn btn-primary']) ?>
    </div> 
</div>
