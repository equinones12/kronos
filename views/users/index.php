<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Usuarios Sistema ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-lg-2 noPadding form-group">
        <?= Html::a('Registrar Usuario', ['site/register'], ['class' => 'btn btn-primary btn-block']) ?>
    </div>
    <div class="table-responsive-xl">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'username',
                'nombres',
                'apellidos',
                // 'numeroIdentificacion',
                'email:email',
                // 'activate',
                // 'id',
                // 'password',
                // 'authKey',
                // 'accessToken',
                // 'role',
                // 'foto',
                // 'tipoIdentificacion',
                'direccion',
                'telefono',
                // 'tipoContrato',
                // 'fechaInicioContrato',
                // 'fechaFinContrato',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-eye"></span>', ['users/view', 'id'=>$model->id], ['title' => 'Ver usuario'] );
                        },
                        'update' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-user-edit"></span>', ['users/update', 'id'=>$model->id], ['title' => 'Ver usuario'] );
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
