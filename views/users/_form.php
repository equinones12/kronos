<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use app\models\TipoIdentificacion;
use kartik\date\DatePicker;

use kartik\select2\Select2;
use yii\bootstrap4\BootstrapPluginAsset;
use yii\bootstrap4\BootstrapAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>


    <!-- ============== informacion personal -->
    <div class="row noPadding">
      <div class="col-lg-6 form-group">
        <?= $form->field($model, "nombres") ?>   
      </div>
      <div class="col-lg-6 form-group">
        <?= $form->field($model, "apellidos") ?>   
      </div>
    </div>
            <div class="row noPadding">
              <div class="col-lg-6 form-group">
                  <?php
                      echo $form->field($model, 'tipoIdentificacion')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(TipoIdentificacion::find()
                                ->all(), 'id', 'nombre'),
                        'options' => ['placeholder' => 'Seleccione tipo de identificacion'],
                        'language' => 'es',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    
                  ?>
                  <div class="help-block"></div>
              </div>
              <div class="col-lg-6 form-group">
                <?= $form->field($model, "numeroIdentificacion") ?>  
              </div>
            </div>
            <div class="row noPadding">
                <div class="col-lg-6 form-group">
                 <?= $form->field($model, "direccion") ?>   
                </div>

                <div class="col-lg-6 form-group">
                 <?= $form->field($model, "telefono") ?>   
                </div>
            </div>
            <div class="row noPadding">
                <div class="col-lg-6 form-group">
                    <?= $form->field($model, "email")->input("email") ?>   
                </div>
            </div>
            <div class="row noPadding">
              <?php if (yii::$app->user->identity->role == 1) { ?>
                <div class="col-lg-6 form-group">
                <?= $form->field($model, "username") ?>   
                </div>
              <?php } ?>
              <div class="col-lg-6 form-group">
                  <?= $form->field($model, "password")->input("password") ?>   
              </div>
            </div>
            <div class="row noPadding">
              <?php if (yii::$app->user->identity->role == 1) { ?>
                <div class="col-lg-6 form-group">
                      <?php

                          echo $form->field($model, 'role')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Rol::find()
                                    ->all(), 'rol_id', 'nombre'),
                            'options' => ['placeholder' => 'Seleccione Rol'],
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        
                      ?>
                      <div class="help-block"></div>
                  </div>
                  <div class="form-group col-lg-6">
                      <label>Estado usuario</label>
                      <?php
                          echo $form->field($model, 'activate')->widget(Select2::classname(), [
                              'data' => [
                                1 => 'ACTIVO',
                                0 => 'INACTIVO'
                              ],
                              'options' => ['placeholder' => 'Seleccione Cliente'],
                              'language' => 'es',
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ])->label(false);
                      ?>
                  </div>
              <?php } ?>
            </div>

    <div class="col-lg-3 col-12 noPadding form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear Usuario' : 'Actualizar Usuario', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
