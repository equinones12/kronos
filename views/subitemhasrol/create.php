<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubitemHasRol */

$this->title = 'Create Subitem';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subitem-has-rol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rol' => $rol,
        'item' => $item,
    ]) ?>

</div>
