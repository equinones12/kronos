<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubitemHasRol */

$this->title = 'Actualizar Subitem: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subitem-has-rol-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rol' => $rol,
        'item' => $item,
        
    ]) ?>

</div>
