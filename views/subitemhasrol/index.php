<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubitemHasRolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subitem Has Rols';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subitem-has-rol-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subitem Has Rol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_subitem',
            'id_rol',
            'estado',
            'orden',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
