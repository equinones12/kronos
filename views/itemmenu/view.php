<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-menu-view">

    <h2 class="alert alert-info"><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'action',
            'etiqueta',
            'id_opcion',
        ],
    ]) ?>

</div>
