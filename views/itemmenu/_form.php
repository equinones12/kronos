<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\OpcionMenu;

/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12 noPadding">
        <div class="col-lg-6">
            <?= $form->field($model, 'etiqueta')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>        
        </div>
    </div>
    <div class="col-lg-12 noPadding">
        <div class="col-lg-6">
            <?= Html::label('Opcion', 'id_opcion', ['class'=>'control-label']) ?>
            <?= Html::activeDropDownList(
                $model,
                'id_opcion',
                ArrayHelper::map(OpcionMenu::find()
                        ->all(), 'id', 'nombre'),
                [
                    'class'=>'form-control',
                    'prompt'=>'[-- Seleccione Opcion --]',
                    'value'=> '',
                    'required' => 'required'
                ]) 
            ?>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="form-group col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
