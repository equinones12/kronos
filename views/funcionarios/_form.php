<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TiposDetalles;
use yii\helpers\ArrayHelper;
use app\models\Sucursales;
/* @var $this yii\web\View */
/* @var $model app\models\Funcionarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="funcionarios-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Registro de Funcionario</div>
        <div class="panel-body">    

            <?= $form->field($model, 'funcionario_nombres')->textInput(['maxlength' => true]) ?>

            <!-- <?= $form->field($model, 'funcionario_sucursal_id')->textInput() ?> -->
            <div class="form-group">
                        <?= $form->field($model, 'funcionario_sucursal_id')->dropDownList(ArrayHelper::map(Sucursales::find()
                                            ->orderBy(['sucursal_nombre'=>SORT_ASC])->all(), 'sucursal_id', 'sucursal_nombre'), 
                                     [
                                    'prompt'=>'[-- Seleccione Sucursal--]',
                                    ]
                                ) ?>
                        <div class="help-block"></div>
                    </div>

            <?= $form->field($model, 'funcionario_cargo')->textInput(['maxlength' => true]) ?>

            <!-- <?= $form->field($model, 'funcionario_estado_id')->textInput() ?> -->

            <div class="form-group">
                        <?= $form->field($model, 'funcionario_estado_id')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                        where(['tipo_detalle_tipo_id'=>2])
                                            ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                     [
                                    'prompt'=>'[-- Seleccione Tipo--]',
                                    ]
                                ) ?>
                        <div class="help-block"></div>
                    </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
