<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Funcionarios */

$this->title = $model->funcionario_id;
$this->params['breadcrumbs'][] = ['label' => 'Funcionarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcionarios-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->funcionario_id], ['class' => 'btn btn-primary']) ?>
        <!-- <?= Html::a('Delete', ['delete', 'id' => $model->funcionario_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> -->
    </p>
    <div class="panel panel-primary">
        <div class="panel-heading">Información del Funcionario</div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'funcionario_id',
                    'funcionario_nombres',
                    
                    [
                        'attribute' => 'funcionario_sucursal_id',
                        'label' => 'Sucursal',
                        'value' => $model->sucursales->sucursal_nombre
                    ],
                    'funcionario_cargo',
                    [
                        'attribute' => 'funcionario_estado_id',
                        'label' => 'Estado',
                        'value' => $model->tiposdetalles->tipo_detalle_nombre
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
