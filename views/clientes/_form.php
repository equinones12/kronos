<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientes_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clientes_nit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clientes_direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clientes_fechacreacion')->hiddenInput(['value' => date('Y-m-d') ])->label(false) ?>


    <div class="form-group col-lg-3 noPadding">
        <?= Html::submitButton('Guardar cliente', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
