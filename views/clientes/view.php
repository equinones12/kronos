<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->clientes_id;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clientes-view">

    <h3 class="alert alert-primary">Cliente ID <?= Html::encode($this->title) ?></h3>

    <div class="col-lg-3 noPadding form-group">
        <?= Html::a('Actualizar cliente', ['update', 'id' => $model->clientes_id], ['class' => 'btn btn-primary btn-block']) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'clientes_id',
            'clientes_nombre',
            'clientes_nit',
            'clientes_direccion',
            'clientes_fechacreacion',
        ],
    ]) ?>

</div>
