<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-index">

    <h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Crear Cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'clientes_id',
                'clientes_nombre',
                'clientes_nit',
                'clientes_direccion',
                'clientes_fechacreacion',
                'clientes_licencia',
                'clientes_licencia_fecha_desde',
                'clientes_licencia_fecha_hasta',
                'clientes_tipolicencia',
                'clientes_cantidadusuarios',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}{generarlicencia}{imprimirlicencia}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-eye"></span> Ver cliente', ['clientes/view', 'id'=>$model->clientes_id], ['title' => 'Ver cliente', 'class' => 'btn btn-primary btn-block btn-sm'] );
                        },
                        'update' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-user-edit"></span> Editar cliente', ['users/update', 'id'=>$model->clientes_id], ['title' => 'Editar cliente', 'class' => 'btn btn-block btn-warning btn-sm'] );
                        },
                        'update' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-user-edit"></span> Editar cliente', ['users/update', 'id'=>$model->clientes_id], ['title' => 'Editar cliente', 'class' => 'btn btn-block btn-warning btn-sm'] );
                        },
                        'generarlicencia' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::button('<span class="fas fa-id-badge"></span> Generar licencia', [ 'id' => $model->clientes_id, 'data-nombre' => $model->clientes_nombre, 'title' => 'Generar licencia', 'class' => 'btn btn-block btn-info btn-sm generarlicencia' ] );
                        },
                        'imprimirlicencia' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::a('<span class="fas fa-print"></span> Descargar licencia', ['pdf/licencia', 'id'=>$model->clientes_id], ['title' => 'Imprimir licencia', 'class' => 'btn btn-block btn-dark btn-sm'] );
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="generadorlicencias" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <?php $form = ActiveForm::begin([
        'action' => ['generarlicencia']
    ]); ?>

        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title" id="exampleModalLabel">Generador de licencia</h5>
            </div>
            <div class="modal-body">
                <p>Generador de licencia para <strong id="namecliente"></strong> </p>
                
                <div class="row form-group">
                    
                    <input type="hidden" name="id" id="id">
                    <div class="col-lg-12 form-group">
                        <label>Tipo de licencia</label>
                        <select name="tipolicencia" class="form-control">
                            <option value="Licencia web basica consumo por usuarios">Licencia web basica consumo por usuarios</option>
                            <option value="Licencia web basica por cantidad de usuarios">Licencia web basica por cantidad de usuarios</option>
                            <option value="Licencia web Lite">Licencia web Lite</option>
                            <option value="Licencia web Pro">Licencia web Pro</option>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>Licencia desde</label>
                        <input type="date" name="fechadesde" class="form-control" value="<?php echo date('Y-m-d') ?>" required>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label>Licencia hasta</label>
                        <input type="date" name="fechahasta" class="form-control" value="<?php echo date('Y-m-d') ?>" required>
                    </div>
                    <div class="col-lg-12 form-group">
                        <label>Cantidad de usuario</label>
                        <input type="number" name="cantidadusuarios" class="form-control">
                    </div>
                </div>

                <hr>
                <button type="submit" class="btn btn-info btn-block">Generar licencia</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cancelar</button>

            </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="modal fade" id="modallicenciasvencidas" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning text-white">
                <h5 class="modal-title" id="exampleModalLabel">Licencias vencidas</h5>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Direccion</th>
                                <th>Numero licencia</th>
                                <th>Fecha desde</th>
                                <th>Fecha hasta</th>
                                <th>Plan</th>
                                <th>Cantidad usuarios</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vencimientolicencia as $vl) { ?>
                                <tr>
                                    <td><?php echo $vl->clientes_nit . ' - ' . $vl->clientes_nombre ?></td>
                                    <td><?php echo $vl->clientes_direccion ?></td>
                                    <td><?php echo $vl->clientes_licencia ?></td>
                                    <td><?php echo $vl->clientes_licencia_fecha_desde ?></td>
                                    <td><?php echo $vl->clientes_licencia_fecha_hasta ?></td>
                                    <td><?php echo $vl->clientes_tipolicencia ?></td>
                                    <td><?php echo $vl->clientes_cantidadusuarios ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <button type="button" class="btn btn-warning btn-block" data-dismiss="modal">Entendido</button>
            </div>
        </div>
    </div>

</div>
<!-- ================ Js =============== -->
<script>
    $(document).ready(function(){
        $('.generarlicencia').click(function(){
            $('#namecliente').text( $(this).data('nombre') );
            $('#id').val( $(this).attr('id') );
            $('#generadorlicencias').modal();
        });
        <?php if ($vencimientolicencia) { ?>
            $('#modallicenciasvencidas').modal();
        <?php } ?>
        
    })
</script>