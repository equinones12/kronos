<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = 'Actualizar Cliente: ' . $model->clientes_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->clientes_id, 'url' => ['view', 'id' => $model->clientes_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clientes-update">

<h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
