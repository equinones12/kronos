<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RbackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rback-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idrback') ?>

    <?= $form->field($model, 'idRol') ?>

    <?= $form->field($model, 'idControlador') ?>

    <?= $form->field($model, 'idAccion') ?>

    <?= $form->field($model, 'estadoRback') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
