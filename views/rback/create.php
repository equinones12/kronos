<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rback */

$this->title = 'Create Rback';
$this->params['breadcrumbs'][] = ['label' => 'Rbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'idRol' => $idRol,
        'rol' => $rol,
        'controlador' => $controlador,
    ]) ?>

</div>
