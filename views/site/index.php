<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\models\Users;
    use yii\bootstrap4\ActiveForm;
    $this->title = 'Panel de Control';
?>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="row">
        <?php if ( in_array(1, $accesosDirectos) ) { // CANTIDAD DE EQUIPOS ?>
            <div class="col-lg-6 col-xs-12">            
                <!-- small box -->
                <div class="small-box bg-primary">
                    <div class="inner">
                        <center><h3>Cantidad de equipos</h3></center>
                        <center><h4 class="btn btn-dark"><?php foreach($equipos as $e){} echo $e['cuenta']?></h4></center>
                    </div>
                    <div class="icon">
                     <i class="fas fa-laptop"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=equipos%2Findex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>  
        <?php } ?>
        <?php if ( in_array(2, $accesosDirectos) ) { // CANTIDAD DE LICENCIAS ?>
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <center><h3>Cantidad de licencias </h3></center>
                        <center><h4 class="btn btn-dark"><?php foreach($licencias as $l){} echo $l['cuenta']?></h4></center>
                    </div>
                    <div class="icon">
                     <i class="fab fa-microsoft"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=licencias%2Findex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php } ?>
        <?php if ( in_array(3, $accesosDirectos) ) { // CRONOGRAMA DE MANTENIMIENTO ?>
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <center><h3>Cronogramas de Mantenimiento </h3></center>
                        <center><p>
                            <?= Html::a('Ver Pendientes', ['cronogramas/index'], ['class' => 'btn btn-dark']) ?>                            
                        </p></center>
                    </div>
                    <div class="icon">
                    <i class="fas fa-calendar-alt"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=cronogramas%2Findex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php } ?> 
        <?php if ( in_array(4, $accesosDirectos) ) { // ESTADISTICAS ?>
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <center><h3>Estadisticas </h3></center>
                        <center><p>
                            <?= Html::a('Estadistica equipos', ['estadisticas/estadisticasequipos'], ['class' => 'btn btn-dark']) ?>
                            <?= Html::a('Estadistica licencias', ['estadisticas/estadisticaslicencias'], ['class' => 'btn btn-dark']) ?>
                            <?= Html::a('Estadistica Cronogramas', ['estadisticas/estadisticascronogramas'], ['class' => 'btn btn-dark ']) ?>
                            <?= Html::a('Estadistica Soportes', ['estadisticas/estadisticassoportes'], ['class' => 'btn btn-dark ']) ?>
                        </p></center>
                    </div>
                    <div class="icon">
                    <i class="fas fa-chart-line"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=estadisticas%2Festadisticasequipos" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php } ?> 
        <?php if ( in_array(4, $accesosDirectos) ) { // MANTENIMIENTOS ASIGNADOS ?>
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <center><h3>Mantenimientos asignados</h3></center>
                        <center><p>
                            <?php foreach($result as $value) {?>                                
                                <a href ="<?php echo Url::base(); ?>/index.php?r=cronogramas%2Fview&id=<?php echo $value['cronograma_id']?>" Target="_blank"><h6 class="btn btn-dark">Sucursal <?php echo $value['sucursal_nombre'].': '. $value['cuenta']?></h6></a>
                            <?php } ?>
                        </p></center>
                    </div>
                    <div class="icon">
                    <i class="fas fa-user-cog"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=cronogramas%2Findex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php } ?> 
        <?php if (in_array(4, $accesosDirectos)){  // REPORTES?> 
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <center><h3>Soportes</h3></center>
                        <center><p>
                        <h6 class="btn btn-dark">Realizados:  <?php echo $soporrealizados?></h6>
                        <h6 class="btn btn-dark">Pendientes:  <?php echo $soporpendientes?></h6>                           
                        </p></center>
                    </div>
                    <div class="icon">
                    <i class="fas fa-toolbox"></i>
                    </div>
                    <a href="<?php echo Url::base(); ?>/index.php?r=soportes%2Findex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div> 
        <?php } ?>
    </div>

<!-- Load -->
<div class="modal fade" id="load" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-body text-center">
            <div class="loader">
                <!-- <img src="<?php echo Url::base(); ?>/images/loading.gif" alt=""> -->
            </div>
            <div clas="loader-txt">
            <p>Espere un momento por favor. <br><br><small>Se esta procesando la solicitud</small></p>
            </div>
        </div>
        </div>
    </div>
</div>



<!-- MODAL PARA REASIGNAR LA SUBACTIVIDAD -->
<div id="modalconsulta" class="modal fade" role="dialog"></div>

<!--  ========= Js ======== -->
<script>
$(document).on('click','.gestion',function(){      

 })         
</script>

