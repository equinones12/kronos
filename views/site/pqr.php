<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Consultas y Reclamos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about col-lg-12 container-fluid">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Si tiene alguna Consulta o Reclamo relacionado con el pago de los Derechos de Autor y Conexos en su establecimiento, por favor llene el siguiente formulario de contacto</p>
    
    <!-- <div class="col-lg-12" style="display:none">
    <iframe src="http://35.175.13.101/OsaIntranetPrueba/web/index.php?r=servicio-al-cliente%2Fformulariosolicitudes" >
    
    </iframe>
    </div>
    
    <iframe src="<?php echo Url::to('http://35.175.13.101/OsaIntranetPrueba/web/index.php?r=servicio-al-cliente%2Fformulariosolicitudes');?>" height="800" width="1000"></iframe> -->
</div>
