<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap4\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
]); ?>

    <div class="row justify-content-md-center form-login">
        <div class="col-md-4 col-md-auto contentLogin">
            <center>
                <br>
                <img class="img-responsive" width="50%" src="<?php echo Url::base(); ?>/images/OSA.png">
                <br>
                <br>
            </center>
            <div class="col-lg-12">
                <div class="col-lg-12 noPadding">
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'username')->textInput(['placeholder' => 'Usuario', 'class' => 'form-control'])->label(false) ?>
                        <i class="fas fa-user user-icon"></i>
                    </div>
                </div>
                <div class="col-lg-12 noPadding">
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'password')->passwordInput(["data-toggle"=>"password",'placeholder' => 'Contraseña', 'class' => 'form-control'])->label(false) ?>
                        <i class="fas fa-key user-icon" ></i>
                    </div>
                </div>
                <div class="form-group col-lg-12 noPadding">
                        <br>
                        <div class="col-lg-12 noPadding ol-lg-offset-2">
                            <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-success btn-block text-bold', 'name' => 'login-button']) ?>
                        </div>
                </div>
            </div>
                <br>
            <center>
            <p>Siguenos en:</p>
            <a target="_blank" href="https://www.facebook.com/UniLibertadores/"><i class="fab fa-facebook fa-2x"></i></a>
            </center>
            
            <?= Html::a('<i class="fas fa-home"></i> Regresar al inicio', ['site/login'], ['class' => 'btn-sm btn-block btn btn-danger', 'style' => 'margin-top:10px;']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>