<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
?>
<div class="index-login">
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>Sistema Full App Ventor</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/blog/">
    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="blog.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
          <div class="col-4 pt-1">
          </div>
          <div class="col-4 text-center">
            <a class="blog-header-logo text-dark" href="#">AppVentor FULL 2020-2</a>
          </div>
          <div class="col-4 d-flex justify-content-end align-items-center">
            <a class="text-muted" href="#">
            </a>
            <?= Html::a('Iniciar Sesión', ['loginindex'], ['class' => 'btn btn-sm btn-outline-secondary']) ?>
          </div>
        </div>
      </header>
      <!-- MENU DEL LA PAGINA WEB -->
      <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
          <!-- <a class="p-2 text-muted" href="#">Inicio</a> -->
          <?= Html::a('Inicio', ['login'], ['class' => 'p-2 text-muted']) ?>
          <!-- <a class="p-2 text-muted" href="#">Noticias</a> -->
          <?= Html::a('Noticias', ['noticias'], ['class' => 'p-2 text-muted']) ?>
          <!-- <a class="p-2 text-muted" href="#">Material Didactico</a> -->
          <?= Html::a('Material Didactico', ['material'], ['class' => 'p-2 text-muted']) ?>
          <!-- <a class="p-2 text-muted" href="#">Anuncios</a> -->
          <?= Html::a('Anuncios', ['anuncios'], ['class' => 'p-2 text-muted']) ?>
          <!-- <a class="p-2 text-muted" href="#">Videos</a> -->
          <!-- <?= Html::a('Videos', ['videos'], ['class' => 'p-2 text-muted']) ?> -->
        </nav>
      </div>
      <!-- IMG PRINCIPAL -->
      <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">NOTICIAS</h1>
          <p class="lead my-3">Aquí todas las noticias de este aplicativo web.</p>
        </div>
      </div>
    </div>

    <!-- PRIMER BLOQUE NOTICIAS -->
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main">
                <div class="row mb-2">
                <?php foreach($noticias as $key => $value){ ?>
                    <!-- aqui inicia la parte dinamica en cada uno de los bloques -->
                    <div class="col-md-6">
                        <div class="card flex-md-row mb-4 box-shadow h-md-250">
                            <div class="card-body d-flex flex-column align-items-start">
                            <strong class="d-inline-block mb-2 text-primary"></strong>
                            <h3 class="mb-0">
                                <a class="text-dark" href="#"><?php echo $value['contenido_titulo'] ?></a>
                            </h3>
                            <div class="mb-1 text-muted"><?php echo $value['contenido_fecha_creacion'] ?></div>
                            <p class="card-text mb-auto"><?php echo $value['contenido_texto'] ?></p>
                            </div>
                            <?php if($value['contenido_tipo_archivo'] == 9){ ?>
                                <img class="card-img-right flex-auto d-none d-md-block" width="200" heigth="250"  src="<?php echo Url::base(); ?>/<?php echo $value['contenido_imagen'] ?> ">
                            <?php }else{ ?>
                                <img class="card-img-right flex-auto d-none d-md-block" width="200" heigth="250"  src="<?php echo Url::base(); ?>/contenido/imagenes/imagen1.png ">
                            <?php } ?>
                        </div>
                    </div>
                    <!-- fin parte dinamica -->
                <?php } ?>
                </div>
            </div><!-- /.blog-main -->
        </div><!-- /.row -->
    </main>