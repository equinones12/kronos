<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
?>
    
<div class="container">
    <div class="card card-container">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <!-- <img id="profile-img" class="" src="<?php echo Url::base(); ?>/images/img/StellarWayLogo.png" /> -->
        <center><b><h1>Kronos</h1></b></center>
        <p id="profile-name" class="profile-name-card"></p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'class'=> 'form-signin'
        ]); ?>
            <span id="reauth-email" class="reauth-email"></span>

            <?= $form->field($model, 'username')->textInput(['placeholder' => 'Usuario', 'class' => 'form-control'])->label(false) ?>        
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Contraseña', 'class' => 'form-control'])->label(false) ?>
                    
            <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-success btn-block text-bold', 'name' => 'login-button']) ?>
        <?php ActiveForm::end(); ?><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->
