<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
?>
 

 

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
<div class="card col-lg-12 container-fluid">
    <div class="card-body">

        <h3><?= $msg ?></h3>

        <div class="row justify-content-md-center form-login">
            <div class="col-md-4 col-md-auto contentLogin">
                <center>        
                    <h1>Recuperar Contraseña</h1>
                    <h1 style="font-size: 55px; margin-top: 35px; margin-bottom: 30px;" class="text-success text-bold">OSA <span style="color: black; font-size: 25px; margin-left: -20px;">Virtual</span> </h1>
                </center>
                <div class="form-group">
                    <div class="alert alert-primary" role="alert">
                        <p>Por favor ingrese el Correo electronico con el que se registro</p>                
                    </div>
                    <?= $form->field($model, "email")->input("email")->label('Correo Electronico') ?>
                </div>
                    <?= Html::submitButton("Recuperar contraseña", ["class" => "btn btn-success"]) ?>
                <?php $form->end() ?>
                <?= Html::a('Regresar', ['site/login'], ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
</div>