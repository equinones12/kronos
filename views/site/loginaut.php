<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
]); ?>

    <?php if (isset($msg)) { ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $msg; ?>
        </div>
    <?php } ?>
    
    <div class="row justify-content-md-center form-login">
        <div class="col-md-4 col-md-auto contentLogin">
            <center>
                <br>
                <img class="img-responsive" width="50%" src="<?php echo Url::base(); ?>/images/OSA.png">
                <br>
                <br>
            </center>
            <div class="col-lg-12">
                <div class="col-lg-12 noPadding">
                    <div class="form-group has-feedback">
                        <i class="icon-key"></i>
                        <?= $form->field($model, 'autoridad_code')->Input(['placeholder' => 'Codigo de Ingreso', 'class' => 'form-control'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group col-lg-12 noPadding">
                        <br>
                        <div class="col-lg-12 noPadding ol-lg-offset-2">
                            <?= Html::submitButton('Ingresar a la Plataforma', ['class' => 'btn btn-success btn-block text-bold', 'name' => 'login-button']) ?>
                        </div>
                </div>
            </div>
             <br>
             <br>
            <center>
            <p>Siguenos en:</p>
            <a target="_blank" href="https://www.facebook.com/OrganizacionSaycoAcinpro/"><i class="fab fa-facebook fa-2x"></i></a>
            </center>
            
        </div>
    </div>

<?php ActiveForm::end(); ?>