<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

// echo "<pre>";
// print_r($equiposGeneral);die();

// foreach ($datos as $key => $value) {
// 	# code...
// }

// die();
$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");


	$objPHPExcel->createSheet(0);
	$objPHPExcel->createSheet(1);
	$objPHPExcel->createSheet(2);

	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$from = "A1"; // or any value
    $to = "M1"; // or any value
    $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('P2:P100000')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
   	foreach(range('A1','AG1') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'SUCURSALES')
	            ->setCellValue('C1', 'EQUIPO NOMBRE ACTUAL')
                ->setCellValue('D1', 'CRONOGRAMA FECHA INICIO')
                ->setCellValue('E1', 'CRONOGRAMA FECHA FIN')
                ->setCellValue('F1', 'QUIEN REALIZA')
                ->setCellValue('G1', 'TIPO DE MANTENIMIENTO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'FECHA DE MANTENIMIENTO')
                ->setCellValue('J1', 'ESTADO DE MANTENIMIENTO')
                ->setCellValue('K1', 'REALIZADO')
                ->setCellValue('L1', 'QUIEN APRUEBA ')
                ->setCellValue('M1', 'FECHA MANTENIMIENTO')
                ->setCellValue('N1', 'HORA INICIO')
                ->setCellValue('O1', 'HORA FIN')
                ->setCellValue('P1', 'TIEMPO UTILIZADO');                
	$count = 2; 
    }

	foreach ($cronogramageneral as $keyG => $valueG) {
		
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $valueG['zona'])
		            ->setCellValue('B'.$count, $valueG['Sucursal'])
		            ->setCellValue('C'.$count, $valueG['equipo_nombre_actual'])
		            ->setCellValue('D'.$count, $valueG['cronograma_fecha_inicio'])
		            ->setCellValue('E'.$count, $valueG['cronograma_fecha_fin'])                    
                    ->setCellValue('F'.$count, $valueG['Quien_realiza'])
		            ->setCellValue('G'.$count, $valueG['Tipo_mantenimiento'])
                    ->setCellValue('H'.$count, $valueG['Estado'])
                    ->setCellValue('I'.$count, $valueG['Fecha_de_mantenimiento'])
                    ->setCellValue('J'.$count, $valueG['Estado_manteniento'])
                    ->setCellValue('K'.$count, $valueG['Mantenimiento_realizado'])
                    ->setCellValue('L'.$count, $valueG['Quien_aprueba'])
                    ->setCellValue('M'.$count, $valueG['cronograma_detalle_fecha'])                 
                    ->setCellValue('N'.$count, $valueG['Hora_inicio'])
                    ->setCellValue('O'.$count, $valueG['Hora_fin'])
                    ->setCellValue('P'.$count, $valueG['Tiempo_utilizado']); 


		$count++;
	}


	$objPHPExcel->getActiveSheet()->setTitle('Detalle general');
	$objPHPExcel->setActiveSheetIndex(0);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(1)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'CRONOGRAMAS')
                ->setCellValue('C1', 'MANTENIMIENTOS')
                ->setCellValue('D1', 'PREVENTIVOS')
                ->setCellValue('E1', 'CORRECTIVOS')
                ->setCellValue('F1', 'APROBADO')
                ->setCellValue('G1', 'SIN APROBAR')
                ->setCellValue('H1', 'REALIZADO')
                ->setCellValue('I1', 'PENDIENTE');

	$countS = 2;
	

	foreach ($cronogramaZonas as $keyS => $valueS) {
		
		$objPHPExcel->setActiveSheetIndex(1)
		            ->setCellValue('A'.$countS, $valueS['zona'])
		            ->setCellValue('B'.$countS, $valueS['Cronogramas'])
		            ->setCellValue('C'.$countS, $valueS['Mantenimientos'])
                    ->setCellValue('D'.$countS, $valueS['Preventivos'])
                    ->setCellValue('E'.$countS, $valueS['Correctivos'])                    
                    ->setCellValue('F'.$countS, $valueS['aprobado'])
                    ->setCellValue('G'.$countS, $valueS['sin_aprobar'])
                    ->setCellValue('H'.$countS, $valueS['realizado'])
                    ->setCellValue('I'.$countS, $valueS['Pendiente']);

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por zona');
	$objPHPExcel->setActiveSheetIndex(1);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(2)
                    ->setCellValue('A1', 'SUCURSAL')
                    ->setCellValue('B1', 'CRONOGRAMAS')
                    ->setCellValue('C1', 'MANTENIMIENTOS')
                    ->setCellValue('D1', 'PREVENTIVOS')
                    ->setCellValue('E1', 'CORRECTIVOS')
                    ->setCellValue('F1', 'APROBADO')
                    ->setCellValue('G1', 'SIN APROBAR')
                    ->setCellValue('H1', 'REALIZADO')
                    ->setCellValue('I1', 'PENDIENTE');               


	$countS = 2;
	

	foreach ($cronogramaSucursales as $keyS => $valueM) {
		
		$objPHPExcel->setActiveSheetIndex(2)
                    ->setCellValue('A'.$countS, $valueM['Sucursal'])
                    ->setCellValue('B'.$countS, $valueM['Cronogramas'])
                    ->setCellValue('C'.$countS, $valueM['Mantenimientos'])
                    ->setCellValue('D'.$countS, $valueM['Preventivos'])
                    ->setCellValue('E'.$countS, $valueM['Correctivos'])                    
                    ->setCellValue('F'.$countS, $valueM['aprobado'])
                    ->setCellValue('G'.$countS, $valueM['sin_aprobar'])
                    ->setCellValue('H'.$countS, $valueM['realizado'])
                    ->setCellValue('I'.$countS, $valueM['Pendiente']);

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por sucursal');
	$objPHPExcel->setActiveSheetIndex(2);


	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="estadistica_mantenimientos_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>