<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

// echo "<pre>";
// print_r($equiposGeneral);die();

// foreach ($datos as $key => $value) {
// 	# code...
// }

// die();
$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");


	$objPHPExcel->createSheet(0);
	$objPHPExcel->createSheet(1);
	$objPHPExcel->createSheet(2);

	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$from = "A1"; // or any value
    $to = "M1"; // or any value
    $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
   	foreach(range('A1','AG1') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'SUCURSALES')
	            ->setCellValue('C1', 'AREA')
                ->setCellValue('D1', 'NOMBRE DEL EQUIPO')
                ->setCellValue('E1', 'MARCA')
                ->setCellValue('F1', 'ESTADO')
                ->setCellValue('G1', 'RESPONSABLE')
                ->setCellValue('H1', 'CAPACIDAD RAM')
                ->setCellValue('I1', 'TIPO RAM')
                ->setCellValue('J1', 'CAPACIDAD DISCO DURO')
                ->setCellValue('K1', 'PROCESADOR')
                ->setCellValue('L1', 'SISTEMA OPERATIVO')
                ->setCellValue('M1', 'LICENCIA OFFICE');                
	$count = 2; 
    }

	foreach ($equiposGeneral as $keyG => $valueG) {
		
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $valueG['Zona_id'])
		            ->setCellValue('B'.$count, $valueG['Sucursales'])
		            ->setCellValue('C'.$count, $valueG['area'])
		            ->setCellValue('d'.$count, $valueG['equipo_nombre_actual'])
		            ->setCellValue('e'.$count, $valueG['Marca_de_equipo'])                    
                    ->setCellValue('f'.$count, $valueG['Estado'])
		            ->setCellValue('g'.$count, $valueG['Responsable'])
                    ->setCellValue('h'.$count, $valueG['Capacidad_Ram'])
                    ->setCellValue('i'.$count, $valueG['Tipo_de_ram'])
                    ->setCellValue('j'.$count, $valueG['Capacidad_disco_duro'])
                    ->setCellValue('k'.$count, $valueG['Procesasador'])
                    ->setCellValue('l'.$count, $valueG['Sistema_operativo'])
                    ->setCellValue('m'.$count, $valueG['Licencia_officce']);                  


		$count++;
	}


	$objPHPExcel->getActiveSheet()->setTitle('Reporte general');
	$objPHPExcel->setActiveSheetIndex(0);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(1)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'EQUIPOS ACTIVOS')
                ->setCellValue('C1', 'EQUIPOS INACTIVOS')
                ->setCellValue('D1', 'EQUIPOS DADOS DE BAJA')
                ->setCellValue('E1', 'TOTAL EQUIPOS');

	$countS = 2;
	

	foreach ($equiposZona as $keyS => $valueS) {
		
		$objPHPExcel->setActiveSheetIndex(1)
		            ->setCellValue('A'.$countS, $valueS['Zona'])
		            ->setCellValue('B'.$countS, $valueS['Equipos_activos'])
		            ->setCellValue('C'.$countS, $valueS['Equipos_inactivos'])
                    ->setCellValue('D'.$countS, $valueS['Equipos_dado_bajas'])
                    ->setCellValue('E'.$countS, $valueS['total_equipos']);                    

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por zona');
	$objPHPExcel->setActiveSheetIndex(1);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(2)
                ->setCellValue('A1', 'SUCURSAL')
                ->setCellValue('B1', 'EQUIPOS ACTIVOS')
                ->setCellValue('C1', 'EQUIPOS INACTIVOS')
                ->setCellValue('D1', 'EQUIPOS DADOS DE BAJA')
                ->setCellValue('E1', 'TOTAL EQUIPOS');                


	$countS = 2;
	

	foreach ($equiposSuc as $keyS => $valueM) {
		
		$objPHPExcel->setActiveSheetIndex(2)
                    ->setCellValue('A'.$countS, $valueM['sucursal'])
                    ->setCellValue('B'.$countS, $valueM['Equipos_activos'])
                    ->setCellValue('C'.$countS, $valueM['Equipos_inactivos'])
                    ->setCellValue('D'.$countS, $valueM['Equipos_dado_bajas'])
                    ->setCellValue('E'.$countS, $valueM['total_equipos']);

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por sucursal');
	$objPHPExcel->setActiveSheetIndex(2);


	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="estadistica_equipos_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>