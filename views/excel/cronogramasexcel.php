<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;
use yii\helpers\Url;

// echo "<pre>";
// print_r($Cronograma);
// print_r($CronogramaDetalles);
// die();

// foreach ($datos as $key => $value) {
// 	# code...
// }

// die();
$objPHPExcel = new PHPExcel(); 
// $ = new PHPExcel_Worksheet_HeaderFooterDrawing(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A7:P7")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A8:F14")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A15:P15")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A16:P16")->getFont()->setBold(true);

   	// foreach(range('A','Z') as $columnID) {
   	//     $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	//         ->setAutoSize(true);
   	// }

   	// CARGA DEL LOGO DE LA OSA
	$gdImage = imagecreatefromjpeg('images/osa.jpg'); 
	$objDrawing = new PHPExcel_Worksheet_MemoryDrawing(); 
	$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image'); 
	$objDrawing->setImageResource($gdImage); 
	$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG); 
	$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT); 
	$objDrawing->setHeight(120);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');$objWriter->save(str_replace('.php', '.xlsx', __FILE__)); 
	// ALINEACION DEL TITULO AL LADO DE LA IMAGEN
	$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);


	$objPHPExcel->getActiveSheet()->getStyle('A15')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);

	$objPHPExcel->getActiveSheet()->getStyle('A7:P7')->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A15:P15')->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);



	$estilo = array( 
		  'borders' => array(
		    'outline' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

	$estiloF = array( 
		  'borders' => array(
		    'outline' => array(
		      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
		    )
		  )
		);

	$estiloFirmas = array( 
		  'borders' => array(
		    'top' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

	$objPHPExcel->getActiveSheet()->getStyle('A7:P7')->applyFromArray($estilo);
	$objPHPExcel->getActiveSheet()->getStyle('A15:P15')->applyFromArray($estilo);
	$objPHPExcel->getActiveSheet()->getStyle('A16:P16')->applyFromArray($estilo);

 	$objPHPExcel->getActiveSheet()->getStyle('A8:F8')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A9:F9')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A10:F10')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A11:F11')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A12:F12')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A13:F13')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A14:F14')->applyFromArray($estilo);

 	$objPHPExcel->getActiveSheet()->getStyle('G8:P8')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G9:P9')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G10:P10')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G11:P11')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G12:P12')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G13:P13')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G14:P14')->applyFromArray($estilo);


	//COMBINAR CELDAS
	$objPHPExcel->getActiveSheet()->mergeCells('C1:P6');
	// ESCRITURA EN CELDAS COMBINADAS
   	$objPHPExcel->getActiveSheet()->setCellValue('C1','CRONOGRAMA  DETALLADO DE MANTENIMIENTOS');
   	$objPHPExcel->getActiveSheet()->mergeCells('A7:P7');
   	$objPHPExcel->getActiveSheet()->setCellValue('A7','DATOS GENERALES');

   	// TITULOS PRINCIPALES //
   	$objPHPExcel->getActiveSheet()->mergeCells('A8:F8');
   	$objPHPExcel->getActiveSheet()->mergeCells('A9:F9');
   	$objPHPExcel->getActiveSheet()->mergeCells('A10:F10');
   	$objPHPExcel->getActiveSheet()->mergeCells('A11:F11');
   	$objPHPExcel->getActiveSheet()->mergeCells('A12:F12');
   	$objPHPExcel->getActiveSheet()->mergeCells('A13:F13');
   	$objPHPExcel->getActiveSheet()->mergeCells('A14:F14');
   	// CELDAS DE ESCRITURA DE DATOS //
   	$objPHPExcel->getActiveSheet()->mergeCells('G8:P8');
   	$objPHPExcel->getActiveSheet()->mergeCells('G9:P9');
   	$objPHPExcel->getActiveSheet()->mergeCells('G10:P10');
   	$objPHPExcel->getActiveSheet()->mergeCells('G11:P11');
   	$objPHPExcel->getActiveSheet()->mergeCells('G12:P12');
   	$objPHPExcel->getActiveSheet()->mergeCells('G13:P13');
   	$objPHPExcel->getActiveSheet()->mergeCells('G14:P14');
   	// TITULOS //
   	$objPHPExcel->getActiveSheet()->setCellValue('A8','Sede a Realizar Mantenimiento');
   	$objPHPExcel->getActiveSheet()->setCellValue('A9','Direccion');
   	$objPHPExcel->getActiveSheet()->setCellValue('A10','Telefonos fijo');
   	$objPHPExcel->getActiveSheet()->setCellValue('A11','Telefono Celular');
   	$objPHPExcel->getActiveSheet()->setCellValue('A12','Encargado');
   	$objPHPExcel->getActiveSheet()->setCellValue('A13','Fecha Inicio Programacion');
   	$objPHPExcel->getActiveSheet()->setCellValue('A14','Fecha Fin Programacion');
   	// ESCRITURA DATOS //
   	$objPHPExcel->getActiveSheet()->setCellValue('G8',$Cronograma['0']['sucursal_nombre']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G9',$Cronograma['0']['sucursal_direccion']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G10',$Cronograma['0']['sucursal_telefono']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G11',$Cronograma['0']['sucursal_telefono']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G12',$Cronograma['0']['sucursal_contacto']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G13',$Cronograma['0']['cronograma_fecha_inicio']);
   	$objPHPExcel->getActiveSheet()->setCellValue('G14',$Cronograma['0']['cronograma_fecha_fin']);

   	$objPHPExcel->getActiveSheet()->mergeCells('A15:P15');
   	$objPHPExcel->getActiveSheet()->setCellValue('A15','DETALLE DE LA ACTIVIDAD A REALIZAR');

   	$objPHPExcel->getActiveSheet()->mergeCells('A16:C16');
   	$objPHPExcel->getActiveSheet()->mergeCells('D16:F16');
   	$objPHPExcel->getActiveSheet()->mergeCells('G16:I16');
   	$objPHPExcel->getActiveSheet()->mergeCells('L16:P16');

   	$objPHPExcel->getActiveSheet()->setCellValue('A16','# ACTIVO');
   	$objPHPExcel->getActiveSheet()->setCellValue('D16','NOMBRE MAQUINA');
   	$objPHPExcel->getActiveSheet()->setCellValue('G16','USUARIO');
   	$objPHPExcel->getActiveSheet()->setCellValue('J16','HORA I.');
   	$objPHPExcel->getActiveSheet()->setCellValue('K16','HORA F.');
   	$objPHPExcel->getActiveSheet()->setCellValue('L16','OBSERVACION ESPECIFICA');

   	$count = 17;
	foreach ($CronogramaDetalles as $key => $value) {
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$count.':C'.$count);
	   	$objPHPExcel->getActiveSheet()->mergeCells('D'.$count.':F'.$count);
	   	$objPHPExcel->getActiveSheet()->mergeCells('G'.$count.':I'.$count);
	   	$objPHPExcel->getActiveSheet()->mergeCells('L'.$count.':P'.$count);

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $value['equipo_numero_activo_contabilidad'])
		            ->setCellValue('D'.$count, $value['equipo_nombre_actual'])
		            ->setCellValue('G'.$count, $value['funcionario_nombres'])
		            ->setCellValue('J'.$count, $value['cronograma_detalle_hora_inicio'])
		            ->setCellValue('K'.$count, $value['cronograma_detalle_hora_fin'])
		            ->setCellValue('L'.$count, '');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':C'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$count.':F'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$count.':I'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$count.':J'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$count.':K'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('L'.$count.':P'.$count)->applyFromArray($estilo);

		$count++;

	}



	$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':P'.$count)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A'.$count)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':P'.$count)->getFont()->setBold(true);
	// $count = $count + 1;
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count.':P'.$count);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count,'OBSERVACIONES');


   	$count2 = $count + 1; 
   	$count3 = $count + 6; 
   	$count4 = $count3 + 1;
   	$count5 = $count4 + 1;
   	$count6 = $count4 + 6;
   	$count7 = $count6 + 1;
   	$count8 = $count7 + 1;
   	$count9 = $count7 + 13;

   	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count2.':P'.$count3);

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count4.':P'.$count4)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

   	
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count4)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);

	$objPHPExcel->getActiveSheet()->getStyle('A'.$count4.':P'.$count4)->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count4.':P'.$count4);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count4,'RECOMENDACIONES');
   	  

   	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count5.':P'.$count6);


   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count7.':P'.$count7)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

   	
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count7)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);

	$objPHPExcel->getActiveSheet()->getStyle('A'.$count7.':P'.$count7)->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count7.':P'.$count7);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count7,'FIRMAS');

   	// $objPHPExcel->getActiveSheet()->mergeCells('A'.$count8.':P'.$count9);

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count8.':P'.$count9)->applyFromArray($estilo);
   	$countF1 = $count8 + 5;
   	$countF2 = $countF1 + 5;


   	$objPHPExcel->getActiveSheet()->setCellValue('C'.$countF1,'USUARIO EQUIPO');
   	$objPHPExcel->getActiveSheet()->setCellValue('C'.$countF2,'DIRECTOR/DELEGADO');
   	$objPHPExcel->getActiveSheet()->setCellValue('K'.$countF1,'TECNICO');
   	$objPHPExcel->getActiveSheet()->setCellValue('K'.$countF2,'COORD. INFRAESTRUCTURA');


   	$objPHPExcel->getActiveSheet()->getStyle('C'.$countF1.':F'.$countF1)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->getStyle('C'.$countF2.':F'.$countF2)->applyFromArray($estiloFirmas);


   	$objPHPExcel->getActiveSheet()->getStyle('K'.$countF1.':M'.$countF1)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->getStyle('K'.$countF2.':M'.$countF2)->applyFromArray($estiloFirmas);

   	$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$count9)->applyFromArray($estiloF);
   	


	$objPHPExcel->getActiveSheet()->setTitle('Cronograma');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Cronograma_'.$Cronograma['0']['sucursal_nombre']."_".date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

?>