<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;
use yii\helpers\Url;

// echo "<pre>";
// print_r($Cronograma);
// print_r($CronogramaDetalles);
// die();

// foreach ($datos as $key => $value) {
// 	# code...
// }

// die();
$objPHPExcel = new PHPExcel(); 
// $ = new PHPExcel_Worksheet_HeaderFooterDrawing(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");
	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A7:K7")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A8:F14")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A15:K15")->getFont()->setBold(true);
   	$objPHPExcel->getActiveSheet()->getStyle("A16:K16")->getFont()->setBold(true);

   	// foreach(range('A','Z') as $columnID) {
   	//     $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	//         ->setAutoSize(true);
   	// }

   	// CARGA DEL LOGO DE LA OSA
	$gdImage = imagecreatefromjpeg('images/osa.jpg'); 
	$objDrawing = new PHPExcel_Worksheet_MemoryDrawing(); 
	$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image'); 
	$objDrawing->setImageResource($gdImage); 
	$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG); 
	$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT); 
	$objDrawing->setHeight(120);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');$objWriter->save(str_replace('.php', '.xlsx', __FILE__)); 
	// ALINEACION DEL TITULO AL LADO DE LA IMAGEN
	$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);


	$objPHPExcel->getActiveSheet()->getStyle('A15')->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);

	$objPHPExcel->getActiveSheet()->getStyle('A7:K7')->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A15:K15')->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);



	$estilo = array( 
		  'borders' => array(
		    'outline' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

	$estiloF = array( 
		  'borders' => array(
		    'outline' => array(
		      'style' => PHPExcel_Style_Border::BORDER_MEDIUM
		    )
		  )
		);
	$estiloFirmas = array( 
		  'borders' => array(
		    'bottom' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

	$objPHPExcel->getActiveSheet()->getStyle('A7:K7')->applyFromArray($estilo);
	$objPHPExcel->getActiveSheet()->getStyle('A15:K15')->applyFromArray($estilo);
	$objPHPExcel->getActiveSheet()->getStyle('A16:K16')->applyFromArray($estilo);

 	$objPHPExcel->getActiveSheet()->getStyle('A8:F8')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A9:F9')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A10:F10')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A11:F11')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A12:F12')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A13:F13')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('A14:F14')->applyFromArray($estilo);

 	$objPHPExcel->getActiveSheet()->getStyle('G8:K8')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G9:K9')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G10:K10')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G11:K11')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G12:K12')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G13:K13')->applyFromArray($estilo);
 	$objPHPExcel->getActiveSheet()->getStyle('G14:K14')->applyFromArray($estilo);


	//COMBINAR CELDAS
	$objPHPExcel->getActiveSheet()->mergeCells('C1:K6');
	// ESCRITURA EN CELDAS COMBINADAS
   	$objPHPExcel->getActiveSheet()->setCellValue('C1','CRONOGRAMA  GENERAL DE MANTENIMIENTOS');
   	$objPHPExcel->getActiveSheet()->mergeCells('A7:K7');
   	$objPHPExcel->getActiveSheet()->setCellValue('A7','DATOS GENERALES');

   	// TITULOS PRINCIPALES //
   	$objPHPExcel->getActiveSheet()->mergeCells('A8:F8');
   	$objPHPExcel->getActiveSheet()->mergeCells('A9:F9');
   	$objPHPExcel->getActiveSheet()->mergeCells('A10:F10');
   	$objPHPExcel->getActiveSheet()->mergeCells('A11:F11');
   	$objPHPExcel->getActiveSheet()->mergeCells('A12:F12');
   	$objPHPExcel->getActiveSheet()->mergeCells('A13:F13');
   	$objPHPExcel->getActiveSheet()->mergeCells('A14:F14');
   	// CELDAS DE ESCRITURA DE DATOS //
   	$objPHPExcel->getActiveSheet()->mergeCells('G8:K8');
   	$objPHPExcel->getActiveSheet()->mergeCells('G9:K9');
   	$objPHPExcel->getActiveSheet()->mergeCells('G10:K10');
   	$objPHPExcel->getActiveSheet()->mergeCells('G11:K11');
   	$objPHPExcel->getActiveSheet()->mergeCells('G12:K12');
   	$objPHPExcel->getActiveSheet()->mergeCells('G13:K13');
   	$objPHPExcel->getActiveSheet()->mergeCells('G14:K14');
   	// TITULOS //
   	$objPHPExcel->getActiveSheet()->setCellValue('A8','NOMBRE DE LA EMPRESA');
   	$objPHPExcel->getActiveSheet()->setCellValue('A9','NIT');
   	$objPHPExcel->getActiveSheet()->setCellValue('A10','TIPO DE MANTENIMIENTO');
   	$objPHPExcel->getActiveSheet()->setCellValue('A11','REALIZADO POR');
   	$objPHPExcel->getActiveSheet()->setCellValue('A12','FECHA DE INICIO');
   	$objPHPExcel->getActiveSheet()->setCellValue('A13','FECHA DE FINALIZACION');
   	// ESCRITURA DATOS //
   	$objPHPExcel->getActiveSheet()->setCellValue('G8','ORGANIZACION SAYCO ACINPRO');
   	$objPHPExcel->getActiveSheet()->setCellValue('G9','800021811-9');
   	$objPHPExcel->getActiveSheet()->setCellValue('G10','PREVENTIVO');
   	$objPHPExcel->getActiveSheet()->setCellValue('G11','DPTO TECNOLOGIA Y TELECOMUNICACIONES');
   	$objPHPExcel->getActiveSheet()->setCellValue('G12','2019-03-11');
   	$objPHPExcel->getActiveSheet()->setCellValue('G13','2019-03-14');

   	$objPHPExcel->getActiveSheet()->mergeCells('A15:K15');
   	$objPHPExcel->getActiveSheet()->setCellValue('A15','DETALLE DE LA ACTIVIDAD A REALIZAR');

   	$objPHPExcel->getActiveSheet()->mergeCells('A16:C16');
   	$objPHPExcel->getActiveSheet()->mergeCells('D16:F16');
   	$objPHPExcel->getActiveSheet()->mergeCells('G16:I16');

   	$objPHPExcel->getActiveSheet()->setCellValue('A16','CIUDAD');
   	$objPHPExcel->getActiveSheet()->setCellValue('D16','DIRECCION');
   	$objPHPExcel->getActiveSheet()->setCellValue('G16','FECHA INICIO');
   	$objPHPExcel->getActiveSheet()->setCellValue('J16','FECHA FIN');
   	$objPHPExcel->getActiveSheet()->setCellValue('K16','# EQUIPOS');

   	$count = 17;
	foreach ($datos as $key => $value) {
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$count.':C'.$count);
	   	$objPHPExcel->getActiveSheet()->mergeCells('D'.$count.':F'.$count);
	   	$objPHPExcel->getActiveSheet()->mergeCells('G'.$count.':I'.$count);

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $value['sucursal_nombre'])
		            ->setCellValue('D'.$count, $value['sucursal_direccion'])
		            ->setCellValue('G'.$count, $value['cronograma_fecha_inicio'])
		            ->setCellValue('J'.$count, $value['cronograma_fecha_fin'])
		            ->setCellValue('K'.$count, $value['numeroEquipos']);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':C'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$count.':F'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$count.':I'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$count.':J'.$count)->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('K'.$count.':K'.$count)->applyFromArray($estilo);
		

		$count++;

	}



	$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':K'.$count)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A'.$count)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':K'.$count)->getFont()->setBold(true);
	// $count = $count + 1;
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count.':K'.$count);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count,'INFORMACION ESPECIFICA');


   	$count2 = $count + 1; 

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':K'.$count2)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A'.$count2)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':K'.$count2)->getFont()->setBold(true);
	// $count = $count + 1;
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count2.':K'.$count2);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count2,'CRONOGRAMA REALIZADO POR');

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':K'.$count2)->applyFromArray($estilo);

   	$count3 = $count2 + 2;
   	$count4 = $count3 + 1;
   	$count5 = $count4 + 1;

   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count3,'NOMBRE:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count3.':I'.$count3)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count4,'DOCUMENTO:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count4.':I'.$count4)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count5,'CARGO:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count5.':I'.$count5)->applyFromArray($estiloFirmas);


   	// $objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':K'.$count5)->applyFromArray($estilo);

   	$count6 = $count5 + 2;
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':K'.$count6)->applyFromArray($estilo);
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count6.':K'.$count6)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count6)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count6.':K'.$count6)->getFont()->setBold(true);
	// $count = $count + 1;
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count6.':K'.$count6);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count6,'AUTORIZADO POR');

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count6.':K'.$count6)->applyFromArray($estilo);



   	// $count6 = $count4 + 6;
   	$count7 = $count6 + 2;
   	$count8 = $count7 + 1;
   	$count9 = $count8 + 1;


   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count7,'NOMBRE:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count7.':I'.$count7)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count8,'DOCUMENTO:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count8.':I'.$count8)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count9,'CARGO:');
	$objPHPExcel->getActiveSheet()->getStyle('E'.$count9.':I'.$count9)->applyFromArray($estiloFirmas);

  


   	$count10 = $count9 + 2;
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count6.':K'.$count10)->applyFromArray($estilo);
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count10.':K'.$count10)->applyFromArray(
	    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => '9B9595')
	        )
	    )
	);
 
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count10)->getAlignment()->applyFromArray(
    	array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count10.':K'.$count10)->getFont()->setBold(true);
	// $count = $count + 1;
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$count10.':K'.$count10);
   	$objPHPExcel->getActiveSheet()->setCellValue('A'.$count10,'RECIBIDO POR');

   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count10.':K'.$count10)->applyFromArray($estilo);


   	// $count6 = $count4 + 6;
   	$count11 = $count10 + 2;
   	$count12 = $count11 + 1;
   	$count13 = $count12 + 1;

   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count11,'NOMBRE:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count11.':I'.$count11)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count12,'DOCUMENTO:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count12.':I'.$count12)->applyFromArray($estiloFirmas);
   	$objPHPExcel->getActiveSheet()->setCellValue('D'.$count13,'CARGO:');
   	$objPHPExcel->getActiveSheet()->getStyle('E'.$count13.':I'.$count13)->applyFromArray($estiloFirmas);

   	$count13 = $count13+2;
   	$objPHPExcel->getActiveSheet()->getStyle('A'.$count10.':K'.$count13)->applyFromArray($estilo);


	$objPHPExcel->getActiveSheet()->setTitle('Cronograma');
	$objPHPExcel->setActiveSheetIndex(0);
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Cronograma_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

?>