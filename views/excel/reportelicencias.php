<?php 


use app\models\Negocios;
use app\models\Documentos;
use app\models\TarifasBase;

// echo "<pre>";
// print_r($ReportelicenciasSucursales);die();

// foreach ($datos as $key => $value) {
// 	# code...
// }

// die();
$objPHPExcel = new PHPExcel(); 

	$objPHPExcel->
	    getProperties()
	        ->setCreator("TEDnologia.com")
	        ->setLastModifiedBy("TEDnologia.com")
	        ->setTitle("Exportar Excel con PHP")
	        ->setSubject("Documento de prueba")
	        ->setDescription("Documento generado con PHPExcel")
	        ->setKeywords("usuarios phpexcel")
	        ->setCategory("reportes");


	$objPHPExcel->createSheet(0);
	$objPHPExcel->createSheet(1);
	$objPHPExcel->createSheet(2);

	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('29bb04');
   	// Add some data
   	$from = "A1"; // or any value
    $to = "M1"; // or any value
    $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
   	foreach(range('A1','AG1') as $columnID) {
   	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
   	        ->setAutoSize(true);
   	
   	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'SUCURSALES')
	            ->setCellValue('C1', 'TIPO LICENCIA')
                ->setCellValue('D1', 'LLAVE')
                ->setCellValue('E1', 'VERSION')
                ->setCellValue('F1', 'ESTADO')
                ->setCellValue('G1', 'ESPECIFICACIONES')
                ->setCellValue('H1', 'UBICACION TECNOLOGIA');
                               
	$count = 2; 
    }

	foreach ($licenciasGeneral as $keyG => $valueG) {
		
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$count, $valueG['Zona'])
		            ->setCellValue('B'.$count, $valueG['Sucursal'])		            
		            ->setCellValue('C'.$count, $valueG['Tipo_licencia'])
		            ->setCellValue('D'.$count, $valueG['Llave'])                    
                    ->setCellValue('E'.$count, $valueG['Version'])
		            ->setCellValue('F'.$count, $valueG['Estado'])
                    ->setCellValue('G'.$count, $valueG['licencia_especificaciones'])
                    ->setCellValue('H'.$count, $valueG['Ubicacion_tecnologia']);                                     

		$count++;
	}


	$objPHPExcel->getActiveSheet()->setTitle('Reporte general');
	$objPHPExcel->setActiveSheetIndex(0);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(1)
	            ->setCellValue('A1', 'ZONA')
	            ->setCellValue('B1', 'SIN ASIGNAR')
                ->setCellValue('C1', 'ASIGNADO')
                ->setCellValue('D1', 'TOTAL');                

	$countS = 2;
	

	foreach ($ReportelicenciasZonas as $keyS => $valueS) {
		
		$objPHPExcel->setActiveSheetIndex(1)
		            ->setCellValue('A'.$countS, $valueS['Zona'])
		            ->setCellValue('B'.$countS, $valueS['Sin_asignar'])
		            ->setCellValue('C'.$countS, $valueS['Asignado'])
                    ->setCellValue('D'.$countS, $valueS['Total']);                                     

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por zona');
	$objPHPExcel->setActiveSheetIndex(1);



	// $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->setActiveSheetIndex(2)
                ->setCellValue('A1', 'SUCURSAL')
                ->setCellValue('B1', 'EQUIPOS ACTIVOS')
                ->setCellValue('C1', 'EQUIPOS INACTIVOS')
                ->setCellValue('D1', 'EQUIPOS DADOS DE BAJA')
                ->setCellValue('E1', 'TOTAL EQUIPOS');                


	$countS = 2;
	

	foreach ($ReportelicenciasSucursales as $keyS => $valueM) {
		
		$objPHPExcel->setActiveSheetIndex(2)
                    ->setCellValue('A'.$countS, $valueM['Sucursales'])
                    ->setCellValue('B'.$countS, $valueM['Sin_asignar'])
                    ->setCellValue('C'.$countS, $valueM['Asignado'])
                    ->setCellValue('D'.$countS, $valueM['Total']);                    

		$countS++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Detallado por sucursal');
	$objPHPExcel->setActiveSheetIndex(2);


	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="estadistica_licencias_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;

 ?>