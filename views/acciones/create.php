<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Acciones */

$this->title = 'Crear Accion';
$this->params['breadcrumbs'][] = ['label' => 'Acciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acciones-create">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
