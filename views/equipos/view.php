<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use app\models\User;
use app\models\Users;
use app\models\HistoricoEquipos;
use app\models\TiposDetalles;
use app\models\Soporteequipos;


/* @var $this yii\web\View */
/* @var $model app\models\Equipos */


// echo "<pre>";
// print_r($soporte);
// die();

$this->title = 'Gestión de Equipos';
$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$rol =  Yii::$app->user->identity->role;
$idUsuario =  Yii::$app->user->identity->id;
?>
<div class="equipos-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    
    <?php if ($rol==1) { ?>    
        <div class="">                             
                <div class="card-body">
                    <h1 color="Olive">Información del Equipo: <?php echo $model[0]['equipo_identificacion_tecnologia']; ?></h1>
                    <ul class="nav nav-pills card-header-pills">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#equipo" class="alert alert-info">Equipo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#licencia" class="alert alert-info">Licencia</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#mantenimiento" class="alert alert-info">Mantenimientos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#soporte" class="alert alert-info">Soporte</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#historico" class="alert alert-info">Historico</a>
                        </li>                        
                    </ul>

                </div>                
        
            <!-- INICIO DEL PANEL PARA EQUIPO -->
            <div class="tab-content">
                <div id="equipo" class="tab-pane fade show active">                    
                    <!-- Default panel contents -->                                            
                    <table class="table table-bordered">
                        <tr>
                        <?php if ($model[0]['equipo_estado_id']!=24){?>
                            <td colspan="10" id="estado">
                                <p>
                                
                                <?= Html::a('Actualizar', ['update', 'id' => $model[0]['equipo_id']], ['class' => 'btn btn-primary']) ?>                                        
                                <button type="button" class="btn btn-warning baja">Dar de baja</button>
                                <button type="button" class="btn btn-success ping" id='<?php echo $model[0]['equipo_id']?>'>Ping</button>                                                                                                                                                                                                                                        
                                </p>  
                            </td>
                        <?php } ?>                                                                           
                        </tr>                        
                        <tr>
                            <td colspan="5"><b>Indentificacón T & T</b></td>
                            <td colspan="5"><b>Nombre Actual</b></td>                                    
                            <td colspan="5"> <b>Ubicación</b></td>
                            <td colspan="5"><b>Ip Asignada</b></td>
                            
                        </tr>                                                
                        <tr>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_identificacion_tecnologia'] ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_nombre_actual'] ?></td>                                    
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['sucursal_nombre']. " - ". $model[0]['area_nombre'] ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_ip']?></td>

                        </tr>
                        <tr>
                            <td colspan="5"><b>Funcionario</b></td>
                            <td colspan="5"><b>Cargo</b></td>
                            <td colspan="5"><b>Correo</b></td>
                            <td colspan="5"><b>Estado Equipo</b></td>
                        </tr>
                        
                        <tr>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['funcionario_nombres']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['funcionario_cargo']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['funcionario_email']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['estado']; ?></td>
                        </tr>
                        <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                        <tr><td colspan="20" class="alert alert-primary"> INFORMACIÓN DE LA MAQUINA</td></tr>
                        <?php } else{ ?>
                            <tr><td colspan="20" class ="alert alert-danger"> INFORMACIÓN DE LA MAQUINA</td></tr>
                        <?php } ?>
                        <tr>                                
                            <td colspan="5"><b>Marca del Equipo</b></td>
                            <td colspan="5"><b>Modelo del Equipo</b></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_marca']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_modelo']; ?></td>
                        </tr>
                        
                        <tr><td colspan="2.5"><b>Pantalla</b></td>
                            <td colspan="2.5"><b>RAM</b></td>
                            <td colspan="5"><b>Tipo de Memoria RAM</b></td>
                            <td colspan="5"><b>Marca Disco Duro</b></td>
                            <td colspan="5"><b>Capacidad Disco Duro</b></td>
                        </tr>
                        <tr>
                            <td colspan="2.5" style="color:#656B71"><?php echo $model[0]['equipo_pantalla']; ?></td>
                            <td colspan="2.5" style="color:#656B71"><?php echo $model[0]['ram']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['tipo_memoria_ram']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_marca_disco_duro']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['capacidad_disk_duro']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2.5"><b>Procesador</b></td>
                            <td colspan="2.5"><b>Velocidad del Procesador</b></td>
                            <td colspan="5"><b>Nucleos</b></td>
                            <td colspan="5"><b>Sistema Operativo</b></td>
                            <td colspan="5"><b>Paquete de Office</b></td>
                        </tr>
                        <tr>
                            <td colspan="2.5" style="color:#656B71"><?php echo $model[0]['procesador']; ?></td>
                            <td colspan="2.5" style="color:#656B71"><?php echo $model[0]['equipo_velocidad_procesador']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['equipo_cantidad_nucleos_procesador']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['sistemaO']; ?></td>
                            <td colspan="5" style="color:#656B71"><?php echo $model[0]['office']; ?></td>
                        </tr>
                        <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                        <tr><td colspan="20" class="alert alert-primary"> INFORMACIÓN CONTABLE</td></tr>
                        <?php } else{ ?>
                            <tr><td colspan="20" class ="alert alert-danger"> INFORMACIÓN CONTABLE</td></tr>
                        <?php } ?>                                
                        <tr>                                
                            <td colspan="7"><b>Número de Activo</b></td>
                            <td colspan="7"><b>Fecha de Ingreso</b></td>
                            <td colspan="6"><b>Valor</b></td>
                        </tr>
                        <tr>
                            <td colspan="7"><?php echo $model[0]['equipo_numero_activo_contabilidad']; ?></td>
                            <td colspan="7"><?php echo $model[0]['equipo_fecha_ingreso']; ?></td>
                            <td colspan="6"><?php echo $model[0]['equipo_valor']; ?></td>
                        </tr>
                        <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                        <tr><td colspan="20" class="alert alert-primary"> OBSERVACIONES</td></tr>
                        <?php } else{ ?>
                            <tr><td colspan="20" class ="alert alert-danger"> OBSERVACIONES</td></tr>
                        <?php } ?>                                
                        <tr>
                            <td colspan="20">
                                <?php echo $model[0]['equipo_observaciones']; ?>
                            </td>
                        </tr>  


                    </table>                                                                  
                </div>                                                                 
                <!-- FIN DEL PANEL DE EQUIPO -->

                <!-- INICIO DEL PANEL PARA LICENCIA -->        
                <div id="licencia" class="tab-pane fade">
                    <div class="card card-primary">
                        <!-- Default panel contents -->
                        <div class="card-body">
                            <div class="table-responsive" >
                            <?php if ($licencias && $licencias[0]['licencia_estado_id']!=20 ) { ?>
                                <table class="table table-bordered">                        
                                <!-- <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                    <tr>
                                        <td colspan="5">
                                            <p>
                                            <?= Html::a('Actualizar', ['update', 'id' => $model[0]['equipo_id']], ['class' => 'btn btn-primary']) ?>
                                            </p>
                                        </td>
                                    </tr> -->
                                <!-- <?php } ?>   -->
                                    <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                        <tr><td colspan="20" class = "alert alert-primary"> INFORMACIÓN DE LA LICENCIA DE WINDOWS</td></tr>
                                    <?php } else{ ?>
                                        <tr><td colspan="20" class = "alert alert-danger"> INFORMACIÓN DE LA LICENCIA DE WINDOWS</td></tr>
                                    <?php } ?>                             
                                    <tr>
                                        <td colspan="4"><b>Producto</b></td>
                                        <td colspan="4"><b>Versión</b></td>
                                        <td colspan="4"><b>Indentificación T & T</b></td>
                                        <td colspan="4"><b>Clave del producto</b></td>
                                        <td colspan="4"><b>Especificaciones</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><?php echo $licencias[1]['tipo'] ?></td>
                                        <td colspan="4"><?php echo $licencias[1]['licencia_versiones'] ?></td>
                                        <td colspan="4"><?php echo $licencias[1]['licencia_identificacion_tecnologia'] ?></td>
                                        <td colspan="4"><?php echo $licencias[1]['licencia_numero'] ?></td>
                                        <td colspan="4"><?php echo $licencias[1]['licencia_especificaciones'] ?></td>
                                    </tr>
                                    <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                        <tr><td colspan="20" class = "alert alert-primary"> INFORMACIÓN DE LA LICENCIA DE OFFICE</td></tr>
                                    <?php } else{ ?>
                                        <tr><td colspan="20" class = "alert alert-danger"> INFORMACIÓN DE LA LICENCIA DE OFFICE</td></tr>
                                    <?php } ?>
                                                                
                                    <tr>
                                        <td colspan="4"><b>Producto</b></td>
                                        <td colspan="4"><b>Versión</b></td>
                                        <td colspan="4"><b>Indentificación T & T</b></td>
                                        <td colspan="4"><b>Clave del producto</b></td>
                                        <td colspan="4"><b>Especificaciones</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><?php echo $licencias[0]['tipo'] ?></td>
                                        <td colspan="4"><?php echo $licencias[0]['licencia_versiones'] ?></td>
                                        <td colspan="4"><?php echo $licencias[0]['licencia_identificacion_tecnologia'] ?></td>
                                        <td colspan="4"><?php echo $licencias[0]['licencia_numero'] ?></td>
                                        <td><?php echo $licencias[0]['licencia_especificaciones'] ?></td>
                                    </tr>
                                </table>
                            <?php }else{ ?>
                            <div class="alert alert-danger" role="alert">El equipo no tiene licencias asignadas</div>
                            <?php }  ?>
                            </div>
                        </div>
                    </div>    
                </div>    
                <!-- FIN DEL PANEL DE LICENCIAS -->
            
                <!-- INICIO DEL PANEL PARA MANTENIMIENTOS -->       
                <div id="mantenimiento" class="tab-pane fade">
                    <div class="card card-primary">                                        
                        <!-- Default panel contents -->
                        <div class="card-body">
                            <div class="table-responsive" >  
                            <?php if ($cronogramas){?>                  
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td><b>Hora de Inicio</b></td>
                                            <td><b>Hora de Finalizacion</b></td>
                                            <td><b>Quien lo Realiza</b></td>
                                            <td><b>Estado</b></td>
                                            <td><b>Aprobado</b></td>
                                            <td><b>Accion</b></td>
                                        </tr>
                                        <?php foreach ($cronogramas as $keyC => $valueC) { ?>
                                        <tr>
                                            <td><?php echo $valueC['cronograma_detalle_hora_inicio'] ?></td>
                                            <td><?php echo $valueC['cronograma_detalle_hora_fin'] ?></td>
                                            <td><?php echo $valueC['quienRealiza'] ?></td>
                                            <td><?php echo $valueC['estado'] ?></td>
                                            <td><?php echo $valueC['aprobado']."".$valueC['quienAprueba'] ?></td>
                                            <td>
                                                <?php if ($valueC['cronograma_detalle_realizado'] == 0) {  ?>
                                                    
                                                    <?= Html::a('Registrar Check List', ['check-list/create', 'id'=>$valueC['cronograma_detalle_equipo_id'],'cronograma'=>$valueC['cronograma_detalle_id']], ['class' => 'btn btn-success', 'id'=>'aprobar']) ?>    
                                                <?php }else{ ?>
                                                    
                                                    <?= Html::a('Ver Mantenimiento Registrado', ['check-list/view', 'id'=>$valueC['cronograma_detalle_id'], 'Equipo' => $valueC['cronograma_detalle_equipo_id']], ['class' => 'btn btn-primary']) ?>
                                                    
                                                    <!-- <?php if ($valueC['cronograma_detalle_aprobado']==1) { ?>
                                                        <img id="btn-excel" src="images/exceln.png" alt="" width="40" title="Descargar Formato">
                                                        <?php $form = ActiveForm::begin([
                                                                'action' => ['excel/checklist'],
                                                                'method' => 'post',
                                                                'id' => 'liquidaciones',
                                                            ]); ?>
                                                            <input type="hidden" name="datos" value="<?php echo htmlentities(serialize($valueC['cronograma_detalle_id']))?>">
                                                            <?php ActiveForm::end()?>
                                                    <?php } ?> -->
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                <?php } else { ?> 
                                <div class="alert alert-danger" role="alert">El equipo no presenta mantenimientos</div>
                                <?php } ?>                                       
                            </div>                               
                        </div>
                    </div>    
                </div>                       
                <!-- FIN DEL PANEL DE MANTENIMIENTOS -->

                <!-- INICIO DEL PANEL PARA SOPORTE -->       
                <div id="soporte" class="tab-pane fade">
                    <div class="card card-primary">                                        
                        <!-- Default panel contents -->
                        <div class="card-body">
                            <div class="table-responsive" >  
                            <?php if ($soporte){?>                  
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <td><b>Tipo de soporte</b></td>
                                            <td><b>Quién solicita</b></td>
                                            <td><b>fecha de solicitud</b></td>
                                            <td><b>Estado del soporte</b></td>
                                            <td><b>Acción</b></td>                                            
                                        </tr>
                                        <?php foreach ($soporte as $sopo => $sequip) { ?>
                                        <tr>
                                            <td><?php echo $sequip->tiposoporte->tipo_detalle_nombre?></td>
                                            <td><?php echo $sequip->nombre.' '.$sequip->apellido?></td>
                                            <td><?php echo $sequip->fecha_solicitud ?></td>
                                            <td><?php echo $sequip->estado_soporte == 0 ? "Pendiente" : "Realizado";?></td>                                            
                                            <td>                                                                                                                                                    
                                                <button class="class' => 'btn btn-primary btn-block vermantenimiento" id ="<?php echo $sequip->equipos_idequipo?>">Ver</button>                                                   
                                             <?php if (!$sequip->estado_soporte == 1) {?>                                                
                                                <button class="class' => 'btn btn-warning btn-block gestionmant" id ="<?php echo $sequip->equipos_idequipo?>">Gestionar</button>
                                             <?php } ?>   
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                <?php } else { ?> 
                                <div class="alert alert-danger" role="alert">El equipo no presenta mantenimientos</div>
                                <?php } ?>                                       
                            </div>                               
                        </div>
                    </div>    
                </div>                       
                <!-- FIN DEL PANEL PARA SOPORTE -->

                <!-- INICIO DEL PANEL HISTORICO --> 
                <div id="historico" class="tab-pane fade">
                    <div class="card card-primary">                
                        <div class="card-body">
                            <div class="table-responsive">
                            <?php if($historico) {?>
                                <?php if ($historico[0]['proceso_id']==24) { ?>
                                    <table class="table table-bordered">                                                
                                        <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                            <tr><td colspan="5" class="alert alert-primary"> HISTÓRICO DE LA LICENCIA DE OFFICE</td></tr>
                                        <?php } else{ ?>
                                            <tr><td colspan="5" class="alert alert-danger"> HISTÓRICO DE LA LICENCIA DE OFFICE</td></tr>
                                        <?php } ?>                             
                                        <tr>
                                            <td><b>Producto</b></td>
                                            <td><b>Versión</b></td>
                                            <td><b>Indentificación T & T</b></td>
                                            <td><b>Clave del producto</b></td>
                                            <td><b>Especificaciones</b></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $historico[1]['producto'] ?></td>
                                            <td><?php echo $historico[1]['licencia_version'] ?></td>
                                            <td><?php echo $historico[1]['licencia_identificacion_tecnologia'] ?></td>
                                            <td><?php echo $historico[1]['licencia_numero'] ?></td>
                                            <td><?php echo $historico[1]['licencia_especificaciones'] ?></td>
                                        </tr>
                                        <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                            <tr><td colspan="5" class="alert alert-primary"> HISTÓRICO DE LA LICENCIA DE WINDOWS</td></tr>
                                        <?php } else{ ?>
                                            <tr><td colspan="5" class="alert alert-danger"> HISTÓRICO DE LA LICENCIA DE WINDOWS</td></tr>
                                        <?php } ?>
                                                                    
                                        <tr>
                                            <td><b>Producto</b></td>
                                            <td><b>Versión</b></td>
                                            <td><b>Indentificación T & T</b></td>
                                            <td><b>Clave del producto</b></td>
                                            <td><b>Especificaciones</b></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $historico[0]['producto'] ?></td>
                                            <td><?php echo $historico[0]['licencia_version'] ?></td>
                                            <td><?php echo $historico[0]['licencia_identificacion_tecnologia'] ?></td>
                                            <td><?php echo $historico[0]['licencia_numero'] ?></td>
                                            <td><?php echo $historico[0]['licencia_especificaciones'] ?></td>
                                        </tr>                            
                                    </table>
                                <?php } ?>                                              
                            <?php }else{ ?>
                                <div class="alert alert-danger" role="alert">El equipo no tiene historico de licencias</div>
                            <?php }  ?>
                            <table class="table table-bordered">
                                <?php if ($historicoequipos || $model[0]['equipo_estado_id'] == 24) {?>
                                    <?php if ($model[0]['equipo_estado_id']!=24) { ?>
                                        <tr><td colspan="5" class="alert alert-primary"> HISTÓRICO DEL EQUIPO</td></tr>
                                    <?php } else{ ?>
                                        <tr><td colspan="5" class="alert alert-danger"> HISTÓRICO DEL EQUIPO</td></tr>
                                    <?php } ?>                                                         
                                        <tr>
                                            <td><b>Proceso</b></td>
                                            <td><b>Observación</b></td>
                                            <td><b>Fecha Creación</b></td>
                                            <td><b>Responsable</b></td>
                                            <td><b>Acción</b></td>
                                        </tr>
                                    <?php foreach($historicoequipos as $histequi) {?>                       
                                        <tr>
                                            <td><?php if ($histequi['proceso_id'] == 26){echo ("Actualizacion");} ?></td>
                                            <td><?php echo $histequi['observacion'] ?></td>
                                            <td><?php echo $histequi['fecha_creacion'] ?></td>
                                            <td><?php echo $histequi->users->nombres." ".$histequi->users->apellidos ?></td>
                                            <td><button type="button" class="btn btn-primary detalleequipo" id="<?php echo $histequi['historico_equipo_id']?>">ver</button></td>
                                        </tr>
                                    <?php } ?> 
                                    <?php if ($model[0]['equipo_estado_id'] == 24) { ?>
                                        <?php foreach($historicoli as $hist) {?>
                                        <?php } ?>
                                        <tr>
                                            <td><?php echo $hist->estado->tipo_detalle_nombre?></td>
                                            <td><?php echo $hist['observacion']?></td>
                                            <td><?php echo $hist['fecha_creacion']?></td>
                                            <td><?php echo $hist->user->nombres." ".$hist->user->apellidos ?></td>                                
                                            <td><a href="images/actas/<?php echo $model[0]['equipo_baja_activo']?>" dowload=<?php echo $model[0]['equipo_baja_activo']?>><img src="images/pdf.png"></a></td>
                                        </tr> 
                                    <?php }?>                   
                                        
                                <?php } else { ?>
                                    <div class="alert alert-danger" role="alert">El equipo no tiene historicos</div>
                                <?php } ?>        
                            </table>          
                            </div>
                        </div>                
                    </div>                
                </div>                                       
                <!-- FIN DEL PANEL HISTORICO --> 
            </div>        
             
          <?php }else{ ?>
          <div class="col-lg-12">
            <h1 style="color:#FFFFFF"></h1> 
            <div class="alert alert-success" role="alert"><h2>Mantenimientos del equipo: <?php echo $model[0]['equipo_identificacion_tecnologia']; ?></h2></div>       
            <!-- INICIO DEL PANEL PARA MANTENIMIENTOS -->               
                <div class="panel panel-primary">                                        
                    <!-- Default panel contents -->
                    <div class="panel-heading">Mantenimientos registrados</div>
                    <div class="panel-body">
                        <div class="table-responsive" >  
                        <?php if ($cronogramas){?>                  
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td><b>Hora de Inicio</b></td>
                                        <td><b>Hora de Finalizacion</b></td>
                                        <td><b>Quien lo Realiza</b></td>
                                        <td><b>Estado</b></td>
                                        <td><b>Aprobado</b></td>
                                        <td><b>Accion</b></td>
                                    </tr>
                                    <?php foreach ($cronogramas as $keyC => $valueC) { ?>
                                    <tr>
                                        <td><?php echo $valueC['cronograma_detalle_hora_inicio'] ?></td>
                                        <td><?php echo $valueC['cronograma_detalle_hora_fin'] ?></td>
                                        <td><?php echo $valueC['quienRealiza'] ?></td>
                                        <td><?php echo $valueC['estado'] ?></td>
                                        <td><?php echo $valueC['aprobado']."".$valueC['quienAprueba'] ?></td>
                                        <td>
                                            <?php if ($valueC['cronograma_detalle_realizado'] == 0) {  ?>
                                                
                                                <?= Html::a('Registrar Check List', ['check-list/create', 'id'=>$valueC['cronograma_detalle_equipo_id'],'cronograma'=>$valueC['cronograma_detalle_id']], ['class' => 'btn btn-success', 'id'=>'aprobar']) ?>    
                                            <?php }else{ ?>
                                                
                                                <?= Html::a('Ver Mantenimiento Registrado', ['check-list/view', 'id'=>$valueC['cronograma_detalle_id'], 'Equipo' => $valueC['cronograma_detalle_equipo_id']], ['class' => 'btn btn-primary']) ?>
                                                
                                                <!-- <?php if ($valueC['cronograma_detalle_aprobado']==1) { ?>
                                                    <img id="btn-excel" src="images/exceln.png" alt="" width="40" title="Descargar Formato">
                                                    <?php $form = ActiveForm::begin([
                                                            'action' => ['excel/checklist'],
                                                            'method' => 'post',
                                                            'id' => 'liquidaciones',
                                                        ]); ?>
                                                        <input type="hidden" name="datos" value="<?php echo htmlentities(serialize($valueC['cronograma_detalle_id']))?>">
                                                        <?php ActiveForm::end()?>
                                                <?php } ?> -->
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            <?php } else { ?> 
                            <div class="alert alert-danger" role="alert">El equipo no presenta mantenimientos</div>
                            <?php } ?>                                       
                        </div>                               
                    </div>
                </div>    
                                
            <!-- FIN DEL PANEL DE MANTENIMIENTOS --> 
        </div>    
        
    <?php } ?>
   <!-- MODAL MANTENIMIENTO -->
   <div class="modal fade" id="modalsoporte" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">                                         
                </div> -->
                <div class="modal-body text-center container12">
                    
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">                                                                                                                       
                        <div class="form-group">
                        <br>                            
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>                                                          
                        </div>                            
                    </div>
                </div>                
                </div>
            </div>
        </div>
        </div>    
    </div> 
    

   <!-- MODAL GUARDAR -->
   <div class="modal fade" id="gestionarmantenimient" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title btn btn-success btn-block" id="exampleModalLabel"><center><b>Gestionar mantenimiento</b><center></h5>                        
                </div>               
                <div class="modal-footer">
                    <div class="form-row">
                        <div class="col-lg-12">
                            <div class="solicitudpermisos-form">                                                                                                                               
                                <div class="">
                                <center><textarea name="observacion" rows="5" cols="70" class="form-control" required="required" val="" id = "observ"></textarea></center>
                                <input type="hidden" value='<?php echo $model[0]['equipo_id']?>' name="id">
                                <br>                         
                                <!-- <input type="file" class=""  id="acta" name ="actabaja">   -->                                                        
                                <br>
                                    <button type="submit" class="btn btn-warning gestionado" id='<?php echo $model[0]['equipo_id']?>'>Enviar</button>                                    
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>                                                          
                            </div>                            

                        </div>                            
                    </div>                                        
                </div>
            </div>        
        </div>    
    </div>      
<!-- MODAL INFORMACION -->
<div class="modal fade" id="Informacionhis" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">                                         
                </div> -->
                <div class="modal-body text-center container1">
                    
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">                                                                                                                       
                        <div class="form-group">
                        <br>                            
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>                                                          
                        </div>                            
                    </div>
                </div>                
                </div>
            </div>
        </div>
        </div>    
    </div> 
<!-- MODAL PING -->
    <div class="modal fade" id="ping" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><center><b>Ping :<?php echo $model[0]['equipo_ip']?></b><center></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body containeraprobar" id="contarping" >
                    !Esta seguro de aprobar la solicitud¡
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">
                        <?php $form = ActiveForm::begin(['action' => ['aprobarnom'], 'method' => 'get' ]); ?>                                                                                            
                        <div class="form-group">
                        <br>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>                                                          
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>                
                </div>
            </div>
        </div>
    </div>
<!-- MODAL INFORMACION -->
    <div class="modal fade" id="Informacion" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><center><b>! Atención ¡</b><center></h5>                        
                </div>
                <div class="modal-body alert alert-danger text-center">
                    ! Esta seguro de dar de baja al equipo :  <?php echo $model[0]['equipo_identificacion_tecnologia']?> ¡
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">                                                                                                                       
                        <div class="form-group">
                        <br>
                            <button type="button" class="btn btn-warning informacion">Aceptar</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>                                                          
                        </div>                            
                    </div>
                </div>                
                </div>
            </div>
        </div>
        </div>    
    </div> 
<!-- MODAL CONFIRMACIÓN -->
    <div class="modal fade" id="confirmacion" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><center><b>! Atención ¡</b><center></h5>                        
                </div>
                <div class="modal-body alert alert-danger text-center">
                    ! Recuerde que el proceso es irreversible ¡
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">                                                                                                                      
                        <div class="form-group">
                        <br>
                            <button type="button" class="btn btn-warning confirmacion">Aprobar</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>                                                          
                        </div>                            
                    </div>
                </div>                
                </div>
            </div>
        </div>
         </div>    
    </div> 
<!-- MODAL GUARDAR -->
    <div class="modal fade" id="guardar" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title btn btn-success btn-block" id="exampleModalLabel"><center><b>Observación</b><center></h5>                        
                </div>               
                <div class="modal-footer">
                    <div class="form-row">
                        <div class="col-lg-12">
                            <div class="solicitudpermisos-form">
                                    <?php $form = ActiveForm::begin(['action' => ['equipos/dardebaja'],'options' => ['enctype' => 'multipart/form-data', 'id'=>"form"]]); ?>                                                                                            
                                <div class="">
                                <center><textarea name="observacion" rows="5" cols="70" class="form-control" required="required" val="" id = "obs"></textarea></center>
                                <input type="hidden" value='<?php echo $model[0]['equipo_id']?>' name="id">
                                <br>                         
                                <!-- <input type="file" class=""  id="acta" name ="actabaja">   -->                        
                                <center><?= $form->field($model_equipos, "equipo_baja_activo")->fileInput(['required'=>'required'])->label('Acta baja de activo') ?></center>
                                <br>
                                    <button type="submit" class="btn btn-warning aceptar" id='<?php echo $model[0]['equipo_id']?>'>Enviar</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>                                                          
                            </div>
                            <?php ActiveForm::end(); ?>

                        </div>                            
                    </div>                                        
                </div>
            </div>        
        </div>    
    </div>
<!--MODAL ERROR -->
    <div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="contentModalError">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">cerrar</button>
            </div>
        </div>
      </div>
    </div>         


<script>
    $(document).ready(function(){        
        var obs = $('#obs').val(); 
        $("#btn-excel").click(function(){

            $("#liquidaciones").submit();
        });
        //// verificar conexion del equipo
        $(".ping").click(function(){
            alert('njfsjdfs');
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/estado',
                type : 'get',
                data :{
                    id :$(this).attr('id')
                },
                success: function(data){
                $('#contarping').html(data);
                $('#ping').modal();  
                }
            });
        });
        ///// ver detalle de mantenimiento
        $(".vermantenimiento").click(function(){            
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/vermantenimiento',
                type : 'get',
                data :{
                    id :$(this).attr('id')
                },
                success: function(data){
                $('.container12').html(data);
                $('#modalsoporte').modal();  
                }
            });            
        });
        //// gestioanar mantenimiento/////////////////////////////////////////////////
        $(".gestionmant").click(function(){                        
                $('#gestionarmantenimient').modal();                             
        });
        $(".gestionado").click(function(){
            $(this).hide();
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/gestionarmantenimiento',
                type : 'get',
                data :{
                    id :$(this).attr('id'),
                    observacion : $('#observ').val(),
                },
                success: function(data){
                    location.reload(); 
                }
            });            
        });
        
        /// proceso de baja de activos
        $('.baja').click(function(){
            $('#Informacion').modal();
        })
        $('.informacion').click(function(){
            $('#confirmacion').modal();
        })
        $('.confirmacion').click(function(){
            $('#guardar').modal();
        })
        $('.aceptar').click(function(){         
            if ($('#obs').val()!= ''){
                $.ajax({            
                    url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/dardebaja',
                    type : 'get',
                    data : {
                    id : $(this).attr('id'),                
                    },
                    success: function(data){ 
                    // alert('uuuu');               
                }
            }); 

            }else{
                var content = '<div class="alert alert-danger text-center"> ! Agregue una observación valida ¡<div>';                        
                $('#contentModalError').html(content); 
                $('#modalError').modal('show'); 
            }
            if ($('#acta').val() != ''){
                $('#archivo').css('display','block');

            }else{
                $('#archivo').css('display','none');
            }                                      
        });
        $('.detalleequipo').click(function(){            
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/verhistorico',
                type : 'get',
                data : {
                    id : $(this).attr('id'),
                },
                success: function(data){
                    $('.container1').html(data);                    
                    $('#Informacionhis').modal();
                }
            })
        })      
    })    
    
</script>