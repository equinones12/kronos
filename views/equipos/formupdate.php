<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TiposDetalles;
use app\models\Sucursales;
use app\models\Funcionarios;
use app\models\Areas;
use app\models\Equipos;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Equipos */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" href="<?php echo Url::base(); ?>/css/adminlte.min.css">

<div class="equipos-form">

    <?php $form = ActiveForm::begin(['action' => ['actualizar'], 'method' => 'get' ]); ?>
    <div class="form-row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h2 class="card-title text-center">Panel de actualización</h2>
                </div>
                <div class="form-row">                
                    <!-- //card # 1 -->
                    <div class="col-lg-12"> 
                     <div class="card card-primary">               
                        <div class="card-header">
                            <h5 class="card-title">Información general</h5>
                        </div></br>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                               <input type="hidden" value='<?php echo $model['equipo_id']?>' name="id">            
                                    
                                <?= $form->field($model, 'equipo_identificacion_tecnologia')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4 mb-3">
                                <?= $form->field($model, 'equipo_nombre_actual')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4 mb-3">
                                <?= $form->field($model, 'equipo_ip')->textInput(['maxlenght'=> true])?>
                            </div>
                            <div class="col-md-4 mb-3">  
                                <?= $form->field($model, 'equipo_tipo_equipo_id')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                where(['tipo_detalle_tipo_id'=>1])
                                                    ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                [
                                            'prompt'=>'[-- Seleccione Tipo--]',
                                            ]
                                        ) ?>
                                <div class="help-block"></div>
                            </div>    
                                        
                            <div class="col-md-4 mb-3">
                                <?= $form->field($model, 'equipo_marca')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                where(['tipo_detalle_tipo_id'=>9])
                                                    ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                            [
                                            'prompt'=>'[-- Seleccione marca --]',
                                            ]
                                        ) ?>
                                <div class="help-block"></div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <?= $form->field($model, 'equipo_modelo')->textInput(['maxlength' => true]) ?>
                            </div> 
                        </div>                                     
                     </div> 
                    </div>                                                              
                    <!-- //card # 2 -->
                    <div class="col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h5 class="card-title">Informacición técnica</h5>
                            </div></br> 
                            <div class="form-row">               
                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_ram')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                    where(['tipo_detalle_tipo_id'=>8])
                                                        ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                    [
                                                'prompt'=>'[-- Seleccione ram--]',
                                                ]
                                            ) ?>
                                    <div class="help-block"></div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_tipo_memoria_ram')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                    where(['tipo_detalle_tipo_id'=>11])
                                                        ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                    [
                                                'prompt'=>'[-- Seleccione ram--]',
                                                ]
                                            ) ?>
                                    <div class="help-block"></div>
                                </div>        
                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_marca_disco_duro')->textInput(['maxlength' => true]) ?>
                                </div>


                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_capacidad_disco_duro')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                    where(['tipo_detalle_tipo_id'=>10])
                                                        ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                    [
                                                'prompt'=>'[-- Seleccione capacid--]',
                                                ]
                                            ) ?>
                                    <div class="help-block"></div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_procesador')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                    where(['tipo_detalle_tipo_id'=>7])
                                                        ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                    [
                                                'prompt'=>'[-- Seleccione procesador--]',
                                                ]
                                            ) ?>
                                    <div class="help-block"></div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <?= $form->field($model, 'equipo_velocidad_procesador')->textInput(['maxlength' => true]) ?> 
                                </div>
                                <div class="col-md-6 mb-3">
                                    <?= $form->field($model, 'equipo_cantidad_nucleos_procesador')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <?= $form->field($model, 'equipo_pantalla')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <!-- //card # 3 -->
                    <div class="col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h5 class="card-title">ubicación y licencia</h5>
                            </div></br> 
                            <div class="form-row">                                
                                <div class="col-md-6 mb-3">
                                    <!-- <?= $form->field($model, 'equipo_systema_operativo_id')->textInput() ?>  -->
                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_systema_operativo_id')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                        where(['tipo_detalle_tipo_id'=>3])
                                                            ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Tipo--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <!-- <?= $form->field($model, 'equipo_office_id')->textInput() ?>  -->
                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_office_id')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                        where(['tipo_detalle_tipo_id'=>4])
                                                            ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Tipo--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>                                
                                <div class="col-md-6 mb-3">
                                    <!-- <?= $form->field($model, 'equipo_sucursal_id')->textInput() ?> -->
                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_sucursal_id')->dropDownList(ArrayHelper::map(Sucursales::find()
                                                            ->orderBy(['sucursal_nombre'=>SORT_ASC])->all(), 'sucursal_id', 'sucursal_nombre'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Sucursal--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <!-- <?= $form->field($model, 'equipo_area_id')->textInput() ?> -->
                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_area_id')->dropDownList(ArrayHelper::map(Areas::find()
                                                            ->orderBy(['area_nombre'=>SORT_ASC])->all(), 'area_id', 'area_nombre'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Area--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                            </div>                
                        </div>
                    </div>
                    <!-- //card # 4 -->                   
                    <div class="col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Informacición contable</h3>
                            </div></br>
                            <div class="form-row">                    
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_usuario_id')->dropDownList(ArrayHelper::map(Funcionarios::find()
                                                            ->orderBy(['funcionario_nombres'=>SORT_ASC])->all(), 'funcionario_id', 'funcionario_nombres'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Funcionario--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <!-- <?= $form->field($model, 'equipo_estado_id')->textInput() ?>  -->

                                    <div class="form-group">
                                        <?= $form->field($model, 'equipo_estado_id')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                        where(['tipo_detalle_tipo_id'=>2])
                                                            ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                        [
                                                    'prompt'=>'[-- Seleccione Tipo--]',
                                                    ]
                                                ) ?>
                                        <div class="help-block"></div>
                                    </div>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="start">Fecha de ingreso</label></br>                                    
                                    <input type="date" id="start" name="fecha" value="<?php echo $model->equipo_fecha_ingreso?>" placeholder="<?php echo $model->equipo_fecha_ingreso?>" class="form-control"> 
                                </div>

                                <div class="col-md-6 mb-3">                                   
                                    <?= $form->field($model, 'equipo_numero_activo_contabilidad')->textInput(['maxlength' => true]) ?>                                                                         
                                </div>
                                <div class="col-md-6 mb-3">
                                    <?= $form->field($model, 'equipo_valor')->textInput(['maxlength' => true]) ?>
                                </div>            

                                    
                                <div class="col-md-12 mb-3">
                                    <?= $form->field($model, 'equipo_observaciones')->textarea(['rows' => '6']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">                    
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
        </div> 
        <div class="box box-primary"></div>                        

            <?php ActiveForm::end(); ?>
        
    </div>
    

</div>
