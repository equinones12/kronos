<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Equipos */

$this->title = 'Registro de Equipo';
$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipos-create">

   

    <!-- <div class="alert alert-success" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div> -->



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
