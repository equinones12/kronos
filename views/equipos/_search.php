<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EquiposSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="equipos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'equipo_id') ?>

    <?= $form->field($model, 'equipo_identificacion_tecnologia') ?>

    <?= $form->field($model, 'equipo_nombre_actual') ?>

    <?= $form->field($model, 'equipo_tipo_equipo_id') ?>

    <?= $form->field($model, 'equipo_marca') ?>

    <?php // echo $form->field($model, 'equipo_ram') ?>

    <?php // echo $form->field($model, 'equipo_tipo_memoria_ram') ?>

    <?php // echo $form->field($model, 'equipo_cantidad_memoria_ram') ?>

    <?php // echo $form->field($model, 'equipo_marca_disco_duro') ?>

    <?php // echo $form->field($model, 'equipo_capacidad_disco_duro') ?>

    <?php // echo $form->field($model, 'equipo_procesador') ?>

    <?php // echo $form->field($model, 'equipo_velocidad_procesador') ?>

    <?php // echo $form->field($model, 'equipo_cantidad_nucleos_procesador') ?>

    <?php // echo $form->field($model, 'equipo_systema_operativo_id') ?>

    <?php // echo $form->field($model, 'equipo_office_id') ?>

    <?php // echo $form->field($model, 'equipo_pantalla') ?>

    <?php // echo $form->field($model, 'equipo_sucursal_id') ?>

    <?php // echo $form->field($model, 'equipo_usuario_id') ?>

    <?php // echo $form->field($model, 'equipo_estado_id') ?>

    <?php // echo $form->field($model, 'equipo_numero_activo_contabilidad') ?>

    <?php // echo $form->field($model, 'equipo_fecha_ingreso') ?>

    <?php // echo $form->field($model, 'equipo_valor') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
