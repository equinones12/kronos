<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Equipos */

$this->title = 'Actualización de Equipo: ' . ' ' . $model->equipo_identificacion_tecnologia;
$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->equipo_id, 'url' => ['view', 'id' => $model->equipo_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="equipos-update">
    <div class="alert alert-light" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div>
    <?= $this->render('formupdate', [
        'model' => $model,
    ]) ?>
</div>
