<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
// echo "<pre>";
// print_r($result);die();
$this->title = 'Kronos';
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Url::base(); ?>/css/site.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Url::base(); ?>/accordion/css/estilos.css"> -->
<!-- <script type="text/javascri    pt" src="<?php echo Url::base(); ?>/accordion/js/accordion.js"></script> -->
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="site-index" style="padding">
     <div class="jumbotron">
        <h2 style="color:#FFFFFF;">Diagrama de red Osa</h2>        
    </div>
        <div>
              <figure class="marco">
                <a><center><img src="images/red/rack.png"style="width: 14%; height:21%;position: absolute;left:43%;bottom:56%;"></center></a>                
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:35.5%; bottom:70.8%;z-index: 1;"><b>Equipo</b> :Rack Tecnologia</figcaption>
              </figure>                
            </div >
            <div>
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:35%; bottom:23%;"id="s1" href="#equi_s1" data-toggle="modal";></a>                                  
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:35.7%; bottom:28.9%;z-index: 1;"><b>Switch</b> : 1 <br><b>Modelo</b> : 3CBLSG24<br><b>No.puertos</b> : 24<br><b>Marca</b> : 3COM <br><b>Ubicacion</b> : RACK CT</figcaption>
              </figure>
            </div>
            <div>
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:56%; bottom:23%;"id="s2" href="#equi_s2" data-toggle="modal";></a>                                  
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:56.6%; bottom:28.9%;z-index: 1;"><b>Switch</b> : 2 <br><b>Modelo</b> : 3CBLUG24<br><b>No.puertos</b> : 24<br><b>Marca</b> : 3COM<br><b>Ubicacion</b> : RACK CT</figcaption>
              </figure>
            </div>
            <div>
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:19%; bottom:35%;"id="s3" href="#equi_s3" data-toggle="modal";></a>                                  
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:19.5%; bottom:40.8%;z-index: 1;"><b>Switch</b> : 3 <br><b>Modelo</b> : s105<br><b>No.puertos</b> : 5<br><b>Marca</b> : Tenda <br><b>Ubicacion</b> : Comunicaciones</figcaption>
              </figure>
            </div>
            <div >
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%; position: absolute; left:25%;bottom: 46.3%"; id="s5" href="#equi_s5" data-toggle="modal"></a>                
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:25.5%; bottom:51.8%;z-index: 1;"><b>Switch</b> : 5 <br><b>Modelo</b> : 3C16793<br><b>No.puertos</b> : 5<br><b>Marca</b> : 3COM <br><b>Ubicacion</b> : Transportes</figcaption>
              </figure>
            </div>
            <div >
              <figure>
                 <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:31%; bottom:35%;" id="s4" href="#equi_s4" data-toggle="modal";></a>                
                  <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:31.5%; bottom:40.8%;z-index: 1;"><b>Switch</b> : 4 <br><b>Modelo</b> : <br><b>No.puertos</b> : 24<br><b>Marca</b> :  <br><b>Ubicacion</b> : Gestion Humana</figcaption>
              </figure>
            </div>
            <div >
              <figure>
                 <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:45%;bottom: 46.3%"; id="s7" href="#equi_s7" data-toggle="modal"></a>                
                  <figcaption class="figcaption"style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:45.5%; bottom:51.8%;z-index: 1;"><b>Switch</b> : 7 <br><b>Modelo</b> : TLSG1024D<br><b>No.puertos</b> : 24<br><b>Marca</b> : TPlink <br><b>Ubicacion</b> : Call center</figcaption>
              </figure>
            </div>  
            <div >
              <figure>
                 <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:45%;bottom:35%;" id="s6" href="#equi_s6" data-toggle="modal";></a>                
                  <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:45.5%; bottom:40.8%;z-index: 1;"><b>Switch</b> : 6 <br><b>Modelo</b> : SG100D<br><b>No.puertos</b> : 8<br><b>Marca</b> : Cisco <br><b>Ubicacion</b> : Recaudo</figcaption>
              </figure>
            </div> 
            <div >
              <figure>
                <a><img src="images/red/acpoint.png"style="width: 10%; height: 16%;position: absolute; left:45%;bottom:14%" id="ac1";></a>                
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:46.7%; bottom:9.5%;z-index: 1;"><b>Equipo</b> : Acces point<br><b>Marca</b> : AP WiFi Linksys "Osa-Ap-2"<br><b>Usuario</b>: OSA-AP-2<br><b>Clave</b> :OSA2016.,.</figcaption>
              </figure>
            </div> 
            <div >
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:60%;bottom: 46.3%"; id="s8" href="#equi_s8" data-toggle="modal";></a>                
                 <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:60.5%; bottom:51.8%;z-index: 1;"><b>Switch</b> : 8 <br><b>Modelo</b> : 3CFSU08<br><b>No.puertos</b> : 8<br><b>Marca</b> : 3COM <br><b>Ubicacion</b> : Direccion Zona</figcaption>                  
              </figure>
            </div> 
            <div >
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:60%;bottom:35%"; id="s9" href="#equi_s9" data-toggle="modal"></a>                
                  <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:60.5%; bottom:40.8%;z-index: 1;"><b>Switch</b> : 9 <br><b>Modelo</b> : SWI-005<br><b>No.puertos</b> : 5<br><b>Marca</b> : STEREN <br><b>Ubicacion</b> : Recepcion</figcaption>
              </figure>
            </div> 
            <div >
              <figure>
                <a><img src="images/red/switch3.png"style="width: 10%; height: 7%;position: absolute; left:75%;bottom: 46.3%"; id="s10" href="#equi_s10" data-toggle="modal"></a>                
                  <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:75.5%; bottom:51.8%;z-index: 1;"><b>Switch</b> : 10 <br><b>Modelo</b> : DES-1008D<br><b>No.puertos</b> : 8<br><b>Marca</b> : DLINK <br><b>Ubicacion</b> : Tecnologia</figcaption>
              </figure>
            </div> 
            <div >
              <figure>
                <a><img src="images/red/acpoint.png"style="width: 10%; height: 16%;position: absolute; left:75%;bottom:27%;"; id="ac2"></a>                
                <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:76.7%; bottom:20.9%;z-index: 1;"><b>Equipo</b> : Acces point <br><b>Marca</b> : AP Wifi TPLink "Organizacion" <br><b>Usuario</b>: OrganizacionSaycoAcinpro<br><b>Clave</b> :OSADMIN2017.,.</figcaption>
              </figure>
            </div> 


             <hr style=" width: 0.1%;height: 37%;background-color:#337A63;position: absolute; left:42%;bottom:26.6%;"></hr>
             <hr style=" width: 4.4%;height: 2px;background-color:#337A63;position: absolute; left:42%;bottom: 63.4%;"></hr> 

             <hr style=" width: 0.1%;height: 42.4%;background-color:#337A63;position: absolute; left:59%;bottom:26.6%;"></hr>
             <hr style=" width: 6.4%;height: 2px;background-color:#337A63;position: absolute; left:52.7%;bottom: 69%;"></hr> 

            <hr style=" width: 22%;height: 2px;background-color:#337A63;position: absolute; left:30%;bottom: 66.3%;"></hr> 
            <hr style=" width: 0.1%;height: 16.4%;background-color:#337A63;position: absolute; left:30%;bottom: 50%;"></hr> 

            <hr style=" width: 28.3%;height: 2px;background-color:#337A63;position: absolute; left:24%;bottom: 68%;"></hr> 
            <hr style=" width: 0.1%;height: 29.7%;background-color:#337A63;position: absolute; left:24%;bottom: 38.5%;"></hr>

            <hr style=" width: 9.4%;height: 2px;background-color:#337A63;position: absolute; left:37%;bottom: 67.2%;"></hr> 
            <hr style=" width: 0.1%;height: 29%;background-color:#337A63;position: absolute; left:37%;bottom: 38.3%;"></hr> 

            <hr style=" width: 9%;height: 2px;background-color:#337A63;position: absolute; left:48.4%;bottom: 67.2%;"></hr> 
            <hr style=" width: 0.1%;height: 19%;background-color:#337A63;position: absolute; left:57.3%;bottom: 48.2%;"></hr>
            <hr style=" width: 2.9%;height: 2px;background-color:#337A63;position: absolute; left:54.5%;bottom: 48.2%;"></hr>   

            <hr style=" width: 34%;height: 2px;background-color:#337A63;position: absolute; left:46.2%;bottom: 72.2%;"></hr> 
            <hr style=" width: 0.1%;height: 22.7%;background-color:#337A63;position: absolute; left:80.1%;bottom: 49.6%;"></hr>    

            <hr style=" width: 14%;height: 2px;background-color:#337A63;position: absolute; left:50.8%;bottom: 61.2%;"></hr> 
            <hr style=" width: 0.1%;height: 11.8%;background-color:#337A63;position: absolute; left:64.8%;bottom: 49.6%;"></hr>

            <hr style=" width: 0.1%;height: 7%;background-color:#337A63;position: absolute; left:49.8%;bottom: 38.6%;"></hr>
            <hr style=" width: 0.1%;height: 15.7%;background-color:#337A63;position: absolute; left:49.8%;bottom: 18.6%;"></hr>
            <hr style=" width: 0.1%;height: 7%;background-color:#337A63;position: absolute; left:64.8%;bottom: 38.6%;"></hr>
            <hr style=" width: 0.1%;height: 14%;background-color:#337A63;position: absolute; left:80.1%;bottom: 31.6%;"></hr> 

            <hr style=" width: 0.1%;height: 1%;background-color:#337A63;position: absolute; left:46.2%;bottom: 71.4%;"></hr>                      
            <hr style=" width: 0.1%;height: 3%;background-color:#337A63;position: absolute; left:52.2%;bottom: 68.2%;"></hr>                      
            <hr style=" width: 0.1%;height: 2.2%;background-color:#337A63;position: absolute; left:46.3%;bottom: 65.2%;"></hr>
            <hr style=" width: 0.1%;height: 2.2%;background-color:#337A63;position: absolute; left:48.3%;bottom: 65.2%;"></hr>  
            <hr style=" width: 0.1%;height: 1.2%;background-color:#337A63;position: absolute; left:52%;bottom: 65.3%;"></hr>    
            <hr style=" width: 0.1%;height: 2.3%;background-color:#337A63;position: absolute; left:50.7%;bottom: 59.1%;"></hr>
  
            <!--///////////////////////// modal equipos (switch 1)!////////////////////////-->
            <div class="modal fade" id="equi_s1" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 1</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;" ></a>                            
                              <figcaption class="figcaption"style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:27%; bottom:82%;z-index: 1;"><b>Switch</b> : 2 <br><b>Modelo</b> : 3CBLUG24<br><b>No.puertos</b> : 24<br><b>Marca</b> : 3COM<br><b>Ubicacion</b> : RACK CT</figcaption>
                            </figure>                              
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/firewall.png"style="position: absolute; left:-1%; bottom:32.2%;"class="compu" id="imagen" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=1' ;></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:1%; bottom:42.5%;z-index: 1;">
                               <b>Equipo</b> : Firewall<br><b>Puerto</b> : 4 <br><b>Ip</b>:<text>192.168.0.1</text>
                              </figcaption>                            
                            </figure>
                              <hr style=" width: 48.4%;height: 2px;background-color:#337A63;position: absolute; left:5.3%;bottom: 72.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 12.2%;background-color:#337A63;position: absolute; left:53.4%;bottom: 72.6%;"></hr>                              
                              <hr style=" width: 0.2%;height: 35.2%;background-color:#337A63;position: absolute; left:5.1%;bottom: 37.4%;"></hr> 
                          </div>                                                                                                                                                                         
                          <div>
                            <figure>                  
                              <a><img src="images/red/server.png"style="height:14%;width:9%; position: absolute; left:18.3%; bottom:33.1%;";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:17%; bottom42.2%;z-index: 1;">
                                 <b>Equipo</b> : Servidor <br><b>Puerto</b> : 7 <br><b>Ip</b>:<text>192.168.0.240</text>
                               </figcaption>
                            </figure>                                                              
                              <hr style=" width: 25.6%;height: 2px;background-color:#337A63;position: absolute; left:23.4%;bottom: 84.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 42%;background-color:#337A63;position: absolute; left:23.2%;bottom: 42.6%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:24.5%; bottom:52%;" class="compu" id="imagen" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=2';></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:24%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Huellero<br><b>Puerto</b> : Une<br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 5.2%;background-color:#337A63;position: absolute; left:51.6%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 23.6%;height: 2px;background-color:#337A63;position: absolute; left:28%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 23.6%;background-color:#337A63;position: absolute; left:28%;bottom: 55.9%;"></hr>                                                                                          
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:29%; bottom:33%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:29.6%; bottom:42.2%;z-index: 1;">
                                <b>Equipo</b> : Asistente impuestos<br><b>Puerto</b> : 10 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure>
                              <hr style=" width: 13.6%;height: 2px;background-color:#337A63;position: absolute; left:38.3%;bottom: 85.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 47%;background-color:#337A63;position: absolute; left:38.1%;bottom: 38.6%;"></hr>                              
                          </div> 
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:38%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:41%; bottom:61%;z-index: 1;">
                               <b>Equipo</b> : Auxiliar contabilidad<br><b>Puerto</b> : 12 <br><b>Ip</b>:<text class="ip"></text>
                            </figure> 
                              <hr style=" width: 0.2%;height:20.8%;background-color:#337A63;position: absolute; left:43.7%;bottom: 55.9%;"></hr> 
                              <hr style=" width: 6.1%;height: 2px;background-color:#337A63;position: absolute; left:43.9%;bottom: 76.4%;"></hr> 
                              <hr style=" width: 0.2%;height: 8.5%;background-color:#337A63;position: absolute; left:49.8%;bottom: 76.4%;"></hr>                             
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:45%; bottom:33%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:48%; bottom:42.2%;z-index: 1;">
                                <b>Equipo</b> : Fondo empleados<br><b>Puerto</b> : 13 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height:46.6%;background-color:#337A63;position: absolute; left:55%;bottom: 38.6%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/plantatel.png"style="position: absolute; left:58%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:59.6%; bottom:61%;z-index: 1;"><b>Equipo</b> : Planta telefonica<br><b>Puerto</b> : 14 <br><b>Ip</b>:<text>pendiente</text></figcaption>
                            </figure>
                            <hr style=" width: 0.2%;height: 9.6%;background-color:#337A63;position: absolute; left:56%;bottom: 75.2%;"></hr>
                              <hr style=" width: 15.1%;height: 2px;background-color:#337A63;position: absolute; left:56.1%;bottom: 75.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 37%;background-color:#337A63;position: absolute; left:70.9%;bottom: 38.6%;"></hr>  
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:62%; bottom:33%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:68.6%; bottom:42.2%;z-index: 1;">
                                <b>Equipo</b> : Asistente contabilidad<br><b>Puerto</b> : 15 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.3%;height: 7.1%;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 12.6%;height: 2px;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 22%;background-color:#337A63;position: absolute; left:65%;bottom:57.6%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:72%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:74.6%; bottom:61%;z-index: 1;">
                               <b>Equipo</b> : Compras<br><b>Puerto</b> : 18 <br><b>Ip</b>:<text>192.168.0.45</text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 30.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 23.8%;background-color:#337A63;position: absolute; left:81%;bottom: 57.6%;"></hr>
                          </div>
                          <div>
                            <figure>                  
                              <a><img src="images/red/impresora.png"style="position: absolute; left:6%; bottom:52%;" class="compu";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:7%; bottom:61%;z-index: 1;"><b>Equipo</b> : Impresora Contabilidad<br><b>Puerto</b> : 19 <br><b>Ip</b>:<text>192.168.0.200</text></figcaption>
                            </figure>                                                            
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:12.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 23.8%;background-color:#337A63;position: absolute; left:12%;bottom: 57.8%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr>                                                           
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:80.6%; bottom:32.9%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=22'id="22";></a>                                            
                                <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:42.2%;z-index: 1;">
                                   <b>Equipo</b> : Aux. auditoria<br><b>Puerto</b> : 21<br><b>Ip</b>:<text class="ip"></text><br></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:59.6%;bottom: 85.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 27.7%;background-color:#337A63;position: absolute; left:96%;bottom: 57.9%;"></hr>                              
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:85.6%; bottom:52%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=20'id="20";></a>                                            
                                <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:83.9%; bottom:61%;z-index: 1;">
                                  <b>Equipo</b> : Servicio al cliente<br><b>Puerto</b> : 23 <br><b>Ip</b>:<text class="ip"></text><br></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 27.8%;height: 2px;background-color:#337A63;position: absolute; left:57.8%;bottom: 84.4%;"></hr> 
                              <hr style=" width: 0.2%;height: 48.2%;background-color:#337A63;position: absolute; left:85.5%;bottom: 36.5%;"></hr>                              
                          </div>                                                                                                                         
                        </div>

                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal Equipos (switch 2)!////////////////////////--> 
            <div class="modal fade" id="equi_s2" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 2</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:27%; bottom:82%;z-index: 1;"><b>Switch</b> : 2 <br><b>Modelo</b> : 3CBLUG24<br><b>No.puertos</b> : 24<br><b>Marca</b> : 3COM<br><b>Ubicacion</b> : RACK CT</figcaption>
                            </figure>                              
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:1%; bottom:33.2%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=17' id="17";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:1%; bottom:42.5%;z-index: 1;">
                               <b>Equipo</b> : Tesoreria<br><b>Puerto</b> : 1<br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>                            
                            </figure>
                              <hr style=" width: 48.4%;height: 2px;background-color:#337A63;position: absolute; left:5.3%;bottom: 72.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 12.2%;background-color:#337A63;position: absolute; left:53.4%;bottom: 72.6%;"></hr>                              
                              <hr style=" width: 0.2%;height: 35.3%;background-color:#337A63;position: absolute; left:5.1%;bottom: 37.2%;"></hr> 
                          </div>                                                                                                                                                                         
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:14.3%; bottom:33.1%;" class="compu";></a>                            
                              <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:17%; bottom:42.2%;z-index: 1;">
                                <b>Equipo</b> : Asistente juridico <br><b>Puerto</b> : 4<br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure>                                                              
                              <hr style=" width: 25.6%;height: 2px;background-color:#337A63;position: absolute; left:23.4%;bottom: 84.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 46%;background-color:#337A63;position: absolute; left:23.2%;bottom: 38.6%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:24.5%; bottom:52%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=158' id="158"></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:auto; background-color:#C3C3C3;position: absolute; left:24%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Dir. juridica<br><b>Puerto</b> : 6<br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 5.2%;background-color:#337A63;position: absolute; left:51.6%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 23.6%;height: 2px;background-color:#337A63;position: absolute; left:28%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 23.6%;background-color:#337A63;position: absolute; left:28%;bottom: 55.9%;"></hr>                                                                                          
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:29%; bottom:33%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=151' id="151";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:29.6%; bottom:42.2%;z-index: 1;">
                               <b>Equipo</b> : Analista programacion<br><b>Puerto</b> : 8 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure>
                              <hr style=" width: 13.6%;height: 2px;background-color:#337A63;position: absolute; left:38.3%;bottom: 85.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 47%;background-color:#337A63;position: absolute; left:38.1%;bottom: 38.6%;"></hr>                              
                          </div> 
                          <div >
                            <figure>  
                             <a><img src="images/red/portatil.png"style="position: absolute; left:38%; bottom:52%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=154' id="154";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:41%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Dir. tecnologia<br><b>Puerto</b> : 10 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 19.5%;background-color:#337A63;position: absolute; left:43.7%;bottom: 57.3%;"></hr> 
                              <hr style=" width: 6.1%;height: 2px;background-color:#337A63;position: absolute; left:43.9%;bottom: 76.4%;"></hr> 
                              <hr style=" width: 0.2%;height: 8.5%;background-color:#337A63;position: absolute; left:49.8%;bottom: 76.4%;"></hr>                             
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:45%; bottom:33%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=27' id="27";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:48%; bottom:42.2%;z-index: 1;">
                               <b>Equipo</b> : Asesor recaudo 1<br><b>Puerto</b> : 12 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height:46.6%;background-color:#337A63;position: absolute; left:55%;bottom: 38.6%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:58%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:59.6%; bottom:61%;z-index: 1;">
                               <b>Equipo</b> : Asesor recaudo 2<br><b>Puerto</b> : 13 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure>
                            <hr style=" width: 0.2%;height: 9.6%;background-color:#337A63;position: absolute; left:56%;bottom: 75.2%;"></hr>
                              <hr style=" width: 15.1%;height: 2px;background-color:#337A63;position: absolute; left:56.1%;bottom: 75.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 37%;background-color:#337A63;position: absolute; left:70.9%;bottom: 38.6%;"></hr>  
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:62%; bottom:33%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:63.8%; bottom:42.2%;z-index: 1;">
                               <b>Equipo</b> : Asesor recaudo 3<br><b>Puerto</b> : 14 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.3%;height: 7.1%;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 15.6%;height: 2px;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 22%;background-color:#337A63;position: absolute; left:68%;bottom:57.6%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:72%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:74.6%; bottom:61%;z-index: 1;">
                               <b>Equipo</b> : Asesor recaudo 4<br><b>Puerto</b> : 15 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 30.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 23.8%;background-color:#337A63;position: absolute; left:81%;bottom: 57.6%;"></hr>
                          </div>
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:6%; bottom:52%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=28' id="28";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:7%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Asesor recaudo 5<br><b>Puerto</b> : 16 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure>                                                            
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 23.8%;background-color:#337A63;position: absolute; left:15%;bottom: 57.8%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr>                                                           
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:80.6%; bottom:32.9%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=7' id="7";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:42.2%;z-index: 1;">
                               <b>Equipo</b> : Caja recaudo<br><b>Puerto</b> : 17 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure>
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:59.6%;bottom: 85.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 28.7%;background-color:#337A63;position: absolute; left:96%;bottom: 56.7%;"></hr>                              
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/dvr.png"style="position: absolute; left:85.6%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:83.9%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : DVR CCTV<br><b>Puerto</b> : 23 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure>
                              <hr style=" width: 27.8%;height: 2px;background-color:#337A63;position: absolute; left:57.8%;bottom: 84.4%;"></hr> 
                              <hr style=" width: 0.2%;height: 48.2%;background-color:#337A63;position: absolute; left:85.5%;bottom: 36.5%;"></hr>                              
                          </div>                                                                                                                         
                        </div>

                    </div>
                </div>                                             
            </div>          
            <!--///////////////////////// modal equipos (switch 3)!////////////////////////-->                    
            <div class="modal fade" id="equi_s3" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="height=50%;left=50%;">Equipos Switch 3</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 3 <br><b>Modelo</b> : s105<br><b>No.puertos</b> : 5<br><b>Marca</b> : Tenda <br><b>Ubicacion</b> : Comunicaciones</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/portatil.png"style="position: absolute; left:9%; bottom:52%;" class="portatil" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=21' id="21";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:7%; bottom:59%;z-index: 1;">
                                 <b>Equipo</b> : Comunicaciones <br><b>Puerto</b> : 2 <br><b>Ip</b>:<text class="ip"></text>                                 
                                </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:47%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=18' id="18";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:46%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Aux Tesoreria <br><b>Puerto</b> : 4 <br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 25.5%;background-color:#337A63;position: absolute; left:87%;bottom: 56%;"></hr>
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:83%; bottom:52%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=19' id="19";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Dir.calidad <br><b>Puerto</b> : 5 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption> 
                            </figure> 
                          </div>                                                                                                
                        </div>

                    </div>
                </div>                                             
            </div> 
            <!--///////////////////////// modal equipos (switch 5)!////////////////////////-->
            <div class="modal fade" id="equi_s5" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 5</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                         <div>
                            <figure>
                              <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                                <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 5 <br><b>Modelo</b> : 3C16793<br><b>No.puertos</b> : 5<br><b>Marca</b> : 3COM <br><b>Ubicacion</b> : Transportes</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:9%; bottom:52%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=15'id="15";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:7%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Asistente transportes<br><b>Puerto</b> : 2 <br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>
                          </div>
                          <div>
                           <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:47%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=16'id="16";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:46%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Dir.Usu especiales<br><b>Puerto</b> : 3 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                           </figure>
                          </div>
                                                                                                                                                  
                        </div><!--jhjjjj!-->

                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 4)!////////////////////////-->  
            <div class="modal fade" id="equi_s4" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 4</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 4 <br><b>Modelo</b> : <br><b>No.puertos</b> : 24<br><b>Marca</b> :  <br><b>Ubicacion</b> : Gestion Humana</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:9%; bottom:52%;"class="compu" ;></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:7%; bottom:59%;z-index: 1;">
                                <b>Equipo</b> : Aux.dpto.juridico<br><b>Puerto</b> : 1 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:47%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=12' id="12";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:46%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Asistente dir.ejecutiva<br><b>Puerto</b> : 3 <br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 25.5%;background-color:#337A63;position: absolute; left:87%;bottom: 56%;"></hr>
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/portatil.png"style="position: absolute; left:83%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Aux.nomina <br><b>Puerto</b> : 4 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                          </div>                                                                                                
                        </div>

                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 6)!////////////////////////-->  
            <div class="modal fade" id="equi_s6" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 6</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                         <div>
                            <figure>
                              <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                                <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 6 <br><b>Modelo</b> : SG100D<br><b>No.puertos</b> : 8<br><b>Marca</b> : Cisco <br><b>Ubicacion</b> : Recaudo</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>
                          <div>
                            <figure>                  
                              <a><img src="images/red/impresora.png"style="position: absolute; left:9%; bottom:52%;" class="compu";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:7%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Impresora recaudo<br><b>Puerto</b> : 4 <br><b>Ip</b>: 192.168.0.112
                              </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>
                          </div>
                          <div>
                           <figure> 
                              <a><img src="images/red/acpoint.png"style="width: 18%; height: 22%;position: absolute; left:41%; bottom:50%;";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:42%; bottom:69%;z-index: 1;">
                                <b>Equipo</b> : Acces point<br>AP WiFi Linksys "Osa-Ap-2"<br><b>Puerto</b> : 7
                               </figcaption>
                           </figure>
                          </div>
                                                                                                                                                  
                        </div><!--jhjjjj!-->

                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 7)!////////////////////////-->
            <div class="modal fade" id="equi_s7" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 7</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 7 <br><b>Modelo</b> : TLSG1024D<br><b>No.puertos</b> : 24<br><b>Marca</b> : TPlink <br><b>Ubicacion</b> : Call center</figcaption>
                            </figure>                              
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:16%; bottom:35%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=34' id="34";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:15%; bottom:44%;z-index: 1;">
                                  <b>Equipo</b> : Coordinador call center <br><b>Puerto</b> : 1 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                                </figcaption>
                            </figure>                                                              
                              <hr style=" width: 25.6%;height: 2px;background-color:#337A63;position: absolute; left:23.4%;bottom: 84.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 46%;background-color:#337A63;position: absolute; left:23.2%;bottom: 38.6%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:25%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=35' id="35";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:24%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Call center activo 001-00318<br><b>Puerto</b> : 2 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                                </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 5.2%;background-color:#337A63;position: absolute; left:51.6%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 20.6%;height: 2px;background-color:#337A63;position: absolute; left:31%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 23.6%;background-color:#337A63;position: absolute; left:31%;bottom: 55.9%;"></hr>                                                                                          
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:34%; bottom:35%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=36' id="36";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:29.6%; bottom:44%;z-index: 1;">
                                <b>Equipo</b> : Call center activo 001-00164<br><b>Puerto</b> : 3 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                               </figcaption>
                            </figure>
                              <hr style=" width: 13.6%;height: 2px;background-color:#337A63;position: absolute; left:38.3%;bottom: 85.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 47%;background-color:#337A63;position: absolute; left:38.1%;bottom: 38.6%;"></hr>                              
                          </div> 
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:42%; bottom:52%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=37' id="37";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:41%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Call center activo 001-00167<br><b>Puerto</b> : 4 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>                              
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:52%; bottom:35%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=38' id="38";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:48%; bottom:44%;z-index: 1;">
                                <b>Equipo</b> : Call center activo 001-00305<br><b>Puerto</b> : 5 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height:46.6%;background-color:#337A63;position: absolute; left:55%;bottom: 38.6%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:63%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=33' id="33";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:58.6%; bottom:60.9%;z-index: 1;">
                                <b>Equipo</b> : Call center activo 001-00221<br><b>Puerto</b> : 6 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                               </figcaption>
                            </figure>
                            <hr style=" width: 0.2%;height: 9.6%;background-color:#337A63;position: absolute; left:56%;bottom: 75.2%;"></hr>
                              <hr style=" width: 22.1%;height: 2px;background-color:#337A63;position: absolute; left:56.1%;bottom: 75.3%;"></hr> 
                              <hr style=" width: 0.2%;height: 37%;background-color:#337A63;position: absolute; left:78%;bottom: 38.6%;"></hr>  
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:73%; bottom:35%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=32' id="32" ;></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:68.6%; bottom:44.6%;z-index: 1;">
                                <b>Equipo</b> : Call center activo 001-00326<br><b>Puerto</b> : 9 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.3%;height: 7.1%;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 15.6%;height: 2px;background-color:#337A63;position: absolute; left:52.4%;bottom: 79.3%;"></hr> 
                              <hr style=" width: 0.3%;height: 23.6%;background-color:#337A63;position: absolute; left:68%;bottom: 56%;"></hr> 
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:82%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=38' id="38";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:61%;z-index: 1;">
                               <b>Equipo</b> : Call center activo 001-00305<br><b>Puerto</b> : 10 <br><b>Ip</b>:<text class="ip"></text><br><b>Estado</b>:<text class="estado"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 25.5%;background-color:#337A63;position: absolute; left:87%;bottom: 56%;"></hr>
                          </div>
                          <div>
                            <figure>                  
                              <a><img src="images/red/impresora.png"style="position: absolute; left:9%; bottom:52%;" class="compu";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:7%; bottom:59%;z-index: 1;">
                                 <b>Equipo</b> : Impresora Caja recaudo<br><b>Puerto</b> : 14 <br><b>Ip</b>: 192.168.0.113
                                </figcaption>
                            </figure>                                                            
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr>                                                           
                          </div>                                                                                                                         
                        </div>

                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 8)!////////////////////////-->
            <div class="modal fade" id="equi_s8" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 8</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 8 <br><b>Modelo</b> : 3CFSU08<br><b>No.puertos</b> : 8<br><b>Marca</b> : 3COM <br><b>Ubicacion</b> : Direccion Zona</figcaption>
                            </figure>                              
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/impresora.png"style="position: absolute; left:9%; bottom:52%;" class="compu";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:9%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b>: Impresora direccion<br><b>Puerto</b> : 3<br><b>Ip</b>:192.168.0.203
                                </figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 23.9%;background-color:#337A63;position: absolute; left:15%;bottom: 57.7%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr>                                                         
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:33%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=163' id="163";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:32%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> :Dir.Zona <br><b>Puerto</b> : 4 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure>
                              <hr style=" width: 0.2%;height: 8.1%;background-color:#337A63;position: absolute; left:51.5%;bottom: 77%;"></hr> 
                              <hr style=" width: 11.6%;height: 2px;background-color:#337A63;position: absolute; left:40%;bottom:77.2%;"></hr>
                              <hr style=" width: 0.2%;height: 21.5%;background-color:#337A63;position: absolute; left:40%;bottom: 56%;"></hr>                                                          
                          </div>
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:61%; bottom:52%;"class="compu"href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=160' id="160";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:60%; bottom:61%;z-index: 1;">
                                <b>Equipo</b> : Admin interno<br><b>Puerto</b> : 6 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 8.1%;background-color:#337A63;position: absolute; left:52.5%;bottom: 77%;"></hr> 
                              <hr style=" width: 11.6%;height: 2px;background-color:#337A63;position: absolute; left:52.4%;bottom:77.2%;"></hr>
                              <hr style=" width: 0.2%;height: 21.5%;background-color:#337A63;position: absolute; left:64%;bottom: 56%;"></hr>
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/compu.png"style="position: absolute; left:83%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:82.6%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Coord.externo<br><b>Puerto</b> : 7 <br><b>Ip</b>:<text class="ip"></text>
                              </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 25.5%;background-color:#337A63;position: absolute; left:87%;bottom: 56%;"></hr>
                          </div>                                                                                                
                        </div>
                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 9)!////////////////////////-->
            <div class="modal fade" id="equi_s9" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 9</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 9 <br><b>Modelo</b> : SWI-005<br><b>No.puertos</b> : 5<br><b>Marca</b> : STEREN <br><b>Ubicacion</b>: Recepcion</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:9%; bottom:52%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=152' id="152";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:8%; bottom:61%;z-index: 1;">
                                <b>equipo</b> : Recepcion<br>Puerto : 2 <br><b>Ip</b>:<text class="ip"></text>
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/huellero.png"style="width:7%; height:12%;position: absolute; left:46.3%; bottom:47%;";></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:42%; bottom:59%;z-index: 1;">
                                 <b>equipo</b> : Huellero<br>Puerto : 3 <br><b>Ip</b>:192.168.0.158
                               </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr> 
                              <hr style=" width: 36.6%;height: 2px;background-color:#337A63;position: absolute; left:50.6%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.2%;height: 25.5%;background-color:#337A63;position: absolute; left:87%;bottom: 56%;"></hr>
                          </div>
                          <div >
                            <figure>  
                             <a><img src="images/red/impresora.png"style="position: absolute; left:81%; bottom:52%;"class="compu";></a>                                            
                              <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:79.6%; bottom:61%;z-index: 1;">
                               <b>equipo</b> :Impresora recepcion<br>Puerto : 4 <br><b>Ip</b>:192.168.0.103
                              </figcaption>
                            </figure> 
                          </div>                                                                                                
                        </div>
                    </div>
                </div>                                             
            </div>
            <!--///////////////////////// modal equipos (switch 10)!////////////////////////--> 
            <div class="modal fade" id="equi_s10" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg " role="document" id="modal1">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Equipos Switch 10</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal1" id="containeraprobar" > 
                          <div >
                            <figure>
                            <a><img src="images/red/switch3.png"style="width: 18%; height: 7%;position: absolute; left:44%; bottom:85%;"></a>                            
                              <figcaption class="figcaption"style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:32%; bottom:82%;z-index: 1;"><b>Switch</b> : 10 <br><b>Modelo</b> : DES-1008D<br><b>No.puertos</b> : 8<br><b>Marca</b> : DLINK <br></b>Ubicacion</b> : Tecnologia</figcaption>
                            </figure>
                              <hr style=" width: 33.6%;height: 2px;background-color:#337A63;position: absolute; left:15.2%;bottom: 81.3%;"></hr>
                              <hr style=" width: 0.3%;height: 25.5%;background-color:#337A63;position: absolute; left:15%;bottom: 56%;"></hr>
                              <hr style=" width: 0.2%;height: 5.1%;background-color:#337A63;position: absolute; left:48.7%;bottom: 81.3%;"></hr> 
                          </div>                                                                                                                                                
                          <div>
                            <figure>                  
                              <a><img src="images/red/compu.png"style="position: absolute; left:9%; bottom:52%;" class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=155' id="155";></a>                            
                               <figcaption class="figcaption" style="height:auto; width:13%; background-color:#C3C3C3;position: absolute; left:8%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Aux. base de datos<br><b>Puerto</b> : 3 <br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure> 
                              <hr style=" width: 0.2%;height: 30.5%;background-color:#337A63;position: absolute; left:49.7%;bottom: 56%;"></hr>                          
                          </div> 
                          <div >
                            <figure> 
                              <a><img src="images/red/compu.png"style="position: absolute; left:47%; bottom:52%;"class="compu" href='<?php echo Yii::$app->request->baseUrl?>/?r=equipos%2Fview&id=4' id="4" ;></a>                                             
                               <figcaption class="figcaption" style="height:auto; width:16%; background-color:#C3C3C3;position: absolute; left:46%; bottom:61%;z-index: 1;">
                                 <b>Equipo</b> : Analista infraestructura<br><b>Puerto</b> : 7 <br><b>Ip</b>:<text class="ip"></text>
                                </figcaption>
                            </figure>                                
                          </div>                                                                                                
                        </div>
                    </div>
                </div>                                             
            </div>
           
     
        </div>   
</div>  


            <!--margen -->
 
<!-- 
 <audio   src="<?php echo Url::base(); ?>/images/fotos/audio.mp3" preload="auto" ></audio>
 
 <audio controls autoplay> 
  <source src="horse.ogg" type="audio/ogg">
  <source src="<?php echo Url::base(); ?>/images/fotos/audio.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>  -->

<!-- <script type="text/javascript">
    $(document).ready(function(){
        $("#s3").hover(function(){
            $('#switch3').css("display", "block");            
            },
              function(){
                $('#switch3').css("display", "none");
              }
            );
        $("#r1").hover(function(){
            $('#rack').css("display", "block");            
            },
              function(){
                $('#rack').css("display", "none");
              }
            );
        $("#s5").hover(function(){
            $('#switch5').css("display", "block");            
            },
              function(){
                $('#switch5').css("display", "none");
              }
            );
        $("#s4").hover(function(){
            $('#switch4').css("display", "block");            
            },
              function(){
                $('#switch4').css("display", "none");
              }
            );
        $("#s6").hover(function(){
            $('#switch6').css("display", "block");            
            },
              function(){
                $('#switch6').css("display", "none");
              }
            );    
        $("#s7").hover(function(){
            $('#switch7').css("display", "block");            
            },
              function(){
                $('#switch7').css("display", "none");
              }
            );      
        $("#ac1").hover(function(){
            $('#access1').css("display", "block");            
            },
              function(){
                $('#access1').css("display", "none");
              }
            );
        $("#ac2").hover(function(){
            $('#access2').css("display", "block");            
            },
              function(){
                $('#access2').css("display", "none");
              }
            );
        $("#s8").hover(function(){
            $('#switch8').css("display", "block");            
            },
              function(){
                $('#switch8').css("display", "none");
              }
            ); 
        $("#s9").hover(function(){
            $('#switch9').css("display", "block");            
            },
              function(){
                $('#switch9').css("display", "none");
              }
            );
        $("#s10").hover(function(){
            $('#switch10').css("display", "block");            
            },
              function(){
                $('#switch10').css("display", "none");
              }
            );       
    });

</script> -->

<script type="text/javascript">
  $(document).ready(function(){    
      $(".compu").click(function(event){
        window.open( $(this).attr('href'));
      });
      $(".portatil").click(function(event){
        window.open( $(this).attr('href'));
      });

   $('.compu').hover(function(){    
    $.ajax({
      url : '<?php echo Yii::$app->request->baseUrl?>/?r=equipos/ip',
      type :'get',
      data :{
        id :$(this).attr('id')
      },
      success: function(data){
       $('.ip').html(data);
      }      
    });
  }); 
 
  // $('.compu').hover(function(){    
  //   $.ajax({
  //     url : '<?php echo Yii::$app->request->baseUrl?>/?r=equipos/estado',
  //     type :'get',
  //     data :{
  //       id :$(this).attr('id')
  //     },
  //     success: function(data){
  //      $('.estado').html(data);
  //     }      
  //   });
  // });      
 });
</script>

