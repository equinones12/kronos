<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Equipos registrados';
$this->params['breadcrumbs'][] = $this->title;
?>
    
<div class="card card-purple">
    <div class="card-header">
    <!-- <h3 class="card-title">DataTable with default features</h3> -->Resultados encontrados
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            <?= Html::a('Registrar Equipo', ['equipos/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="col-lg-12">
            <div class="row">
             <div class="table-responsive">              
                <table id="example1" class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                    <th>Identificación tecnologia</th>
                    <th>Nombre actual</th>
                    <th>Tipo de Equipo</th>
                    <th>equipo_marca</th>
                    <th>Sucursal</th>
                    <th>Funcionario</th>
                    <th>Estado</th>
                    <th>No. Activo</th>
                    <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model as $key => $value) { ?>
                    <tr>
                        <td><?php echo $value['equipo_identificacion_tecnologia'] ?></td>
                        <td><?php echo $value['equipo_nombre_actual'] ?></td>
                        <td><?php echo $value->tipoMaquina->tipo_detalle_nombre ?></td>
                        <td><?php echo $value->marca->tipo_detalle_nombre?></td>                        
                        <td><?php echo $value->sucursal['sucursal_nombre'] ?></td>
                        <td><?php echo $value->funcionarios['funcionario_nombres']?></td>
                        <td><?php echo $value->estado->tipo_detalle_nombre?></td>
                        <td><?php echo $value->equipo_numero_activo_contabilidad?></td>
                        <td>
                            <?= Html::a('Ver', ['equipos/view', 'id'=> $value['equipo_id']], ['class' => 'btn btn-primary btn-block ']) ?>
                            <?= Html::a('Actualizar', ['equipos/update', 'id'=> $value['equipo_id']], ['class' => 'btn btn-warning btn-block ']) ?>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div>  
            </div>
        </div>    
    </div>
    <!-- /.card-body -->
</div>
<script>
  $(function () {
    $("#example1").DataTable({
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>