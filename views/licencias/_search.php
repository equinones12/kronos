<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LicenciasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licencias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'licencia_id') ?>

    <?= $form->field($model, 'licencia_equipo_id') ?>

    <?= $form->field($model, 'licencia_tipo_licencia_id') ?>

    <?= $form->field($model, 'licencia_numero') ?>

    <?= $form->field($model, 'licencia_version') ?>

    <?php // echo $form->field($model, 'licencia_especificaciones') ?>

    <?php // echo $form->field($model, 'licencia_estado_id') ?>

    <?php // echo $form->field($model, 'licencia_sucursal_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
