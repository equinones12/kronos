<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Licencias */

$this->title = 'Actualizacion de Licencia : ' . ' ' . $model->tipolicencia->tipo_detalle_nombre;
$this->params['breadcrumbs'][] = ['label' => 'Licencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->licencia_id, 'url' => ['view', 'id' => $model->licencia_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="licencias-update">

    <!-- <div class="alert alert-success" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div> -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
