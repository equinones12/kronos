<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sucursales;
use app\models\Funcionarios;
use app\models\Tipos;
use app\models\TiposDetalles;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Licencias */
/* @var $form yii\widgets\ActiveForm */

// print_r($model);die();
// echo "<pre>";
?>
 <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="licencias-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Panel de licencia</h3>                    
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                     <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">Información general</h4>
                        </div></br>
                        <div class="form-row">
                           <div class="col-md-6 mb3">
                             <?= $form->field($model, 'licencia_identificacion_tecnologia')->textInput(['maxlength' => true]) ?>
                           </div>
                           <div class="col-md-6 mb3">
                                <?= Html::label('Sucursal', 'select-sucusales', ['class'=>'control-label']) ?>
                                    <?= Html::dropDownList(
                                        'select-sucusales',
                                        $model->licencia_sucursal_id,
                                        ArrayHelper::map(Sucursales::find()->orderBy(['sucursal_nombre'=>SORT_ASC])->all(),'sucursal_id', 'sucursal_nombre'),
                                    [
                                            'class'=>'form-control',
                                            'prompt'=>'[-- Seleccione Sucursal --]',
                                            'value'=> 0,
                                            'id' => 'select-sucusales',
                                            'required' => 'required',
                                            'onchange' => '$.post("index.php?r=funcionarios/lists&id='.'"+$(this).val(), function(data){
                                                    $("#select-funcionarios").val("");
                                                    $("#select-funcionarios").html(data);
                                                    $("#select-funcionarios").removeAttr("disabled");
                                            })',
                                        ]) 
                                ?>
                           </div>
                           <div class="col-md-12 mb3">
                           <?= Html::label('Funcionario', 'select-funcionarios2', ['class'=>'control-label']) ?>
                                    <?= Html::dropDownList(
                                        'select-funcionarios',
                                        $model->licencia_equipo_id,
                                        ArrayHelper::map(funcionarios::find()
                                                                ->orderBy(['funcionario_nombres'=>SORT_ASC])
                                                                ->all(), 'equipos.equipo_id', 'funcionario_nombres'),
                                        [
                                            'class'=>'form-control',
                                            'prompt'=>'[-- Seleccione Funcionario --]',
                                            'value'=> 0,
                                            'required' => 'required',
                                            'id'=>'select-funcionarios',
                                            'onchange' => '',
                                            'disabled' => 'disabled',                            
                                        ]) 
                                    ?>
                           </div>
                        </div>
                      </div>
                    </div>                    
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4 class="card-title">Informacición técnica</h4>
                            </div></br>                                                                                
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <?= Html::label('Tipo de licencia', 'select-tipo1', ['class'=>'control-label']) ?>
                                        <?= Html::dropDownList(
                                            'select-tipol',
                                            '',
                                            ArrayHelper::map(Tipos::find()
                                                                    ->Where(['in','tipo_id', array(3,4)])
                                                                    ->orderBy(['tipo_nombre'=>SORT_ASC])->all(),'tipo_id', 'tipo_nombre'),
                                        [
                                                'class'=>'form-control',
                                                'prompt'=>'[-- Seleccione Tipo de Licencia --]',
                                                'value'=> 0,
                                                'id' => 'select-tipo1',
                                                'required' => 'required',
                                                'onchange' => '$.post("index.php?r=licencias/lists&id='.'"+$(this).val(), function(data){
                                                        $("#select-licencias").val("");
                                                        $("#select-licencias").html(data);
                                                        $("#select-licencias").removeAttr("disabled");
                                                })',
                                            ]) 
                                        ?>
                              </div>
                              <div class="col-md-6 mb-3">
                                <?= Html::label('Producto', 'select-tiposD', ['class'=>'control-label']) ?>
                                    <?= Html::dropDownList(
                                        'select-tiposD',
                                        $model->licencia_tipo_licencia_id,
                                        ArrayHelper::map(TiposDetalles::find()
                                                                ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])
                                                                ->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'),
                                        [
                                            'class'=>'form-control',
                                            'prompt'=>'[-- Seleccione Licencia --]',
                                            'value'=> 0,
                                            'id'=>'select-licencias',
                                            'onchange' => '',
                                            'disabled' => 'disabled',
                                        ]) 
                                    ?>
                              </div> 
                              <div class="col-md-6 mb3">
                               <?= $form->field($model, 'licencia_version')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                                    where(['tipo_detalle_tipo_id'=>12])
                                                        ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                                [
                                                'prompt'=>'[-- Seleccione Tipo--]',
                                                ]
                                            ) ?>
                                    <div class="help-block"></div>
                              </div> 
                              <div class="col-md-6 mb3">
                                 <?= $form->field($model, 'licencia_numero')->textInput(['maxlength' => true]) ?>
                              </div>
                              <div class="col-md-12 mb3">
                                 <?= $form->field($model, 'licencia_especificaciones')->textarea(['rows' => 6]) ?>
                              </div>
                            </div>
                        </div>
                            <?php ActiveForm::end(); ?>
                    </div>        
                </div>
                <div class="col-lg-12">                                    
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                </div>                             
            </div>            
        </div>
    </div>            
</div>
<script>
    $(document).ready(function(){
   //código a ejecutar cuando el DOM está listo para recibir instrucciones.
    
    });
</script>