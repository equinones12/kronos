<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Licencias */

$this->title = 'Registro de Licencia';
$this->params['breadcrumbs'][] = ['label' => 'Licencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licencias-create">

    <!-- <div class="alert alert-success" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
