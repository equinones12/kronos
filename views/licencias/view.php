<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Licencias */

$this->title = "Información de licencias";
$this->params['breadcrumbs'][] = ['label' => 'Licencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licencias-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1>-->
    <h1 color="Olive">Información de la Licencia: <?php echo $licencias[0]['licencia_identificacion_tecnologia']; ?></h1>
 
    
    <div class="card card-primary">
        <!-- Default card contents -->
        <div class="card-body">
            <div class="table-responsive" >
                <table class="table table-bordered">
                    <tr>
                        <td colspan="5">
                            <p>
                            <?= Html::a('Actualizar', ['update', 'id' => $licencias[0]['licencia_id']], ['class' => 'btn btn-success']) ?>
                            </p>
                        </td>
                    </tr>
                    <tr><td colspan="5" class="alert alert-primary"> INFORMACIÓN GENERAL</td></tr>
                    <tr>
                        <td><b>Identificación T & T</b></td>
                        <td><b>Producto</b></td>
                        <td><b>Versión</b></td>
                        <td><b>Clave del producto</b></td>
                    </tr>
                    <tr>
                        <td><?php echo $licencias[0]['licencia_identificacion_tecnologia'] ?></td>
                        <td><?php echo $licencias[0]['producto'] ?></td>
                        <td><?php echo $licencias[0]['version_nombre'] ?></td>
                        <td><?php echo $licencias[0]['licencia_numero'] ?></td>
                        
                    </tr>
                    <tr>
                        <td colspan="4"><b>Especificaciones</b></td>
                    </tr>
                    <tr>
                        <td colspan="4"><?php echo $licencias[0]['licencia_especificaciones'] ?></td>
                    </tr>
                    <tr><td colspan="5" class="alert alert-primary"> INFORMACIÓN MAQUINA ASIGNADA</td></tr>
                    <tr>
                        <td><b>Acción</b></td>
                        <td><b>Identificación T & T</b></td>
                        <td><b>Nombre Actual</b></td>
                        <td colspan="2"><b>Ubicación</b></td>
                    </tr>
                    <tr>
                        <td><p>
                            <?= Html::a('Ver Equipo', ['equipos/view', 'id' => $licencias[0]['equipo_id']], ['class' => 'btn btn-success']) ?>
                            </p></td>
                        <td><?php echo $licencias[0]['equipo_identificacion_tecnologia'] ?></td>
                        <td><?php echo $licencias[0]['equipo_nombre_actual'] ?></td>
                        <td colspan="2"><?php echo $licencias[0]['sucursal']." - ". $licencias[0]['area'] ?></td>
                    </tr>
                    <tr>   
                        <td><b>Funcionario</b></td>
                        <td><b>Cargo</b></td>
                        <td><b>Email</b></td>
                        <td colspan="2"><b>Telefonos</b></td>
                    </tr>
                    <tr>
                        <td><?php echo $licencias[0]['funcionario_nombres'] ?></td>
                        <td><?php echo $licencias[0]['funcionario_cargo'] ?></td>
                        <td><?php echo $licencias[0]['funcionario_email'] ?></td>
                        <td colspan="2"><?php echo $licencias[0]['funcionario_telefono1']."-".$licencias[0]['funcionario_telefono2'] ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
