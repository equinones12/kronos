<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Licencias';
$this->params['breadcrumbs'][] = $this->title;
?>
    
<div class="card card-purple">
    <div class="card-header">
    <!-- <h3 class="card-title">DataTable with default features</h3> -->Resultados encontrados
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            <?= Html::a('Registrar licencia', ['licencias/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="col-lg-12">
            <div class="row">
             <div class="table-responsive">              
                <table id="example1" class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                    <th>Tipo Licencia</th>
                    <th>Producto</th>
                    <th>Versión</th>
                    <th>Identificación tecnologia</th>
                    <th>Estado</th>
                    <th>Acción</th>                    
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model as $key => $value) {?>
                    <tr>
                        <td><?php echo $value->tipolicencia->tiposd->tipo_nombre ?></td>
                        <td><?php echo $value->tipolicencia->tipo_detalle_nombre ?></td>
                        <td><?php echo $value->version->tipo_detalle_nombre ?></td>
                        <td><?php echo $value['licencia_identificacion_tecnologia'] ?></td>
                        <td><?php echo $value->estado->tipo_detalle_nombre ?></td>
                        
                       
                        <td>
                            <?= Html::a('Ver', ['licencias/view', 'id'=> $value['licencia_id']], ['class' => 'btn btn-primary btn-block ']) ?>
                            <?= Html::a('Actualizar', ['licencias/update', 'id'=> $value['licencia_id']], ['class' => 'btn btn-warning btn-block ']) ?>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div>  
            </div>
        </div>    
    </div>
    <!-- /.card-body -->
</div>
<script>
  $(function () {
    $("#example1").DataTable({
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>