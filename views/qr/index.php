<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TiposDetalles;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActividadesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Codigos QR';
$this->params['breadcrumbs'][] = ['label' => 'Codigos QR', 'url' => ['index']];
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<div class="qr-index">
  <?php

  if (isset($result)) {
    if ($result==0){ ?>
  <div id="alert" class="alert alert-danger" role="alert">No Se encontraron Concidencias en la busqueda</div>
  <?php }else{ ?>
  <div class="card card-purple">
    <!-- Default panel contents -->
    <div class="card-header">RESULTADO DE LA BUSQUEDA</div>
    <div class="card-body">
       <?php $form = ActiveForm::begin([
                        'method' => 'post',
                        'options' => ["enctype" => "multipart/form-data"],
                        'action' => ['qr/generacion'],
                      ]); ?>

        <div class="col-lg-12">
          <div class="row">
            <div class="table-responsive">        
             <table id="example1" class="table table-bordered table-hover table-striped">
                <thead>
                  <tr>
                      <th>Propietario</th>     
                      <th>No de Maquina</th>
                      <th>Tipo de Maquina</th>
                      <th>Codigo QR</th>
                      <th>Accion</th>
                      <th><input type="checkbox" class="select-all" /></th>
                  </tr>
                </thead> 
                <tbody>
                 <?php foreach ($result as $key ) { ?>
                  <tr>
                      <td><?= $key['funcionario_nombres']; ?></td>
                      <td><?= $key['equipo_id']; ?></td>
                      <td><?= $key['tipo_detalle_nombre'] ?></td>
                      <td><?php echo Html::img("@web/QR/".$key['equipo_id'].".png", ["width"=>"100px" , "height" => "80px"]) ?></td>
                      <td><input type="checkbox" class="chk-box" name="seleccion[]" value="<?php echo $key['equipo_id'] ?>"></td>
                      <td></td>
                  </tr>         
                 <?php } ?>
                </tbody> 
              </table> 
            </div>
          </div> 
        </div>        
      </div>
    </div>  
<div class="col-lg-12">
    <input type="submit" class="btn btn-success btn-block">
</div>
  <?php } ?>
<?php } ?>
</div>
<?php ActiveForm::end()?>
<script type="text/javascript">
    
  $('document').ready(function(){
    $(function () {
      $("#example1").DataTable({
        "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });

    $(".select-all").click(function () 
    {
      $('.chk-box').attr('checked', this.checked)
    });
      
    $(".chk-box").click(function()
    {
        if($(".chk-box").length == $(".chk-box:checked").length) 
        {
          $(".select-all").attr("checked", "checked");
        } 
        else 
        {
          $(".select-all").removeAttr("checked");
        }
      });
  });

</script>
