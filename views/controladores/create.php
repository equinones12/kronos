<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Controladores */

$this->title = 'Crear Controlador';
$this->params['breadcrumbs'][] = ['label' => 'Controladores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="controladores-create">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
