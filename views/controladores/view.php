<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Controladores */

$this->title = 'Controlador: ' . $model->nombreControlador;
$this->params['breadcrumbs'][] = ['label' => 'Controladores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="controladores-view">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idcontrolador], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcontrolador',
            'nombreControlador',
        ],
    ]) ?>

</div>
