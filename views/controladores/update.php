<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Controladores */

$this->title = 'Actualizar Controlador: ' . ' ' . $model->nombreControlador;
$this->params['breadcrumbs'][] = ['label' => 'RBAC', 'url' => ['rback/index']];
$this->params['breadcrumbs'][] = ['label' => 'Controladores', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="controladores-update">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
