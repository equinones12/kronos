<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cargos2 */

$this->title = 'Create Cargos2';
$this->params['breadcrumbs'][] = ['label' => 'Cargos2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cargos2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
