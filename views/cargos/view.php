<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cargos2 */

$this->title = $model->idCargo;
$this->params['breadcrumbs'][] = ['label' => 'Cargos2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cargos2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idCargo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idCargo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCargo',
            'nombre',
            'activo',
            'fechaCreacion',
        ],
    ]) ?>

</div>
