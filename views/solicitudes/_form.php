<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitudes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitudes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'solicitudes_tiposolicitud')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitudes_nombres')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitudes_telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitudes_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitudes_ubicacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'solicitudes_detalle')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'solicitudes_estado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
