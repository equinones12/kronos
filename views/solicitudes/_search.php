<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitudes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'solicitudes_id') ?>

    <?= $form->field($model, 'solicitudes_tiposolicitud') ?>

    <?= $form->field($model, 'solicitudes_nombres') ?>

    <?= $form->field($model, 'solicitudes_telefono') ?>

    <?= $form->field($model, 'solicitudes_email') ?>

    <?php // echo $form->field($model, 'solicitudes_ubicacion') ?>

    <?php // echo $form->field($model, 'solicitudes_detalle') ?>

    <?php // echo $form->field($model, 'solicitudes_estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
