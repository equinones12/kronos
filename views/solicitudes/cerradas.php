<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitudes cerradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitudes-index">

    <h3 class="alert alert-primary"><?= Html::encode($this->title) ?></h3>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'solicitudes_id',
                'solicitudes_tiposolicitud',
                'solicitudes_nombres',
                'solicitudes_telefono',
                'solicitudes_email:email',
                'solicitudes_ubicacion',
                'solicitudes_detalle:ntext',
                'solicitudes_descripcionrespuesta',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '',
                    'buttons' => [
                        'responder' => function ($url, $model, $key) {
                            // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                            return Html::Button('<span class="fas fa-check"></span> Responder solicitud', ['class' => 'btn-sm btn btn-block btn-primary respondersolicitud', 'id'=>$model->solicitudes_id, 'data-toggle'] );
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>


</div>

<div id="modalresponder" class="modal fade" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title">Responder solicitud</h5>
            </div>
            <div class="modal-body">
                <h3>Detalle de la solicitud</h3>
                <div id="contentsolicitud">
                </div>
                <hr class="bg-primary">
                <div class="row">
                    <div class="col-lg-12">
                        <label>Descripcion respuesta</label>
                        <textarea id="descripcionrespuesta" rows="2" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cerrarcorreo" class="btn btn-primary">Enviar y cerrar solicitud</button>
                <button type="button" id="cerrarsolicitud" class="btn btn-info">Cerrar solicitud</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<!-- Js -->
<script>
    $(document).ready(function(){
        $solicitudes_id = 0;
        $('.respondersolicitud').click(function(){
            solicitudes_id = $(this).attr('id');
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudes/ajaxfindone',
                type: 'get',
                data: {
                    id: $(this).attr('id')
                },
                success: function (data) {
                    $('#contentsolicitud').html(data);
                    $('#modalresponder').modal({
                        backdrop: 'static', 
                        keyboard: false
                    });
                }
            }); // fin ajax
        });
        $('#cerrarcorreo').click(function(){
            if ($('#descripcionrespuesta').val() ) {
                $('#descripcionrespuesta').removeClass('is-invalid');
                $('#descripcionrespuesta').addClass('is-valid');
                $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudes/cerrarcorreo',
                    type: 'get',
                    data: {
                        id: solicitudes_id,
                        descripcion: $('#descripcionrespuesta').val()
                    },
                    success: function (data) {
                        location.reload();
                    }
                }); // fin ajax
            }else{
                $('#descripcionrespuesta').removeClass('is-valid');
                $('#descripcionrespuesta').addClass('is-invalid');
            }
        });
        $('#cerrarsolicitud').click(function(){
            if ($('#descripcionrespuesta').val() ) {
                $('#descripcionrespuesta').removeClass('is-invalid');
                $('#descripcionrespuesta').addClass('is-valid');
                $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl ?>/?r=solicitudes/cerrarsolicitud',
                    type: 'get',
                    data: {
                        id: solicitudes_id,
                        descripcion: $('#descripcionrespuesta').val()
                    },
                    success: function (data) {
                        location.reload();
                    }
                }); // fin ajax
            }else{
                $('#descripcionrespuesta').removeClass('is-valid');
                $('#descripcionrespuesta').addClass('is-invalid');
            }
        });
        
    })// document ready
</script>
