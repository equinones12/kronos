<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Vista de Cronograma';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Cronogramas', 'url' => ['index']];

?>
<div class="cronogramas-view">    
    <div class="card card-purple">
        <div class="card-header">
        <!-- <h3 class="card-title">DataTable with default features</h3> -->Equipos asociados al mantenimiento
        </div>
        <!-- /.card-header -->        
        <div class="card-body">        
            <div class="col-lg-12">
                <div class="row">
                    <div class="table-responsive">              
                        <table id="example1" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr >
                                    <td>Usuario</b>                                    
                                    <td>Identificacion T & T</b>
                                    <td>No. Activo</b>
                                    <td>Fecha</b>
                                    <td>Horario</b>
                                    <td>Estado</b>
                                    <td>Aprobado</b>
                                    <td>Accion</b>
                                </tr>
                            </thead>                            
                            <tbody> 
                                <?php 
                        
                                    foreach ($result as $key => $value) { 
                                    if ($value['cronograma_detalle_realizado']==1 && $value['cronograma_detalle_aprobado']== 1 ) {?>
                                        <tr class="info">
                                        <td><?php echo $value['funcionario_nombres'] ?></td>
                                        <!-- <td><?php echo $value['equipo_nombre_actual'] ?></td> -->
                                        <td><?php echo $value['equipo_identificacion_tecnologia'] ?></td>
                                        <td><?php echo $value['equipo_numero_activo_contabilidad'] ?></td>
                                        <td><?php echo $value['cronograma_detalle_fecha'] ?></td>
                                        <td><?php echo $value['cronograma_detalle_hora_inicio']." a ".$value['cronograma_detalle_hora_fin']?></td>
                                        <td>Realizado</span></td>
                                        <td>Aprobado</span></td>
                                        <td><?= Html::a('Ir al Equipo', ['/equipos/view', 'id' => $value['equipo_id']], ['class' => 'btn btn-success btn-xs  ', 'onclick' => ''] );?></td>
                                        
                                        <?php } else if($value['cronograma_detalle_realizado']==1 && $value['cronograma_detalle_aprobado']== 0){?>
                
                                        <tr class="danger">
                                        <td><?php echo $value['funcionario_nombres'] ?></td>
                                        <!-- <td><?php echo $value['equipo_nombre_actual'] ?></td> -->
                                        <td><?php echo $value['equipo_identificacion_tecnologia'] ?></td>
                                        <td><?php echo $value['equipo_numero_activo_contabilidad'] ?></td>
                                        <td><?php echo $value['cronograma_detalle_fecha'] ?></td>
                
                                        <td><?php echo $value['cronograma_detalle_hora_inicio']." a ".$value['cronograma_detalle_hora_fin']?></td>
                                        <td><b>Realizado</b></td>
                                        <td><b>Pendiente</b></span></td>
                                        <td><?= Html::a('Ir al Equipo', ['/equipos/view', 'id' => $value['equipo_id']], ['class' => 'btn btn-success btn-xs ', 'onclick' => ''] );?></td>
                                        
                                    </tr>
                                    <?php }else if($value['cronograma_detalle_realizado']==0 && $value['cronograma_detalle_aprobado']== null){?>
                                            
                                        <tr class="danger">
                                        <td><?php echo $value['funcionario_nombres'] ?></td>
                                        <!-- <td><?php echo $value['equipo_nombre_actual'] ?></td> -->
                                        <td><?php echo $value['equipo_identificacion_tecnologia'] ?></td>
                                        <td><?php echo $value['equipo_numero_activo_contabilidad'] ?></td>
                                        <td><?php echo $value['cronograma_detalle_fecha'] ?></td>
                                        
                                        <td><?php echo $value['cronograma_detalle_hora_inicio']." a ".$value['cronograma_detalle_hora_fin']?></td>
                                        <td><b>Pendiente</b></td>
                                        <td><b>Pendiente</b></span></td>
                                        <td><?= Html::a('Ir al Equipo', ['/equipos/view', 'id' => $value['equipo_id']], ['class' => 'btn btn-success btn-xs ', 'onclick' => ''] );?></td>
                
                                    <?php }
                
                                } ?>                       
                            </tbody>                            
                        </table>
                    </div>  
                </div>
            </div>    
        </div>
        <!-- /.card-body -->
    </div>    
</div>    
<script>
  $(function () {
    $("#example1").DataTable({
        "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>