<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cronogramas */

$this->title = 'Update Cronogramas: ' . ' ' . $model->cronograma_id;
$this->params['breadcrumbs'][] = ['label' => 'Cronogramas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cronograma_id, 'url' => ['view', 'id' => $model->cronograma_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cronogramas-update">

    <div class="alert alert-danger" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
