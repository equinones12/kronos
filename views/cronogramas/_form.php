<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TiposDetalles;
use app\models\Sucursales;
use app\models\Users;
use kartik\date\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CronogramasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// echo $html;
// echo '<pre>';
// print_r($model);
// die();

$this->title = 'Generación de cronogramas';
$this->params['breadcrumbs'][] = $this->title;
$data = null;
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="cronogramas-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-row"> 
       <div class="col-lg-12">   
        <div class="card card-primary">
            <div class="card-header">Panel de Busqueda <?= Html::encode($this->title) ?></div>            
                <div class="form-row">
                    
                    <div class="col-md-6 mb-3">
                           <?= $form->field($model, 'cronograma_tipo_mantenimiento')->dropDownList(ArrayHelper::map(TiposDetalles::find()->
                                            where(['tipo_detalle_tipo_id'=>6])
                                                ->orderBy(['tipo_detalle_nombre'=>SORT_ASC])->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'), 
                                        [
                                        'class'=>'form-control a',
                                        'prompt'=>'[-- Seleccione Tipo--]',
                                        ]
                                    ) ?>
                            <div class="help-block"></div>
                    </div>                    
                    <div class="col-md-6 mb-3">
                       <?= $form->field($model, 'cronograma_quien_realiza')
                                ->dropDownList(ArrayHelper::map(Users::find()
                                    ->where(['>','id','3'])
                                    ->Andwhere(['activate' => 1])
                                    ->orderBy(['id'=>SORT_ASC])
                                    ->all(), 'id', function($model){
                                        return strtoupper($model->nombres." ".$model->apellidos);
                                    }), 
                                    [
                                    'class'=>'form-control a',
                                    'prompt'=>'[-- Seleccione Usuario--]',
                                    ]
                                ) ?>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="start">Fecha de Inicio</label></br>                                    
                        <input type="date" id="start" name="cronograma_fecha_inicio" value="" placeholder="yyyy-mm-dd" class="form-control a" required ="required">
                    </div>
                    <div class="col-md-6 mb-3">                                                                
                        <label for="start">Fecha de Finalización</label></br>                                    
                        <input type="date" id="start" name="cronograma_fecha_fin" value="" placeholder="yyyy-mm-dd" class="form-control a" required ="required">                     
                    </div>    
                    <div class="col-md-12 mb-3">
                        <?= Html::label('Sucursal', 'label-sucursal', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-sucursal',
                                '',
                                ArrayHelper::map(Sucursales::find()->orderBy(['sucursal_nombre'=>'asc'])->all(),'sucursal_id', 'sucursal_nombre'),
                            [
                                    'class'=>'form-control a',
                                    'prompt'=>'[-- Seleccione Sucursal --]',
                                    'value'=> 0,
                                    'id' => 'select-sucursal',
                                    // 'onchange' => '$.get("index.php?r=equipos/lists&id='.'"+$(this).val(), function(data){
                                    //     $("#div-maquinas").val("");
                                    //     $("#div-maquinas").html(data);
                                    //     $("#div-maquinas").removeAttr("disabled");
                                    // })',
                                ]) 
                            ?>                                             
                        
                    </div> 
                    <div class="col-md-6 mb-3">
                        <input type="submit" id="enviar" class="btn btn-success" value="enviar">
                    </div>   
                </div>            
        </div>
        <div class="card alert alert-secondary  equipos" style="display:none;"> 
            <div class="card-header alert alert-secondary  equipos" style="display:none;">Equipos encontrados</div>           
            <div class="form-row hide" id="div-maquinas">              
            </div>
        </div> 
      </div> 
    </div>     
    <?php ActiveForm::end(); ?>
</div>

<script>

    $('#select-sucursal').change(function(){               
        $.ajax({            
            url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/lists',
            type : 'get',
            data : {
            id : $(this).val(),                
            },
            success: function(data){
                if (data){
                    $("#div-maquinas").html(data); 
                    $('.equipos').show(); 
                }else{
                    $("#div-maquinas").html(data);
                    $('.equipos').hide();
                }                 
                             
            }
        })    
    })

    $("#enviar").click(function(){

        var validate = false;
        
        $('.a').each(function(){
            if ($(this).val() == "" || $(this).val() == null ) {
                $(this).css("border", "1px solid red");
                validate = false;

            }else{
                $(this).css("border", "1px solid #ccc");
                validate = true;
            }
        });

        if(validate) {
            return true;
        }else{
            confirm('ERROR: se debe seleccionar la fecha para realizar la Busqueda');
            return false;
        }

    });

    $("#btn-excel").click(function(){

        $("#cronograma").submit();
    });    
</script>