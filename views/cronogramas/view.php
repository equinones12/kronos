<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Vista de Cronograma';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'Cronogramas', 'url' => ['index']];

?>
<div class="cronogramas-view">    
    <div class="card card-purple">
        <div class="card-header">
        <!-- <h3 class="card-title">DataTable with default features</h3> -->Equipos asociados al mantenimiento
        </div>
        <!-- /.card-header -->        
        <div class="card-body">        
            <div class="col-lg-12">
                <div class="row">
                    <div class="table-responsive">              
                        <table id="example1" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>                    
                                    <th>Identificación T & T</th>
                                    <th>Hora de Inicio</th>
                                    <th>Hora de Finalización</th>
                                    <th>Fecha</th>
                                    <th>Encargado del Equipo</th>
                                    <th>Ubicación</th>
                                    <th>Acciones</th>                                                                        
                                </tr>
                            </thead>                            
                            <tbody> 
                              <?php foreach ($CronogramaDetalles as $keyD => $valueD) { ?>                       
                                <tr>
                                    <td><?php echo $valueD['equipo_identificacion_tecnologia'] ?></td>
                                    <td><?php echo $valueD['cronograma_detalle_hora_inicio'] ?></td>
                                    <td><?php echo $valueD['cronograma_detalle_hora_fin'] ?></td>
                                    <td><?php echo $valueD['cronograma_detalle_fecha'] ?></td>
                                    <td><?php echo strtoupper($valueD['funcionario_nombres']) ?></td>
                                    <td><?php echo strtoupper($valueD['area_nombre']) ?></td>
                                    <td>
                                    <?= Html::a('Ir al Equipo', ['equipos/view', 'id'=> $valueD['equipo_id']], ['class' => 'btn btn-primary btn-block ', 'onclick' => '','target'=>'_blank']) ?>
                                
                                        <?php if ($valueD['cronograma_detalle_realizado'] == 0) {  ?>
                                            
                                            <?= Html::a('Registrar Check List', ['check-list/create', 'id'=>$valueD['equipo_id'],'cronograma'=>$valueD['cronograma_detalle_id']], ['class' => 'btn btn-warning btn-block ', 'id'=>'aprobar']) ?>    
                                        <?php }else{ ?>
                                            
                                            <?= Html::a('Ver Mantenimiento', ['check-list/view', 'id'=>$valueD['cronograma_detalle_id'], 'Equipo' => $valueD['equipo_id']], ['class' => 'btn btn-success btn-block ','target'=>'_blank']) ?>
                                            
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>                        
                            </tbody>                            
                        </table>
                    </div>  
                </div>
            </div>    
        </div>
        <!-- /.card-body -->
    </div>    
</div>    
<script>
  $(function () {
    $("#example1").DataTable({
        "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>