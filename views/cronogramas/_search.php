<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CronogramasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cronogramas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cronograma_id') ?>

    <?= $form->field($model, 'cronograma_sucursal_id') ?>

    <?= $form->field($model, 'cronograma_fecha_inicio') ?>

    <?= $form->field($model, 'cronograma_fecha_fin') ?>

    <?= $form->field($model, 'cronograma_quien_realiza') ?>

    <?php // echo $form->field($model, 'cronograma_aprobado') ?>

    <?php // echo $form->field($model, 'cronograma_quien_aprueba') ?>

    <?php // echo $form->field($model, 'cronograma_fecha_aprobacion') ?>

    <?php // echo $form->field($model, 'cronograma_tipo_mantenimiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
