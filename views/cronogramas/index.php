<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\TiposDetalles;
use app\models\Sucursales;
use app\models\Areas;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CronogramasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cronogramas de Mantenimiento';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="cronogramas-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Generar Cronograma', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
<?php $form = ActiveForm::begin([
            "method" => "post",
            "options" => ["enctype" => "multipart/form-data"],
        ]); ?>
    <div class="form-row"> 
       <div class="col-lg-12">   
        <div class="card card-primary">
            <div class="card-header">Panel de Busqueda <?= Html::encode($this->title) ?></div>            
                <div class="form-row">
                    
                    <div class="col-md-12 mb-3">
                        <?= Html::label('Zona', 'sucursallabel', ['class'=>'control-label','id'=>'labelSelecMotivos']) ?>
                                    <?= Html::dropDownList(
                                        'sucursal',
                                        '',
                                        ArrayHelper::map(Sucursales::find()
                                                ->Where(['>','Zona_id','0'])
                                                ->groupBy(['Zona_id'])
                                                ->orderBy(['Zona_id'=>'asc'])
                                                ->all(), 'Zona_id', 'Zona_id'),
                                        [
                                            'class'=>'form-control ',
                                            'prompt'=>'[-- Seleccione Sucursal --]',
                                            'value'=> 0,
                                            'id'=>'sucursalID'
                                        ]) 
                                    ?><br>
                    </div>                    
                    <div class="col-md-6 mb-3">
                        <?= Html::label('Tipo de Mantenimiento', 'tipo_mantenimiento', ['class'=>'control-label','id'=>'labelSelecMotivos']) ?>
                                    <?= Html::dropDownList(
                                        'tipo_mantenimiento',
                                        '',
                                        ArrayHelper::map(TiposDetalles::find()
                                                ->Where(['tipo_detalle_tipo_id' => 6])
                                                ->orderBy(['tipo_detalle_nombre'=>'asc'])
                                                ->all(), 'tipo_detalle_id', 'tipo_detalle_nombre'),
                                        [
                                            'class'=>'form-control ',
                                            'prompt'=>'[-- Seleccione Tipo de Mantenimiento --]',
                                            'value'=> 0,
                                            'id'=>'tipo_maquina'
                                        ]) 
                                    ?>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="">Estado Mantenimiento</label>
                        <select name="estadoMantenimiento" id="" class="form-control">
                            <option value="">[-- Seleccione el Estado del Mantenimiento --]</option>
                            <option value="0">PENDIENTE</option>
                            <option value="1">REALIZADO</option>
                        </select><br>
                    </div>
                    <div class="col-md-6 mb-3">                                                                
                        <label for="start">Fecha de Inicio</label></br>                                    
                        <input type="date" id="start" name="fechaInicial" value="" placeholder="yyyy-mm-dd" class="form-control a" required ="required">                     
                    </div>    
                    <div class="col-md-6 mb-3">                                             
                        <label for="start">Fecha de fin</label></br>                                    
                        <input type="date" id="start" name="fechaFinal" value="" placeholder="yyyy-mm-dd" class="form-control a" required ="required">
                    </div> 
                    <div class="col-md-6 mb-3">
                        <?= Html::submitButton("Buscar",["class"=>"btn btn-primary","id"=>"enviar"])?>
                    </div>   
                </div>            
        </div>
      </div> 
    </div>     
    <?php ActiveForm::end(); ?>
<?php if (isset($result) ) {
            if ($result!=null) { ?>
    <div class="form-row"> 
      <div class="col-lg-12">             
        <div class="card card-primary">
            <div class="card-header">Resultados Encontrados
                        <!-- <button type="button" id="btn-excel" class="btn btn-default" aria-label="Left Align"> -->
                            <img id="btn-excel" src="images/exceln.png" alt="" width="60" title="Reporte General" > 
                        <!-- </button> -->
            </div>            
                <?php $form = ActiveForm::begin([
                    'action' => ['excel/cronogramas2'],
                    'method' => 'post',
                    'id' => 'cronograma',
                ]); ?>
                <input type="hidden" name="datos" value="<?php echo htmlentities(serialize($result)); ?>">
                <?php ActiveForm::end()?>
                <div class="table-responsive">
                        
                    <table class="table table-bordered table-hover table-info">
                        <tr>
                            <td><b>Sucursal</b></td>
                            <td><b>Fecha Inicial</b></td>
                            <td><b>Fecha Final</b></td>
                            <td><b>Estado</b></td>
                            <td><b>No Equipos</b></td>
                            <td><b>Acciones</b></td>
                        </tr>
                        <?php foreach ($result as $key => $value) { ?>
                        <tr style =" bgcolor= #00FFFF">
                            <td><?php echo strtoupper($value['sucursal_nombre']); ?></td>
                            <td><?php echo $value['cronograma_fecha_inicio']; ?></td>
                            <td><?php echo $value['cronograma_fecha_fin']; ?></td>
                            <td><?php echo $value['estado']; ?></td>
                            <td><?php echo $value['numeroEquipos']; ?></td>
                            <td><?= Html::a('Detalle', ['view', 'id' => $value['cronograma_id'],], ['class' => 'btn btn-primary'] ); ?> </td>
                        </tr>    
                        <?php } ?>
                    </table>
                </div>            
        </div>
      </div> 
    </div>    
<?php 
    }
} ?>
</div>

<script>
    $("#enviar").click(function(){

        var validate = false;
        
        $('.a').each(function(){
            if ($(this).val() == "" || $(this).val() == null ) {
                $(this).css("border", "1px solid red");
                validate = false;

            }else{
                $(this).css("border", "1px solid #ccc");
                validate = true;
            }
        });

        if(validate) {
            return true;
        }else{
            confirm('ERROR: se debe seleccionar la fecha para realizar la Busqueda');
            return false;
        }

    });

    $("#btn-excel").click(function(){

        $("#cronograma").submit();
    });
</script>