<?php

use yii\helpers\Html;

$html = '';
/* @var $this yii\web\View */
/* @var $model app\models\Cronogramas */

$this->title = 'Generación de Cronogramas';
$this->params['breadcrumbs'][] = ['label' => 'Cronogramas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cronogramas-create">

    <!-- <div class="alert alert-success" role="alert"> <h3><?= Html::encode($this->title) ?></h3></div> -->

    <?= $this->render('_form', [
        'model' => $model,        
    ]) ?>

</div>
