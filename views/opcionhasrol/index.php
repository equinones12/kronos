<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OpcionHasRolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-has-rol-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Asignar Opcion', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
  
  <p class="alert alert-info">
      <?= Html::a('Crear Opcion', ['opcionmenu/index'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Crear Item', ['itemmenu/index'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Crear Subitem', ['subitemmenu/index'], ['class' => 'btn btn-primary']) ?>
  </p>
  <div class="panel panel-primary">
    <div class="panel-heading">PANEL DE MENUS</div>
    <div class="panel-body">  
      <?php foreach ($rol as $r ) { ?>
        <h2><?php echo 'Rol ' . $r->nombre; ?></h2>
        <?php foreach ($opcion as $o ) { ?>
            <?php if ($r->id == $o->id_rol) { ?>
               <div class="col-lg-12 noPadding alert alert-info">

                  <?= Html::a('', ['update', 'id' => $o->id], ["class"=>"glyphicon glyphicon-pencil pull-left", "style"=>"margin-left: 10px; margin-top: 3px; cursor:pointer; text-decoration:none;"]) ?>

                  <p style="margin-left: 20px; width:150px; float:left;"><?php echo $o->idOpcion->nombre; ?></p>
                  <?php if ($o->estado == 1) { ?>
                    <input data-rol="<?php echo $r->id; ?>" data-action="opcionhasrol/opcion" data-id="<?php echo $o->id; ?>" type="checkbox" class="switch" checked >
                  <?php }else{ ?>
                     <input data-rol="<?php echo $r->id; ?>" data-action="opcionhasrol/opcion" data-id="<?php echo $o->id; ?>" type="checkbox" class="switch">  
                  <?php } ?>

                  <?= Html::a('Asignar Item', ['itemhasrol/create', 'idRol' => $r->id, 'idOpcion' => $o->idOpcion->id], ['class' => 'btn btn-primary btn-xs pull-right']) ?>

               </div>
               <?php foreach ($item as $i) { ?>
                    <?php if ($o->idOpcion->id == $i->idItem->id_opcion && $r->id == $i->id_rol ) { ?>
                        <div class="col-lg-11 col-lg-offset-1  noPadding  alert alert-success">
                        <?= Html::a('', ['itemhasrol/update', 'idRol'=>$r->id,'idItem' => $i->idItem->id,'itemhasrol' => $i->id], ["class"=>"glyphicon glyphicon-pencil pull-left", "style"=>"margin-left: 10px; margin-top: 3px; cursor:pointer; text-decoration:none;"]) ?>
                          <p style="margin-left: 20px; width:150px; float:left;"><?php echo $i->idItem->etiqueta; ?></p>
                          <?php if ($i->estado == 1) { ?>
                            <input data-rol="<?php echo $r->id; ?>" data-action="itemhasrol/item" data-id="<?php echo $i->id; ?>" type="checkbox" class="switch" checked>
                          <?php }else{ ?>
                            <input data-rol="<?php echo $r->id; ?>" data-action="itemhasrol/item" data-id="<?php echo $i->id; ?>" type="checkbox" class="switch">
                          <?php } ?>
                          <?= Html::a('Asignar Subitem', ['subitemhasrol/create', 'idRol' => $r->id, 'idItem' => $i->idItem->id], ['class' => 'btn btn-primary btn-xs pull-right']) ?>
                        </div>
                        <?php foreach ($subitem as $s) { ?>
                            <?php if ($i->idItem->id == $s->idSubitem->id_item && $r->id == $s->id_rol) { ?>
                                <div class="col-lg-10 col-lg-offset-2 noPadding  alert alert-warning">
                                <?= Html::a('', ['subitemhasrol/update', 'idRol'=>$r->id,'idSubitem' => $s->id], ["class"=>"glyphicon glyphicon-pencil pull-left", "style"=>"margin-left: 10px; margin-top: 3px; cursor:pointer; text-decoration:none;"]) ?>
                                    <p style="margin-left: 20px; width:150px; float:left;"><?php echo $s->idSubitem->etiqueta; ?></p>
                                    <?php if ($s->estado == 1) { ?>
                                      <input data-rol="<?php echo $r->id; ?>" data-action="subitemhasrol/subitem" data-id="<?php echo $s->id; ?>" type="checkbox" class="switch" checked>
                                    <?php }else{ ?>
                                      <input data-rol="<?php echo $r->id; ?>" data-action="subitemhasrol/subitem" data-id="<?php echo $s->id; ?>" type="checkbox" class="switch">
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
               <?php } ?>
            <?php } ?>
        <?php } ?>
      <?php } ?>
    </div>
  </div>


</div>
