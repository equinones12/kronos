<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiposDetallesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipos-detalles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tipo_detalle_id') ?>

    <?= $form->field($model, 'tipo_detalle_tipo_id') ?>

    <?= $form->field($model, 'tipo_detalle_nombre') ?>

    <?= $form->field($model, 'tipo_detalle_activo') ?>

    <?= $form->field($model, 'tipo_detalle_creador') ?>

    <?php // echo $form->field($model, 'tipo_detalle_fcreador') ?>

    <?php // echo $form->field($model, 'tipo_detalle_modificado') ?>

    <?php // echo $form->field($model, 'tipo_detalle_fmodificado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
