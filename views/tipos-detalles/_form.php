<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiposDetalles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipos-detalles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo_detalle_tipo_id')->textInput() ?>

    <?= $form->field($model, 'tipo_detalle_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_detalle_activo')->textInput() ?>

    <?= $form->field($model, 'tipo_detalle_creador')->textInput() ?>

    <?= $form->field($model, 'tipo_detalle_fcreador')->textInput() ?>

    <?= $form->field($model, 'tipo_detalle_modificado')->textInput() ?>

    <?= $form->field($model, 'tipo_detalle_fmodificado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
