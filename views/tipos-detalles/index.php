<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TiposDetallesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipos Detalles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipos-detalles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tipos Detalles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tipo_detalle_id',
            'tipo_detalle_tipo_id',
            'tipo_detalle_nombre',
            'tipo_detalle_activo',
            'tipo_detalle_creador',
            //'tipo_detalle_fcreador',
            //'tipo_detalle_modificado',
            //'tipo_detalle_fmodificado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
