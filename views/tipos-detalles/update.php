<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiposDetalles */

$this->title = 'Update Tipos Detalles: ' . $model->tipo_detalle_id;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Detalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tipo_detalle_id, 'url' => ['view', 'id' => $model->tipo_detalle_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipos-detalles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
