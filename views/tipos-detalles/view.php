<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TiposDetalles */

$this->title = $model->tipo_detalle_id;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Detalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tipos-detalles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tipo_detalle_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tipo_detalle_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tipo_detalle_id',
            'tipo_detalle_tipo_id',
            'tipo_detalle_nombre',
            'tipo_detalle_activo',
            'tipo_detalle_creador',
            'tipo_detalle_fcreador',
            'tipo_detalle_modificado',
            'tipo_detalle_fmodificado',
        ],
    ]) ?>

</div>
