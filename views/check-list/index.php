<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Check Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Check List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'check_list_id',
            'check_list_cronograma_id',
            'check_list_equipo_id',
            'check_list_usuario_registra',
            'check_list_fecha_registro',
            // 'check_list_usuario_confirma',
            // 'check_list_campo_id',
            // 'check_list_valor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
