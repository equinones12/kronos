<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CheckList */

$this->title = "Mantenimiento Registrado";
$this->params['breadcrumbs'][] = ['label' => 'Equipos Asignados', 'url' => ['/cronogramas/lista', 'id'=>$cronogramas[0]['cronograma_detalle_cronograma_id'] ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-list-view">

    

    <div class="card card-primary">
    <div class="card-header"><h1><?= Html::encode($this->title) ?></h1></div>
    <div class="">
    
        <div class="card card-info">
            <div class="card-header">Informacion General</div>
            <div class="">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td><b>Identificacion T & T</b></td>
                            <td><b>Nombre Actulal</b></td>
                            <td><b>Tipo de Mantenimiento</b></td>
                            <td><b>Quien lo realiza</b></td>
                            <td><b>Hora del Mantenimiento</b></td>
                            <td><b>Estado</b></td>
                            <td><b>Tiempo utilizado(H:m:s)</b></td>
                            <td><b>Fecha</b></td>
                        </tr>    
                        <tr>
                            <td><?php echo $cronogramas[0]['equipo_identificacion_tecnologia'] ?></td>
                            <td><?php echo $cronogramas[0]['equipo_nombre_actual'] ?></td>
                            <td><?php echo $cronogramas[0]['tipoMantenimiento'] ?></td>
                            <td><?php echo $cronogramas[0]['quienRealiza'] ?></td>
                            <td><?php echo $cronogramas[0]['cronograma_detalle_hora_inicio']." a ".$cronogramas[0]['cronograma_detalle_hora_fin'] ?></td>
                            <td><?php echo $cronogramas[0]['estado'] ?></td>
                            <td><?php echo $cronogramas[0]['cronograma_tiempo_final_utilizado'] ?></td>
                            <td><?php echo $cronogramas[0]['fechaRegistro'] ?></td>
                        </tr>
                    </table>  
                </div>

                <?php if($cronogramas[0]['cronograma_detalle_aprobado']==1){ ?>
                <div class="col-lg-12">
                    <div class="card card-primary">                        
                        <div class="card-header"><h6>Firma de Aprobación</h6></div> 
                          <div class="table-responsive">                       
                            <table class="table table-bordered table-hover">                            
                                <tr>
                                    <td><b>Quien Autoriza</b></td>
                                    <td><b>Observación</b></td>
                                    <td><b>Firma Digital</b></td>
                                </tr>    
                                <tr>
                                    <td><?php echo $cronogramas[0]['cronograma_detalle_quien_aprueba'] ?></td>
                                    <td><?php echo $cronogramas[0]['cronograma_detalle_observacion_usuario'] ?></td>
                                    <td><?php echo Html::img("@web/images/firmasdigitales/mantenimiento_".$cronogramas[0]['cronograma_detalle_id']."_".$cronogramas[0]['cronograma_detalle_equipo_id'].".png") ?></td>
                                </tr>
                            </table>  
                         </div>   
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="card card-success">
            <div class="card-header">Informacion Del Mantenimiento registrado</div>
            <div class="">
                <div class="col-lg-12">
                    <div class="card card-info">
                        <div class="card-header">Imagenes del Mantenimiento</div>
                        <div class="form-row">
                            <?php 
                                    $contador=0;
                                    foreach ($imagenes as $keyI => $valueI) { 

                                        ?>
                                    
                                    <div class="col-lg-6">
                                        <?php echo Html::img("@web/".$valueI['cronograma_detalle_imagen_ruta']."", ["class"=>"img-fluid"]) ?>
                                    </div>                                                                            
                                <?php 
                                    $contador++;
                                    } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <?php foreach ($campos as $keyC => $valueC) { ?>
                    
                <div class="col-lg-12 form-group">
                    <div class="alert alert-info pop"  role="alert"  data-toggle="popover" data-placement="top" title="Instruccion de Labor" data-content="<?php echo $valueC['campo_check_list_popover_valor'] ?>">
                        <?php echo $valueC['campo_check_list_nombre'] ?> : 
                        <?php echo strtoupper($valueC['check_list_valor']);  ?>
                    </div>
                </div>    
                <?php } ?>
                </div>
            </div>
        </div>
        <?php if ($cronogramas[0]['cronograma_detalle_aprobado']==0) { ?>
            <?= Html::a('Aprobar', ['check-list/confirma', 'cronograma'=>$cronogramas[0]['cronograma_detalle_id'],'Equipo'=>$cronogramas[0]['cronograma_detalle_equipo_id']], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Actualizar Información', ['check-list/update', 'cronograma'=>$cronogramas[0]['cronograma_detalle_id'],'Equipo'=>$cronogramas[0]['cronograma_detalle_equipo_id']], ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?= Html::a('Regresar a la Maquina', ['equipos/view', 'id'=>$cronogramas[0]['cronograma_detalle_equipo_id']], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
</div>
<script>
        $('.pop').click(function(event) {
        $('[data-toggle="popover"]').popover('hide');
        $(this).popover('show');
    });
</script>