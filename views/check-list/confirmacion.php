<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\CheckList */

$this->title = 'Confirmacion del Mantenimiento';
$this->params['breadcrumbs'][] = ['label' => 'Equipos Asignados', 'url' => ['/cronogramas/lista', 'id'=>$cronogramas[0]['cronograma_detalle_cronograma_id'] ]];
$this->params['breadcrumbs'][] = $this->title;

// echo "<pre>";
// print_r($cronogramas);die();

?>
<div class="check-list-view">

    
<div class="card card-info">
    <div class="card-header">Mantenimiento Registrado</div>
    <div class="card-body">
    
        <div class="card card-info">
            <div class="card-header">Informacion General</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td><b>Identificacion T & T</b></td>
                            <td><b>Nombre Actulal</b></td>
                            <td><b>Tipo de Mantenimiento</b></td>
                            <td><b>Quien lo realiza</b></td>
                            <td><b>Hora del Mantenimiento</b></td>
                            <td><b>Tiempo utilizado(H:m:s)</b></td>
                        </tr>    
                        <tr>
                            <td><?php echo $cronogramas[0]['equipo_identificacion_tecnologia'] ?></td>
                            <td><?php echo $cronogramas[0]['equipo_nombre_actual'] ?></td>
                            <td><?php echo $cronogramas[0]['tipoMantenimiento'] ?></td>
                            <td><?php echo $cronogramas[0]['quienRealiza'] ?></td>
                            <td><?php echo $cronogramas[0]['cronograma_detalle_hora_inicio']." a ".$cronogramas[0]['cronograma_detalle_hora_fin'] ?></td>
                            <td><?php echo $cronogramas[0]['cronograma_tiempo_final_utilizado'] ?></td>

                        </tr>
                    </table>  
                </div>
            </div>
        </div>

        <div class="card card-success">
            <div class="card-header">Informacion Del Mantenimiento para aprobar</div>
            <div class="card-body">
                <div class="col-lg-12">
                    <div class="card card-info">
                        <div class="card-header">Imagenes del Mantenimiento</div>
                        <div class="card-body">
                            <?php 
                                    $contador=0;
                                    foreach ($imagenes as $keyI => $valueI) { 

                                        ?>

                                    <div class="col-lg-12 ">
                                        <?php echo Html::img("@web/".$valueI['cronograma_detalle_imagen_ruta']."", ["width"=>"300px" , "height" => "200px"]) ?>
                                    </div>
                                <?php 
                                    $contador++;
                                    } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <?php foreach ($checklist as $keyC => $valueC) { ?>
                    
                <div class="col-lg-12 form-group">
                    <div class="alert alert-info pop"  role="alert"  data-toggle="popover" data-placement="top" title="Instruccion de Labor" data-content="<?php echo $valueC['campo_check_list_popover_valor'] ?>">
                        <?php echo $valueC['campo_check_list_nombre'] ?> : 
                        <?php echo strtoupper($valueC['check_list_valor']);  ?>
                    </div>
                </div>    
                <?php } ?>
                
                
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-12">
                    <?= Html::a('ACTUALIZAR LA INFORMACIÓN', ['check-list/update', 'cronograma'=>$cronogramas[0]['cronograma_detalle_id'],'Equipo'=>$cronogramas[0]['cronograma_detalle_equipo_id']], ['class' => 'btn btn-danger btn-block']) ?>
                </div>
                <div class="col-lg-12">
                    <button class="btn btn-success btn-block" id="confirmation" data-toggle="modal" data-target="#myModal">CONFIRMAR EL MANTENIMIENTO</button>
                </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmación de Mantenimiento</h4>
      </div>
      <?php $form = ActiveForm::begin([
                "method" => "post",
                "options" => ["enctype" => "multipart/form-data"],
                "action" => ["aprobacion"]
            ]); ?>
      <input type="hidden" name="cronograma" value="<?php echo$cronogramas[0]['cronograma_detalle_id'] ?>"> 
      <input type="hidden" name="equipo" value="<?php echo $cronogramas[0]['cronograma_detalle_equipo_id'] ?>"> 
      <div class="modal-body">
        <label for="">Quien Aprueba</label>
        <input type="text" name="aprueba" class="form-control">
        <label for="">Observaciones</label>
        <input type="text" name="observacion" class="form-control">
        <!-- firma digital -->
        <div class="row">                 
            <div class="col-lg-12 col-xs-12 form-group" id="contentFirmaDigital">
            <label for="">Firma Digital</label>
                <div id="signArea" >
                    <canvas class="sign-pad" id="sign-pad" style="border: 1px solid black" height="100"></canvas>
                </div>
                <button class="btn btn-success" type="button" id="btnSaveSign">Guardar Firma</button>
                <button class="btn btn-danger" type="button" id="btnClearSign">Limpiar</button>
                <input type="hidden" name="Visitas[firmadigital]" id="inputfirma" value="1">
            </div>
            <div class="col-lg-12 col-xs-12 form-group">
                <img src="" class="hide" id="imageFirma"> <br>
                <!-- <button class="btn btn-primary hide" type="button" id="btnNewSign">Nueva firma</button> -->
            </div>
        </div>
        <!-- /firma digital -->

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">cerrar</button>
        <button type="submit" class="btn btn-primary" id="validarCodigo">Aprobar Mantenimiento</button>
        <!-- <input type="submit" class="btn btn-primary">Aprobar Mantenimiento -->
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<script>
        
    $('.pop').click(function(event) {
        $('[data-toggle="popover"]').popover('hide');
        $(this).popover('show');
    });


    // FIRMA DIGITAL 
    $("#btnClearSign").click(function(e){
            $('#signArea').signaturePad().clearCanvas();
    });
            
    $("#btnSaveSign").click(function(e){
        html2canvas([document.getElementById('sign-pad')], {
            onrendered: function (canvas) {
                var canvas_img_data = canvas.toDataURL('image/png');
                var data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                //ajax call to save image inside folder
                $.ajax({
                    url: '<?php echo Yii::$app->request->baseUrl ?>/?r=check-list/guardarfirmadigitalindependiente',
                    data: { 
                        data:data, 
                        idproducto : <?php echo $cronogramas[0]['cronograma_detalle_id'] ?>,
                        equipo : <?php echo $cronogramas[0]['cronograma_detalle_equipo_id'] ?>,

                    },
                    type: 'POST',
                    success: function (file_name) {
                        // window.location.reload();
                        $('#imageFirma').addClass('hide');
                        $('#imageFirma').attr('src','');
                        $('#imageFirma').attr('src',file_name);
                        $('#imageFirma').removeClass('hide');
                        $('#btnNewSign').removeClass('hide');
                        
                        $('#contentFirmaDigital').addClass('hide');
                    }
                });
            }
        });
    });

    $("#btnNewSign").click(function(e){
        $('#imageFirma').addClass('hide');
        $('#imageFirma').attr('src','');
        $('#btnNewSign').addClass('hide');
        $('#signArea').signaturePad().clearCanvas();
        $('#contentFirmaDigital').removeClass('hide');
    });

    $('#btn-aprobado').click(function(event) {

        $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl ?>/?r=check-list/aprobacion',
                type: 'get',
                data: {
                    id : $(this).data("id"),
                    cronograma : $(this).data("cronograma"),
                },
                success: function (data) {
                    <?php $arrayParams = [ 'id'=>$cronogramas[0]['cronograma_detalle_id'], 'Equipo'=> $cronogramas[0]['cronograma_detalle_equipo_id'] ];
                                              $params = array_merge(["check-list/view"], $arrayParams);
                                              $url = Yii::$app->urlManager->createUrl($params); ?>

                    location.href = "<?php echo $url; ?>";
                }
            }); // fin ajax
    });    
</script>