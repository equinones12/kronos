 <?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

// echo "<pre>";
// print_r($_REQUEST);die();
/* @var $this yii\web\View */
/* @var $model app\models\CheckList */
/* @var $form yii\widgets\ActiveForm */

?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<div class="check-list-form">
<?php $form = ActiveForm::begin([
    "method" => "post",
    "options" => ["enctype" => "multipart/form-data"],
]); ?>
<div class="card card-warning">
  <div class="card-header">Control del Tiempo de la actividad</div>
  <div class="card-body">
    <div id="timer" class="form-group">
        <div class="col-lg-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                        <input type="text" id="txtStopWatch" class="form-control" name="TiempoFinal"/><br>
                        <button id="start" type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-play" aria-hidden="true"></span> Start</button>
                        <!-- <button class="btn btn-danger" id="stop"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span> Stop</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

    
<div id="contenedor" style="display: none">    
    <div class="card card-info">
        <div class="card-header">Imagenes de Hardware</div>
        <div class="card-body">
            <div class="col-lg-6">
                <?= $form->field($model, "file[]")->fileInput(['multiple' => true, 'class'=>'a' ])->label('Imagen Inicial')  ?> 
            </div>
        </div>
    </div>    
        
    <div class="card card-primary">
        <div class="card-header">Registro de Check list</div>
        <div class="card-body">
            <div class="col-lg-12">
                
                <?php foreach ($campos as $keyC => $valueC) { ?>
                    
                <div class="col-lg-12 form-group">
                    <div class="alert alert-success pop"  role="alert"  data-toggle="popover" data-placement="top" title="Instruccion de Labor" data-content="<?php echo $valueC['campo_check_list_popover_valor'] ?>"   ><?php echo $valueC['campo_check_list_nombre'] ?>
                    </div>
                    <?php if ($valueC['campo_check_list_popover_trae']==0) { ?>
                    <input type="text" class="form-control a" placeholder="Registre los Valores" name="Valores[8][<?php echo $valueC['campo_check_list_id'] ?>]"> 
                    <?php }else{ 

                        if ($valueC['campo_check_list_id']!='19' && $valueC['campo_check_list_id']!='20') { ?>
                                <select name="Valores[8][<?php echo $valueC['campo_check_list_id'] ?>]" id="" class="form-control a">
                                    <option value="">[--Seleccione Opcion--]</option>
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>
                                </select>
                        <?php }else { ?>
                            <textarea class="form-control a" name="Valores[8][<?php echo $valueC['campo_check_list_id'] ?>]"></textarea>

                        <?php } ?>
                    
                    <?php } ?>
                    
                </div>    
                <?php } ?> 
                
            </div>
            
            <div class="col-lg-12">
                <div class="card card-warning">
                    <div class="card-header">Imagenes de Entrega</div>
                    <div class="card-body">
                        <div class="col-lg-12">
                            <?= $form->field($model, "file[]")->fileInput(['multiple' => true, 'class'=>'a'])->label('Imagen Final')  ?> 
                        </div>
                    </div>
                </div>    
            </div>
            
            <div class="col-lg-12">
                <div class="col-lg-12 form-group">
                    <button type="submit" class="btn btn-success" id="registrar">Registar</button>
                </div>
            </div>
        </div>
    </div>
    
    
    <?php ActiveForm::end(); ?>
</div>
</div>
<script type="text/javascript">
        
$("#registrar").click(function(event) {
    
    $('.a').each(function(){
           if ($(this).val() == "" || $(this).val() == null ) {
               $(this).css("border", "1px solid red");
               validate = false;

           }else{
               $(this).css("border", "1px solid #ccc");
               validate = true;
           }
       });

       if(validate) {
           
           return true;
       }else{
           alert('SE DEBEN DILIGENCIAR TODOS LOS CAMPOS PARA REGISTRAR EL MANTENIMIENTO');
           return false;
       }



});


$(document).ready(function(){

    $('.pop').click(function(event) {
            $('[data-toggle="popover"]').popover('hide');            
            $(this).popover('show');
        });

var stopWatchInterval = null;
var stopWatchStart = null;
var stopWatchValue = null;
var stopWatchDisplay = null;

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function UpdateStopWatch(newDate)
{
  stopWatchValue = newDate - stopWatchStart;
  var seconds = Math.floor(stopWatchValue / 1000);
  var hours = Math.floor(seconds / 3600);
  var minutes = Math.floor((seconds % 3600) / 60);
  seconds = seconds % 60;
  stopWatchDisplay.val(pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2));
}

$(function()
 {
  stopWatchDisplay = $('#txtStopWatch');
  $('#start').click(function()
                   {

    $("#contenedor").css({
        display: 'block',
        
    });
    stopWatchStart = new Date();
    UpdateStopWatch(stopWatchStart);
    stopWatchInterval = setInterval(
    function()
    {
      var now = new Date();
      UpdateStopWatch(now);
    }, 50);
  });
  
  $('#stop').click(function()
                  {
    clearInterval(stopWatchInterval);
    stopWatchValue = (new Date()) - stopWatchStart;
  });
});


    


})


</script>