<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url; 
// echo "<pre>";
// print_r($_REQUEST);die();
/* @var $this yii\web\View */
/* @var $model app\models\CheckList */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<div class="check-list-form">
    <?php $form = ActiveForm::begin(); ?>
    <!-- <input type="hidden" name="cronograma" valuw="<?php echo $cronogramas[0]['cronograma_detalle_id']; ?>"> -->
    <div class="card card-primary">
        <div class="card-header">Actualizacion de Check list</div>
        <div class="card-body">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td><b>Identificacion T & T</b></td>
                            <td><b>Nombre Actulal</b></td>
                            <td><b>Tipo de Mantenimiento</b></td>
                            <td><b>Quien lo realiza</b></td>
                            <td><b>Hora del Mantenimiento</b></td>
                            <td><b>Estado</b></td>
                        </tr>    
                        <tr>
                            <td><?php echo $cronogramas[0]['equipo_identificacion_tecnologia'] ?></td>
                            <td><?php echo $cronogramas[0]['equipo_nombre_actual'] ?></td>
                            <td><?php echo $cronogramas[0]['tipoMantenimiento'] ?></td>
                            <td><?php echo $cronogramas[0]['quienRealiza'] ?></td>
                            <td><?php echo $cronogramas[0]['cronograma_detalle_hora_inicio']." a ".$cronogramas[0]['cronograma_detalle_hora_fin'] ?></td>
                            <td><?php echo $cronogramas[0]['estado'] ?></td>
                        </tr>
                    </table>  
                </div>
                <?php $form = ActiveForm::begin(); ?>
                <?php foreach ($checklist as $key => $valueC) { ?>
                <div class="col-lg-12 form-group">
                    <div class="alert alert-success pop"  role="alert"  data-toggle="popover" data-placement="top" title="Instruccion de Labor" data-content="<?php echo $valueC['campo_check_list_popover_valor'] ?>"   ><?php echo $valueC['campo_check_list_nombre'] ?>
                    </div>
                    <?php if ($valueC['campo_check_list_popover_trae']==0) { ?>
                    <input type="text" class="form-control a" placeholder="Registre los Valores" name="Valores[8][<?php echo $valueC['check_list_id'] ?>]" value="<?php echo $valueC['check_list_valor'] ?>"> 
                    <?php }else{ 

                        if ($valueC['campo_check_list_id']!='19' && $valueC['campo_check_list_id']!='20') { ?>
                                <select name="Valores[8][<?php echo $valueC['check_list_id'] ?>]" id="" class="form-control a">
                                    <option value="<?php echo $valueC['check_list_valor'] ?>"><?php echo $valueC['check_list_valor'] ?></option>
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>
                                </select>
                        <?php }else { ?>
                            <textarea class="form-control" name="Valores[8][<?php echo $valueC['check_list_id'] ?>]"><?php echo $valueC['check_list_valor'] ?></textarea>

                        <?php } ?>
                    
                    <?php } ?>
                    
                </div> 
                <?php } ?>
                <?php ActiveForm::end(); ?>
                <div class="col-lg-12">
                    
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-12 form-group">
                    <input type="submit" class="btn btn-success" value="Actualizar">
                </div>
            </div>
        </div>
    </div>
    
    
    <?php ActiveForm::end(); ?>
</div>
<script>
        $('.pop').click(function(event) {
            $('[data-toggle="popover"]').popover('hide');            
            $(this).popover('show');
        });
</script>