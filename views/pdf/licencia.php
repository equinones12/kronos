<?php
    use Da\QrCode\QrCode;

?>
<div style="width:100%; text-align:right; color: white; padding-right:40px; padding-top:20px; font-size:15px; background: #1A2048;; ">
    Stellar WAY S.A.S - 10156515655-2
</div>
<div class="col-lg-12" style="text-align:center; color: #4F4F4F; font-size:20px; margin-top:40px;">
    <b>USUARIO</b> <br>
    <?php echo $model->clientes_nombre ?> - <?php echo $model->clientes_nit ?> <br>
</div>

<div class="col-lg-12" style="text-align:center; color: #4F4F4F; font-size:20px; margin-top:35px; padding-left: 40px; padding-right: 40px;">
    <table class="table">
        <thead>
            <tr>
                <th>PLAN</th>
                <th>CANTIDAD USUARIOS</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> <?php echo $model->clientes_tipolicencia ?></td>
                <td> <?php echo $model->clientes_cantidadusuarios ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
            <tr>
                <th>PERIODO LICENCIA</th>
                <th>VALOR PLAN LICENCIAMIENTO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $model->clientes_licencia_fecha_desde . ' Hasta ' . $model->clientes_licencia_fecha_hasta ?></td>
                <td>$ <?php echo number_format(612000,0,'','.') ?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-lg-12" style="text-align:center; color: #4F4F4F; font-size:20px; margin-top:55px; ">
    <b>NUMERO DE LICENCIA</b>
    <p style="background: #aaaaaa; color: white; font-size:44px;"><?php echo $model->clientes_licencia ?></p>
</div>

<div class="col-lg-12" style="text-align:center; color: #4F4F4F; font-size:20px; margin-top:55px; ">
    <b>QR LICENCIA</b>
    <br>
    <br>
    <?php
        $qrCode = (new QrCode('http://35.175.13.101/framos/helpdesk/web/index.php?r=pdf/licencia&id='.$model->clientes_id))
            ->setSize(100)
            ->setMargin(5)
            ->useForegroundColor(0, 0, 0);
    ?>
    <img  src="<?php echo $qrCode->writeDataUri() ?>">
</div>
<div class="col-lg-12" style="text-align:center; color: #4F4F4F; font-size:20px; margin-top:35px; padding-left: 40px; padding-right: 40px;">
    <b>CARACTERISTICAS WEB PEM</b>
    <table class="table">
        <thead>
            <tr>
                <th>Soporte</th>
                <th>Cert. Seguridad</th>
                <th>Almacenamiento</th>
                <th>Procesamiento</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Helpdesk</td>
                <td>SSL (Https)</td>
                <td>60 Gb</td>
                <td>8 Gb Ram / Cpu 3.40 Ghz</td>
            </tr>
        </tbody>
    </table>
</div>




<!-- BORDES -->
<div class="col-lg-12" style="right:0px; height:1000px; color: white; font-size:60px; top:0px;; width:3px; position:absolute; background: #1A2048; "></div>
<div class="col-lg-12" style="left:0px; height:1000px; color: white; font-size:60px; top:0px;; width:1px; position:absolute; background: #1A2048; "></div>

<!-- FOOTER -->
<div class="col-lg-12" style="left:-15px; height:100px; color: white; font-size:60px; bottom:0px;; width:100%; position:absolute; background: #1A2048;"></div>
<div class="col-lg-12" style="left:-15px; height:150px; color: white; font-size:60px; bottom:10px;; width:150px; position:absolute; background: #1A2048; "></div>
<div class="col-lg-12" style="right:0px; height:150px; color: white; font-size:60px; bottom:10px;; width:150px; position:absolute; background: #1A2048; "></div>
<div class="col-lg-12" style="right:410px; height:120px; color: white; font-size:60px; bottom:10px;; width:150px; position:absolute; background: #1A2048; "></div>
<div class="col-lg-12" style="right:220px; height:120px; color: white; font-size:60px; bottom:10px;; width:150px; position:absolute; background: #1A2048; "></div>

<div class="col-lg-12" style="left:-15px; height:80px; color: white; font-size:15px; bottom:0px;; width:100%; position:absolute; background: #1A2048; color:white; text-align:center;">
    Stellar WAY S.A.S - 10156515655-2 <br>
    57894445 - 3125484885 <br>
    Cra 156 No 84 f 24, 7 de Agosto / Bogota - Colombia <br>
</div>
