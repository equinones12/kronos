<?php

use yii\helpers\Html;
use yii\grid\GridView;
// echo '<pre>';
// print_r($model);
// die();

$this->title = 'Soporte a equipos';
$this->params['breadcrumbs'][] = $this->title;
?>
    
<div class="card card-purple">
    <div class="card-header">
    <!-- <h3 class="card-title">DataTable with default features</h3> -->Resultados encontrados
    </div>
    <!-- /.card-header -->
    <div class="card-body">
       
        <div class="col-lg-12">
            <div class="row">
             <div class="table-responsive">              
                <table id="example1" class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                    <th>Nombre de equipo</th>
                    <th>Quién solicita</th>
                    <th>Tipo de mantenimiento</th>
                    <th>Fecha de solicitud</th>
                    <th>Fecha de respuesta</th>
                    <th>Estado</th>                    
                    <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model as $value) { ?>
                    <tr>
                        <td><?php echo $value->equipo->equipo_identificacion_tecnologia ?></td>
                        <td><?php echo $value->nombre.' '.$value->apellido ?></td>
                        <td><?php echo $value->tiposoporte->tipo_detalle_nombre?></td>                        
                        <td><?php echo $value->fecha_solicitud?></td>                        
                        <td><?php echo $value->fecha_cierre?></td>                        
                        <td><?php echo $value->estado_soporte == 0 ? "Pendiente":"Realizado"?></td>
                        <td>
                            <button class="class' => 'btn btn-primary btn-block vermantenimiento" id ="<?php echo $value->equipos_idequipo?>">Ver</button>                                                   
                            <?php if (!$value->estado_soporte) {?>                                                
                              <button class="class' => 'btn btn-warning btn-block gestionmant" id ="<?php echo $value->equipos_idequipo?>">Gestionar</button>
                            <?php } ?>                               
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div>  
            </div>
        </div>    
    </div>
    <!-- /.card-body -->
</div>

 <!-- MODAL MANTENIMIENTO -->
 <div class="modal fade" id="modalsoporte" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">                                         
                </div> -->
                <div class="modal-body text-center container12">
                    
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="solicitudpermisos-form">                                                                                                                       
                        <div class="form-group">
                        <br>                            
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>                                                          
                        </div>                            
                    </div>
                </div>                
                </div>
            </div>
        </div>
        </div>    
    </div> 
    

   <!-- MODAL GUARDAR -->
   <div class="modal fade" id="gestionarmantenimient" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title btn btn-success btn-block" id="exampleModalLabel"><center><b>Gestionar mantenimiento</b><center></h5>                        
                </div>               
                <div class="modal-footer">
                    <div class="form-row">
                        <div class="col-lg-12">
                            <div class="solicitudpermisos-form">                                                                                                                               
                                <div class="">
                                <center><textarea name="observacion" rows="5" cols="70" class="form-control" required="required" val="" id = "observ"></textarea></center>
                                <input type="hidden" value='<?php echo $value->equipos_idequipo?>' name="id">
                                <br>                         
                                <!-- <input type="file" class=""  id="acta" name ="actabaja">   -->                                                        
                                <br>
                                    <button type="submit" class="btn btn-warning gestionado" id='<?php echo $value->equipos_idequipo?>'>Enviar</button>                                    
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>                                                          
                            </div>                            

                        </div>                            
                    </div>                                        
                </div>
            </div>        
        </div>    
    </div>  
<script>
  $(function () {
    $("#example1").DataTable({
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
   ///// ver detalle de mantenimiento
   $(".vermantenimiento").click(function(){            
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/vermantenimiento',
                type : 'get',
                data :{
                    id :$(this).attr('id')
                },
                success: function(data){
                $('.container12').html(data);
                $('#modalsoporte').modal();  
                }
            });            
        });
        //// gestioanar mantenimiento/////////////////////////////////////////////////
        $(".gestionmant").click(function(){                        
                $('#gestionarmantenimient').modal();                             
        });
        $(".gestionado").click(function(){
            $(this).hide();
            $.ajax({
                url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/gestionarmantenimiento',
                type : 'get',
                data :{
                    id :$(this).attr('id'),
                    observacion : $('#observ').val(),
                },
                success: function(data){
                    location.reload(); 
                }
            });            
        });
</script>