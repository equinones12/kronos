<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;

/* @var $this yii\web\View */
/* @var $model app\models\AccesosDirectos */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .color{
        height: 20px;
    }
    .colorSelect{
        border: 2px solid black;
        box-shadow: 0px 0px 5px black;
    }
</style>


<div class="accesos-directos-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12 noPadding form-group">
        <div class="col-lg-6">
            <?= $form->field($model, 'etiqueta')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'color')->hiddenInput(['maxlength' => true])->label(false) ?>

    <div class="col-lg-6 form-group">
        <?= Html::label('Rol', 'idRol', ['class'=>'control-label']) ?>
        <?= Html::activeDropDownList(
            $model,
            'idRol',
            ArrayHelper::map(Rol::find()
                    ->all(), 'id', 'nombre'),
            [
                'class'=>'form-control',
                'prompt'=>'[-- Seleccione Rol --]',
                'value'=> '',
                'required' => 'required'
            ]) 
        ?>
        <div class="help-block"></div>
    </div>

    <div class="col-lg-6 form-group">
        <?= Html::label('Estado', 'estado', ['class'=>'control-label']) ?>
        <?= Html::activeDropDownList(
            $model,
            'estado',
            [
                1 => 'Activo',
                0 => 'Inactivo'
            ],
            [
                'class'=>'form-control',
                'prompt'=>'[-- Seleccione Estado --]',
                'value'=> '',
                'required' => 'required'
            ]) 
        ?>
        <div class="help-block"></div>
    </div>

    <!-- ======== paleta de colores ========== -->
    <div class="contentPaletaColores col-lg-12 form-group">
        <label for="" class="label-form">Color</label>
        <br>
        <div class="btn btn-default color" id="default"></div>
        <div class="btn btn-primary color" id="primary"></div>
        <div class="btn btn-success color" id="success"></div>
        <div class="btn btn-info color" id="info"></div>
        <div class="btn btn-warning color" id="warning"></div>
        <div class="btn btn-danger color" id="danger"></div>
    </div>

    <div class="form-group col-lg-12">
        <br>
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<!-- Js -->
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
<script>
    $(document).ready(function(){

        $("#<?php echo $model->color; ?>").addClass('colorSelect');

        $('.color').click(function(){
            $('.color').removeClass('colorSelect');
            $(this).addClass('colorSelect');
            $('#accesosdirectos-color').val($(this).attr('id'));
        })

    });
</script>