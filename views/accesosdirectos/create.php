<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AccesosDirectos */

$this->title = 'Crear Acceso Directo';
$this->params['breadcrumbs'][] = ['label' => 'Accesos Directos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accesos-directos-create">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
