<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AccesosDirectos */

$this->title = 'Actualizar Acceso Directo: ' . ' ' . $model->etiqueta;
$this->params['breadcrumbs'][] = ['label' => 'Accesos Directos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->etiqueta, 'url' => ['view', 'id' => $model->idaccesos_directos]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="accesos-directos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
