<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AccesosDirectos */

$this->title = $model->etiqueta;
$this->params['breadcrumbs'][] = ['label' => 'Accesos Directos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accesos-directos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idaccesos_directos], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idaccesos_directos',
            'etiqueta',
            'action',
            'color',
            'idRol',
            'estado',
        ],
    ]) ?>

</div>
