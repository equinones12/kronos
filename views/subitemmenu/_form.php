<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\ItemMenu;

/* @var $this yii\web\View */
/* @var $model app\models\SubitemMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subitem-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12 noPadding">
    	<div class="col-lg-6">
    		<?= $form->field($model, 'etiqueta')->textInput(['maxlength' => true]) ?>
    	</div>
    	<div class="col-lg-6">
    		<?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>
    	</div>
    </div>

	<div class="col-lg-12 noPadding">
        <div class="col-lg-6">
        	<?= Html::label('Item', 'id_item', ['class'=>'control-label']) ?>
        	<?= Html::activeDropDownList(
        	    $model,
        	    'id_item',
        	    ArrayHelper::map(ItemMenu::find()
        	            ->all(), 'id', 'etiqueta'),
        	    [
        	        'class'=>'form-control',
        	        'prompt'=>'[-- Seleccione Item --]',
        	        'value'=> '',
        	        'required' => 'required'
        	    ]) 
        	?>
            <div class="help-block"></div>

        </div>
    </div>

    <div class="form-group col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
