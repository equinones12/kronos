<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\db\Connection;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Departamentos;
use app\models\Localidades;
use app\models\Sucursales;
use app\models\Areas;
use app\models\Users;
use app\models\TiposDetalles;
use app\models\Zona;
use app\models\PuntosRecaudo;
use kartik\date\DatePicker;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CronogramasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estadistica de Mantenimientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo Url::base(); ?>/js/highcharts.js"></script>
<script src="<?php echo Url::base(); ?>/js/exporting.js"></script>
<script src="<?php echo Url::base(); ?>/js/export-data.js"></script>
<script src="<?php echo Url::base(); ?>/js/highcharts-3d.js"></script>
<script src="<?php echo Url::base(); ?>/js/cylinder.js"></script>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="estadisticascronogramas-index">    
    
    <?php    
     $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'login-form'],
    ]) ?>
    <div class="form-row"> 
       <div class="col-lg-12">   
        <div class="card card-primary">
            <div class="card-header">Panel de Busqueda <?= Html::encode($this->title) ?></div></br>            
            <div class="form-row">
                
                <div class="col-md-6 mb-3">
                    <?= Html::label('Zona', 'select-zona', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-zona',
                                '',
                                ArrayHelper::map(Zona::find()->all(),'idzona', 'nombre'),
                                [
                                    'class'=>'form-control ',                                    
                                    'prompt'=>'[-- Seleccione Zona --]',
                                    'value'=> '',
                                    'id' => 'select-zona',
                                    'onchange' => '$.get("index.php?r=estadisticas/lists&id='.'"+$(this).val(), function(data){
                                        $("#select-sucursales").html(data);
                                    })',
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>                    
                <div class="col-md-6 mb-3">
                     <?= Html::label('Sucursales', 'select-sucursales', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-sucursales',
                                '',
                                // ArrayHelper::map(Sucursales::find()->all(),'idSucursal', 'nombre'),
                                [],
                                [
                                    'class'=>'form-control ',
                                    'prompt'=>'[-- Seleccione Sucursal --]',
                                    'value'=> '',
                                    'id' => 'select-sucursales',
                                    'onchange' => '$.post("index.php?r=departamentos/lists&id='.'"+$(this).val(), function(data){
                                        $("#select-departamentos").html(data);
                                    })',
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>
                <div class="col-md-6 mb-3">
                    <?= Html::label('Tipo Mantenimiento', 'tipo-mantenimiento', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-mantenimiento',
                                '',
                                ArrayHelper::map(TiposDetalles::find()->where(['tipo_detalle_id'=> [21,22]])->all(),'tipo_detalle_id', 'tipo_detalle_nombre'),
                                [
                                    'class'=>'form-control ',                                    
                                    'prompt'=>'[-- Seleccione Tipo --]',
                                    'value'=> '',
                                    'id' => 'select-mantenimiento',                                    
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>
                <div class="col-md-6 mb-3">                                                                
                    <label for="">Estado mantenimiento</label>
                    <select name="estado" class="form-control">
                            <option value="" selected>[-- Seleccione estado --]</option>
                            <option value="1">REALIZADO</option>
                            <option value="0">PENDIENTE</option>
                    </select>                   
                </div>    
                <div class="col-md-6 mb-3">                                             
                     <label for="">Aprobado</label>
                    <select name="aprobado" class="form-control">
                            <option value="" selected>[-- Seleccione opcion --]</option>
                            <option value="1">SI</option>
                            <option value="0">NO</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <?= Html::label('Tecnico', 'Tecnico', ['class'=>'control-label']) ?>
                        <?= Html::dropDownList(
                            'tecnico',
                            '',
                            ArrayHelper::map(Users::find()
                                                        ->select('*')
                                                        ->leftjoin('cronogramas','id = cronograma_quien_realiza')
                                                        ->all(),'id',function ($model, $defaultValue){
                                                        return $model->nombres .' '. $model->apellidos;
                                                        }), 
                                                        
                            [
                                'class'=>'form-control ',                                    
                                'prompt'=>'[-- Seleccione area --]',
                                'value'=> '',
                                'id' => 'select-tecnico',                                    
                            ]) 
                        ?>
                        <div class="help-block"></div>
                </div>                              
            </div> 
            <div class="col-md-6 mb-3">
                    <?= Html::submitButton("Buscar",["class"=>"btn btn-primary","id"=>"enviar"])?>
                </div>             
        </div>
      </div> 
    </div>     
    <?php ActiveForm::end(); ?>
    <?php if ($cronogramaZonas!=null) { ?>
    <div class="form-row"> 
      <div class="col-lg-12">             
        <div class="card card-primary">
            <div class="card-header">Resultados Encontrados
                        <!-- <button type="button" id="btn-excel" class="btn btn-default" aria-label="Left Align"> -->
                            <img id="btn-excel" src="images/exceln.png" alt="" width="60" title="Reporte General" > 
                        <!-- </button> -->
            </div>            
                        <?php $form = ActiveForm::begin([
                                'action' => ['excel/reportecronogramas'],
                                'method' => 'post',
                                'id' => 'equipos',
                                ]); ?>
                            <input type="hidden" name="filtro" value="<?php echo $filtro ?>">               
                    <?php ActiveForm::end()?>
                <p><div class="alert alert-warning" role="alert"><center>Gráficas estadisticas</center></div></p>
                <div class="table-responsive">
                        <p><img id="btn-reporte" src="<?php echo Url::base(); ?>/images/excel.png ?>" alt="" title="Reporte Excel" style="cursor:pointer;margin-left: 20px;"></p>                          
                        <caption><center>ESTADISTICA DE CRONOGRAMAS POR ZONA</center></caption>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr class = "active">
                            <td><b>Zona</b></td>                 
                            <td><b>Cronogramas</b></td>
                            <td><b>Mantenimientos</b></td>
                            <td><b>Preventivos</b></td>                 
                            <td><b>Correctivos</b></td>                 
                            <td><b>Realizado</b></td>
                            <td><b>Pendiente</b></td>
                        </tr>
                        </thead>                                                                        
                           <tbody>
                           <?php foreach($cronogramaZonas as $DC) { ?>
                            <tr class = "alert alert-info">
                                <td><?php echo $DC['zona']?></td>                    
                                <td><?php echo $DC['Cronogramas']?></td>
                                <td><?php echo $DC['Mantenimientos']?></td>
                                <td><?php echo $DC['Preventivos']?></td>                   
                                <td><?php echo $DC['Correctivos']?></td>                    
                                <td><?php echo $DC['realizado']?></td>
                                <td><?php echo $DC['Pendiente']?></td>
                            </tr>
                            <?php } ?>
                           </tbody>                                                                                
                    </table>
                </div>
                    <div class="">
                    <div id = "container"></div>
                    </div>
                    <!-- Container de diagrama circular por zonas -->
                    <div class="">           
                    <div id = "container3"></div>
                    <?= Html::Button("Ver detalle por sucursal",["class"=>"btn btn-warning", 'id' => "ver-sucursales"])?>  
                    </div>
                    <!-- Container de diagrama circular por sucursales -->
                    <div class="">
                    <div id = "container4"></div>
                    </div>
                    </div>            
        </div>
      </div> 
    </div> 
    <!-- modal para ver detalle licencias por sucursales -->      
        <div class="modal fade" id="verlicencias" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body" id="equipos" >
                    <!-- Tabla de equipos por sucursal -->
                    <div class="card card-info">
                        <div class="card-header"><center>Tabla de licencias por sucursal</center></div>
                    <div class="card-body">                                        
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">                    
                                <tr class="active">
                                    <td><b>Sucursal</b></td>                 
                                    <td><b>Cronogramas</b></td>
                                    <td><b>Mantenimientos</b></td>
                                    <td><b>Preventivos</b></td>                 
                                    <td><b>Correctivos</b></td>                        
                                    <td><b>Realizado</b></td>
                                    <td><b>Pendiente</b></td>
                                </tr>
                                <?php foreach($cronogramaSucursales as $CS) { ?>
                                    <tr class="alert alert-info">
                                        <td><?php echo $CS['Sucursal']?></td>                    
                                        <td><?php echo $CS['Cronogramas']?></td>
                                        <td><?php echo $CS['Mantenimientos']?></td>
                                        <td><?php echo $CS['Preventivos']?></td>                   
                                        <td><?php echo $CS['Correctivos']?></td>                            
                                        <td><?php echo $CS['realizado']?></td>
                                        <td><?php echo $CS['Pendiente']?></td>                                                              
                                    </tr>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>         
                    </div>            
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <div class="solicitudpermisos-form">                                                      
                                <div class="form-group">
                                <br>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">cerrar</button>                                                                                                                                                                    
                                </div>                                                                             
                            </div>
                        </div>                
                    </div>
                </div>     
            </div>        
        </div>   
        <?php } else { ?>
     <div class="alert alert-warning" role="alert"> No existen registros para los filtros seleccionados</div>
    <?php } ?>  
</div>

     

<script type="text/javascript">
   $(document).ready(function(){
  // acción clic para abrir modal equipo por sucursal
    $('#ver-sucursales').click(function(){  
        $('#verequipos').modal();        
    })
           
    });
    $("#btn-reporte").click(function(){
            $("#equipos").submit();
    })

  // construcción de diagrama de barras
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Diagrama de barras'
        },
        xAxis: {
            
            categories: [<?php echo $zonaC?>],
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
            text: 'valores'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
            pointPadding: 0.2,
            borderWidth: 0
            }
        },
        series: [{
            name: 'Preventivos',
            data: [<?php echo $preventivo?>]

        }, {
            name: 'Correctivos',
            data: [<?php echo $correctivo?>]

        }, {
            name: 'Pendiente',
            data: [<?php echo $pendiente?>]

        }, {    
            name: 'Realizado',
            data: [<?php echo $realizado?>]
        }]
    });
    // Construcción de diagrama circular por zona
    Highcharts.chart('container3', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Estadistica de mantenimientos por zona'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        tooltip: {
            pointFormat: '<b>{series.name}:{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '<b>Zona {point.name}</b>'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Porcentaje',
            data: [
                <?php echo $cronogramaz?>
            ]
        }]
    });
      // Construcción de diagrama circular por zona
      Highcharts.chart('container4', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Estadistica de mantenimientos por sucursales'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        tooltip: {
            pointFormat: '<b>{series.name}:{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Porcentaje',
            data: [
                <?php echo $cronogramasuc?>
            ]
        }]
    });
</script>