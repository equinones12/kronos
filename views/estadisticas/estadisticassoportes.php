<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\db\Connection;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Departamentos;
use app\models\Localidades;
use app\models\Sucursales;
use app\models\Areas;
use app\models\Users;
use app\models\TiposDetalles;
use app\models\Zona;
use app\models\PuntosRecaudo;
use kartik\date\DatePicker;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CronogramasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estadistica de Soportes';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo Url::base(); ?>/js/highcharts.js"></script>
<script src="<?php echo Url::base(); ?>/js/exporting.js"></script>
<script src="<?php echo Url::base(); ?>/js/export-data.js"></script>
<script src="<?php echo Url::base(); ?>/js/highcharts-3d.js"></script>
<script src="<?php echo Url::base(); ?>/js/cylinder.js"></script>
<script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>

<div class="estadisticascronogramas-index">    
    
    <?php    
     $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'login-form'],
    ]) ?>
    <div class="form-row"> 
       <div class="col-lg-12">   
        <div class="card card-primary">
            <div class="card-header">Panel de Busqueda <?= Html::encode($this->title) ?></div></br>            
            <div class="form-row">
                
                <div class="col-md-6 mb-3">
                    <?= Html::label('Zona', 'select-zona', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-zona',
                                '',
                                ArrayHelper::map(Zona::find()->all(),'idzona', 'nombre'),
                                [
                                    'class'=>'form-control ',                                    
                                    'prompt'=>'[-- Seleccione Zona --]',
                                    'value'=> '',
                                    'id' => 'select-zona',
                                    'onchange' => '$.get("index.php?r=estadisticas/lists&id='.'"+$(this).val(), function(data){
                                        $("#select-sucursales").html(data);
                                    })',
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>                    
                <div class="col-md-6 mb-3">
                     <?= Html::label('Sucursales', 'select-sucursales', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-sucursales',
                                '',
                                // ArrayHelper::map(Sucursales::find()->all(),'idSucursal', 'nombre'),
                                [],
                                [
                                    'class'=>'form-control ',
                                    'prompt'=>'[-- Seleccione Sucursal --]',
                                    'value'=> '',
                                    'id' => 'select-sucursales',
                                    'onchange' => '$.get("index.php?r=departamentos/lists&id='.'"+$(this).val(), function(data){
                                        $("#select-departamentos").html(data);
                                    })',
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>
                <div class="col-md-6 mb-3">
                    <?= Html::label('Tipo Soporte', 'select-soporte', ['class'=>'control-label']) ?>
                            <?= Html::dropDownList(
                                'select-soporte',
                                '',
                                ArrayHelper::map(TiposDetalles::find()->where(['tipo_detalle_tipo_id'=> [13]])->all(),'tipo_detalle_id', 'tipo_detalle_nombre'),
                                [
                                    'class'=>'form-control ',                                    
                                    'prompt'=>'[-- Seleccione Tipo --]',
                                    'value'=> '',
                                    'id' => 'select-mantenimiento',                                    
                                ]) 
                            ?>
                            <div class="help-block"></div>
                </div>                                            
            </div> 
            <div class="col-md-6 mb-3">
                    <?= Html::submitButton("Buscar",["class"=>"btn btn-primary","id"=>"enviar"])?>
                </div>             
        </div>
      </div> 
    </div>     
    <?php ActiveForm::end(); ?>
    <?php if ($soportezonas!=null) { ?>
    <div class="form-row"> 
      <div class="col-lg-12">             
        <div class="card card-primary">
            <div class="card-header">Resultados Encontrados
                        <!-- <button type="button" id="btn-excel" class="btn btn-default" aria-label="Left Align"> -->                            
                        <!-- </button> -->
            </div>            
                        <?php $form = ActiveForm::begin([
                                'action' => ['excel/reportecronogramas'],
                                'method' => 'post',
                                'id' => 'equipos',
                                ]); ?>
                            <input type="hidden" name="filtro" value="<?php echo $filtro ?>">               
                    <?php ActiveForm::end()?>
                <p><div class="alert alert-warning" role="alert"><center>Gráficas estadisticas</center></div></p>
                <div class="card-body">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="table-responsive">
                                <!-- <p><img id="btn-reporte" src="<?php echo Url::base(); ?>/images/excel.png ?>" alt="" title="Reporte Excel" style="cursor:pointer;margin-left: 20px;"></p>                           -->
                                <caption><center>ESTADISTICA DE SOPORTES POR ZONA</center></caption>
                                <table id="example1" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class = "active">
                                        <td><b>ZONA</b></td>                 
                                        <td><b>EQUIPO_NO_ENCIENDE</b></td>
                                        <td><b>NO_ARRANCA_SISTEMA_OPERATIVO</b></td>
                                        <td><b>PROBLEMAS_DE_CONEXIÓN</b></td>                 
                                        <td><b>EQUIPO_SIN_CARPETA_DE_RED</b></td>                 
                                        <td><b>NO_HAY_CONEXIÓN_CON_IMPRESORA</b></td>
                                        <td><b>ERROR_CON_EL_OFFICCE</b></td>
                                        <td><b>CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN</b></td>
                                        <td><b>EQUIPO_LENTO</b></td>
                                    </tr>
                                    </thead>                                                                        
                                    <tbody>
                                    <?php foreach($soportezonas as $SZ) { ?>
                                        <tr class = "alert alert-info">
                                            <td><?php echo $SZ['ZONA']?></td>                    
                                            <td><?php echo $SZ['EQUIPO_NO_ENCIENDE']?></td>
                                            <td><?php echo $SZ['NO_ARRANCA_SISTEMA_OPERATIVO']?></td>
                                            <td><?php echo $SZ['PROBLEMAS_DE_CONEXIÓN']?></td>                   
                                            <td><?php echo $SZ['EQUIPO_SIN_CARPETA_DE_RED']?></td>                    
                                            <td><?php echo $SZ['NO_HAY_CONEXIÓN_CON_IMPRESORA']?></td>
                                            <td><?php echo $SZ['ERROR_CON_EL_OFFICCE']?></td>
                                            <td><?php echo $SZ['CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN']?></td>
                                            <td><?php echo $SZ['EQUIPO_LENTO']?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>                                                                                
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                                        
                    <div class="row">
                        <div class="col-lg-6">
                            <div id = "container"></div>
                        </div>
                        <div class="col-lg-6">
                            <div id = "container2"></div>
                        </div>
                        <div class="col-lg-12">
                            <div id = "container5"></div>
                        </div>
                        <div class="col-lg-12">
                            <div id = "container6"></div>
                        </div>
                    </div>                           
                    <!-- Container de diagrama circular por zonas -->
                    <div class="">           
                    <div id = "container3"></div>
                    <?= Html::Button("Ver detalle por sucursal",["class"=>"btn btn-warning", 'id' => "ver-sucursales"])?>  
                    </div>
                    <!-- Container de diagrama circular por sucursales -->
                    <div class="">
                    <div id = "container4"></div>
                    </div>
                           
        </div>
      </div> 
    </div> 
    <!-- modal para ver detalle licencias por sucursales -->      
        <div class="modal fade" id="verlicencias" tabindex="-1" role="dialog" aria-labelledby="aprobarLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body" id="equipos" >
                    <!-- Tabla de equipos por sucursal -->
                    <div class="card card-info">
                        <div class="card-header"><center>Tabla de soportes por sucursal</center></div>
                    <div class="card-body">                                        
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-hover">                    
                                    <tr class="active">
                                        <td><b>SUCURSAL</b></td>                 
                                        <td><b>EQUIPO_NO_ENCIENDE</b></td>
                                        <td><b>NO_ARRANCA_SISTEMA_OPERATIVO</b></td>
                                        <td><b>PROBLEMAS_DE_CONEXIÓN</b></td>                 
                                        <td><b>EQUIPO_SIN_CARPETA_DE_RED</b></td>                 
                                        <td><b>NO_HAY_CONEXIÓN_CON_IMPRESORA</b></td>
                                        <td><b>ERROR_CON_EL_OFFICCE</b></td>
                                        <td><b>CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN</b></td>
                                        <td><b>EQUIPO_LENTO</b></td>
                                    </tr>
                                    <?php foreach($soportesucursales as $SS) { ?>
                                        <tr class="alert alert-info">
                                        <td><?php echo $SS['SUCURSALES']?></td>                    
                                        <td><?php echo $SS['EQUIPO_NO_ENCIENDE']?></td>
                                        <td><?php echo $SS['NO_ARRANCA_SISTEMA_OPERATIVO']?></td>
                                        <td><?php echo $SS['PROBLEMAS_DE_CONEXIÓN']?></td>                   
                                        <td><?php echo $SS['EQUIPO_SIN_CARPETA_DE_RED']?></td>                    
                                        <td><?php echo $SS['NO_HAY_CONEXIÓN_CON_IMPRESORA']?></td>
                                        <td><?php echo $SS['ERROR_CON_EL_OFFICCE']?></td>
                                        <td><?php echo $SS['CONFIGURACIÓN_DE_PAGINA_DE_IMPRESIÓN']?></td>
                                        <td><?php echo $SS['EQUIPO_LENTO']?></td>                                                             
                                        </tr>
                                    <?php } ?>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </div>         
                    </div>            
                    </div>
                    <div class="modal-footer">
                        <div class="form-group">
                            <div class="solicitudpermisos-form">                                                      
                                <div class="form-group">
                                <br>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">cerrar</button>                                                                                                                                                                    
                                </div>                                                                             
                            </div>
                        </div>                
                    </div>
                </div>     
            </div>        
        </div>   
        <?php } else { ?>
     <div class="alert alert-warning" role="alert"> No existen registros para los filtros seleccionados</div>
    <?php } ?>  
</div>

     

<script type="text/javascript">

$(document).ready(function(){

    $(function () {
        $("#example1").DataTable({
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    // acción clic para abrir modal equipo por sucursal
    $('#select-sucursal').change(function(){               
        $.ajax({            
            url :'<?php echo Yii::$app->request->baseUrl?>/?r=equipos/lists',
            type : 'get',
            data : {
            id : $(this).val(),                
            },
            success: function(data){
                if (data){
                    $("#div-maquinas").html(data); 
                    $('.equipos').show(); 
                }else{
                    $("#div-maquinas").html(data);
                    $('.equipos').hide();
                }                 
                             
            }
        })    
    })
    $('#ver-sucursales').click(function(){  
        $('#verlicencias').modal();        
    })
           
    });
    $("#btn-reporte").click(function(){
            $("#equipos").submit();
    })

    // construcción de diagrama de barras
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Estadistica soporte por tipo'
        },
        xAxis: {
            
            categories: [<?php echo $soportes?>],
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
            text: 'valores'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
            pointPadding: 0.2,
            borderWidth: 0
            }
        },
        series: [{
            name: 'Soportes',
            data: [<?php echo $totalsoportes?>]        
        }]
    });
    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Estadistica soporte por zona'
        },
        xAxis: {
            
            categories: [<?php echo $zonaE?>],
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
            text: 'valores'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
            pointPadding: 0.2,
            borderWidth: 0
            }
        },
        series: [{
            name: 'Zonas',
            data: [<?php echo $tezonas?>]        
        }]
    });
    Highcharts.chart('container6', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 equipos con mas soportes'
        },
        xAxis: {
            
            categories: [<?php echo $equipos?>],
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
            text: 'valores'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
            pointPadding: 0.2,
            borderWidth: 0
            }
        },
        series: [{
            name: 'Equipos',
            data: [<?php echo $soportequipos?>]        
        }]
    });
    Highcharts.chart('container5', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Estadistica soporte por sucursales'
        },
        xAxis: {
            
            categories: [<?php echo $sucursales?>],
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
            text: 'valores'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
            pointPadding: 0.2,
            borderWidth: 0
            }
        },
        series: [{
            name: 'Sucursales',
            data: [<?php echo $totalsucursales?>]        
        }]
    });
  

</script>