<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\models\Users;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;
    use kartik\nav\NavX;
    use app\models\OpcionHasRol;
    use app\models\ItemHasRol;
    use app\models\SubitemHasRol;
    use app\models\TiposDetalles;
    use app\models\Menuperfil;
    use app\models\OpcionMenu;
    use app\models\ItemMenu;
    use app\models\SubitemMenu;
    use yii\helpers\ArrayHelper;
   

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="en">


<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />


  <title>KRONOS</title>
  <!-- <?php $this->head() ?> -->
  <!-- Custom fonts for this template-->
  <link href="<?php echo Url::base(); ?>/sbadmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <!-- <link href="<?php echo Url::base(); ?>/sbadmin/css/sb-admin-2.min.css" rel="stylesheet"> -->



  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>KRONOS</title>

   <!-- Google Font: Source Sans Pro -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/summernote/summernote-bs4.min.css">

  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">



  <!-- Custom styles for this template-->
  <!-- <link href="<?php echo Url::base(); ?>/sbadmin/css/sb-admin-2.min.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo Url::base(); ?>/css/site.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo Url::base(); ?>/css/whatsapp.css" rel="stylesheet"> -->

  <link href="<?php echo Url::base(); ?>/css/materialize.css" rel="stylesheet"> 
  <link href="<?php echo Url::base(); ?>/css/login.css" rel="stylesheet"> 
  <!-- <script src="<?php echo Url::base(); ?>/js/jquery.min.js"></script> -->
  
  
  <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script>
  <!-- <script src="<?php echo Url::base(); ?>/js/popper.min.js"></script> -->
  
 <?= Html::csrfMetaTags() ?>
</head>


<?php if (!Yii::$app->user->isGuest) { ?>
  <body id="page-top" class="hold-transition  layout-fixed">

      <!-- Page Wrapper -->
      <div id="wrapper">

        <!-- Sidebar -->
        <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
          <img class="animation__shake" src="<?php echo Url::base(); ?>/images/img/StellarWayLogo.png" alt="AdminLTELogo" height="120" width="160">
        </div>
        <?php $user = Users::findOne(yii::$app->user->identity->id) ?>
          
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
              <a href="<?php echo Url::base(); ?>/index.php?r=site%2Findex" class="nav-link">Inicio</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
              <!-- <a href="#" class="nav-link">Acerca</a> -->
              <button class="btn btn-warning" href="<?php echo Url::base(); ?>/index.php?r=equipos/redes">Diagrama de red</button> 
            </li>
          </ul>

          <!-- Right navbar links -->
          <ul class="navbar-nav ml-auto">
            <!-- Navbar Search -->
            <!-- FullScreem -->
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
            <!-- Fin FullScren -->

            <!-- Panel Lateral -->
            <!-- <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
              </a>
            </li> -->
            <!-- Fin Panel Lateral -->
          </ul>
        </nav>
        <!-- /.navbar -->
    
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
          <!-- Brand Logo -->
          <a href="<?php echo Url::base(); ?>/index.php?r=site%2Findex" class="brand-link">
            <!-- <img src="<?php echo Url::base(); ?>/images/img/EMLogo.png" alt="AdminLTE Logo" class="brand-image img-rectangle elevation-3" style="opacity: .6"> -->
            <center><span class="brand-text font-weight-light">KRONOS</span></center>
          </a>

          <!-- Sidebar -->
          <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                <img src="<?php echo Url::base(); ?>/images/fotos/user.png" class="img-circle elevation-2" alt="User Image">
              </div>
              <div class="info">
                <a href="#" class="d-block"><?php echo strtoupper($user->nombres." ".$user->apellidos) ?></a>
              </div>
            </div>
                  
            <!-- Sidebar Menu -->
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
                      with font-awesome or any other icon font library -->
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fas fa-file-signature"></i>
                      <p>
                        Inventario
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="index.php?r=equipos/index" class="nav-link active">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Equipos</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=licencias/index" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Licencias</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=qr/index" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Codigo Qr</p>
                        </a>
                      </li>
                    </ul>
                  </li>                
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fas fa-tools"></i>
                      <p>
                        Mantenimientos
                        <i class="fas fa-angle-left right"></i>
                        <span class="badge badge-info right"></span>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="index.php?r=cronogramas/index" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Cronogramas</p>
                        </a>
                      </li>                    
                    </ul>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="index.php?r=soportes/index" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Soporte equipos</p>
                        </a>
                      </li>                    
                    </ul>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fa-chart-pie"></i>
                      <p>
                        Estadisticas
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="index.php?r=estadisticas%2Festadisticasequipos" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Reporte equipos</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=estadisticas%2Festadisticaslicencias" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Reporte licencias</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=estadisticas%2Festadisticascronogramas" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Reporte Cronogramas</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=estadisticas%2Festadisticassoportes" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Reporte soportes</p>
                        </a>
                      </li>                     
                    </ul>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fas fa-users-cog"></i>
                      <p>
                        Configuración
                        <i class="fas fa-angle-left right"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="index.php?r=users%2Findex" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Gestionar usuarios</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=opcionhasrol%2Findex" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Menu</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=accesosdirectos%2Findex" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Accesos directos</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=rback%2Findex" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Permisos</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="index.php?r=funcionarios%2Findex" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Funcionarios</p>
                        </a>
                      </li>                    
                    </ul>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="modal" data-target="#logoutModal" aria-expanded="true" aria-controls="collapsePages">
                      <i class="nav-icon far fa-window-close"></i>
                      <p>
                        Cerrar Sesión
                      </p>
                    </a>
                  </li>               
                </ul>
              </nav>          
              
            <!-- /.sidebar-menu -->
          
          </div>
          <!-- /.sidebar -->
        </aside>
        <!-- Fin Main Sidebar Container -->

        <div class="">
         <div class="content-wrapper">
              <section class="content">
                <div class="container-fluid">                  
                  <div class="">                              
                    <div class="card">
                      <div class="col-xs-12">
                        <div class="card-header bg-info">
                          <h3 class="card-title"><?= Html::encode($this->title) ?></h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                              <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                              <i class="fas fa-times"></i>
                            </button>
                          </div>
                        </div>

                        <div class="card-body">
                            <div class="">
                              <div class="">
                              <?php echo $content ?>
                            </div>  
                          </div>
                        </div>
                      </div>  
                    </div>
                  </div>                                                                   
                </div>
              </section>
            </div>
            <!-- /.container-fluid -->
        </div>             
      </div>
      <!-- Footer -->    
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://www.osa.org.co/" target="_blank">OSA</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          <b>Version</b> 1.0
        </div>
      </footer>
      <!-- End of Footer -->
  </body>

<?php }else{ ?>

<body id="page-top"  style="height: 100%; background-repeat: no-repeat;background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));">
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <!-- Topbar -->
      <div class="container-fluid" >
        <?php echo $content ?>
      </div>
    </div>
  </div>
</body>
<?php } ?>
    


<div class="modal fade" id="logoutModal">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
      <div class="modal-header bg-lightblue">
        <h4 class="modal-title ">Cerrar Sesion</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php $form = ActiveForm::begin([
          'method' => 'post',
          'action' => ['site/logout']
        ]); ?>
        <div class="alert bg-lightblue">¿Esta seguro de cerrar sesión?</div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Cerrar Sesión</button>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php if (!yii::$app->user->isGuest){ ?>
    <!-- modal creacion de ticket -->
    <div id="modalticket" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header btn-warning">
                    <h4 class="modal-title">Creación de ticket</h4>
                </div>
                <?php $form = ActiveForm::begin([
                    'action' => ['tickets/create'],
                    "options" => ["enctype" => "multipart/form-data"],
                ]); ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <label>Cliente ID:</label>
                                <p><?php echo yii::$app->user->identity->cliente_id ?></p>
                                <input class="form-control" type="hidden" name="cliente_id" value="<?php echo yii::$app->user->identity->cliente_id ?>" >
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Tipo de ticket</label>
                                <select class="form-control" name="tipodeticket">
                                    <option value="13">SOPORTE TECNICO</option>
                                    <option value="12">COMERCIAL</option>
                                </select>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Modulo</label>
                                <input type="text" name="modulo" required class="form-control">
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Proceso</label>
                                <input type="text" name="proceso" required class="form-control">
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Pantallazo del error</label>
                                <input type="file" class="form-control" name="pantallazo">
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Descripcion del error</label>
                                <textarea name="descripcion" required rows="2" class="form-control"></textarea>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Nombre contacto</label>
                                <input type="text" name="nombrecontacto" required class="form-control">
                            </div>
                            <div class="col-lg-12 form-group">
                                <label>Telefono contacto</label>
                                <input type="text" name="telefonocontacto" required class="form-control">
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <?= Html::submitButton("Crear ticket",["class"=>"btn btn-block btn-warning"])?>
                        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cancelar</button>
                    </div>
                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
<?php } ?>



<!-- jQuery -->
<script src="<?php echo Url::base(); ?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo Url::base(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo Url::base(); ?>/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo Url::base(); ?>/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<!-- <script src="<?php echo Url::base(); ?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<script src="<?php echo Url::base(); ?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo Url::base(); ?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo Url::base(); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo Url::base(); ?>/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo Url::base(); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo Url::base(); ?>/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo Url::base(); ?>/js/demo.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo Url::base(); ?>/js/pages/dashboard.js"></script>

<script src="<?php echo Url::base(); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="<?php echo Url::base(); ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="<?php echo Url::base(); ?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo Url::base(); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="<?php echo Url::base(); ?>/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="<?php echo Url::base(); ?>/plugins/dropzone/min/dropzone.min.js"></script>


<!-- PARA PINTAR LAS TABLAS  -->
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- FIN DE PARA LAS TABLAS -->





<!-- <?php $this->endBody() ?> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        $(function () {
          $('[data-toggle="popover"]').popover()
        });
    </script>


    <script>
      $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
          theme: 'bootstrap4'
        })

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date picker
        $('#reservationdate').datetimepicker({
            format: 'L'
        });

        //Date and time picker
        $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          locale: {
            format: 'MM/DD/YYYY hh:mm A'
          }
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
          {
            ranges   : {
              'Today'       : [moment(), moment()],
              'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month'  : [moment().startOf('month'), moment().endOf('month')],
              'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment()
          },
          function (start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
          }
        )

        //Timepicker
        $('#timepicker').datetimepicker({
          format: 'LT'
        })

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox()

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
          $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        })

        $("input[data-bootstrap-switch]").each(function(){
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
        })

      })
      // BS-Stepper Init
      document.addEventListener('DOMContentLoaded', function () {
        window.stepper = new Stepper(document.querySelector('.bs-stepper'))
      })

      // DropzoneJS Demo Code Start
      Dropzone.autoDiscover = false

      // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
      var previewNode = document.querySelector("#template")
      previewNode.id = ""
      var previewTemplate = previewNode.parentNode.innerHTML
      previewNode.parentNode.removeChild(previewNode)

      var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
        url: "/target-url", // Set the url
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 20,
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: "#previews", // Define the container to display the previews
        clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
      })

      myDropzone.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
      })

      // Update the total progress bar
      myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
      })

      myDropzone.on("sending", function(file) {
        // Show the total progress bar when upload starts
        document.querySelector("#total-progress").style.opacity = "1"
        // And disable the start button
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
      })

      // Hide the total progress bar when nothing's uploading anymore
      myDropzone.on("queuecomplete", function(progress) {
        document.querySelector("#total-progress").style.opacity = "0"
      })

      // Setup the buttons for all transfers
      // The "add files" button doesn't need to be setup because the config
      // `clickable` has already been specified.
      document.querySelector("#actions .start").onclick = function() {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
      }
      document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true)
      }
      // DropzoneJS Demo Code End
    </script>

</body>
</html>
